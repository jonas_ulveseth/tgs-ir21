# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190621105627) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "activities", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.integer  "activity_type_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.text     "message"
    t.integer  "status",           default: 1
    t.integer  "import_id"
    t.integer  "ip_status"
    t.integer  "lte_status"
    t.integer  "stp_status"
    t.integer  "routing_status"
    t.integer  "network_id"
  end

  add_index "activities", ["import_id"], name: "index_activities_on_import_id", using: :btree
  add_index "activities", ["network_id"], name: "index_activities_on_network_id", using: :btree

  create_table "activity_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "change_histories", force: :cascade do |t|
    t.hstore   "data"
    t.integer  "network_section_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "network_id"
  end

  add_index "change_histories", ["network_id"], name: "index_change_histories_on_network_id", using: :btree
  add_index "change_histories", ["network_section_id"], name: "index_change_histories_on_network_section_id", using: :btree

  create_table "code_filtration_steps", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "code_line_range_issues", force: :cascade do |t|
    t.integer  "code_range_version_id"
    t.integer  "code_range_id"
    t.text     "message"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "code_line_range_issues", ["code_range_id"], name: "index_code_line_range_issues_on_code_range_id", using: :btree
  add_index "code_line_range_issues", ["code_range_version_id"], name: "index_code_line_range_issues_on_code_range_version_id", using: :btree

  create_table "code_range_filtereds", force: :cascade do |t|
    t.integer  "code_id"
    t.integer  "code_range_id"
    t.string   "range_start"
    t.string   "range_stop"
    t.string   "message"
    t.integer  "removed_by_id"
    t.boolean  "removed"
    t.boolean  "filtered"
    t.string   "original_range_start"
    t.string   "original_range_stop"
    t.string   "generated_by_step"
    t.string   "source"
    t.boolean  "archived"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "code_range_filtereds", ["code_id"], name: "index_code_range_filtereds_on_code_id", using: :btree
  add_index "code_range_filtereds", ["code_range_id"], name: "index_code_range_filtereds_on_code_range_id", using: :btree
  add_index "code_range_filtereds", ["range_start"], name: "index_code_range_filtereds_on_range_start", using: :btree
  add_index "code_range_filtereds", ["removed_by_id"], name: "index_code_range_filtereds_on_removed_by_id", using: :btree

  create_table "code_range_versions", force: :cascade do |t|
    t.integer  "code_id"
    t.integer  "network_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "import_id"
    t.text     "failed_message"
  end

  add_index "code_range_versions", ["code_id"], name: "index_code_range_versions_on_code_id", using: :btree
  add_index "code_range_versions", ["import_id"], name: "index_code_range_versions_on_import_id", using: :btree
  add_index "code_range_versions", ["network_id"], name: "index_code_range_versions_on_network_id", using: :btree

  create_table "code_ranges", force: :cascade do |t|
    t.string   "range_start"
    t.string   "range_stop"
    t.integer  "network_id"
    t.integer  "code_id"
    t.string   "message"
    t.boolean  "removed"
    t.boolean  "filtered"
    t.integer  "removed_by_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "code_range_id"
    t.string   "cc"
    t.string   "ndc"
    t.integer  "created_by_id"
    t.integer  "code_range_version_id"
    t.string   "original_range_start"
    t.string   "original_range_stop"
    t.integer  "generated_by_step"
    t.string   "generated_range"
    t.integer  "source"
    t.boolean  "archived"
  end

  add_index "code_ranges", ["code_id"], name: "index_code_ranges_on_code_id", using: :btree
  add_index "code_ranges", ["code_range_id"], name: "index_code_ranges_on_code_range_id", using: :btree
  add_index "code_ranges", ["code_range_version_id"], name: "index_code_ranges_on_code_range_version_id", using: :btree
  add_index "code_ranges", ["created_by_id"], name: "index_code_ranges_on_created_by_id", using: :btree
  add_index "code_ranges", ["generated_by_step"], name: "index_code_ranges_on_generated_by_step", using: :btree
  add_index "code_ranges", ["network_id"], name: "index_code_ranges_on_network_id", using: :btree
  add_index "code_ranges", ["range_start"], name: "index_code_ranges_on_range_start", using: :btree
  add_index "code_ranges", ["removed_by_id"], name: "index_code_ranges_on_removed_by_id", using: :btree

  create_table "codes", force: :cascade do |t|
    t.integer  "created_by_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "network_id"
    t.integer  "rosi_id"
    t.string   "imsi"
    t.string   "imported_from"
    t.integer  "country_id"
    t.string   "country_name"
    t.string   "name"
  end

  add_index "codes", ["created_by_id"], name: "index_code_on_created_by_id", using: :btree
  add_index "codes", ["network_id"], name: "index_codes_on_network_id", using: :btree
  add_index "codes", ["rosi_id"], name: "index_codes_on_rosi_id", using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.string   "iso_code"
    t.string   "iso2_code"
    t.string   "e164_code"
    t.boolean  "mnp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "two_digit"
    t.string   "old_iso_2"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "election_countries", force: :cascade do |t|
    t.string   "name"
    t.string   "iso_code"
    t.string   "iso_2_code"
    t.integer  "e_164"
    t.string   "mccs"
    t.boolean  "mnp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "export_fields", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "export_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "function_call"
    t.text     "description"
  end

  create_table "exports", force: :cascade do |t|
    t.string   "file"
    t.integer  "user_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.binary   "data"
    t.integer  "export_type_id"
    t.boolean  "public",         default: false
    t.boolean  "in_progress"
    t.string   "progress"
  end

  add_index "exports", ["export_type_id"], name: "index_exports_on_export_type_id", using: :btree

  create_table "hermes_operators", force: :cascade do |t|
    t.integer  "network_id"
    t.hstore   "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "hermes_operators", ["network_id"], name: "index_hermes_operators_on_network_id", using: :btree

  create_table "import_missing_sections", force: :cascade do |t|
    t.integer  "import_id"
    t.integer  "parent_id"
    t.string   "tag_name"
    t.integer  "network_section_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.text     "xml_structure"
    t.boolean  "accept_data"
  end

  add_index "import_missing_sections", ["import_id"], name: "index_import_missing_sections_on_import_id", using: :btree
  add_index "import_missing_sections", ["network_section_id"], name: "index_import_missing_sections_on_network_section_id", using: :btree
  add_index "import_missing_sections", ["parent_id"], name: "index_import_missing_sections_on_parent_id", using: :btree

  create_table "imports", force: :cascade do |t|
    t.string   "file_name"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.boolean  "archive",               default: false
    t.binary   "data"
    t.integer  "percentage_done"
    t.string   "file_type"
    t.boolean  "failed"
    t.string   "error_message"
    t.binary   "pdf_data"
    t.string   "original_filename"
    t.integer  "conflicts"
    t.boolean  "processing",            default: false
    t.text     "error_stack"
    t.integer  "network_id"
    t.integer  "organisation_id"
    t.string   "global_file_name"
    t.datetime "file_creation_date"
    t.boolean  "checked_for_diff"
    t.boolean  "ready_for_import"
    t.integer  "user_id"
    t.boolean  "removed",               default: false, null: false
    t.integer  "xml_version_id"
    t.boolean  "resolved_by_admin"
    t.text     "organisation_override", default: [],                 array: true
  end

  add_index "imports", ["id"], name: "index_imports_on_id", using: :btree
  add_index "imports", ["organisation_id"], name: "index_imports_on_organisation_id", using: :btree
  add_index "imports", ["xml_version_id"], name: "index_imports_on_xml_version_id", using: :btree

  create_table "lte_mno_ips", force: :cascade do |t|
    t.integer  "sms_number_administration_id"
    t.string   "country"
    t.string   "carrier"
    t.string   "MNO_IP_Range"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "lte_mno_ips", ["sms_number_administration_id"], name: "index_lte_mno_ips_on_sms_number_administration_id", using: :btree

  create_table "lte_sccp_ips", force: :cascade do |t|
    t.integer  "GSMRoSiSCCPprovider_ID"
    t.string   "SCCP_IP_Range"
    t.string   "Country"
    t.string   "SCCP_Hostname"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "SCCP_provider_Name"
  end

  add_index "lte_sccp_ips", ["GSMRoSiSCCPprovider_ID"], name: "index_lte_sccp_ips_on_GSMRoSiSCCPprovider_ID", using: :btree

  create_table "network_diffs", force: :cascade do |t|
    t.integer  "network_id"
    t.integer  "section_id"
    t.integer  "import_id"
    t.string   "message"
    t.text     "snapshot"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.date     "old_effective_date_of_change"
    t.integer  "notify_field_id"
  end

  add_index "network_diffs", ["import_id"], name: "index_network_diffs_on_import_id", using: :btree
  add_index "network_diffs", ["network_id"], name: "index_network_diffs_on_network_id", using: :btree
  add_index "network_diffs", ["notify_field_id"], name: "index_network_diffs_on_notify_field_id", using: :btree
  add_index "network_diffs", ["section_id"], name: "index_network_diffs_on_section_id", using: :btree

  create_table "network_line_range_children", force: :cascade do |t|
    t.string   "start_range"
    t.string   "stop_range"
    t.integer  "network_line_range"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "network_line_range_children", ["network_line_range"], name: "index_network_line_range_children_on_network_line_range", using: :btree

  create_table "network_line_ranges", force: :cascade do |t|
    t.string   "line_range"
    t.integer  "network_id"
    t.string   "index"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "range_start"
    t.string   "range_stop"
    t.boolean  "removed"
    t.string   "message"
    t.string   "line_type"
    t.integer  "orginate_id"
    t.boolean  "regenerate"
  end

  add_index "network_line_ranges", ["orginate_id"], name: "index_network_line_ranges_on_orginate_id", using: :btree

  create_table "network_rows", force: :cascade do |t|
    t.integer  "network_id"
    t.hstore   "data"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "network_section_id"
    t.integer  "network_row_id"
    t.date     "effective_date_of_change"
  end

  add_index "network_rows", ["network_id"], name: "index_network_rows_on_network_id", using: :btree
  add_index "network_rows", ["network_section_id"], name: "index_network_rows_on_network_section_id", using: :btree

  create_table "network_section_fields", force: :cascade do |t|
    t.integer  "network_section_id"
    t.string   "name"
    t.integer  "sort_order"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "network_section_fields", ["network_section_id"], name: "index_network_section_fields_on_network_section_id", using: :btree

  create_table "network_sections", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "field_name"
    t.integer  "parent_id"
    t.boolean  "multi_line",        default: true
    t.boolean  "collapsed",         default: false
    t.string   "xml_field"
    t.integer  "sort_order"
    t.boolean  "sortable",          default: false
    t.boolean  "is_parent_section", default: false
    t.boolean  "invisible"
  end

  add_index "network_sections", ["parent_id"], name: "index_network_sections_on_parent_id", using: :btree

  create_table "network_status_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "network_statuses", force: :cascade do |t|
    t.integer  "network_id"
    t.string   "status"
    t.integer  "user_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.datetime "sccp_effective_date"
    t.datetime "routing_effective_date"
    t.integer  "network_status_type_id"
  end

  add_index "network_statuses", ["network_id"], name: "index_network_statuses_on_network_id", using: :btree
  add_index "network_statuses", ["network_status_type_id"], name: "index_network_statuses_on_network_status_type_id", using: :btree
  add_index "network_statuses", ["user_id"], name: "index_network_statuses_on_user_id", using: :btree

  create_table "networks", force: :cascade do |t|
    t.integer  "organisation_id"
    t.string   "tadig_code"
    t.string   "network_name"
    t.boolean  "terrestrial"
    t.string   "country_initials_and_mnn"
    t.string   "abbreviated_mnn"
    t.string   "network_color_code"
    t.string   "source"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.hstore   "routing"
    t.string   "display_name"
    t.integer  "import_id"
    t.date     "xml_creation_date"
    t.time     "xml_creation_time"
    t.string   "tadig_gen_schema_version"
    t.string   "raex_ir21_schema_version"
    t.integer  "country_id"
    t.integer  "rosi_id"
    t.datetime "deleted_at"
    t.boolean  "needs_to_resolve",                   default: false
    t.string   "resolve_message"
    t.string   "pdf_import_id"
    t.text     "comment"
    t.integer  "parent_id"
    t.integer  "sccp_status",              limit: 2
    t.integer  "routing_status",           limit: 2
  end

  add_index "networks", ["country_id"], name: "index_networks_on_country_id", using: :btree
  add_index "networks", ["deleted_at"], name: "index_networks_on_deleted_at", using: :btree
  add_index "networks", ["import_id"], name: "index_networks_on_import_id", using: :btree
  add_index "networks", ["organisation_id"], name: "index_networks_on_organisation_id", using: :btree
  add_index "networks", ["parent_id"], name: "index_networks_on_parent_id", using: :btree
  add_index "networks", ["pdf_import_id"], name: "index_networks_on_pdf_import_id", using: :btree
  add_index "networks", ["rosi_id"], name: "index_networks_on_rosi_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "network_id"
    t.string   "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "notifications", ["network_id"], name: "index_notifications_on_network_id", using: :btree

  create_table "notify_field_users", force: :cascade do |t|
    t.integer  "notify_field_id"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "notify_field_users", ["notify_field_id"], name: "index_notify_field_users_on_notify_field_id", using: :btree
  add_index "notify_field_users", ["user_id"], name: "index_notify_field_users_on_user_id", using: :btree

  create_table "notify_fields", force: :cascade do |t|
    t.integer  "network_section_id"
    t.boolean  "main_field"
    t.string   "name"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "notify_fields", ["network_section_id"], name: "index_notify_fields_on_network_section_id", using: :btree

  create_table "notify_groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.boolean  "notify_all_networks"
  end

  create_table "notify_groups_networks", force: :cascade do |t|
    t.integer  "network_id"
    t.integer  "notify_group_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "notify_groups_networks", ["network_id"], name: "index_notify_groups_networks_on_network_id", using: :btree
  add_index "notify_groups_networks", ["notify_group_id"], name: "index_notify_groups_networks_on_notify_group_id", using: :btree

  create_table "notify_groups_organisations", force: :cascade do |t|
    t.integer  "organisation_id"
    t.integer  "notify_group_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "notify_groups_organisations", ["notify_group_id"], name: "index_notify_groups_organisations_on_notify_group_id", using: :btree
  add_index "notify_groups_organisations", ["organisation_id"], name: "index_notify_groups_organisations_on_organisation_id", using: :btree

  create_table "notify_groups_users", force: :cascade do |t|
    t.integer  "notify_group_id"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "notify_groups_users", ["notify_group_id"], name: "index_notify_groups_users_on_notify_group_id", using: :btree
  add_index "notify_groups_users", ["user_id"], name: "index_notify_groups_users_on_user_id", using: :btree

  create_table "organisations", force: :cascade do |t|
    t.string   "name"
    t.integer  "country_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "sender_tadig"
  end

  add_index "organisations", ["country_id"], name: "index_organisations_on_country_id", using: :btree

  create_table "pdf_networks", force: :cascade do |t|
    t.integer  "pdf_id"
    t.integer  "network_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "pdf_networks", ["network_id"], name: "index_pdf_networks_on_network_id", using: :btree
  add_index "pdf_networks", ["pdf_id"], name: "index_pdf_networks_on_pdf_id", using: :btree

  create_table "pdfs", force: :cascade do |t|
    t.string   "name"
    t.string   "original_filename"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "import_id"
    t.integer  "organisation_id"
  end

  add_index "pdfs", ["import_id"], name: "index_pdfs_on_import_id", using: :btree
  add_index "pdfs", ["organisation_id"], name: "index_pdfs_on_organisation_id", using: :btree

  create_table "personal_export_fields", force: :cascade do |t|
    t.integer  "export_field_id"
    t.integer  "personal_export_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "section_id"
  end

  add_index "personal_export_fields", ["personal_export_id"], name: "index_personal_export_fields_on_personal_export_id", using: :btree
  add_index "personal_export_fields", ["section_id"], name: "index_personal_export_fields_on_section_id", using: :btree

  create_table "personal_exports", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "personal_exports", ["user_id"], name: "index_personal_exports_on_user_id", using: :btree

  create_table "reports", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "reports", ["user_id"], name: "index_reports_on_user_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "scrapings", force: :cascade do |t|
    t.string   "username"
    t.string   "password"
    t.string   "source"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "seed_migration_data_migrations", force: :cascade do |t|
    t.string   "version"
    t.integer  "runtime"
    t.datetime "migrated_on"
  end

  create_table "sms_hub_line_ranges", force: :cascade do |t|
    t.string   "range_start"
    t.string   "range_stop"
    t.integer  "network_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.boolean  "removed"
    t.string   "message"
    t.integer  "sms_hub_id"
    t.boolean  "filtered"
    t.integer  "removed_by_id"
    t.boolean  "removed_by_global"
    t.integer  "source"
  end

  add_index "sms_hub_line_ranges", ["network_id"], name: "index_sms_hub_line_ranges_on_network_id", using: :btree
  add_index "sms_hub_line_ranges", ["range_start"], name: "index_sms_hub_line_ranges_on_range_start", using: :btree
  add_index "sms_hub_line_ranges", ["sms_hub_id"], name: "index_sms_hub_line_ranges_on_sms_hub_id", using: :btree
  add_index "sms_hub_line_ranges", ["source"], name: "index_sms_hub_line_ranges_on_source", using: :btree

  create_table "sms_hubs", force: :cascade do |t|
    t.integer  "network_id"
    t.integer  "sms_hub"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "rosi_id"
    t.string   "name"
    t.string   "country"
    t.string   "imsi"
    t.string   "country_name"
    t.integer  "country_id"
    t.string   "imported_from"
    t.boolean  "under_process"
  end

  add_index "sms_hubs", ["country_id"], name: "index_sms_hubs_on_country_id", using: :btree
  add_index "sms_hubs", ["network_id"], name: "index_sms_hubs_on_network_id", using: :btree
  add_index "sms_hubs", ["rosi_id"], name: "index_sms_hubs_on_rosi_id", using: :btree
  add_index "sms_hubs", ["sms_hub"], name: "index_sms_hubs_on_sms_hub", using: :btree

  create_table "user_view_groups", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "view_group_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "user_view_groups", ["user_id"], name: "index_user_view_groups_on_user_id", using: :btree
  add_index "user_view_groups", ["view_group_id"], name: "index_user_view_groups_on_view_group_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "crypted_password"
    t.string   "password_salt"
    t.string   "persistence_token"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "reset_password_token"
    t.datetime "notified"
    t.boolean  "inactive"
    t.integer  "view_id"
  end

  add_index "users", ["view_id"], name: "index_users_on_view_id", using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",          null: false
    t.integer  "item_id",            null: false
    t.string   "event",              null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.integer  "network_id"
    t.text     "object_changes"
    t.integer  "network_section_id"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "view_fields", force: :cascade do |t|
    t.string   "name"
    t.integer  "order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "view_groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "view_groups_view_fields", force: :cascade do |t|
    t.integer  "view_group"
    t.integer  "view_field"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "view_groups_view_fields", ["view_field"], name: "index_view_groups_view_fields_on_view_field", using: :btree
  add_index "view_groups_view_fields", ["view_group"], name: "index_view_groups_view_fields_on_view_group", using: :btree

  create_table "xml_sections", force: :cascade do |t|
    t.integer  "parent_id"
    t.integer  "network_section_id"
    t.integer  "xml_version_id"
    t.string   "name"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.boolean  "accept_data"
  end

  add_index "xml_sections", ["network_section_id"], name: "index_xml_sections_on_network_section_id", using: :btree
  add_index "xml_sections", ["parent_id"], name: "index_xml_sections_on_parent_id", using: :btree
  add_index "xml_sections", ["xml_version_id"], name: "index_xml_sections_on_xml_version_id", using: :btree

  create_table "xml_values", force: :cascade do |t|
    t.string   "value"
    t.integer  "xml_section_id"
    t.integer  "import_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "child_order"
    t.string   "xml_path"
    t.boolean  "removed"
  end

  add_index "xml_values", ["import_id"], name: "index_xml_values_on_import_id", using: :btree
  add_index "xml_values", ["xml_section_id"], name: "index_xml_values_on_xml_section_id", using: :btree

  create_table "xml_versions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
