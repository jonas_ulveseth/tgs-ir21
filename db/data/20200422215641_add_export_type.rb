class AddExportType < SeedMigration::Migration
  def up
  	unless ExportType.where(id: 11).first
      ExportType.create(id: 11, name: 'Advanced Sor export', function_call: '/exports/advanced-sor-export')
    end
  end

  def down
  	ExportType.find(11).destroy
  end
end
