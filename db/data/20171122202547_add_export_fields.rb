class AddExportFields < SeedMigration::Migration
  def up
  	ExportField.create!(name: 'organisation')
  	ExportField.create!(name: 'display_name')
  	ExportField.create!(name: 'country_initials')
  	ExportField.create!(name: 'mcc')
		ExportField.create!(name: 'mnc')
		ExportField.create!(name: 'cc')
		ExportField.create!(name: 'nc')
		ExportField.create!(name: 'number portability')
		ExportField.create!(name: 'xml creation date')
		ExportField.create!(name: 'xml creation time')
		ExportField.create!(name: 'tadig schema version')
		ExportField.create!(name: 'RAEX schema version')
		ExportField.create!(name: 'Abbreviated mnn')
		ExportField.create!(name: 'Terrestrial?')
		ExportField.create!(name: 'filedate')
		ExportField.create!(name: 'comments')
		ExportField.create!(name: 'tadig')
		ExportField.create!(name: 'tadig')
		ExportField.create!(name: 'gt_number_ranges')
		ExportField.create!(name: 'msrn_number_ranges')
		ExportField.create!(name: 'msisdn_number_ranges')
  end

  def down
  	ExportField.all.delete_all
  end
end
