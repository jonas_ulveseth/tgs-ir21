class AddEpcRealmsForRoamingList < SeedMigration::Migration
  def up
  	NetworkSection.create!(id: 171, name: "EPC realms for roaming", field_name: "epc_realms_for_roaming", parent_id:112, multi_line: false, collapsed: false, xml_field: "EPCRealmsForRoamingList", sort_order: nil, sortable: false)
  end

  def down

  end
end
