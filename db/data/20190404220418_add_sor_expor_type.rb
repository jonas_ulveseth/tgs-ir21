class AddSorExporType < SeedMigration::Migration
  def up
    unless ExportType.where(id: 9).first
      ExportType.create(id: 9, name: 'Sor export', function_call: '/exports/sor-export')
    end
  end

  def down
    ExportType.find(9).destroy
  end
end
