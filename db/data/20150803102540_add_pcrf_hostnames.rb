class AddPcrfHostnames < SeedMigration::Migration
  def up
  	NetworkSection.create!(id: 166, name: "PCRF Hostnames", field_name: "pcrf_hostnames", parent_id:57, multi_line: false, collapsed: false, xml_field: "S9/HostnamesPCRFList", sort_order: nil, sortable: false)
  end

  def down
  	#NetworkSection.find(166).destroy
  end
end
