class AddUSers < SeedMigration::Migration
  def up
    unless Rails.env.test?
    	User.create!([
          {email: 'tgs-system@datek.no', first_name: 'TGS System', password: '12345678',password_confirmation: '12345678', created_at: Time.now},
          {email: 'espen@datek.no', first_name: 'Espen Westgaard',password: '12345678',password_confirmation: '12345678',created_at: Time.now},
          {email: 'uwe@datek.no', first_name: 'Uwe Kubosch', password: '12345678',password_confirmation: '12345678', created_at: Time.now},
          {email: 'jonas@datek.no', first_name: 'Jonas Ulveseth', password: '12345678',password_confirmation: '12345678', created_at: Time.now},
          {email: 'ir21@datek.no', first_name: 'Telenor', password: '12345678',password_confirmation: '12345678', created_at: Time.now}
      ])
    end
  end

  def down
    unless Rails.env.test?
  	  User.delete_all
    end
  end
end
