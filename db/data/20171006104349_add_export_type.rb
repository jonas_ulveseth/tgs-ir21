class AddExportType < SeedMigration::Migration
  def up
    unless ExportType.find 6
  	  ExportType.create(id: 6, name: 'Sms Hub export', function_call: '/exports/export-sms-hub')
    end
  end

  def down
  	ExportType.find(6).destroy
  end
end
