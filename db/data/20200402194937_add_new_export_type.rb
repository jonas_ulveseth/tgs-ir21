class AddNewExportType < SeedMigration::Migration
  def up
  	unless ExportType.where(id: 10).first
      ExportType.create(id: 10, name: 'Export all networks - XLSX', function_call: '/exports/export-all-networks-xlsx.xlsx')
    end
  end

  def down
  	ExportType.find(9).destroy
  end
end
