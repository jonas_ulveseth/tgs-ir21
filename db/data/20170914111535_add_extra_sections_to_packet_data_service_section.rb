class AddExtraSectionsToPacketDataServiceSection < SeedMigration::Migration
  def up
  	NetworkSection.create!(id: 174, name: 'Apn operator identifier list', parent_id: 108, xml_field: 'APNOperatorIdentifierList',field_name: 'apn_operator_identifier_list', multi_line: true, collapsed: :false, sortable: false )
  end

  def down
  	
  end
end
