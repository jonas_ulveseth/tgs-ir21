class AddSuperAdminToRoles < SeedMigration::Migration
  def up
    Role.create!(name: 'superadmin')
  end

  def down
    #Role.where(name: 'superadmin').first.destroy!
  end
end
