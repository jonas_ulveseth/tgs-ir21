class AddA7CodeType < SeedMigration::Migration
  def up
    unless ExportType.where(id: 8).first
      ExportType.create(id: 8, name: 'Codes single line export', function_call: '/exports/generate-single-codes')
    end
  end

  def down
    ExportType.find(8).destroy
  end
end
