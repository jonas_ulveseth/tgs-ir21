class AddIpxInterConnection < SeedMigration::Migration
  def up
  	NetworkSection.create!(id: 170, name: "IPX inter connection info", field_name: "ipx_inter_connection_info", parent_id:112, multi_line: false, collapsed: false, xml_field: "IPXInterconnectionInfo", sort_order: nil, sortable: false)
  end

  def down

  end
end
