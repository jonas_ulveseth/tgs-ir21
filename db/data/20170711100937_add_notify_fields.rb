class AddNotifyFields < SeedMigration::Migration
	def up
  	# Ip roaming
  	network_section = NetworkSection.where(field_name: 'ip_roaming_info').first
  	NotifyField.create!(id: 1, name: network_section.name, main_field: false, network_section_id: network_section.id)
		# Roaming inter connection / LTE
		network_section = NetworkSection.where(field_name: 'roaming_inter_connection').first
		NotifyField.create!(id: 2, name: network_section.name, main_field: false, network_section_id: network_section.id)
		#International sccp carrier
		network_section = NetworkSection.where(field_name: 'international_sccp_carrier').first
		NotifyField.create!(id: 3, name: network_section.name, main_field: false, network_section_id: network_section.id)
		# Routing section
		NotifyField.create!(id: 4, name: 'routing', main_field: true)
		# Number of network changed
		NotifyField.create!(id: 5, name: 'number of networks changed', main_field: true)
	end

	def down
		NotifyField.delete_all
	end
end
