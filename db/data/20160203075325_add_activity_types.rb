class AddActivityTypes < SeedMigration::Migration
  def up
  	ActivityType.create([
  	{	id: 1, name: "xml import"},
  	{ id: 2, name: "excel import"},
  	{ id: 3, name: "pdf import"}])
  end

  def down

  end
end
