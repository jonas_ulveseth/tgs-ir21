class AddRoamingSupportSection < SeedMigration::Migration
  def up
  	NetworkSection.create!(id: 165, name: "Roaming support", field_name: "roaming_support", parent_id:144, multi_line: false, collapsed: false, xml_field: "S6a/HostnamesHSS_MMEList/HostName", sort_order: nil, sortable: false)
  end

  def down
  	#NetworkSection.find(165).destroy
  end
end
