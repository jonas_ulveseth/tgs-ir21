class AddNotifyImport < SeedMigration::Migration
  def up
  	# Notify of import
    nf = NotifyField.find_by(id: 6)
    unless nf
  	  NotifyField.create!(id: 6, name: 'Notify new import', main_field: true)
    end
  end

  def down

  end
end
