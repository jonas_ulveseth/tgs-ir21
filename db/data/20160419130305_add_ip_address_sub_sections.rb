class AddIpAddressSubSections < SeedMigration::Migration
  def up
  	NetworkSection.create!(id: 172, name: "Primary ip address", field_name: "diameter_primary_ip_address", parent_id:57, multi_line: false, collapsed: false, xml_field: "EPCRealmsForRoamingList", sort_order: nil, sortable: false)
  	NetworkSection.create!(id: 173, name: "Secondary ip address", field_name: "diameter_secondary_ip_address", parent_id:57, multi_line: false, collapsed: false, xml_field: "EPCRealmsForRoamingList", sort_order: nil, sortable: false)
  end

  def down

  end
end
