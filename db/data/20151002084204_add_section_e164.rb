class AddSectionE164 < SeedMigration::Migration
	def up
		NetworkSection.create!(id: 167, name: "GT number ranges", field_name: "gt_number_ranges", parent_id:80, multi_line: true, collapsed: false, xml_field: "GT_NumberRanges", sort_order: nil, sortable: false)
		NetworkSection.create!(id: 168, name: "Msrn number ranges", field_name: "msrn_number_ranges", parent_id:80, multi_line: true, collapsed: false, xml_field: "GT_NumberRanges", sort_order: nil, sortable: false)
		NetworkSection.create!(id: 169, name: "Msisdn number ranges", field_name: "msisdn_number_ranges", parent_id:80, multi_line: true, collapsed: false, xml_field: "GT_NumberRanges", sort_order: nil, sortable: false)
	end

	def down
		#NetworkSection.find(167).destroy
		#NetworkSection.find(168).destroy
		#NetworkSection.find(169).destroy
	end
end
