class AddEmptyFieldsToNotifyFields < SeedMigration::Migration
  def up
  	# Number of network changed
		NotifyField.create!(id: 7, name: 'Fields are missing', main_field: true)
  end

  def down

  end
end
