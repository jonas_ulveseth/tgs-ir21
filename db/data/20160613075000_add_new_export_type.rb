class AddNewExportType < SeedMigration::Migration
  def up
    unless ExportType.find 4
  	 ExportType.create!(id: 4, name: "Network List", function_call: "/exports/export-all-networks.xls")
    end
  end

  def down
  	ExportType.find(4).destroy
  end
end
