class RenameNotificationGroups < SeedMigration::Migration
  def up
  	network_section = NetworkSection.where(field_name: 'roaming_inter_connection').first
  	network_section.update_columns(field_name: 'Lte info')
  end

  def down
  	network_section = NetworkSection.where(field_name: 'Lte info').first
  	network_section.update_columns(field_name: 'roaming_inter_connection')
  end
end
