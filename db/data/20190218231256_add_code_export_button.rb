class AddCodeExportButton < SeedMigration::Migration
  def up
    unless ExportType.where(id: 7).first
      ExportType.create(id: 7, name: 'Codes export', function_call: '/exports/generate-codes')
    end
  end

  def down
    ExportType.find(7).destroy
  end
end
