class NewExport < SeedMigration::Migration
  def up
    unless ExportType.where(id: 12).first
      ExportType.create(id: 12, name: 'Advanced network export', function_call: '/exports/advanced-network-export')
    end
  end

  def down
    ExportType.find(12).destroy
  end
end
