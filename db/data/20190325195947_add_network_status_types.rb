class AddNetworkStatusTypes < SeedMigration::Migration
  def up
    NetworkStatusType.create(id: 1, name: 'Sccp status')
    NetworkStatusType.create(id: 2, name: 'Routing status')
  end

  def down
    NetworkStatusType.find(1).delete
    NetworkStatusType.find(2).delete
  end
end
