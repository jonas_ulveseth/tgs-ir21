class AddNetworkWasDeleted < SeedMigration::Migration
  def up
  	NotifyField.create!(id: 8, name: 'notify when network is removed', main_field: true)
  end

  def down
  	NotifyField.find(8).destroy
  end
end
