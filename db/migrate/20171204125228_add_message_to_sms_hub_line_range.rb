class AddMessageToSmsHubLineRange < ActiveRecord::Migration
  def change
    add_column :sms_hub_line_ranges, :message, :string
  end
end
