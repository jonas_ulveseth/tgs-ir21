class AddVisibleFieldToNetworkSection < ActiveRecord::Migration
  def change
    add_column :network_sections, :invisible, :boolean
  end
end
