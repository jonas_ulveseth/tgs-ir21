class AddDescriptionToExportType < ActiveRecord::Migration
  def change
    add_column :export_types, :description, :text
  end
end
