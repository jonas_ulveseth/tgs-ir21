class AddRemovedColumnToImports < ActiveRecord::Migration
  def change
    add_column :imports, :removed, :boolean, null: false, default: false
  end
end
