class AddNetworkIdToVersions < ActiveRecord::Migration
  def change
    add_column :versions, :network_id, :integer
  end
end
