class AddNotifiedToUSers < ActiveRecord::Migration
  def change
    add_column :users, :notified, :datetime
  end
end
