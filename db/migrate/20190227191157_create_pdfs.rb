class CreatePdfs < ActiveRecord::Migration
  def change
    create_table :pdfs do |t|
      t.string :name
      t.string :original_filename

      t.timestamps null: false
    end
  end
end
