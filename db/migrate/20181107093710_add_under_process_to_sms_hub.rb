class AddUnderProcessToSmsHub < ActiveRecord::Migration
  def change
    add_column :sms_hubs, :under_process, :boolean
  end
end
