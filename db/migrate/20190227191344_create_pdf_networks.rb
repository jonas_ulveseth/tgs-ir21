class CreatePdfNetworks < ActiveRecord::Migration
  def change
    create_table :pdf_networks do |t|
      t.integer :pdf_id
      t.integer :network_id

      t.timestamps null: false
    end
    add_index :pdf_networks, :pdf_id
    add_index :pdf_networks, :network_id
  end
end
