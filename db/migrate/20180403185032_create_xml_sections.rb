class CreateXmlSections < ActiveRecord::Migration
  def change
    create_table :xml_sections do |t|
      t.integer :parent_id
      t.integer :network_section_id
      t.integer :xml_version_id
      t.string :name

      t.timestamps null: false
    end
    add_index :xml_sections, :parent_id
    add_index :xml_sections, :network_section_id
    add_index :xml_sections, :xml_version_id
  end
end
