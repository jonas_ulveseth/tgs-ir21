class CreatePersonalExports < ActiveRecord::Migration
  def change
    create_table :personal_exports do |t|
      t.string :name
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :personal_exports, :user_id
  end
end
