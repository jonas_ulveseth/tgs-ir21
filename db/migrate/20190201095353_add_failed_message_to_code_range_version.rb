class AddFailedMessageToCodeRangeVersion < ActiveRecord::Migration
  def change
    add_column :code_range_versions, :failed_message, :text
  end
end
