class AddProgressToExport < ActiveRecord::Migration
  def change
    add_column :exports, :progress, :string
  end
end
