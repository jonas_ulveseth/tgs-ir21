class CreateCodeLineRangeIssues < ActiveRecord::Migration
  def change
    create_table :code_line_range_issues do |t|
      t.integer :code_range_version_id
      t.integer :code_range_id
      t.text :message

      t.timestamps null: false
    end
    add_index :code_line_range_issues, :code_range_version_id
    add_index :code_line_range_issues, :code_range_id
  end
end
