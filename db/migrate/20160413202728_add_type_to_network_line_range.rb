class AddTypeToNetworkLineRange < ActiveRecord::Migration
  def change
    add_column :network_line_ranges, :line_type, :string
  end
end
