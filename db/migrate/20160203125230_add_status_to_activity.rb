class AddStatusToActivity < ActiveRecord::Migration
  def change
    add_column :activities, :status, :integer, default: 1
  end
end
