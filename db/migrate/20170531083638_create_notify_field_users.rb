class CreateNotifyFieldUsers < ActiveRecord::Migration
  def change
    create_table :notify_field_users do |t|
      t.integer :notify_field_id
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :notify_field_users, :notify_field_id
    add_index :notify_field_users, :user_id
  end
end
