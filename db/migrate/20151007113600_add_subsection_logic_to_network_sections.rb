class AddSubsectionLogicToNetworkSections < ActiveRecord::Migration
  def up
    add_column :network_sections, :is_parent_section, :boolean, default: false
    NetworkSection.update_all(is_parent_section: false)
  end

  def down
  	remove_column :network_sections, :is_parent_section	
  end
end
