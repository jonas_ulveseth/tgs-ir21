class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :name
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :reports, :user_id
  end
end
