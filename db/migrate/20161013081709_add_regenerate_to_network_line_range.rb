class AddRegenerateToNetworkLineRange < ActiveRecord::Migration
  def change
    add_column :network_line_ranges, :regenerate, :boolean
  end
end
