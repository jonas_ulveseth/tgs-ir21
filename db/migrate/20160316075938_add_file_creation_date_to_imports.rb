class AddFileCreationDateToImports < ActiveRecord::Migration
  def change
    add_column :imports, :file_creation_date, :datetime
  end
end
