class AddFieldNameToNetworks < ActiveRecord::Migration
  def change
    add_column :networks, :display_name, :string
  end
end
