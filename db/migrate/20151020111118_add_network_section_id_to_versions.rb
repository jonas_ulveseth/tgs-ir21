class AddNetworkSectionIdToVersions < ActiveRecord::Migration
  def change
    add_column :versions, :network_section_id, :integer
  end
end
