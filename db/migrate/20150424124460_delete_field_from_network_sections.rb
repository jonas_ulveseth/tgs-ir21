class DeleteFieldFromNetworkSections < ActiveRecord::Migration
  def change
  	remove_column :network_sections, :fields
  end
end
