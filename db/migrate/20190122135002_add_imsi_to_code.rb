class AddImsiToCode < ActiveRecord::Migration
  def change
    add_column :codes, :imsi, :string
  end
end
