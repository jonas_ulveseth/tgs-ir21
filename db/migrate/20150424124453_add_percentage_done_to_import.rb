class AddPercentageDoneToImport < ActiveRecord::Migration
  def change
    add_column :imports, :percentage_done, :integer
  end
end
