class RemoveOragnisationIdFromNotifyGroupsNetworks < ActiveRecord::Migration
  def change
  	remove_column :notify_groups_networks, :organisation_id, :integer
  end
end
