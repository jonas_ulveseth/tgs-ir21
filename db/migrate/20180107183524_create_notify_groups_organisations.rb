class CreateNotifyGroupsOrganisations < ActiveRecord::Migration
  def change
    create_table :notify_groups_organisations do |t|
      t.integer :organisation_id
      t.integer :notify_group_id
      t.timestamps null: false
    end
    add_index :notify_groups_organisations, :organisation_id
    add_index :notify_groups_organisations, :notify_group_id
  end
end
