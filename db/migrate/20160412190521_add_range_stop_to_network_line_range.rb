class AddRangeStopToNetworkLineRange < ActiveRecord::Migration
  def change
    add_column :network_line_ranges, :range_stop, :string
  end
end
