class AddNetworkSectionsToNetworkRows < ActiveRecord::Migration
  def change
    add_column :network_rows, :network_section_id, :integer
    add_index :network_rows, :network_section_id
  end
end
