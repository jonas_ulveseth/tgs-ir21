class AddCreatedByIdToCodeRange < ActiveRecord::Migration
  def change
    add_column :code_ranges, :created_by_id, :integer
    add_index :code_ranges, :created_by_id
  end
end
