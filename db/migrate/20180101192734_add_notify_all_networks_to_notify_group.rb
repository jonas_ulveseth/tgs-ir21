class AddNotifyAllNetworksToNotifyGroup < ActiveRecord::Migration
  def change
    add_column :notify_groups, :notify_all_networks, :boolean
  end
end
