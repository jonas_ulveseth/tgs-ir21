class AddLineRangeToNetwork < ActiveRecord::Migration
  def change
    add_column :networks, :line_range, :string
  end
end
