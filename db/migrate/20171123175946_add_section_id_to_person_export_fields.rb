class AddSectionIdToPersonExportFields < ActiveRecord::Migration
  def change
    add_column :personal_export_fields, :section_id, :integer
    add_index :personal_export_fields, :section_id
  end
end
