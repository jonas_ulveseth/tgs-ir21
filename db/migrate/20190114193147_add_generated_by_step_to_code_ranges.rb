class AddGeneratedByStepToCodeRanges < ActiveRecord::Migration
  def change
    add_column :code_ranges, :generated_by_step, :integer
    add_index :code_ranges, :generated_by_step
  end
end
