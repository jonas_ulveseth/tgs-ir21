class AddErrorStackToImports < ActiveRecord::Migration
  def change
    add_column :imports, :error_stack, :text
  end
end
