class CreateExportFields < ActiveRecord::Migration
  def change
    create_table :export_fields do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
