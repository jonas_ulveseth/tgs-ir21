class AddNetworkIdToActivity < ActiveRecord::Migration
  def change
    add_column :activities, :import_id, :integer
    add_index :activities, :import_id
  end
end
