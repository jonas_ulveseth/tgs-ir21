class AddParentIdToNetworks < ActiveRecord::Migration
  def change
    add_column :networks, :parent_id, :integer
    add_index :networks, :parent_id
  end
end
