class AddRosiDataToNetworks < ActiveRecord::Migration
  def change
    add_column :networks, :rosi_id, :integer
    add_index :networks, :rosi_id
  end
end
