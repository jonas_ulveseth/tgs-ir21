class AddOrganisationIdToNotifyGroupsNetwork < ActiveRecord::Migration
  def change
    add_column :notify_groups_networks, :organisation_id, :integer
    add_index :notify_groups_networks, :organisation_id
  end
end
