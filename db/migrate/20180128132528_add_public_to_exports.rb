class AddPublicToExports < ActiveRecord::Migration
  def change
    add_column :exports, :public, :boolean, default: false
  end
end
