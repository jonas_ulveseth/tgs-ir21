class AddSccpStatusToNetworks < ActiveRecord::Migration
  def change
    add_column :networks, :sccp_status, :smallint
  end
end
