class AddViewIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :view_id, :integer
    add_index :users, :view_id
  end
end
