class AddConflictsToImports < ActiveRecord::Migration
  def change
    add_column :imports, :conflicts, :integer
  end
end
