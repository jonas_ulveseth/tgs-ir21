class CreateUserViewGroups < ActiveRecord::Migration
  def change
    create_table :user_view_groups do |t|
      t.integer :user_id
      t.integer :view_group_id

      t.timestamps null: false
    end
    add_index :user_view_groups, :user_id
    add_index :user_view_groups, :view_group_id
  end
end
