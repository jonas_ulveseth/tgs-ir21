class CreatePersonalExportFields < ActiveRecord::Migration
  def change
    create_table :personal_export_fields do |t|
      t.integer :export_field_id
      t.integer :personal_export_id

      t.timestamps null: false
    end
    add_index :personal_export_fields, :personal_export_id
  end
end
