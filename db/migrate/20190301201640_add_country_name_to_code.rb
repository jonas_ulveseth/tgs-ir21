class AddCountryNameToCode < ActiveRecord::Migration
  def change
    add_column :codes, :country_name, :string
  end
end
