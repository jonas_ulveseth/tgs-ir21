class AddResolvedByAdminToImport < ActiveRecord::Migration
  def change
    add_column :imports, :resolved_by_admin, :boolean
  end
end
