class CreateOrganisations < ActiveRecord::Migration
	def change
		create_table :organisations do |t|
			t.string :name
			t.integer :country_id

			t.timestamps null: false
		end
		add_index :organisations, :country_id
	end
end
