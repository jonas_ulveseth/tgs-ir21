class CreateXmlVersions < ActiveRecord::Migration
  def change
    create_table :xml_versions do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
