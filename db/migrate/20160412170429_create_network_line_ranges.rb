class CreateNetworkLineRanges < ActiveRecord::Migration
  def change
    create_table :network_line_ranges do |t|
      t.string :line_range
      t.integer :network_id
      t.string :index

      t.timestamps null: false
    end
  end
end
