class CreateScrapings < ActiveRecord::Migration
  def change
    create_table :scrapings do |t|
      t.string :username
      t.string :password
      t.string :source

      t.timestamps null: false
    end
  end
end
