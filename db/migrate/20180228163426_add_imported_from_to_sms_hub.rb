class AddImportedFromToSmsHub < ActiveRecord::Migration
  def change
    add_column :sms_hubs, :imported_from, :string
  end
end
