class AddExportTypeToExport < ActiveRecord::Migration
  def change
    add_column :exports, :export_type_id, :integer
    add_index :exports, :export_type_id
  end
end
