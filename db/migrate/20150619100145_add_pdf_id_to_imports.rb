class AddPdfIdToImports < ActiveRecord::Migration
  def change
    add_column :imports, :network_id, :integer
  end
end
