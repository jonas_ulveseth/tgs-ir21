class CreateCodeRangeVersions < ActiveRecord::Migration
  def change
    create_table :code_range_versions do |t|
      t.integer :code_id
      t.integer :network_id

      t.timestamps null: false
    end
    add_index :code_range_versions, :code_id
    add_index :code_range_versions, :network_id
  end
end
