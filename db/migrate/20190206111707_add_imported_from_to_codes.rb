class AddImportedFromToCodes < ActiveRecord::Migration
  def change
    add_column :codes, :imported_from, :string
  end
end
