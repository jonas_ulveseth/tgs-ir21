class AddSmsHubIdToSmsHubLineRange < ActiveRecord::Migration
  def change
    add_column :sms_hub_line_ranges, :sms_hub_id, :integer
    add_index :sms_hub_line_ranges, :sms_hub_id
  end
end
