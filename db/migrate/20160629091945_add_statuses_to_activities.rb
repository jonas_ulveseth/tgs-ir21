class AddStatusesToActivities < ActiveRecord::Migration
  def change
  	add_column :activities, :ip_status, :integer
  	add_column :activities, :lte_status, :integer
  	add_column :activities, :stp_status, :integer
  	add_column :activities, :routing_status, :integer
  end
end
