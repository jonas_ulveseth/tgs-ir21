class AddSccpEffectiveDateToNetworkStatus < ActiveRecord::Migration
  def change
    add_column :network_statuses, :sccp_effective_date, :datetime
    add_column :network_statuses, :routing_effective_date, :datetime
  end
end
