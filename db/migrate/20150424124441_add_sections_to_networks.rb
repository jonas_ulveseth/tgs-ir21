class AddSectionsToNetworks < ActiveRecord::Migration
  def change
    add_column :networks, :sections, :hstore
  end
end
