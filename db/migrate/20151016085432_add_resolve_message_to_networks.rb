class AddResolveMessageToNetworks < ActiveRecord::Migration
  def change
    add_column :networks, :resolve_message, :string
  end
end
