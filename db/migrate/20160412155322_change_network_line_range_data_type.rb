class ChangeNetworkLineRangeDataType < ActiveRecord::Migration
  def change
  	remove_column :networks, :line_range
  	add_column :networks, :line_range, :hstore
  end
end
