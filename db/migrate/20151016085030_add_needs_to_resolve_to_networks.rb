class AddNeedsToResolveToNetworks < ActiveRecord::Migration
  def change
    add_column :networks, :needs_to_resolve, :boolean, default: false
  end
end
