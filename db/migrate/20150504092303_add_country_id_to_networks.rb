class AddCountryIdToNetworks < ActiveRecord::Migration
  def change
    add_column :networks, :country_id, :integer
    add_index :networks, :country_id
  end
end
