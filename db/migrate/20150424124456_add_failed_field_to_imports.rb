class AddFailedFieldToImports < ActiveRecord::Migration
  def change
    add_column :imports, :failed, :boolean
    add_column :imports, :error_message, :string
  end
end
