class AddCountryNameToSmsHub < ActiveRecord::Migration
  def change
    add_column :sms_hubs, :country_name, :string
  end
end
