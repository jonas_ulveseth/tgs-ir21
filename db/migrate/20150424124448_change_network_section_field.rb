class ChangeNetworkSectionField < ActiveRecord::Migration
  def change
  	rename_column :networks, :sections, :routing
  end
end
