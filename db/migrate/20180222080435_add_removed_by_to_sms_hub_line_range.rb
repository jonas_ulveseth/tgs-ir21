class AddRemovedByToSmsHubLineRange < ActiveRecord::Migration
  def change
    add_column :sms_hub_line_ranges, :removed_by_id, :integer
  end
end
