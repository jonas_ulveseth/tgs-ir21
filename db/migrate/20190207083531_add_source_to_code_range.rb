class AddSourceToCodeRange < ActiveRecord::Migration
  def change
    add_column :code_ranges, :source, :integer
  end
end
