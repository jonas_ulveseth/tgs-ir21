class CreateElectionCountries < ActiveRecord::Migration
  def change
    create_table :election_countries do |t|
      t.string :name
      t.string :iso_code
      t.string :iso_2_code
      t.integer :e_164
      t.string :mccs
      t.boolean :mnp

      t.timestamps null: false
    end
  end
end
