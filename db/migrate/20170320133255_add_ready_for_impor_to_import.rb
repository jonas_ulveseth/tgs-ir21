class AddReadyForImporToImport < ActiveRecord::Migration
  def change
    add_column :imports, :ready_for_import, :boolean
  end
end
