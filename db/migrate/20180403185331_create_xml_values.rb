class CreateXmlValues < ActiveRecord::Migration
  def change
    create_table :xml_values do |t|
      t.string :value
      t.integer :xml_section_id
      t.integer :import_id

      t.timestamps null: false
    end
    add_index :xml_values, :xml_section_id
    add_index :xml_values, :import_id
  end
end
