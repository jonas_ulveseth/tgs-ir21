class AddOrganisationIdToPdf < ActiveRecord::Migration
  def change
    add_column :pdfs, :organisation_id, :integer
    add_index :pdfs, :organisation_id
  end
end
