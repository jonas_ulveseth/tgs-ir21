class AddOriginalFileNameToImports < ActiveRecord::Migration
  def change
  	add_column :imports, :original_filename, :string
  end
end
