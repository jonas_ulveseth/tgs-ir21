class CreateCountries < ActiveRecord::Migration
  def change
    create_table :countries do |t|
      t.string :name
      t.string :iso_code
      t.string :iso2_code
      t.string :e164_code
      t.boolean :mnp

      t.timestamps null: false
    end
  end
end
