class AddCollapsedOptionToNetworSections < ActiveRecord::Migration
  def change
    add_column :network_sections, :collapsed, :boolean, default: false
  end
end