class CreateSmsHubs < ActiveRecord::Migration
  def change
    create_table :sms_hubs do |t|
      t.integer :network_id
      t.integer :sms_hub

      t.timestamps null: false
    end
    add_index :sms_hubs, :network_id
    add_index :sms_hubs, :sms_hub
  end
end
