class AddRemovedToSmsLineRange < ActiveRecord::Migration
  def change
    add_column :sms_hub_line_ranges, :removed, :boolean
  end
end
