class CreateNetworks < ActiveRecord::Migration
  def change
    create_table :networks do |t|
      t.integer :organisation_id
      t.string :tadig_code
      t.string :network_name
      t.boolean :terrestrial
      t.string :presentation_of_country_initials_and_mnn
      t.string :abbreviated_mnn
      t.string :network_color_code
      t.string :source

      t.timestamps null: false
    end
    add_index :networks, :organisation_id
  end
end
