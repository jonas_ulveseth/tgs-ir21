class AddRoutingStatusToNetworks < ActiveRecord::Migration
  def change
    add_column :networks, :routing_status, :smallint
  end
end
