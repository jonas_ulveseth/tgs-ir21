class CreateLteMnoIps < ActiveRecord::Migration
  def change
    create_table :lte_mno_ips do |t|
      t.integer :sms_number_administration_id
      t.string :country
      t.string :carrier
      t.string :MNO_IP_Range

      t.timestamps null: false
    end
    add_index :lte_mno_ips, :sms_number_administration_id
  end
end
