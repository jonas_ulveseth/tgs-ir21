class AddNotifyFieldToNetworkDiff < ActiveRecord::Migration
  def change
    add_column :network_diffs, :notify_field_id, :integer
    add_index :network_diffs, :notify_field_id
  end
end
