class AddImportIdToPdf < ActiveRecord::Migration
  def change
    add_column :pdfs, :import_id, :integer
    add_index :pdfs, :import_id
  end
end
