class AddNetworkIdToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :network_id, :integer
    add_index :activities, :network_id
  end
end
