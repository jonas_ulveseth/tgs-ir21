class AddMessageToSorGtLineRange < ActiveRecord::Migration
  def change
    add_column :sor_gt_line_ranges, :message, :string
  end
end
