class CreateNetworkRows < ActiveRecord::Migration
  def change
    create_table :network_rows do |t|
      t.integer :network_id
      t.hstore :data

      t.timestamps null: false
    end
    add_index :network_rows, :network_id
  end
end
