class CreateNotifyGroupsUsers < ActiveRecord::Migration
  def change
    create_table :notify_groups_users do |t|
      t.integer :notify_group_id
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :notify_groups_users, :notify_group_id
    add_index :notify_groups_users, :user_id
  end
end
