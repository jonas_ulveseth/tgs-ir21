class AddCcAndNdcToCodeLineRange < ActiveRecord::Migration
  def change
    add_column :code_ranges, :cc, :string
    add_column :code_ranges, :ndc, :string
  end
end
