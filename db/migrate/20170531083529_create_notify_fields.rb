class CreateNotifyFields < ActiveRecord::Migration
  def change
    create_table :notify_fields do |t|
      t.integer :network_section_id
      t.boolean :main_field
      t.string :name

      t.timestamps null: false
    end
    add_index :notify_fields, :network_section_id
  end
end
