class CreateSmsHubLineRanges < ActiveRecord::Migration
  def change
    create_table :sms_hub_line_ranges do |t|
      t.string :range_start
      t.string :range_stop
      t.integer :network_id

      t.timestamps null: false
    end
    add_index :sms_hub_line_ranges, :network_id
  end
end
