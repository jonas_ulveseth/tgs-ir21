class AddCountryToSmsHub < ActiveRecord::Migration
  def change
    add_column :sms_hubs, :country, :string
  end
end
