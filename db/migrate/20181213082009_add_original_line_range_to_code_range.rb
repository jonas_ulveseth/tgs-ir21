class AddOriginalLineRangeToCodeRange < ActiveRecord::Migration
  def change
    add_column :code_ranges, :original_range_start, :string
    add_column :code_ranges, :original_range_stop, :string
  end
end
