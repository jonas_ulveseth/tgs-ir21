class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :network_id
      t.string :message

      t.timestamps null: false
    end
    add_index :notifications, :network_id
  end
end
