class AddTwoDigitCodeToCountry < ActiveRecord::Migration
  def change
    add_column :countries, :two_digit, :integer
  end
end
