class AddXmlFieldToNetworkSections < ActiveRecord::Migration
  def change
    add_column :network_sections, :xml_field, :string
  end
end
