class AddOrganisationIdToImport < ActiveRecord::Migration
  def change
    add_column :imports, :organisation_id, :integer
    add_index :imports, :organisation_id
  end
end
