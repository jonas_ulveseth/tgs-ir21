class AddRaexInformationToNetworks < ActiveRecord::Migration
  def change
    add_column :networks, :xml_creation_date, :date
    add_column :networks, :xml_creation_time, :time
    add_column :networks, :tadig_gen_schema_version, :string
    add_column :networks, :raex_ir21_schema_version, :string
  end
end
