class AddDeletedAtToNetworks < ActiveRecord::Migration
  def change
    add_column :networks, :deleted_at, :datetime
    add_index :networks, :deleted_at
  end
end
