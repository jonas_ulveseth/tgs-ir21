class ChangColumnNameOfNetworkSection < ActiveRecord::Migration
  def change
  	rename_column :network_sections, :network_section_id, :parent_id
  end
end
