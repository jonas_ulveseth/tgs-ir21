class AddArchiveToImports < ActiveRecord::Migration
  def change
    add_column :imports, :archive, :boolean, default: false
    add_column :imports, :data, :binary, limit: 10.megabyte
  end
end
