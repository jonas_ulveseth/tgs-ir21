class CreateNetworkSectionFields < ActiveRecord::Migration
  def change
    create_table :network_section_fields do |t|
      t.integer :network_section_id
      t.string :name
      t.integer :sort_order

      t.timestamps null: false
    end
    add_index :network_section_fields, :network_section_id
  end
end
