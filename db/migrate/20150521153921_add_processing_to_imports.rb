class AddProcessingToImports < ActiveRecord::Migration
  def change
    add_column :imports, :processing, :boolean, default: false
  end
end
