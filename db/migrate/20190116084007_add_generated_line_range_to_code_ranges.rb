class AddGeneratedLineRangeToCodeRanges < ActiveRecord::Migration
  def change
    add_column :code_ranges, :generated_range, :string
  end
end
