class AddFunctionCallToExportTypes < ActiveRecord::Migration
  def change
    add_column :export_types, :function_call, :string
  end
end
