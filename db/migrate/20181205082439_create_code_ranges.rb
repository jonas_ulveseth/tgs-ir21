class CreateCodeRanges < ActiveRecord::Migration
  def change
    create_table :code_ranges do |t|
      t.string :range_start
      t.string :range_stop
      t.integer :network_id
      t.integer :code_id
      t.string :message
      t.boolean :removed
      t.boolean :filtered
      t.integer :removed_by_id

      t.timestamps null: false
    end
    add_index :code_ranges, :range_start
    add_index :code_ranges, :network_id
    add_index :code_ranges, :code_id
    add_index :code_ranges, :removed_by_id
  end
end
