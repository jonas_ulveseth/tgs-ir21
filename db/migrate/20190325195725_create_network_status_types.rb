class CreateNetworkStatusTypes < ActiveRecord::Migration
  def change
    create_table :network_status_types do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
