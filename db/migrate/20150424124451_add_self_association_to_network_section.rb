class AddSelfAssociationToNetworkSection < ActiveRecord::Migration
  def change
    add_column :network_sections, :network_section_id, :integer
    add_index :network_sections, :network_section_id
  end
end
