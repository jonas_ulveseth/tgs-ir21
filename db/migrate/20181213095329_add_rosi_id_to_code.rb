class AddRosiIdToCode < ActiveRecord::Migration
  def change
    add_column :codes, :rosi_id, :integer
    add_index :codes, :rosi_id
  end
end
