class AddInProgressToExport < ActiveRecord::Migration
  def change
    add_column :exports, :in_progress, :boolean
  end
end
