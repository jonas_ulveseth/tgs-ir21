class AddFieldNameToNetworkSection < ActiveRecord::Migration
  def change
    add_column :network_sections, :field_name, :string
  end
end
