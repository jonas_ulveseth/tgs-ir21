class CreateNetworkSections < ActiveRecord::Migration
  def change
    create_table :network_sections do |t|
      t.string :name
      t.hstore :fields

      t.timestamps null: false
    end
  end
end
