class AddNetworkIdToChangeHistories < ActiveRecord::Migration
  def change
    add_column :change_histories, :network_id, :integer
    add_index :change_histories, :network_id
  end
end
