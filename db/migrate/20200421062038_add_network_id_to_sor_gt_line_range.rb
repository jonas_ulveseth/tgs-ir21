class AddNetworkIdToSorGtLineRange < ActiveRecord::Migration
  def change
    add_column :sor_gt_line_ranges, :network_id, :integer
    add_index :sor_gt_line_ranges, :network_id
  end
end
