class AddColumntToNetworkSection < ActiveRecord::Migration
  def change
    add_column :network_sections, :sortable, :boolean, default: false
  end
end
