class CreateNetworkStatuses < ActiveRecord::Migration
  def change
    create_table :network_statuses do |t|
      t.integer :network_id
      t.string :status
      t.integer :user_id
      t.timestamps null: false
    end
    add_index :network_statuses, :network_id
    add_index :network_statuses, :user_id
  end
end
