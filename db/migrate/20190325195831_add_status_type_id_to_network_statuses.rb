class AddStatusTypeIdToNetworkStatuses < ActiveRecord::Migration
  def change
    add_column :network_statuses, :network_status_type_id, :integer
    add_index :network_statuses, :network_status_type_id
  end
end
