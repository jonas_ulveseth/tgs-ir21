class AddImportIdToCodeRangeVersion < ActiveRecord::Migration
  def change
    add_column :code_range_versions, :import_id, :integer
    add_index :code_range_versions, :import_id
  end
end
