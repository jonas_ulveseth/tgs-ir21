class AddBlobFieldToExport < ActiveRecord::Migration
  def change
    add_column :exports, :data, :binary
  end
end
