class CreateViewFields < ActiveRecord::Migration
  def change
    create_table :view_fields do |t|
      t.string :name
      t.integer :order

      t.timestamps null: false
    end
  end
end
