class CreateHermesOperators < ActiveRecord::Migration
  def change
    create_table :hermes_operators do |t|
      t.integer :network_id
      t.hstore :data

      t.timestamps null: false
    end
    add_index :hermes_operators, :network_id
  end
end
