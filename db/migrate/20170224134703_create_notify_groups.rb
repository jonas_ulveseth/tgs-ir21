class CreateNotifyGroups < ActiveRecord::Migration
  def change
    create_table :notify_groups do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
