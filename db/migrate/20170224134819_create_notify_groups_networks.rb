class CreateNotifyGroupsNetworks < ActiveRecord::Migration
  def change
    create_table :notify_groups_networks do |t|
      t.integer :network_id
      t.integer :notify_group_id

      t.timestamps null: false
    end
    add_index :notify_groups_networks, :network_id
    add_index :notify_groups_networks, :notify_group_id
  end
end
