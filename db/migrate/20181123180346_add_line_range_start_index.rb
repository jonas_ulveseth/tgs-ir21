class AddLineRangeStartIndex < ActiveRecord::Migration
  def change
    add_index :sms_hub_line_ranges, :range_start
  end
end
