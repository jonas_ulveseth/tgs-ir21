class AddNameToSmsHub < ActiveRecord::Migration
  def change
    add_column :sms_hubs, :name, :string
  end
end
