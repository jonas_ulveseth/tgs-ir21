class AddCheckForDiffToImport < ActiveRecord::Migration
  def change
    add_column :imports, :checked_for_diff, :boolean
  end
end
