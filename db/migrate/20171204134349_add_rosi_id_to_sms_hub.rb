class AddRosiIdToSmsHub < ActiveRecord::Migration
  def change
    add_column :sms_hubs, :rosi_id, :integer
    add_index :sms_hubs, :rosi_id
  end
end
