class AddOldIso2ToCountries < ActiveRecord::Migration
  def change
    add_column :countries, :old_iso_2, :string
  end
end
