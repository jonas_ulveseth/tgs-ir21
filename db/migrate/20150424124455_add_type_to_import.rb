class AddTypeToImport < ActiveRecord::Migration
  def change
    add_column :imports, :file_type, :string
  end
end
