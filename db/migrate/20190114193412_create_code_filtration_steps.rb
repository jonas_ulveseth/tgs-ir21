class CreateCodeFiltrationSteps < ActiveRecord::Migration
  def change
    create_table :code_filtration_steps do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
