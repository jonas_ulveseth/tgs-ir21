class AddRangeStartToNetworkLIneRange < ActiveRecord::Migration
  def change
    add_column :network_line_ranges, :range_start, :string
  end
end
