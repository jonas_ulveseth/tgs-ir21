class CreateCodes < ActiveRecord::Migration
  def change
    create_table :codes do |t|
      t.integer :created_by_id
      t.timestamps null: false
    end
    add_index :codes, :created_by_id
  end
end
