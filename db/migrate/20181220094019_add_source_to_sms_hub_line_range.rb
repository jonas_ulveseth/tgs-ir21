class AddSourceToSmsHubLineRange < ActiveRecord::Migration
  def change
    add_column :sms_hub_line_ranges, :source, :integer
    add_index :sms_hub_line_ranges, :source
  end
end
