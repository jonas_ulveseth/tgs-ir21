class CreateChangeHistories < ActiveRecord::Migration
  def change
    create_table :change_histories do |t|
      t.hstore :data
      t.integer :network_section_id

      t.timestamps null: false
    end
    add_index :change_histories, :network_section_id
  end
end
