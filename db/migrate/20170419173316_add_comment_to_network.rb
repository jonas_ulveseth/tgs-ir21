class AddCommentToNetwork < ActiveRecord::Migration
  def change
    add_column :networks, :comment, :text
  end
end
