class RenameNetworkColumnName < ActiveRecord::Migration
  def change
    rename_column :networks, :presentation_of_country_initials_and_mnn, :country_initials_and_mnn
  end
end
