class AddVersionIdToCodeRanges < ActiveRecord::Migration
  def change
    add_column :code_ranges, :code_range_version_id, :integer
    add_index :code_ranges, :code_range_version_id
  end
end
