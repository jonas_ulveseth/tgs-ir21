class AddGlobalFileNameToIMport < ActiveRecord::Migration
  def change
    add_column :imports, :global_file_name, :string
  end
end
