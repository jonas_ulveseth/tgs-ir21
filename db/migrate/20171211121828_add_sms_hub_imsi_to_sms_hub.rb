class AddSmsHubImsiToSmsHub < ActiveRecord::Migration
  def change
    add_column :sms_hubs, :imsi, :string
  end
end
