class AddCountryIdToSmsHub < ActiveRecord::Migration
  def change
    add_column :sms_hubs, :country_id, :integer
    add_index :sms_hubs, :country_id
  end
end
