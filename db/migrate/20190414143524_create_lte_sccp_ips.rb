class CreateLteSccpIps < ActiveRecord::Migration
  def change
    create_table :lte_sccp_ips do |t|
      t.integer :GSMRoSiSCCPprovider_ID
      t.string :SCCP_IP_Range
      t.string :Country
      t.string :SCCP_Hostname

      t.timestamps null: false
    end
    add_index :lte_sccp_ips, :GSMRoSiSCCPprovider_ID
  end
end
