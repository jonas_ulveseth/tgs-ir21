class AddOriginateIdToNetworkLineRange < ActiveRecord::Migration
  def change
    add_column :network_line_ranges, :orginate_id, :integer
    add_index :network_line_ranges, :orginate_id
  end
end
