class CreateViewGroupsViewFields < ActiveRecord::Migration
  def change
    create_table :view_groups_view_fields do |t|
      t.integer :view_group
      t.integer :view_field

      t.timestamps null: false
    end
    add_index :view_groups_view_fields, :view_group
    add_index :view_groups_view_fields, :view_field
  end
end
