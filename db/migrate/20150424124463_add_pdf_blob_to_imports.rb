class AddPdfBlobToImports < ActiveRecord::Migration
  def change
    add_column :imports, :pdf_data, :binary, limit: 10.megabyte
  end
end
