class AddImportIdToNetwork < ActiveRecord::Migration
  def change
    add_column :networks, :import_id, :integer
    add_index :networks, :import_id
  end
end
