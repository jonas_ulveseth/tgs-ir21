class AddOrganisationOverrideToImport < ActiveRecord::Migration
  def change
    add_column :imports, :organisation_override, :text, array: true, default: []
  end
end
