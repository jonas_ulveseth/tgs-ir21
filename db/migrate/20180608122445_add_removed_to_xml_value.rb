class AddRemovedToXmlValue < ActiveRecord::Migration
  def change
    add_column :xml_values, :removed, :boolean
  end
end
