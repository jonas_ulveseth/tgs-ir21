class AddNameToCode < ActiveRecord::Migration
  def change
    add_column :codes, :name, :string
  end
end
