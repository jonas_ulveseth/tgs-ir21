class AddRemovedFlagToNetworkLineRange < ActiveRecord::Migration
  def change
    add_column :network_line_ranges, :removed, :boolean
    add_column :network_line_ranges, :message, :string
  end
end
