class CreateExports < ActiveRecord::Migration
  def change
    create_table :exports do |t|
      t.string :file
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
