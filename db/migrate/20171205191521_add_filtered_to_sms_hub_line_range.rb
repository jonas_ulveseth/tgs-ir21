class AddFilteredToSmsHubLineRange < ActiveRecord::Migration
  def change
    add_column :sms_hub_line_ranges, :filtered, :boolean
  end
end
