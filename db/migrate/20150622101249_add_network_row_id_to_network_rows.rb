class AddNetworkRowIdToNetworkRows < ActiveRecord::Migration
  def change
    add_column :network_rows, :network_row_id, :integer
  end
end
