class AddTypeToNetworkSection < ActiveRecord::Migration
  def change
    add_column :network_sections, :multi_line, :boolean, default: true
  end
end
