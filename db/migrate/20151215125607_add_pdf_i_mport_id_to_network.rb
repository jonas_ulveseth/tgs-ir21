class AddPdfIMportIdToNetwork < ActiveRecord::Migration
  def change
    add_column :networks, :pdf_import_id, :string
    add_index :networks, :pdf_import_id
  end
end
