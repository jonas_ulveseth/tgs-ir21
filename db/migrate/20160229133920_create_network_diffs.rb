class CreateNetworkDiffs < ActiveRecord::Migration
  def change
    create_table :network_diffs do |t|
      t.integer :network_id
      t.integer :section_id
      t.integer :import_id
      t.string :message
      t.text :snapshot

      t.timestamps null: false
    end
    add_index :network_diffs, :network_id
    add_index :network_diffs, :section_id
    add_index :network_diffs, :import_id
  end
end
