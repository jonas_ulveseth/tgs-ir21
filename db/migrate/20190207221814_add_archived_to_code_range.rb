class AddArchivedToCodeRange < ActiveRecord::Migration
  def change
    add_column :code_ranges, :archived, :boolean
  end
end
