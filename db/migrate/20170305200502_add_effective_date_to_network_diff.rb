class AddEffectiveDateToNetworkDiff < ActiveRecord::Migration
  def change
    add_column :network_diffs, :old_effective_date_of_change, :date
  end
end
