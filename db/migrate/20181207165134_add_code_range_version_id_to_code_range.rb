class AddCodeRangeVersionIdToCodeRange < ActiveRecord::Migration
  def change
    add_column :code_ranges, :code_range_id, :integer
    add_index :code_ranges, :code_range_id
  end
end
