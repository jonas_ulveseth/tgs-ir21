class AddNetworkIdToCodes < ActiveRecord::Migration
  def change
    add_column :codes, :network_id, :integer
    add_index :codes, :network_id
  end
end
