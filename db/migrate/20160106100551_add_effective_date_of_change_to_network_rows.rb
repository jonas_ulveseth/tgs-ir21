class AddEffectiveDateOfChangeToNetworkRows < ActiveRecord::Migration
  def change
    add_column :network_rows, :effective_date_of_change, :date
  end
end
