class AddSortColumnToNetworkSections < ActiveRecord::Migration
  def change
    add_column :network_sections, :sort_order, :integer
  end
end
