require 'nokogiri'
require 'open-uri'

##
## Scrape country codes from the World Bank. Note! This list is not complete.
##

doc = Nokogiri::XML(open('http://api.worldbank.org/countries?per_page=300'))
doc.remove_namespaces!
doc.xpath('//country').map do |country|
  values = {iso2_code: (country > 'iso2Code').text,
      name: (country > 'name').text, mnp: false}
  Country.where(iso_code: country[:id]).first_or_create(values).update!(values)
end
