ENV['RAILS_ENV'] ||= 'test'
require "authlogic/test_case"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

Authlogic::Session::Base.controller = Authlogic::ControllerAdapters::RailsAdapter.new(self)

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
end

class ActionController::TestCase
  setup :activate_authlogic
end
