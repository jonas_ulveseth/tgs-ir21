require 'test_helper'

class ChangeHistoriesControllerTest < ActionController::TestCase
  setup do
    @change_history = change_histories(:one)
    @user           = users(:one)
  end
  setup :activate_authlogic
  UserSession.create(@user)

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:change_histories)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create change_history" do
    assert_difference('ChangeHistory.count') do
      post :create, change_history: { data: @change_history.data, network_section_id: @change_history.network_section_id }
    end

    assert_redirected_to change_history_path(assigns(:change_history))
  end

  test "should show change_history" do
    get :show, id: @change_history
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @change_history.id
    assert_response :success
  end

  test "should update change_history" do
    patch :update, id: @change_history, change_history: { data: @change_history.data, network_section_id: @change_history.network_section_id }
    assert_redirected_to change_history_path(@change_history.id)
  end

  test "should destroy change_history" do
    assert_difference('ChangeHistory.count', -1) do
      delete :destroy, id: @change_history
    end

    assert_redirected_to change_histories_path
  end
end
