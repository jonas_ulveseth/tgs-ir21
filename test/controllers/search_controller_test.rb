require 'test_helper'
class SearchControllerTest < ActionController::TestCase
	setup do
		@user            = users(:one)
		@network_row     = network_rows(:one)
	end
	setup :activate_authlogic
	UserSession.create(@user)

	test "should get index" do
		get :index, :q => "test"
		assert_response :success
	end
end
