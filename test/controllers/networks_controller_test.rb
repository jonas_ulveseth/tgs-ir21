require 'test_helper'

class NetworksControllerTest < ActionController::TestCase
  setup do
    @network = networks(:one)
    @user    = users(:one)
  end
  setup :activate_authlogic

  UserSession.create(@user)

  test "should get index" do
    get :index, format: :html
    assert_response :success
    assert_not_nil assigns(:networks)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create network" do
    assert_difference('Network.count') do
      post :create, network: { abbreviated_mnn: @network.abbreviated_mnn, network_color_code: @network.network_color_code, network_name: @network.network_name, organisation_id: @network.organisation_id, country_initials_and_mnn: @network.country_initials_and_mnn, source: @network.source, tadig_code: @network.tadig_code, terrestrial: @network.terrestrial }
    end
    @network.save!
    assert_redirected_to network_path(assigns(:network))
  end

  test "should show network" do
    get :show, id: @network
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @network
    assert_response :success
  end

  test "should update network" do
    patch :update, id: @network, network: { abbreviated_mnn: @network.abbreviated_mnn, network_color_code: @network.network_color_code, network_name: @network.network_name, organisation_id: @network.organisation_id, country_initials_and_mnn: @network.country_initials_and_mnn, source: @network.source, tadig_code: @network.tadig_code, terrestrial: @network.terrestrial }
    assert_redirected_to network_path(@network.id)
  end

  test "should destroy network" do
    assert_difference('Network.count', -1) do
      delete :destroy, id: @network
    end

    assert_redirected_to networks_path
  end
end
