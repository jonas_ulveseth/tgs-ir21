require 'test_helper'

class HermesOperatorsControllerTest < ActionController::TestCase
  setup do
    @hermes_operator = hermes_operators(:one)
    @user            = users(:one)
  end
  setup :activate_authlogic
  UserSession.create(@user)

  test "should get index" do
    get :index, id: @hermes_operator.id
    assert_response :success
    assert_not_nil assigns(:hermes_operators)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create hermes_operator" do
    assert_difference('HermesOperator.count') do
      post :create, hermes_operator: { data: @hermes_operator.data, network_id: @hermes_operator.network_id }
      @hermes_operator.save!
    end

    assert_redirected_to hermes_operator_path(assigns(:hermes_operator))
  end

  test "should show hermes_operator" do
    get :show, id: @hermes_operator
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @hermes_operator.id
    assert_response :success
  end

  test "should update hermes_operator" do
    patch :update, id: @hermes_operator, hermes_operator: { data: @hermes_operator.data, network_id: @hermes_operator.network_id }
    assert_redirected_to hermes_operator_path(@hermes_operator.id)
  end

  test "should destroy hermes_operator" do
    assert_difference('HermesOperator.count', -1) do
      delete :destroy, id: @hermes_operator
    end

    assert_redirected_to hermes_operators_path
  end
end
