require 'test_helper'

class NetworkSectionsControllerTest < ActionController::TestCase
  setup do
    @network_section = network_sections(:a)
    @user            = users(:one)
    :activate_authlogic
  end
  UserSession.create(@user)

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:network_sections)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create network_section" do
    assert_difference('NetworkSection.count') do
      post :create, network_section: { name: @network_section.name, field_name: @network_section.field_name  }
      @network_section.save!
    end

    assert_redirected_to network_section_path(assigns(:network_section))
  end

  test "should show network_section" do
    get :show, id: @network_section
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @network_section
    assert_response :success
  end

  test "should update network_section" do
    @request.env['HTTP_REFERER'] = edit_network_section_path(@network_section)
    patch :update, id: @network_section, network_section: { name: @network_section.name, field_name: @network_section.field_name }
    assert_redirected_to edit_network_section_path(@network_section)
  end
    
  test "should destroy network_section" do
    assert_difference('NetworkSection.count', -1) do
      delete :destroy, id: @network_section
    end

    assert_redirected_to network_sections_path
  end
end
