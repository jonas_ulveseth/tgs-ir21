require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
    @user = users(:one)
  end
  setup :activate_authlogic
  UserSession.create(@user)

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post :create, user: {email: 'test@test.com', first_name: 'tester', last_name: 'tester_lastname', password: '12345678',password_confirmation: '12345678'}
    end
    assert_redirected_to user_path(assigns(:user))
  end

  test "should show user" do
    get :show, id: @user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user
    assert_response :success
  end

  test "should update user" do
    patch :update, id: @user, user: { crypted_password: @user.crypted_password, email: @user.email, first_name: @user.first_name, last_name: @user.last_name, password_salt: @user.password_salt, persistence_token: @user.persistence_token }
    assert_redirected_to user_path(@user.id)
  end

  test "should destroy user" do
    # User is updated with removed = true
    assert 'User.last.inactive' do
      delete :destroy, id: @user
    end
  end
end
