require 'test_helper'

class ImportsControllerTest < ActionController::TestCase
  include Authlogic::TestCase
  setup do
    @import = imports(:one)
    @user   = users(:one)
    :activate_authlogic
    UserSession.create(@user)
  end
  
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:imports)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  # Having hard time to get this to work because of file system etc..

  test "should create import" do
    assert_difference('Import.count') do
      post :create, import: { file_name: fixture_file_upload('files/xml-test.xml', 'xml')}
    end
    @import.save!
    assert_redirected_to imports_path
  end

  test "should show import" do
    get :show, id: @import
    assert_response :success
  end

  #test "should get edit" do
  #  get :edit, id: @import
  #  assert_response :success
  #end

  #test "should update import" do
  #  patch :update, id: @import, import: { file_name: @import.file_name }
  #  assert_redirected_to import_path(@import)
  #end

  #test "should destroy import" do
  #  assert_difference('Import.count', -1) do
  #    delete :destroy, id: @import
  #  end

  #  assert_redirected_to imports_path
  #end
end
