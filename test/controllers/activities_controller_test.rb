require 'test_helper'

class ActivitiesControllerTest < ActionController::TestCase
  setup do
    @activity = activities(:one)
    @user            = users(:one)
  end
  setup :activate_authlogic
  UserSession.create(@user)

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:activities)
  end

  test "should get new" do
    activity = Activity.create
    activity.save!
    get :new
    assert_response :success
  end

  test "should create activity" do
    assert_difference('Activity.count') do
      post :create, activity: { name: @activity.name, activity_type: @activity.activity_type, user_id: @activity.user_id }
      @activity.save!
    end
    
    assert_redirected_to activity_path(assigns(:activity))
  end

  test "should show activity" do
    get :show, id: @activity
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @activity
    assert_response :success
  end

  test "should update activity" do
    @request.env['HTTP_REFERER'] = activity_path(@activity)
    patch :update, id: @activity, activity: { name: @activity.name, activity_type: @activity.activity_type, user_id: @activity.user_id }
    assert_redirected_to activity_path
  end

  test "should destroy activity" do
    assert_difference('Activity.count', -1) do
      delete :destroy, id: @activity.id
    end

    assert_redirected_to activities_path
  end
end
