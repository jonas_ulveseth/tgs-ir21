require 'test_helper'

class NetworkSectionFieldsControllerTest < ActionController::TestCase
  setup do
    @request.env["HTTP_REFERER"] = 'http://test.host/network_section_fields'
    @network_section_field = network_section_fields(:one)
    @user            = users(:one)
  end
  setup :activate_authlogic
  UserSession.create(@user)

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:network_section_fields)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create network_section_field" do
    assert_difference('NetworkSectionField.count') do
      post :create, network_section_field: { name: @network_section_field.name, network_section_id: @network_section_field.network_section_id, sort_order: @network_section_field.sort_order }
    end

    assert_redirected_to network_section_fields_path
  end

  test "should show network_section_field" do
    get :show, id: @network_section_field
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @network_section_field
    assert_response :success
  end

  test "should update network_section_field" do
    patch :update, id: @network_section_field, network_section_field: { name: @network_section_field.name, network_section_id: @network_section_field.network_section_id, sort_order: @network_section_field.sort_order }
    assert_redirected_to network_section_field_path(assigns(:network_section_field))
  end

  test "should destroy network_section_field" do
    assert_difference('NetworkSectionField.count', -1) do
      delete :destroy, id: @network_section_field
    end

    assert_redirected_to network_section_fields_path
  end
end
