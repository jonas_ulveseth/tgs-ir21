require 'test_helper'

class ImportTest < ActiveSupport::TestCase

	setup do 
    :activate_authlogic
    Rails.application.load_seed
  end
  test "should import xml" do
    file = File.open(Rails.root.join('test', 'fixtures', 'files' '/xml-test.xml'))
    @import = Import.new
    @import.import(file)
    @import.save!
    p @import
    assert true
  end

  test "should correctly import a pdf file" do
   assert true
 end

 test "should correctly import a excel file" do
  assert true
end
end
