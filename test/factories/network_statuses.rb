FactoryGirl.define do
  factory :network_status do
    network_id 1
    sccp_status "MyString"
    routing_status "MyString"
  end
end
