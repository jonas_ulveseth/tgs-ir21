FactoryGirl.define do
  factory :code_range do
    range_start "MyString"
    range_stop "MyString"
    network_id 1
    code_id 1
    message "MyString"
    removed false
    filtered false
    removed_by_id 1
  end
end
