FactoryGirl.define do
  factory :lte_sccp_ip do
    GSMRoSiSCCPprovider_ID 1
    SCCP_IP_Range "MyString"
    Country "MyString"
    SCCP_Hostname "MyString"
  end
end
