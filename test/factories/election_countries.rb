# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :election_country do
    name "MyString"
    iso_code "MyString"
    iso2_code "MyString"
    e_164 ""
    mccs "MyString"
    mnp false
  end
end
