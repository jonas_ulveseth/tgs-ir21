# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :country do
    name "MyString"
    iso_code "MyString"
    iso2_code "MyString"
    e164_code "MyString"
    mnp false
  end
end
