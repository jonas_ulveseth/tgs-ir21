FactoryGirl.define do
  factory :lte_mno_ip do
    sms_number_administration_id 1
    country "MyString"
    carrier "MyString"
    MNO_IP_Range "MyString"
  end
end
