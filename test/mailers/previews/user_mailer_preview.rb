# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
	def notify_network_change
		UserMailer.notify_network_change(User.last, Network.find(145))
	end
end
