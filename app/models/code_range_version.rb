class CodeRangeVersion < ActiveRecord::Base
  has_many :code_ranges, dependent: :destroy
  belongs_to :network
  belongs_to :code
end
