class NotifyGroupsOrganisation < ActiveRecord::Base
	has_many :notify_groups
	belongs_to :organisation
end
