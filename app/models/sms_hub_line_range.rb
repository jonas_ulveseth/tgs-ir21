class SmsHubLineRange < ActiveRecord::Base
  belongs_to :network
  belongs_to :sms_hub

  def self.condense(object)
    previous = nil
    unless object
      object = self
    end
    p object.count
    @delete_array = []
    object.each_with_index do |range, index|
      #break if range.removed
      p 'one row filtered'
      # If there are previous numbers
      if previous.try(:range_stop)
        p "Got a previous - and start is#{previous.range_start}"
        p "processing line range with start : #{range.range_start}"
        # Create a range in array and start over
        if previous.try(:range_stop).to_i + 1 == range.try(:range_start).to_i
          #import current stop range into previous stop range
          p "It is found #{previous.try(:range_stop)} |||| #{range.try(:range_start)}"
          range.range_start = previous.range_start
          #Delete this range
          pr = SmsHubLineRange.find(previous.id)
          previous = range
          pr.removed = "true"
          pr.message = "Deleted because of consecutive"
          p "It´s deleted #{pr.range_start}"
          p "previous start is #{range.range_start}"
        else
          p "it is not a match #{previous.range_stop} ||||| #{range.try(:range_start)}"
          previous = range
        end
      else
        previous = range
        p 'previous is set'
      end
    end
    object
  end

  def reevaluate_line_ranges
    p "Evaluating a range - #{self.id}"
    if self.network_id
      network = Network.find(self.network_id)
      organisation = network.organisation
      @rosi_id = self.network.rosi_id
      if organisation
        @network_ids = Network.where(organisation_id: organisation.id).pluck(:id).to_a
      else
        @network_ids = Network.where(id: self.network_id).pluck(:id).to_a
      end
    else
      @smshub_ids = SmsHub.where(country: self.sms_hub.country).pluck(:id).to_a
    end
    @range_start = self.try(:range_start)
    @range_stop = self.try(:range_stop)
    return unless @range_start && @range_stop
    return if self.removed

    # Check if the line range has the same length
    unless @range_start.length == @range_stop.length
      p "FIXING LENGTH"
      min_value = [@range_start, @range_stop].min
      max_value = [@range_start, @range_stop].max
      diff = max_value.length - min_value.length
      diff.times do |t|
        if @range_start < @range_stop
          @range_start = "#{@range_start}0"
          p @range_start
        else
          @range_stop = "#{@range_stop}9"
          p @range_stop
        end
      end
      self.message = "Uneven ranges - correcting"
      self.range_start = @range_start
      self.range_stop = @range_stop
      self.save!
    end

    #### Switched start and stop range fix ####
    ###########################################
    if @range_start > @range_stop
      p 'Start range is bigger - fixing'
      self.range_start = @range_stop
      self.range_stop = @range_start
      self.message = 'switched line ranges'
      self.save!
    end
  end

  def fix_double_line_ranges
    #### Fix double line ranges ####
    ################################
    if self.network_id
      double = SmsHubLineRange.where('network_id IN(?) AND range_start = ? AND range_stop = ? AND id != ? AND removed IS NULL', @network_ids, "#{@range_start}", "#{@range_stop}", self.id).first
      p 'network found'
    else
      p 'no network found'
      double = SmsHubLineRange.where('sms_hub_id IN(?) AND range_start = ? AND range_stop = ? AND id != ? AND removed IS NULL', @smshub_ids, "#{@range_start}", "#{@range_stop}", self.id).first
    end
    if double
      p 'FIXING DOUBLE LINE RANGES'
      self.removed = true
      self.message = "Double line range removed #{self.range_start} #{self.range_stop}"
      self.removed_by_id = double.id
      self.save!
    end
  end

  def fix_overlapping
    #### Overlapping fix ####
    ########################
    if self.network_id
      p 'starting overlapping check'
      p "network ids is #{@network_ids}"
      overlapping_ranges = SmsHubLineRange.where("network_id IN(?) AND range_start < ? AND range_stop > ? AND removed is NULL", @network_ids, "#{@range_start}", "#{@range_stop}").first
      p "overlapping-start: #{@range_start} overlapping-stop: #{@range_stop}"
    else
      p 'skipped overlapping'
      overlapping_ranges = SmsHubLineRange.where("sms_hub_id IN(?) AND range_start < ? AND range_stop > ? AND removed is NULL", @smshub_ids, "#{@range_start}", "#{@range_stop}").first
    end
    if overlapping_ranges
      p 'FOUND AN OVERLAP'
      self.removed = true
      self.message = "overlapping ranges with #{overlapping_ranges.range_start} #{overlapping_ranges.range_stop}"
      self.removed_by_id = overlapping_ranges.id
      self.save!
    end
  end

  def fix_partly_overlapping
    #### Partly overlapping fix ####
    ################################
    return if self.removed == true
    if self.network_id
      p 'FOUND A NETWORK WITH ROSI'
      partly_overlapped_top = SmsHubLineRange.where("network_id IN(?) AND range_start < ? AND range_stop > ? AND removed is NULL", @network_ids, "#{@range_start}", "#{@range_stop}").all
      p "network_id is #{@network_ids}"
      p "stop #{@range_stop.to_s}"
      p partly_overlapped_top
    else
      partly_overlapped_top = SmsHubLineRange.where("sms_hub_id IN(?) AND range_start < ? AND range_stop > ? AND removed is NULL", @smshub_ids, "#{@range_start}", "#{@range_stop}").all
    end
    if partly_overlapped_top.first
      partly_overlapped_top.each do |overlap|
        p 'FOUND A PARTLY OVERLAP TOP'
        if self.sms_hub.country && partly_overlapped_top.count > 1
          p 'ITs WORKING!!!!!!!!!!!!'
        end
        unless overlap.removed = true
          self.range_stop = overlap.range_stop
          self.save!
          overlap.removed = true
          overlap.message = "Removed because of partly overlapping top #{self.range_start} #{self.range_stop}"
          overlap.removed_by_id = self.id
          overlap.save!
        end
      end
    end
    if self.network
      partly_overlapped_bottom = SmsHubLineRange.where("network_id IN(?) AND range_start < ? AND range_stop > ? AND removed is NULL", @network_ids, "#{@range_start}", "#{@range_stop}").all
    else
      partly_overlapped_bottom = SmsHubLineRange.where("sms_hub_id IN(?) AND range_start < ? AND range_stop > ? AND removed is NULL", @smshub_ids, "#{@range_start}", "#{@range_stop}").all
    end
    if partly_overlapped_bottom.first
      p 'FOUND A PARTLY OVERLAP BOTTOM'
      partly_overlapped_bottom.each do |overlap|
        unless overlap.removed = true
          self.range_start = overlap.range_start
          self.save!
          overlap.removed = true
          overlap.message = "Removed because of partly overlapping bottom #{self.range_start} #{self.range_stop}"
          overlap.removed_by_id = self.id
          overlap.save!
        end
      end
    end
  end

  def self.fix_outliers
    #### Overlapping fix ####
    ########################
    smshubs = SmsHub.where("rosi_id IN(?)", [913, 303, 301, 243, 208, 121, 83]).pluck(:id).to_a
    line_ranges = SmsHubLineRange.where("sms_hub_id IN(?) AND removed is NULL", smshubs).all
    line_ranges.each do |range|
      overlapping_range = SmsHubLineRange.where("sms_hub_id IN(?) AND range_start < ? AND range_stop > ? AND removed is NULL", smshubs, "#{range.range_start}", "#{range.range_stop}").first
      if overlapping_range
        p 'FOUND AN OVERLAP'
        range.removed = true
        range.message = "overlapping ranges with #{overlapping_range.range_start} #{overlapping_range.range_stop}"
        range.removed_by_id = overlapping_range.id
        range.save!
      end
    end
  end

  def delete_higher_numbers()
    p 'deleting higher numbers'
    unless self.filtered == true
      bigger_line_range = SmsHubLineRange.joins(:sms_hub).where("length(range_start) > ? AND range_start LIKE ? AND sms_hubs.rosi_id = ? AND sms_hub_line_ranges.id != ? AND removed is NULL", "#{range_start.length}", "#{range_start}%", self.sms_hub.rosi_id, self.id).where(removed: [nil, false]).all
      unless bigger_line_range.blank?
        p 'REMOVING OWN'
        bigger_line_range.update_all(removed: true, message: "line range covered by #{self.range_start}", filtered: true)
        return
      else
        p 'No bigger range found'
      end

    # check against other networks
    # Get all sms hub with same rosi id
    if self.sms_hub.try(:rosi_id)
      p "checking for same - #{self.range_start}"
      if self.sms_hub.network
        same_networks = Network.where(organisation_id: self.sms_hub.network.organisation_id).pluck(:id).to_a
        same_sms_hubs = SmsHub.where("rosi_id = ? OR network_id IN(?)", self.sms_hub.rosi_id, same_networks).pluck(:id).to_a
      else
        same_sms_hubs = SmsHub.where("rosi_id = ?", self.sms_hub.rosi_id).pluck(:id).to_a
      end
      
      same = SmsHubLineRange.where("range_start = ? AND id != ? AND sms_hub_id IN(?)", range_start, self.id, same_sms_hubs).where(removed: [nil, false]).first
    end
    if same
      p 'FOUND SAME'
      unless same.network
        p 'Setting the other network as deleted'
        same.update_columns(removed: true, message: "duplicate override by line range #{self.range_start}", removed_by_id: self.id, filtered: true)
      else
        p 'it has a network'
        unless self.network
          p 'removing by date - own'
          self.update_columns(removed: true, message: "duplicate override by line range #{same.range_start}", removed_by_id: same.id)    
        else  
          if self.try(:network).try(:xml_creation_date) > same.try(:network).try(:xml_creation_date)
            p 'removing by date - other'
            same.update_columns(removed: true, message: "duplicate override by line range #{self.range_start}", removed_by_id: self.id, filtered: true)
          else
            p 'removing by date - own'
            self.update_columns(removed: true, message: "duplicate override by line range #{same.range_start}", removed_by_id: same.id)
          end
        end
      end
    else
      p 'No duplicate found'
    end
  end
end

# Method to filter file based line ranges
def cut_down_numbers
  p "start before fix #{self.range_start}"
  p "stop before fix #{self.range_stop}"
  p "id is #{self.id}"
  start = self.range_start.gsub(/0+$/, '')
  stop = self.range_stop.gsub(/9+$/, '')
  p "start after fix #{start}"
  p "stop after fix #{stop}"

  if start.length != stop.length
    if stop > start
      p 'stop is bigger'
      uneven_amount = stop.length - start.length
      equalize_number = "#{start}%0#{uneven_amount}o" % '0'
      start = "#{equalize_number}"
      p "Equalize number is #{equalize_number}"
      p "stop after equalize is #{stop}"
      p "start after equalize is #{start}"
    else
      p 'start is bigger'
      uneven_amount = start.length - stop.length
      equalize_number = "%0#{uneven_amount}o" % '0'
      equalize_number = equalize_number.gsub('0','9')
      stop = "#{stop}#{equalize_number}"
      p "Equalize number is #{equalize_number}"
      p "stop after equalize is #{stop}"
      p "start after equalize is #{start}"
    end
    p "start after fixing length is #{start} and stop #{stop}"
  end

  if start == stop
      # Check for duplicates inside same network 
      double_check = SmsHubLineRange.where(range_start: start).where('id != ?', self.id).first
      if double_check
        self.update(removed: true, message: "removed because of double entries")
      else
        self.update(range_start: start)
      end
      # Check for consuming line ranges
      bigger_line_range = SmsHubLineRange.where("length(range_start) > ? AND range_start LIKE ? AND sms_hub_id = ? AND id != ? AND removed is NULL", "#{range_start.length}", "#{range_start}%", self.sms_hub_id, self.id).where(removed: [nil, false]).all
      unless bigger_line_range.blank?
        p 'REMOVING OWN'
        bigger_line_range.update_all(removed: true, message: "line range covered by #{self.range_start}", filtered: true)
        return
      else
        p 'No bigger range found'
      end
    else
      #increment
      p 'Incrementing'
      p "start is #{start} and stop is #{stop}"
      diffrence = stop.to_i - start.to_i
      unless diffrence > 900
        p 'Increment line range'
        p "diffrence is #{diffrence}"
        p 'it is not the same'
        p "#{}"
        (start..stop).each do |n|
          p 'Looping unmatched'
          SmsHubLineRange.create(range_start: n, network_id: self.network_id, sms_hub_id: self.sms_hub_id, message: "new range created from #{self.id}", filtered: true)
        end
        self.update_columns(removed: true, message: 'removed because of new ranges incremented')
      else
        self.update_columns(message: "to big line range")
        p 'COULD NOT UPDATE'
        # Create equal numbers, by looking at length, and keeping only the latest number in the end.
        combined = start.split("").zip stop.split("")
        p 'combined created'
        p combined
        @done = nil
        combined.each_with_index do |c, c_index|
          p 'started looping combined'
          p c
          unless c.first == c.last
            combined = combined.shift(c_index + 1)
          end
        end
        old_start = start
        old_stop = stop
        start, stop = combined.transpose
        start = start.join('')
        stop = stop.join('')
        diffrence = stop.to_i - start.to_i
        # Increment the new range
        if diffrence < 900
          p 'coding karate'
          p "#{start} - #{stop}"
          p "was before #{old_start} - #{old_stop}"
          (start..stop).each do |n|
            p "#{start} - #{stop}"
            SmsHubLineRange.create(range_start: n, network_id: self.network_id, sms_hub_id: self.sms_hub_id, message: "new range created from #{self.range_start} - originally to big range (was before #{old_start} - #{old_stop})", filtered: true)
          end
        end
        self.update_columns(removed: true, message: 'removed because of new ranges (uneven equalize fix)')
      end
    end
  end

  def self.queue_sms_hub_file_job(create_file = nil)
    # Creates basis networks and line ranges
    self.delay(queue: 'codes').create_sms_hub_with_db
    # Pulls in all line ranges IR21
    Organisation.delay(queue: 'codes').preload_jobs
    # Filter non IR21 Line ranges
    self.delay(queue: 'codes').cut_down_file_numbers
    self.preload_ir21_line_ranges
    self.delay(queue: 'codes').remove_ten_range
    self.delay(queue: 'codes').remove_cc_doubles
    self.delay(queue: 'codes').remove_rosi_id_doubles
    self.delay(queue: 'codes').remove_crossover_lineranges
    self.delay(queue: 'codes').remove_spaces
    self.delay(queue: 'codes').global_deletion
    if create_file
      self.delay(queue: 'codes').create_sms_hub_file
    end
    Code.add_unmatched_networks
  end

  def self.create_sms_hub_with_db
    SmsHub.delete_all
    SmsHubLineRange.delete_all
    #countries_array = Country.joins(:networks).offset(20).take(10).to_a
    #@network_sample = Network.where("country_id IN(?)", countries_array).pluck(:id).to_a
    #Start looping
    incoming_file = "#{Rails.root}/data/upload/code_base_file.csv"
    csv_text = File.read(incoming_file, encoding: "ISO8859-1:utf-8")
    csv_import = CSV.parse(csv_text, :quote_char => '"', :col_sep => ";",:row_sep => :auto, encoding: "ISO-8859")[1 .. -1]
    @line_ranges_added = nil
    csv_import.each do |csv_import_row|
      @mnc_mcc     = csv_import_row[4]
      @country     = csv_import_row[1]
      @rosi_id     = csv_import_row[0]
      @network     = csv_import_row[2]
      @line_ranges = csv_import_row[3]

      next unless @mnc_mcc 

      p csv_import_row
      p @line_ranges
      ############################
      ## More than one network ####
      if @mnc_mcc.split(';').count > 1
        p 'more'
        @mnc_mcc.split(';').each_with_index do |mnc_mcc, i|
          mcc = mnc_mcc.split('-').first.strip
          mnc = mnc_mcc.split('-').last.strip
          p @mnc_mcc
          p mcc
          p mnc
          # Find network
          network = nil
          if network
            p 'found network'
            p 'adding network'
            sms_hub = SmsHub.create(name: @network,
              country: @country,
              country_name: @country,
              imsi: mnc_mcc.strip,
              network_id: network.id,
              country_id: network.country_id,
              imported_from: 'IR21 - matched',
              rosi_id: network.rosi_id)
            # Adding file based line ranges
            #if @line_ranges_added.nil?
              unless @line_ranges.nil?
                @line_ranges.split("\r\n").each do |range|
                  SmsHubLineRange.create(range_start: range.split(' ').first, range_stop: range.split(' ').last, sms_hub_id: sms_hub.id, source: 1)
                end
                @line_ranges_added = true   
              end
            #end  
          else
            p 'not found, adding unmatched'
            same_rosi_id_check = SmsHub.where("rosi_id = ? AND network_id IS NOT NULL",@rosi_id).first 
            unless same_rosi_id_check
              sms_hub = SmsHub.create(name: @network,
                country: @country,
                country_name: @country,
                imsi: mnc_mcc.strip,
                imported_from: 'File unmatched',
                rosi_id: @rosi_id)

              if @line_ranges_added.nil?
                p 'it is first'
                # Line ranges
                unless @line_ranges.nil?
                  # Check if there is a network
                  @line_ranges.split("\r\n").each do |range|
                    SmsHubLineRange.create(range_start: range.split(' ').first, range_stop: range.split(' ').last, sms_hub_id: sms_hub.id, source: 1)
                  end
                  @line_ranges_added = true
                end
              end
            end
          end
        end
        @line_ranges_added = nil
        # Check to see if line ranges should be moved to a network connected hub
        if connected_network_hub = SmsHub.where("rosi_id = ? AND network_id IS NOT NULL",@rosi_id).first
          sms_hubs_array = SmsHub.where(rosi_id: @rosi_id).pluck(:id).to_a
          if sms_hubs = SmsHubLineRange.where("sms_hub_id IN(?)", sms_hubs_array.to_a)
            sms_hubs.update_all(sms_hub_id: connected_network_hub.id)
          end
        end
      else
      ######################### 
      ## One network #########
      p 'one'
      mcc = @mnc_mcc.split('-').first.strip
      mnc = @mnc_mcc.split('-').last.strip
      p mcc
      p mnc
        # Find network
        network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ? AND organisation_id IS NOT NULL", mcc, mnc).first
        if network
          p 'found network'
          sms_hub = SmsHub.create(name: @network,
            country: @country,
            country_name: @country,
            imsi: @mnc_mcc,
            network_id: network.id,
            country_id: network.country_id,
            imported_from: 'IR21 - matched',
            rosi_id: network.rosi_id)
          unless @line_ranges.nil?
            @line_ranges.split("\r\n").each do |range|
              SmsHubLineRange.create(range_start: range.split(' ').first, range_stop: range.split(' ').last, sms_hub_id: sms_hub.id, source: 1)
            end
          end
        else
          p 'not found'
          sms_hub = SmsHub.create(name: @network,
            country: @country,
            country_name: @country,
            imsi: @mnc_mcc,
            imported_from: 'File unmatched',
            rosi_id: @rosi_id)
          # Line ranges
          unless @line_ranges.nil?
            @line_ranges.split("\r\n").each do |range|
              SmsHubLineRange.create(range_start: range.split(' ').first, range_stop: range.split(' ').last, sms_hub_id: sms_hub.id, source: 1)
            end
          end
        end
      end
      @unmatched_line_ranges_added = nil
    end
    # Check and create records for all IR21 records that are not in sms hub file
    matched_networks_array = SmsHub.where("network_id IS NOT NULL").pluck(:network_id).to_a
    unmatched_ir21_networks = Network.where("id NOT IN(?) AND organisation_id IS NOT NULL", matched_networks_array).all
    unmatched_ir21_networks.each do |unmatched_ir21_network|
      SmsHub.create(network_id: unmatched_ir21_network.id,
        rosi_id: unmatched_ir21_network.rosi_id,
        name: unmatched_ir21_network.display_name,
        country: unmatched_ir21_network.try(:country).try(:name),
        country_id: unmatched_ir21_network.try(:country).try(:id),
        imsi: "#{unmatched_ir21_network.routing["e212_mcc"]}-#{unmatched_ir21_network.routing["e212_mnc"]}",
        imported_from: 'IR21 - unmatched')
    end
  end

  # Reduces line range numbers for line ranges that are not matched with a network
  def self.cut_down_file_numbers(sms_hub_id = nil)
    file_sms_hubs = SmsHub.pluck(:id).to_a
    if sms_hub_id
      sms_hubs = SmsHubLineRange.where("sms_hub_id = ?", sms_hub_id).where(removed: [false, nil]).all
    else
      sms_hubs = SmsHubLineRange.where("range_stop IS NOT NULL AND sms_hub_id IN(?) AND filtered is NULL", file_sms_hubs).where(removed: [false, nil]).take(50000)
    end
    sms_hubs.each do |hub|
      hub.update_columns(filtered: true)
      hub.cut_down_numbers
    end
    SmsHubLineRange.where(removed: [false, nil]).each do |range|
      double_check = SmsHubLineRange.where(range_start: range.range_start, removed: [nil, false]).where('id != ?', range.id).first
      p 'checking double'
      if double_check
        p 'double found and removed'
        range.update(removed: true, message: "removed because of double entries #{range.id}")
      end
    end
  end

  def self.preload_ir21_line_ranges
    SmsHubLineRange.all.update_all(filtered: false)
    ##### Filter line ranges ######
    ###############################
    ir21_sms_hubs = SmsHub.where("network_id IS NOT NULL").pluck(:id).to_a
    line_ranges_for_filtering = SmsHubLineRange.where("sms_hub_id IN(?) AND filtered is false AND range_start != ''", ir21_sms_hubs)
    .where(removed: [false, nil])
    .find_in_batches(batch_size: 10000) do |line_ranges|
      # feed delayed job
      p "line range count #{line_ranges.size}"
      self.delay(queue: 'codes').filter_sms_hub_line_ranges(line_ranges)
    end
  end

  def self.filter_sms_hub_line_ranges(line_ranges = nil)
    p 'starting to filter'
    unless line_ranges
      p 'setting batch manually'
      ir21_sms_hubs = SmsHub.where("network_id IS NOT NULL").pluck(:id).to_a
      @line_ranges = SmsHubLineRange.where("sms_hub_id IN(?) AND filtered is false AND range_start != ''", ir21_sms_hubs).where(removed: [false, nil]).take(100000)
      p "processing #{@line_ranges.size}"
      line_ranges = @line_ranges
    else
      @line_ranges = line_ranges
    end
    @line_ranges.each_with_index do |hub, index|
      p "processing #{index} of #{line_ranges.size} "
      hub.delete_higher_numbers
      hub.update_columns(filtered: true)
    end
  end

  def create_large_files
    outgoing_file = "#{Rails.root}/data/export/hermes_condensed_export_large_networks-#{Time.now}.csv"
    data = CSV.open(outgoing_file, "wb", col_sep: ';', force_quotes: false) do |csv_export_row|
      SmsHub.select("DISTINCT ON(rosi_id) *").where("imsi != ? AND imsi != ?", '724-05', '520-03').where(rosi_id: [10, 810, 1007, 1038]).all.each do |hub|
        # Collect all imsis
        if hub.network
          p "processing network #{hub.network.id}"
          # If routing is not defined, notify and move on
          unless hub.network.routing
            p 'routing is not defined'
            next
          end
          imsis = Network.where("rosi_id = ? AND organisation_id IS NOT NULL", hub.network.rosi_id)

          if imsis.count == 1
            imsi = "#{imsis.first.routing['e212_mcc',]}-#{imsis.first.routing['e212_mnc']}"
          else
            # When there is multiple Networks with the same rosi_id, create a imsi range separated with semikolon
            array = []
            imsis.each do |i|
              next unless i.routing
              array.push("#{i.routing['e212_mcc',]}-#{i.routing['e212_mnc']}")
            end
            imsi = array.join(';')
          end
        end
        connected_networks_array = SmsHub.where(rosi_id: hub.rosi_id).pluck(:id).to_a
        lr = SmsHubLineRange.where("sms_hub_id IN(?)", connected_networks_array).where(removed: [nil, false]).all
        line_range_map = lr.map { |l| ["#{l.range_start}"] }
        if hub.network
          if hub.country_id
            country = Country.find(hub.country_id).name
          else
            country = hub.country_name
          end
          csv_export_row << [hub.network.rosi_id, country, hub.network.display_name.encode('WINDOWS-1252', {invalid: :replace, undef: :replace, replace: '?'}), line_range_map.sort.join("\n"), imsi, hub.imported_from]
        else
          p 'not matched'
          p "hub id = #{hub.id}"
          if hub.country_id
            country = Country.find(hub.country_id).name
          else
            country = hub.country_name
          end
          csv_export_row << [hub.rosi_id, country, hub.name, line_range_map.join("\n"), hub.imsi, hub.imported_from]
        end
      end
    end

    export = Export.new
    export.data = nil
    export.user_id = 99
    export.export_type_id = 4
    export.file = outgoing_file.split('/').last
    export.save
  end

  def self.create_sms_hub_file
    outgoing_file = "#{Rails.root}/data/export/code_range_export_#{Time.now}.csv"
    data = CSV.open(outgoing_file, "wb", col_sep: ';', force_quotes: false) do |csv_export_row|
      # not(rosi_id: [10,810,1007,1038])
      #.where("imsi != ? AND imsi != ?", '724-05' , '520-03')
      SmsHub.select("DISTINCT ON(rosi_id) *").where('network_id IS NOT NULL').all.each do |hub|
        # Collect all imsis
        if hub.network
          p "processing network #{hub.network.id}"
          # If routing is not defined, notify and move on
          unless hub.network.routing
            p 'routing is not defined'
            next
          end
          imsis = Network.where("rosi_id = ? AND organisation_id IS NOT NULL", hub.network.rosi_id)

          if imsis.count == 1
            imsi = "#{imsis.first.routing['e212_mcc',]}-#{imsis.first.routing['e212_mnc']}"
          else
            # When there is multiple Networks with the same rosi_id, create a imsi range separated with semikolon
            array = []
            imsis.each do |i|
              next unless i.routing
              array.push("#{i.routing['e212_mcc',]}-#{i.routing['e212_mnc']}")
            end
            imsi = array.join(';')
          end
        end
        if hub.network_id
          connected_networks_array = SmsHub.where(rosi_id: hub.rosi_id).pluck(:id).to_a
        else
          connected_networks_array = SmsHub.where(id: hub.id).pluck(:id).to_a
        end
        lr = SmsHubLineRange.where("sms_hub_id IN(?)", connected_networks_array).where(removed: [nil, false]).all
        line_range_map = lr.map { |l| ["#{l.range_start}"] }
        if hub.network
          if hub.country_id
            country = Country.find_by(id: hub.country_id).try(:name)
          else
            country = hub.country_name
          end
          csv_export_row << [hub.network.rosi_id, country, hub.network.display_name.encode('WINDOWS-1252', {invalid: :replace, undef: :replace, replace: '?'}), line_range_map.sort.join("\n"), imsi, hub.imported_from]
        else
          p 'not matched'
          p "hub id = #{hub.id}"
          if hub.country_id
            country = Country.find(hub.country_id).name
          else
            country = hub.country_name
          end
          csv_export_row << [hub.rosi_id, country, hub.name, line_range_map.join("\n"), hub.imsi, hub.imported_from]
        end
      end

      # Function to add all non-matched networks
      SmsHub.select("DISTINCT ON(rosi_id) *").where(network_id: nil).all.each do |hub|
        # Collect all imsis
        # Check if there is a sms hub with rosi id and network ID and skip if so
        network_with_rosi_check = SmsHub.where("rosi_id = ? AND network_id IS NOT NULL", hub.rosi_id).first
        if network_with_rosi_check
          next
        end
        connected_networks_array = SmsHub.where(rosi_id: hub.rosi_id).pluck(:id).to_a
        lr = SmsHubLineRange.where("sms_hub_id IN(?)", connected_networks_array).where(removed: [nil, false]).all
        line_range_map = lr.map { |l| ["#{l.range_start}"] }
        # Find all networks
        imsis = SmsHub.where("rosi_id = ? AND network_id IS NULL", hub.rosi_id).all
        if imsis.count == 1
          p 'going for one'
          imsi = hub.imsi
        else
          p 'creating an array'
          array = []
          imsis.each do |i|
            array.push("#{i.imsi}")
          end
          imsi = array.join(';')
        end
        p 'not matched'
        p "hub id = #{hub.id}"
        if hub.country_id
          country = Country.find(hub.country_id).name
        else
          country = hub.country_name
        end
        csv_export_row << [hub.rosi_id, country, hub.name, line_range_map.join("\n"), imsi, hub.imported_from]
      end
    end
    export = Export.new
    export.data = nil
    export.user_id = 99
    export.export_type_id = 4
    export.file = outgoing_file.split('/').last
    export.save
  end

  # Removes line ranges that have 0-9 in them 100, 101, 102, 103, 104, 105, 106, 107, 108, 109  = 10
  def self.remove_ten_range
    all_line_ranges = SmsHubLineRange.where('range_start LIKE ?', '%0').where(removed: [nil, false])
    #check_if 9 exist
    all_line_ranges.each_with_index do |range, index|
      if range.range_start.last == '0'
        sms_hubs = SmsHub.where(rosi_id: range.sms_hub.rosi_id).pluck(:id)
        check = SmsHubLineRange.where(range_start: range.range_start.to_i+1..range.range_start.to_i + 9).where("sms_hub_id IN(?)", sms_hubs.to_a).where(removed: [false, nil]).all

        if check.size == 9 && range.range_start.last == '0'
          p 'it can be redused'
          # remove first one from record
          check.update_all(removed: true, message: "replaced by first record #{range.range_start}(#{range.try(:sms_hub).try(:imsi)}) which covers all other ranges")
          range.range_start.chop!
          p range.range_start
          range.removed = false
          range.message = 'Last zero removed from range because of all 0-9 existing'
          range.save
        end
      end
      p "#{index} of #{all_line_ranges.size} processed"
    end
  end

  def self.remove_cc_doubles(range_start = nil)
    #Run this in console to keep track of updated records in case of interuption
    SmsHubLineRange.all.update_all(filtered: false)
    if range_start
      doubles = SmsHubLineRange.where(range_start: range_start)
    else
      doubles = SmsHubLineRange.joins(:sms_hub).where(range_start: SmsHubLineRange.select(:range_start).group(:range_start, :removed).having("count(*) > 1").select(:range_start)).where("sms_hubs.network_id IS NOT NULL").where(removed: [nil, false], filtered: false)
    end
    p doubles.size
    doubles.each_with_index do |double, index|
      p "processing #{index} of #{doubles.length}"
      duplicate = SmsHubLineRange.joins({:sms_hub => {:network => :organisation}})
      .where("range_start = ? AND sms_hub_line_ranges.id != ? AND sms_hubs.network_id IS NOT NULL AND organisations.id != ?", double.range_start, double.id, double.network.try(:organisation).try(:id))
      .where(removed: [nil, false])
      .order("networks.xml_creation_date")
      next if duplicate.blank?
      if duplicate.size < 2
        p 'one duplicate'
        own_xml_creation_date = double.sms_hub.network.xml_creation_date
        other_xml_creation_date = duplicate.first.sms_hub.network.xml_creation_date
        if own_xml_creation_date > other_xml_creation_date
          p 'duplicate removed'
          duplicate.first.update_columns(removed: true, message: "Removed because of cc duplicate with #{double.range_start} (#{double.sms_hub.imsi}) ")
        else
          p 'own line range removed'
          double.update_columns(removed: true, message: "Removed because of cc duplicate with #{duplicate.first.range_start} (#{duplicate.first.sms_hub.imsi})")
        end
      else
        p 'more duplicates'
        p duplicate.size
        most_recent = duplicate.last
        duplicate.where("sms_hub_line_ranges.id != ?", most_recent.id).update_all(removed: true, message: "Removed because of cc duplicate with #{most_recent.range_start} (#{most_recent.sms_hub.imsi})")
      end
      double.update_columns(filtered: true)
    end
  end

  # Remove rosi id doubles
  def self.remove_rosi_id_doubles(sms_hub_id = nil)
    if sms_hub_id
      doubles = SmsHubLineRange.where(id: sms_hub_id).joins(:sms_hub)
      .where(range_start: SmsHubLineRange.select(:range_start)
        .group(:range_start)
        .having("count(*) > 1")
        .select(:range_start))
      .where(removed: [nil, false])
      .where('sms_hubs.rosi_id IS NOT NULL')
    else
      doubles = SmsHubLineRange.joins(:sms_hub)
      .where(range_start: SmsHubLineRange.select(:range_start)
        .group(:range_start)
        .having("count(*) > 1")
        .select(:range_start))
      .where(removed: [nil, false])
      .where('sms_hubs.rosi_id IS NOT NULL')
    end
    p "found #{doubles.size} records"              
    doubles.each_with_index do |double, index|
      p "checking double #{double.range_start}"
      p "processing #{index} of #{doubles.length}"
      duplicate = SmsHubLineRange.joins(:sms_hub)
      .where('range_start = ? AND sms_hubs.rosi_id = ?', double.range_start, double.sms_hub.rosi_id)
      .where('sms_hub_line_ranges.id != ?',double.id)
      .where('sms_hubs.rosi_id IS NOT NULL')
      .where(removed: [nil,false])
      .first
      next unless duplicate
      if duplicate.sms_hub.network_id && double.sms_hub.network_id
        p 'found one'
        own_xml_creation_date = double.sms_hub.network.xml_creation_date
        other_xml_creation_date = duplicate.sms_hub.network.xml_creation_date
        if own_xml_creation_date > other_xml_creation_date
          p 'duplicate removed'
          duplicate.update_columns(removed: true, message: "Removed because of rosi duplicate with #{double.range_start} (#{double.sms_hub.imsi}) ")
        else
          p 'own line range removed'
          double.update_columns(removed: true, message: "Removed because of rosi duplicate with #{duplicate.range_start} (#{duplicate.sms_hub.imsi})")
        end
        next
      end
      if duplicate.network_id
        p 'duplicate has network'
        double.update_columns(removed: true, message: "Removed because of rosi duplicate with #{duplicate.range_start} (#{duplicate.sms_hub.imsi}) ")
        next
      end
      if double.network_id
        p 'double has network'
        duplicate.update_columns(removed: true, message: "Removed because of rosi duplicate with #{double.range_start} (#{double.sms_hub.imsi}) ")
        next
      end
      p 'Setting duplicate as removed'
      # Double check here to see that it is not double removed
      if SmsHubLineRange.where("range_start = ? AND id != ?",duplicate.range_start, duplicate.id).where(removed: [nil,false]).first
        duplicate.update_columns(removed: true, message: "Removed because of rosi duplicate with #{double.range_start} (#{double.sms_hub.imsi} - #{double.id}) ")
      end
    end
  end

  def self.remove_crossover_lineranges
    doubles = SmsHubLineRange
    .where(range_start: SmsHubLineRange.select(:range_start)
      .group(:range_start)
      .having("count(*) > 1")
      .select(:range_start))
    .where(removed: [nil, false])
    .where('network_id IS NOT NULL')
    p "found #{doubles.size} records"              
    doubles.each_with_index do |double, index|
      p 'checking double'
      p "processing #{index} of #{doubles.length}"
      duplicate = SmsHubLineRange
      .where('range_start = ?', double.range_start)
      .where('id != ?',double.id)
      .where("network_id IS NULL")
      .where(removed: [nil,false])
      .first
      p 'query done'
      if duplicate
        p 'removing dupliacte'
        duplicate.update_columns(removed: true, message: "Removed because of duplicate with a IR21 line range (#{double.sms_hub.imsi})")
      end
    end
  end
  
  # Check how many double entries was created 
  def self.check_doubles
    line_ranges = SmsHubLineRange.where('removed IS NOT TRUE')
    unmatched_networks_array = []
    crossover_networks_array = []
    same_network_array = []
    line_ranges.each_with_index do |range,index|
      p "checking #{index} of #{line_ranges.size}"
      range_test = SmsHubLineRange.where('range_start = ? AND removed IS NOT TRUE', range.range_start.gsub(' ', '')).where('id != ?', range.id).first
      if range_test
        if range_test.network && range.network
          same_network_array.push("#{range_test.range_start} #{range.sms_hub.imsi}")
        else
          if range_test.network || range.network
            crossover_networks_array.push("#{range_test.range_start} #{range.sms_hub.imsi}")  
          else
            unmatched_networks_array.push("#{range_test.range_start} #{range.sms_hub.imsi}")  
          end
        end
        p "found #{range.range_start} #{range.sms_hub.imsi}"
      else
        p 'nothing found'
      end
    end
    [unmatched_networks_array,crossover_networks_array,same_network_array]
  end

  # Remove line ranges on a global scale
  # Fix for producing a duplicate free version of the file
  def self.global_deletion
    line_ranges = SmsHubLineRange.where(removed: [nil, false])
    line_ranges.each_with_index do |range,index|
      p "checking #{index} of #{line_ranges.size}"
      range_test = SmsHubLineRange.where("range_start = ? AND removed IS NOT TRUE", range.range_start).where('id != ?', range.id).first
      if range_test
        # Check so other one isn´t removed
        deletion_test = SmsHubLineRange.where(id: range.id).first
        unless deletion_test.removed
          range_test.update(removed: true, message: "Global deletion from #{range.range_start} - #{range.sms_hub.imsi}")
        end
        p "found #{range.range_start} #{range.sms_hub.imsi}"
      else
        p 'nothing found'
      end
    end
  end

  def self.remove_spaces
    SmsHubLineRange.where("RIGHT(range_start::varchar, 1) = ' '").each do |range|
      range.strip
      range.message = 'removed whitespace'
      range.save
    end
  end

  def check_double_deleted
    @ranges = SmsHubLineRange.where(removed: true).all
    p "found #{@ranges.count} records"
    @ranges.all.each_with_index do |range, index|
      p "processing #{index} of #{@ranges.size}"
      error_array = []
      if range.removed
        p 'checking removed line range'
        non_deleted_range = SmsHubLineRange.where('replace(range_start, ' ', '', ) = ? AND id != ? AND removed != true',range.range_start.gsub(' ','').first, range.id)
        unless non_deleted_range
          p "Did not found any #{range.range_start}"
          error_array.push(non_deleted_range)
        end
      else
        p 'found live one'
      end
    end
    error_array
  end  
end