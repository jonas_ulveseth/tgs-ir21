class Country < ActiveRecord::Base
	has_one :organisation
	has_many :networks

  def self.update_iso_2(countries)
    countries.each do |country|
      p 'starting to change country'
      country.old_iso_2 = country.iso2_code
      new_country = ISO3166::Country.find_country_by_alpha3(country.iso_code)
      if new_country
        p "new country iso is #{new_country.alpha2} and it was #{country.iso2_code}"
        country.iso2_code = new_country.alpha2
        if country.save
          p 'country saved'
        end
      else
        p 'no match was found'     
      end
    end
  end
end
