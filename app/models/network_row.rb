class NetworkRow < ActiveRecord::Base
	has_paper_trail meta: {
		network_id: :network_id
	}, ignore: [:updated_at]
	belongs_to :network
	belongs_to :network_sections
	#store_accessor *NetworkSectionField.all.map{|ns| ns.name.to_sym}
	store_accessor :data, :range_start, :range_stop

def self.hstore_keys
	keys = []
	self.select(:data).each do |opt|
		next unless opt.data
		a = opt.data.keys
		a.map! {|v| v.gsub(' ', '_') }
		keys << a
	end
	keys.flatten.uniq
end
end
