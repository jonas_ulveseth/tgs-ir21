class Export < ActiveRecord::Base
	store_accessor :test
	belongs_to :export_type
	require 'csv'  

	def self.to_csv(options = {})
		CSV.generate(options) do |csv|
			csv << column_names
			all.each do |network|
				csv << network.attributes.values_at(*column_names)
			end
		end
	end

	def export_hermes_file
		#delete all previous sms hub
		SmsHub.all.delete_all
		SmsHubLineRange.all.delete_all
		p 'start looping'
		incoming_file = "#{Rails.root}/data/upload/hermes_sorted_line_ranges.csv"
		p 'have read it'
		csv_text = File.read(incoming_file, encoding: 'WINDOWS-1252')
		csv_text.gsub(';;', ';')
		csv_import = CSV.parse(csv_text, :quote_char => "|", col_sep: ';')[1 .. -1]
		csv_import.each do |row|
			row.each do |col|
				if col
					if col.length < 5
						if col.scan(/\D/).empty?
              p 'adding a sms hub'
              if @long_imsi
                long_imsi = @long_imsi.join(';').gsub('/', '').gsub('"','').gsub('--','-')
                p long_imsi
                imsi_split = long_imsi.split('-')
                if imsi_split.count > 2
                  final_imsi = long_imsi.gsub(' ', '')
                  unless @sms_hub.imsi
                    @sms_hub.imsi = final_imsi
                    @sms_hub.save
                  end
                end
                unless @sms_hub.imsi
                  @sms_hub.update!(imsi: @long_imsi.to_s.gsub('[','').gsub(']','').gsub('"','').gsub('--','-'))
                end
              end
              network = Network.find_by(rosi_id: col.to_i)
              if network
                p 'adding a matched'
                @sms_hub = SmsHub.create(
                  network_id: network.try(:id), 
                  rosi_id: col.to_i, 
                  name: row[2], 
                  country_name: network.try(:country).try(:name), 
                  country_id: network.try(:country).try(:id),
                  imported_from: 'IR21 - matched')
              else
                p 'adding unmatched one'
                p "#{row[4]}"
                country = Country.find_by(name: row[2])
                if row[4]
                  imsi = row[4].gsub('"','').gsub('--','-')
                end
                @sms_hub = SmsHub.create( 
                  rosi_id: col.to_i, 
                  name: row[2], 
                  country_name: row[1],
                  imsi: imsi,
                  country_id: country.try(:id),
                  imported_from: 'File - unmatched')  
              end
              # Add inline line range
              if row[3]
                lr = row[3].split(' ')
                SmsHubLineRange.create!(
                  range_start: lr.first.gsub('"',''),
                  range_stop: lr.last.gsub('"',''),
                  sms_hub_id: @sms_hub.id,
                  network_id: @sms_hub.network_id,
                  message: 'IR21 - matched'
                  )
              end
            end
          end

          if col.length > 15
    				## Aggregate line ranges
    				p 'LONG'
    				lr = col.split(' ')
            if lr.first.scan(/\D/).empty?
              @long_imsi = row.drop(1)
              if @sms_hub
               SmsHubLineRange.create!(
                range_start: lr.first.gsub('"',''),
                range_stop: lr.last.gsub('"',''),
                sms_hub_id: @sms_hub.id,
                network_id: @sms_hub.network_id,
                message: 'IR21 - matched'
                )
             end
           end
           p col
         end
       end
     end
   end
 end

 def self.export_sor_csv
  require 'base64'
  outgoing_file = "#{Rails.root}/data/export/sor_export-#{Time.now}.csv"
  data = CSV.open(outgoing_file, "wb", col_sep: ',', force_quotes: false) do |csv_export_row|
    csv_export_row << ['Network ID','Network Name', 'TAP Code', 'Country Name','Country ID', 'MCC', 'MNC', 'CC', 'NC', 'CCNDC']
    Network.where('organisation_id IS NOT NULL').select('id, organisation_id, display_name, network_name, tadig_code, routing, country_id').each do |network|
      organisation_id = Organisation.find_by(id: network.organisation_id).id
      organisation_name = Organisation.find_by(id: network.organisation_id).try(:name).gsub('?', '').gsub('"','').gsub('/','').gsub(',',' ').gsub('  ', ' ')
      display_name    = ActiveSupport::Inflector.transliterate(network.display_name).gsub('?', '').gsub('"','').gsub('/','').gsub(',',' ').gsub('  ', ' ').gsub(/[^\w\s_.\-\']/,'')
      if network.network_name
        network_name    = ActiveSupport::Inflector.transliterate(network.network_name).gsub('?', '').gsub('"','').gsub('/','').gsub(',',' ').gsub('  ', ' ').gsub(':','').gsub(/[^\w\s_.\-\']/,'')
      else
        network_name    = ''
      end
      tadig           = network.tadig_code
      if network.country
        country_name    = network.country.try(:name).gsub('?', '').gsub('"','').gsub('/','').gsub(',',' ').gsub('  ', ' ').gsub(':','').gsub(/[^\w\s_.\-\']/,'')
      else
        country_name    = ''
      end
      iso_2_code      = network.country.try(:iso2_code)
      iso_code        = network.country.try(:iso_code)
      if network.routing
        mcc = network.routing['e212_mcc'].to_s.gsub(' ', '').gsub('N/A','').gsub('/',';').gsub(',',';').gsub('-','')
        next if mcc.blank?
        next unless validate(mcc)
        mnc = network.routing['e212_mnc'].to_s.gsub(' ', '').gsub('N/A','').gsub('/',';').gsub(',',';').gsub('-','')
        next if mnc.blank?
        next unless validate(mnc)
        cc  = network.routing['e214_cc'].to_s.gsub(' ', '').gsub('N/A','').gsub('/',';').gsub(',',';').gsub('-','')
        next if cc.blank?
        next unless validate(cc)
        nc  = network.routing['e214_nc'].to_s.gsub(' ', '').gsub('N/A','').gsub('/',';').gsub(',',';').gsub('-','')
        next if nc.blank?
        next unless validate(nc)
      end
      gt_ranges = NetworkRow.where("network_section_id = 167 AND network_id = ?", network.id).all
      gt_range = gt_ranges.map {|gt| ["#{gt.data['GT - cc']}#{gt.data['GT - ndc']}"] }      
      p "gt_range is #{gt_range}"
      csv_export_row << [organisation_id,display_name, tadig, country_name, iso_2_code, mcc, mnc, cc, nc, gt_range.join(';').to_s]
    end
  end
  @export = Export.new
  @export.data = nil
  @export.user_id = 99
  @export.export_type_id = 9
  @export.file = outgoing_file.split('/').last
  @export.in_progress = false
  @export.save
end



def self.export_advanced_sor(export)
  require 'base64'
  outgoing_file = "#{Rails.root}/data/export/sor_export_advanced-#{Time.now}.csv"
  SorGtLineRange.delete_all
  data = CSV.open(outgoing_file, "wb", col_sep: ',', force_quotes: false) do |csv_export_row|
    csv_export_row << ['Network ID','Network Name', 'TAP Code', 'Country Name','Country ID', 'MCC', 'MNC', 'CC', 'NC', 'CCNDC']
    Network.where('organisation_id IS NOT NULL').select('id, organisation_id, display_name, network_name, tadig_code, routing, country_id').each do |network|
      organisation_id = Organisation.find_by(id: network.organisation_id).id
      organisation_name = Organisation.find_by(id: network.organisation_id).try(:name).gsub('?', '').gsub('"','').gsub('/','').gsub(',',' ').gsub('  ', ' ')
      display_name    = ActiveSupport::Inflector.transliterate(network.display_name).gsub('?', '').gsub('"','').gsub('/','').gsub(',',' ').gsub('  ', ' ').gsub(/[^\w\s_.\-\']/,'')
      if network.network_name
        network_name    = ActiveSupport::Inflector.transliterate(network.network_name).gsub('?', '').gsub('"','').gsub('/','').gsub(',',' ').gsub('  ', ' ').gsub(':','').gsub(/[^\w\s_.\-\']/,'')
      else
        network_name    = ''
      end
      tadig           = network.tadig_code
      if network.country
        country_name    = network.country.try(:name).gsub('?', '').gsub('"','').gsub('/','').gsub(',',' ').gsub('  ', ' ').gsub(':','').gsub(/[^\w\s_.\-\']/,'')
      else
        country_name    = ''
      end
      iso_2_code      = network.country.try(:iso2_code)
      iso_code        = network.country.try(:iso_code)
      if network.routing
        mcc = network.routing['e212_mcc'].to_s.gsub(' ', '').gsub('N/A','').gsub('/',';').gsub(',',';').gsub('-','')
        next if mcc.blank?
        next unless validate(mcc)
        mnc = network.routing['e212_mnc'].to_s.gsub(' ', '').gsub('N/A','').gsub('/',';').gsub(',',';').gsub('-','')
        next if mnc.blank?
        next unless validate(mnc)
        cc  = network.routing['e214_cc'].to_s.gsub(' ', '').gsub('N/A','').gsub('/',';').gsub(',',';').gsub('-','')
        next if cc.blank?
        next unless validate(cc)
        nc  = network.routing['e214_nc'].to_s.gsub(' ', '').gsub('N/A','').gsub('/',';').gsub(',',';').gsub('-','')
        next if nc.blank?
        next unless validate(nc)
      end
      p 'recreating gt ranges'
      SorGtLineRange.recreate(network_id: network.id)
      gt_ranges = SorGtLineRange.where("network_id = ?", network.id).all
      gt_range = gt_ranges.map {|gt| ["#{gt.range_start}"] }      
      p "gt_range is #{gt_range}"
      csv_export_row << [organisation_id,display_name, tadig, country_name, iso_2_code, mcc, mnc, cc, nc, gt_range.sort.join('|').to_s]
    end
  end
  @export = export
  @export.data = nil
  @export.user_id = 99
  @export.export_type_id = 11
  @export.file = outgoing_file.split('/').last
  @export.in_progress = false
  @export.save
end

def export_sccp_ip
  outgoing_file = "#{Rails.root}/data/export/lte_sccp_export-#{Time.now}.csv"  
  data = CSV.open(outgoing_file, "wb", col_sep: ';', force_quotes: false) do |csv_export_row|
    csv_export_row << ['GSMRoSiSCCPprovider_ID','SCCPproviderName', 'SCCP_IP_Range','Country', 'SCCP_Hostname']
    LteSccpIp.all.each do |sccp|
      sccp_provider_name = sccp.SCCP_provider_Name
      sccp_provider_name.strip! if sccp_provider_name
      country = sccp.Country
      country.strip if country
      ip_range = sccp.SCCP_IP_Range
      if ip_range
        ip_range.gsub! /\t/, ''
      end
      csv_export_row << [sccp.GSMRoSiSCCPprovider_ID,sccp_provider_name, ip_range, country, sccp.SCCP_Hostname]
    end
  end
  @export = Export.new
  @export.data = nil
  @export.user_id = 99
  @export.export_type_id = 10
  @export.file = outgoing_file.split('/').last
  @export.in_progress = false
  @export.save
end

def export_mno_ip
  outgoing_file = "#{Rails.root}/data/export/lte_mno_export-#{Time.now}.csv"
  data = CSV.open(outgoing_file, "wb", col_sep: ';', force_quotes: false) do |csv_export_row|
    csv_export_row << ['SMS number administrationID', 'Country','Carrier', 'MNO_IP_Range']
    LteMnoIp.all.each do |mno|
      country = mno.country
      country.strip! if country
      carrier = mno.carrier
      carrier.strip! if carrier
      csv_export_row << [mno.sms_number_administration_id, country, carrier, mno.MNO_IP_Range]
    end
  end
  @export = Export.new
  @export.data = nil
  @export.user_id = 99
  @export.export_type_id = 10
  @export.file = outgoing_file.split('/').last
  @export.in_progress = false
  @export.save
end

def self.validate(str)
 chars = ('a'..'z').to_a + ('A'..'Z').to_a
 str.chars.detect {|ch| chars.include?(ch)}.nil?
end
end
