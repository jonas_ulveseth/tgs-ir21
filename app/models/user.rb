class User < ActiveRecord::Base
  rolify
  has_many :notify_field_users
  has_many :notify_fields, through: :notify_field_users
  acts_as_authentic do |c|
		c.merge_validates_length_of_password_field_options({:minimum => 4})	
	end



#has_secure_password if Rails.env.beta?
  #
	# @return String fullname user object
  #
	def fullname
		"#{self.first_name} #{self.last_name}"
	end	
end
