class Pdf < ActiveRecord::Base
  has_many :pdf_networks
  has_many :networks, through: :pdf_networks
  belongs_to :import
end
