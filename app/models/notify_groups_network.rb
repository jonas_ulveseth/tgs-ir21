class NotifyGroupsNetwork < ActiveRecord::Base
	belongs_to :network
	has_many :notify_groups
end
