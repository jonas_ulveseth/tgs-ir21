class SorGtLineRange < ActiveRecord::Base
	belongs_to :network


	# Model is used to calculate gt ranges when doing sor export
	def self.recreate(network_id: nil)
		self.where(network_id: network_id).delete_all
		#Loop all networks to add ranges
		p "NAMNET ÄR:::"
    p 'looping and adding' 
		gt_ranges = NetworkRow.where("network_section_id = 167 AND network_id = ?", network_id).all
		gt_ranges.each do |range|
			gt_range_start = "#{range.data['GT - cc']}#{range.data['GT - ndc']}#{range.data['GT - range_start']}"
			gt_range_stop = "#{range.data['GT - cc']}#{range.data['GT - ndc']}#{range.data['GT - range_stop']}"       
			self.create(range_start: gt_range_start, range_stop: gt_range_stop, network_id: network_id)
		end
		p 'first step complete'
		network_gt_ranges = self.where(network_id: network_id)
    #Duplicate fixes and filtration
		network_gt_ranges.each do |range|
      p 'detta är min range'
      p range	
			next unless range.range_stop 
			p 'range stop finns'
      range.large_range_fix
			range.filter_uneven
			range.remove_0_and_9			
		end
    p 'försöker global deletion'
		self.global_deletion
	end

	def large_range_fix
		return unless self.range_start
		combined = self.range_start.split("").zip self.range_stop.split("")
		combined.each_with_index do |c,c_index|
			unless c.first == c.last
				combined = combined.shift(c_index + 1)
			end
		end
		old_start = self.range_start
		old_stop  = self.range_stop
		a, b = combined.transpose
		self.range_start = a.join('')
		self.range_stop = b.join('')
		diffrence = self.range_stop.to_i - self.range_start.to_i
    # If last is 0 and 9 do removal of last digits and save the line range right away
    if self.range_start.last == '0' && self.range_stop.last == '9' && diffrence < 20
    	self.update(range_start: self.range_start[0...-1].strip)
    else
      # Increment ranges, skipt the first
      if diffrence < 950
      	(self.range_start..self.range_stop).each_with_index do |n,i|
      		next if i == 0
      		SorGtLineRange.create(range_start: n.strip, network_id: network_id, message: "new range created from #{old_start} - #{old_stop}")
      	end
      else
      	large_range_fix_2
      end
      self.save
    end
  end

  def large_range_fix_2
  	start_index = self.range_start.index('001')
  	stop_index  = self.range_stop.index('999')
  	diffrence = self.start_range - self.range_stop
  	if start_index == stop_index && diffrence < 950
  		old_start = self.range_start
  		old_stop  = self.range_stop
  		self.range_start.slice!('001')
  		self.range_stop.slice!('999')
  		(self.range_start..self.range_start).each do |n|
  			self.create(range_start: n.strip, network_id: network_id, message: "new range created from #{old_start} - #{old_stop} | originally to big range")
  		end
  	else
      ## Save untreated line range
      #CodeLineRangeIssue.create(code_range_version_id: self.code_range_version.id,message: "To big line range #{self.range_start} - #{self.range_stop}")
    end
  end

  def remove_001
  	if self.range_start
  		start_index = self.range_start.index('001')
  		stop_index  = self.range_stop.index('999')
  		if start_index == stop_index
  			self.range_start.slice!('001')
  			self.range_stop.slice!('999')
  		else
  			if start_index
  				if self.range_stop[start_index..self.range_stop.length] == '999'
  					self.range_stop.slice!(start_index, 3)
  					self.range_start.slice!('001')
  				end
  			end
  		end
  		self.save
  	end 
  end

  def fixing_start_length
  	if self.range_start.length != self.range_stop.length
  		if stop > start
  			uneven_amount = stop.length - start.length
  			equalize_number = "#{stop}%0#{uneven_amount}o" % '0'
  			start = equalize_number
  		else
  			uneven_amount = stop.length - start.length
  			equalize_number = "%0#{uneven_amount}o" % '0'
  			equalize_number = equalize_number.gsub('0','9')
  			stop = equalize_number
  		end
  	end
  	self.save
  end

  # This is the global filtration method used between line ranges but in same organisation
  def filter_line_range
  	return unless self.range_start

    # check against other networks
    # Get all sms hub with same rosi id
    if self.code.try(:rosi_id)
    	if self.code.network
    		same_networks = Network.where(organisation_id: self.code.network.organisation_id).pluck(:id).to_a
    		same_sms_hubs = Code.where("rosi_id = ? OR network_id IN(?)", self.code.rosi_id, same_networks).pluck(:id).to_a
    	else
    		same_sms_hubs = Code.where("rosi_id = ?", self.code.rosi_id).pluck(:id).to_a
    	end 
    	same = CodeRange.where("range_start = ? AND id != ? AND code_id IN(?) AND archived IS NULL", self.range_start, self.id, same_sms_hubs).where(removed: [nil, false]).first
    end
    if same
    	unless same.network
    		same.update_columns(removed: true, message: "duplicate override by line range #{self.range_start}", removed_by_id: self.id, filtered: true, generated_by_step: 3)
    	else
    		p 'it has a network'
    		unless self.network
    			p 'removing by date - own'
    			self.update_columns(removed: true, message: "duplicate override by line range #{same.range_start}", removed_by_id: same.id, generated_by_step: 3)    
    		else  
    			if self.try(:network).try(:xml_creation_date) > same.try(:network).try(:xml_creation_date)
    				p 'removing by date - other'
    				same.update_columns(removed: true, message: "duplicate override by line range #{self.range_start}", removed_by_id: self.id, filtered: true, generated_by_step: 3)
    			else
    				p 'removing by date - own'
    				self.update_columns(removed: true, message: "duplicate override by line range #{same.range_start}", removed_by_id: same.id, generated_by_step: 3)
    			end
    		end
    	end
    else
    	p 'No duplicate found'
    end
  end


  def self.remove_spaces
  	self.where("RIGHT(range_start::varchar, 1) = ' '").each do |range|
  		range.range_start.gsub!(' ', '')
  		range.message = 'removed whitespace'
  		range.save
  	end
  end

  def remove_0_and_9
    # Delete 0 and 9
    p 'ja då tar vi bort nollor'
    return if self.range_start.blank?
    self.range_start = self.range_start.gsub(/0+$/, '')
    self.range_stop = self.range_stop.gsub(/9+$/, '')
    self.save
  end

  def filter_uneven
  	return unless self.range_start && self.range_stop 
  	if self.range_start.length != self.range_stop.length
  		start = self.range_start 
  		stop = self.range_stop
  		if stop > start
  			uneven_amount = stop.length - start.length
  			equalize_number = "#{start}%0#{uneven_amount}o" % '0'
  			start = equalize_number
  		else
  			uneven_amount = start.length - stop.length
  			equalize_number = "%0#{uneven_amount}o" % '0'
  			equalize_number = equalize_number.gsub('0','9')
  			stop = "#{stop}#{equalize_number}"
  			p "Equalize number is #{equalize_number}"
  			p "stop after equalize is #{stop}"
  			p "start after equalize is #{start}"
  		end
  		self.range_start = start
  		self.range_stop = stop 
  		self.save
  	end
  end

  def self.global_deletion
  	line_ranges = SorGtLineRange.order('network_id').all
  	line_ranges.each_with_index do |range, index|
  		p "looping range #{index} of #{line_ranges.size}"
  		range_test = SorGtLineRange.where("range_start = ?", range.range_start).where('id != ?', range.id).all
  		p 'found one' if range_test
  		unless range_test.nil?
  			p 'reducing'
          #get the most recent
          network_based_order = SorGtLineRange.where(range_start: range.range_start).joins(:network).order('networks.xml_creation_date desc').first
          if network_based_order
          	p 'network based deletion'
          	all_ranges = SorGtLineRange.where("range_start = ?", range.range_start).order('network_id').where('id != ?', network_based_order.id).all
          	all_ranges.delete_all
          else
          	p 'non network based deletion'
          	range_test.delete_all
          end
        end
      end
    end
  end
