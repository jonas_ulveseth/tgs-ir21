class Code < ActiveRecord::Base
  has_many :code_ranges, dependent: :destroy
  has_many :code_range_versions, dependent: :destroy
  belongs_to :network

  # Process Networks and add codes
  def self.create_code_line_ranges(networks = nil)
    networks.each_with_index do |network, index|
      # For now we delete all previous versions
      #network.code_range_versions.delete_all
      #network.codes.delete_all
      #network.code_ranges.delete_all

      @code = Code.where(network_id: network.id).first_or_create
      @code.imsi       = "#{network.routing['e212_mcc']}-#{network.routing['e212_mnc']}"
      @code.rosi_id    = network.rosi_id 
      @code.country_id = network.country.try(:id)
      @code.save
      unless @code.imported_from
        @code.imported_from = 'IR21 - unmatched'
        @code.save
      end
      @code_range_version = CodeRangeVersion.create(network_id: network.id, code_id: @code.id, import_id: network.import.try(:id))
      # Set previous coderanges as archived
      network.code_ranges.update_all(archived: true)
      mcc = network.routing['e212_mcc']
      mnc = network.routing['e212_mnc']

      # Store line ranges, and do some basic filtering
      @code.add_line_ranges(@code_range_version)
      @code.add_file_line_ranges(@code_range_version)
      #Start line range filtering process
      @code_range_version.code_ranges.each do |range|
        #next if range.range_start.blank?
        range.remove_001
        # Creating complete line range
        range.remove_0_and_9
        range.filter_uneven
        @start = "#{range.cc}#{range.ndc}#{range.range_start}".strip
        @stop = "#{range.cc}#{range.ndc}#{range.range_stop}".strip
        # Processing done - adding to database logic
        if @start == @stop
          # Step 1
          range.update(range_start: @start, range_stop: @stop, network_id: network.id, code_id: @code.id, original_range_start: @start, original_range_stop: @stop, generated_by_step: 1)
        else
          range.update(range_start: @start, range_stop: @stop, network_id: network.id, code_id: @code.id, original_range_start: @start, original_range_stop: @stop, generated_by_step: 1)
          # Incrementing line range
          # Step 2
          @diffrence = @stop.to_i - @start.to_i
          if @diffrence < 950
            (@start..@stop).each_with_index do |n, index|
              next if index == 0
              CodeRange.create(range_start: n,code_range_version_id: range.code_range_version.id, network_id: network.id, code_id: @code.id, original_range_start: @start, original_range_stop: @stop, generated_by_step: 2, message: 'Incremental line range, created by system')
            end
          else
            range.large_range_fix
          end
        end
      end
      @code.filter_line_ranges
      consume_line_ranges(@code_range_version.code_ranges)
      deconsume_line_ranges(@code)
    end
  end


  def self.global_deletion
    line_ranges = CodeRange.order('code_id').where('removed IS NULL AND archived IS NULL')
    line_ranges.each_with_index do |range, index|
      p "looping range #{index} of #{line_ranges.size}"
      range_test = CodeRange.where("range_start = ? AND removed IS NULL AND archived IS NULL", range.range_start).where('id != ?', range.id).all
      p 'found one' if range_test
      unless range_test.nil?
        p 'reducing'
        # Check so other one isn´t removed
        deletion_test = CodeRange.where(id: range.id, archived: nil).first
        next if range.removed
        unless deletion_test.removed
          #get the most recent
          network_based_order = CodeRange.where(range_start: range.range_start, removed: nil).joins(:code).joins(:network).order('networks.xml_creation_date desc').first
          if network_based_order
            p 'network based deletion'
            all_ranges = CodeRange.where("range_start = ? AND removed IS NULL AND archived IS NULL", range.range_start).order('code_id').where('id != ?', network_based_order.id).all
            all_ranges.update_all(removed: true, message: "Global network deletion from #{network_based_order.range_start} - #{network_based_order.code.imsi}")
          else
            p 'non network based deletion'
            range_test.update_all(removed: true, message: "Global deletion from #{range.range_start} - #{range.code.imsi}")
          end
        end
      end
    end
  end

  def self.check_doubles
    line_ranges = CodeRange.where('removed IS NOT TRUE AND archived IS NULL')
    unmatched_networks_array = []
    crossover_networks_array = []
    same_network_array = []
    line_ranges.each_with_index do |range,index|
      range_test = CodeRange.where('range_start = ? AND removed IS NOT TRUE', range.range_start.gsub(' ', '')).where('id != ?', range.id).where(archived: [nil, false]).first
      if range_test
        if range_test.network && range.network
          same_network_array.push("#{range_test.range_start} #{range.code.imsi}")
        else
          if range_test.network || range.network
            crossover_networks_array.push("#{range_test.range_start} #{range.code.imsi}")  
          else
            unmatched_networks_array.push("#{range_test.range_start} #{range.code.imsi}")  
          end
        end
      end
    end
    [unmatched_networks_array,crossover_networks_array,same_network_array]
  end

  def self.check_double_deleted
    line_ranges = CodeRange.where('message LIKE ?', 'Global deletion from%')
    error_array = []
    line_ranges.each do |line_range|
      live_one = CodeRange.where('range_start = ? AND removed is NULL', line_range.range_start).first
      unless live_one
        error_array.push(line_range.range_start)
      end
    end
    error_array
  end

  def self.unset_global_deleted
    line_ranges = CodeRange.where('message LIKE ?', 'Global deletion from%')
    line_ranges.update_all(removed: nil, message: nil)  
  end

  def self.recreate_line_ranges
    Code.delete_all
    CodeRange.delete_all
    CodeRangeVersion.delete_all
    n = Network.where('organisation_id IS NOT NULL').all
    self.create_code_line_ranges(n)
    self.add_unmatched_networks
    self.create_file(filtration: true)
  end

  def self.create_file(filtration=false, export = nil)
    Code.delete_all
    CodeRange.delete_all
    CodeRangeVersion.delete_all
    n = Network.where('organisation_id IS NOT NULL').all
    self.create_code_line_ranges(n)
    self.add_unmatched_networks
    unless export
      @export = Export.new
    else
      @export = export
    end
    @export.save

    if filtration
      @export.update_columns(progress: '5%')
      self.remove_spaces
      @export.update_columns(progress: '20%')
      self.remove_rosi_id_doubles
      @export.update_columns(progress: '30%')
      self.remove_crossover_lineranges
      @export.update_columns(progress: '40%')
      self.remove_organisation_doubles
      @export.update_columns(progress: '60%')
      self.remove_ten_range
      @export.update_columns(progress: '70%')
      self.consume_line_ranges
      @export.update_columns(progress: '80%')
      self.global_deletion
      @export.update_columns(progress: '90%')
    end
    outgoing_file = "#{Rails.root}/data/export/generated_codes-#{Time.now}.csv"
    data = CSV.open(outgoing_file, "wb", col_sep: ';', force_quotes: false) do |csv_export_row|
      Code.select("DISTINCT ON(rosi_id) *").all.each do |hub|
        # Collect all imsis
        if hub.network
          # If routing is not defined, notify and move on
          unless hub.network.routing
            next
          end
          imsis = Network.where("rosi_id = ? AND organisation_id IS NOT NULL", hub.network.rosi_id)
          if imsis.count == 1
            imsi = "#{imsis.first.routing['e212_mcc',]}-#{imsis.first.routing['e212_mnc']}"
          else
            # When there is multiple Networks with the same rosi_id, create a imsi range separated with semikolon
            array = []
            imsis.each do |i|
              next unless i.routing
              array.push("#{i.routing['e212_mcc',]}-#{i.routing['e212_mnc']}")
            end
            imsi = array.join(';')
          end
        else
          imsis = Code.where("rosi_id = ? AND network_id IS NULL", hub.rosi_id).all
          if imsis.count == 1
            imsi = hub.imsi
          else
            array = []
            imsis.each do |i|
              array.push("#{i.imsi}")
            end
            imsi = array.join(';')
          end
        end

        connected_networks_array = Code.where(rosi_id: hub.rosi_id).pluck(:id).to_a
        lr = CodeRange.where("code_id IN(?) AND archived IS NULL", connected_networks_array).where('removed IS NULL').all
        line_range_map = lr.map { |l| ["#{l.range_start}"] }
        if hub.network
          if hub.country_id
            country = Country.find(hub.country_id)
            country.name.split(' ').first.capitalize
          else
            country = hub
            country.country_name.split(' ').first.capitalize
          end
          csv_export_row << [hub.network.rosi_id, country.name, hub.network.display_name.encode('WINDOWS-1252', {invalid: :replace, undef: :replace, replace: '?'}), line_range_map.sort.join("\n"), imsi, hub.imported_from]
        else
          country_name = hub.country_name.split.map(&:capitalize).join(' ').gsub('Usa','United States')
          csv_export_row << [hub.rosi_id, country_name, hub.name, line_range_map.join("\n"), imsi, hub.imported_from]
        end
      end
    end
    @export.data = nil
    @export.user_id = 99
    @export.export_type_id = 4
    @export.file = outgoing_file.split('/').last
    @export.in_progress = false
    @export.save
    self.transfer_file(@export)
  end

  def self.save_file
    outgoing_file = "#{Rails.root}/data/export/generated_codes-#{Time.now}.csv"
    data = CSV.open(outgoing_file, "wb", col_sep: ';', force_quotes: false) do |csv_export_row|
      Code.select("DISTINCT ON(rosi_id) *").all.each do |hub|
        # Collect all imsis
        p "processing #{hub.imsi}"
        if hub.network_id
          p 'detecting network'
          # If routing is not defined, notify and move on
          unless hub.network.routing
            raise 'no routing'
            next
          end
          imsis = Network.where("rosi_id = ? AND organisation_id IS NOT NULL", hub.network.rosi_id)
          if imsis.count == 1
            imsi = "#{imsis.first.routing['e212_mcc',]}-#{imsis.first.routing['e212_mnc']}"
          else
            # When there is multiple Networks with the same rosi_id, create a imsi range separated with semikolon
            array = []
            imsis.each do |i|
              next unless i.routing
              array.push("#{i.routing['e212_mcc',]}-#{i.routing['e212_mnc']}")
            end
            imsi = array.join(';')
          end
        else
          p 'going for no network'
          imsis = Code.where("rosi_id = ? AND network_id IS NULL", hub.rosi_id).all
          if imsis.count == 1
            imsi = hub.imsi
          else
            array = []
            imsis.each do |i|
              array.push("#{i.imsi}")
            end
            imsi = array.join(';')
          end
        end

        connected_networks_array = Code.where(rosi_id: hub.rosi_id).pluck(:id).to_a
        lr = CodeRange.where("code_id IN(?) AND archived IS NULL", connected_networks_array).where('removed IS NULL').all
        line_range_map = lr.map { |l| ["#{l.range_start}"] }
        if hub.network
          if hub.country_id
            country = Country.find(hub.country_id).name.capitalize
          else
            country = hub.country_name.capitalize
          end
          csv_export_row << [hub.network.rosi_id, country, hub.network.display_name.encode('WINDOWS-1252', {invalid: :replace, undef: :replace, replace: '?'}), line_range_map.sort.join("\n"), imsi, hub.imported_from]
        else
          csv_export_row << [hub.rosi_id, hub.country_name, hub.name, line_range_map.join("\n"), imsi, hub.imported_from]
        end
      end
    end
    @export = Export.new
    @export.data = nil
    @export.user_id = 99
    @export.export_type_id = 4
    @export.file = outgoing_file.split('/').last
    @export.in_progress = false
    @export.save
  end

  def self.transfer_file(export)
    require 'net/ssh'
    require 'net/sftp'
    Net::SFTP.start("transfer.nsc.no", "stinga", password: '3a9B45M5DS392CX',host_key: "ssh-rsa", config: false) do |sftp|
      sftp.upload! "#{Rails.root}/data/export/#{export.file}", "IR21_data.csv"
    end
    Net::SFTP.start("10.219.62.4", "stinga_csv", password: 'UOT44nd6OVQbGshC',host_key: "ssh-rsa", config: false) do |sftp|
      sftp.upload! "#{Rails.root}/data/export/#{export.file}", "IR21_data.csv"
    end
    #Net::SFTP.start("sftp.nsc.no", "stinga_sftp", password: "Ffs-feta01", host_key: "ssh-rsa", config: false) do |sftp|
    #  sftp.upload! "#{Rails.root}/data/export/#{export.file}", "IR21_data.csv"
    #end
  end

  def self.create_single_line_file(filter = false, export = nil)
    unless export
      @export = Export.new
      @export.in_progress = true
    else
      @export = export
    end
    @export.save
    
    if filter
      @export.update_columns(progress: '10%')
      self.remove_spaces
      @export.update_columns(progress: '20%')
      self.remove_rosi_id_doubles
      @export.update_columns(progress: '30%')
      self.remove_crossover_lineranges
      @export.update_columns(progress: '50%')
      self.remove_organisation_doubles
      @export.update_columns(progress: '70%')
      self.remove_ten_range
      @export.update_columns(progress: '85%')
      self.global_deletion
      @export.update_columns(progress: '95%')
    end
    ranges = CodeRange.where(archived: [nil, false], removed: [nil,false]).where("range_start > ''").order('range_start::bigint ASC')
    p = Axlsx::Package.new
    wb = p.workbook
    wb.add_worksheet(:name => "Basic Worksheet") do |sheet|
      p 'looping row'
      sheet.add_row ['Destination name', 'Full code', 'Rate USD', 'Effective date']
      ranges.each_with_index do |range,index|
        p "looping #{index} of #{ranges.size}"
        if range.network
          country_name = ActiveSupport::Inflector.transliterate("#{range.code.network.country.name}").to_s
          display_name = ActiveSupport::Inflector.transliterate("#{range.code.network.display_name}").to_s
        else
          country_name = ActiveSupport::Inflector.transliterate("#{range.code.country_name}").to_s
          display_name = ActiveSupport::Inflector.transliterate("#{range.code.name}").to_s
        end
        sheet.add_row ["#{country_name}-#{display_name}", 
         "#{range.range_start}", 
         "$10", 
         "2019-02-20"], types: [:string, :string, :string, :string]
       end
       s = p.to_stream()
       filename = "generated_codes_single_line_sorted-#{Time.now}.xlsx"
       File.open("#{Rails.root}/data/export/#{filename}", 'w') { |f| f.write(s.read) }
       
       @export.export_type_id = 4
       @export.user_id        = 99
       @export.file           = filename
       @export.in_progress    = false
       @export.save 
     end 
   end

   def add_line_ranges(code_range_version)
    network = code_range_version.network
    section_ids = NetworkSection
    .where(field_name: ['msisdn_number_ranges', 'gt_number_ranges', 'msrn_number_ranges'])
    .pluck(:id).to_a
    network_number_ranges = NetworkRow.where("network_section_id IN(?) AND network_id = ?", section_ids, network.id).all
    network_number_ranges.each do |network_number_range|
      case network_number_range.network_section_id
      when 167
        p '167'
        cc = network_number_range.data['GT - cc']
        ndc = network_number_range.data['GT - ndc']
        start = network_number_range.data['GT - range_start']
        stop = network_number_range.data['GT - range_stop']
        if start.nil?
          start = network_number_range.data['GT_NumberRanges_range_start']
          stop = network_number_range.data['GT_NumberRanges_range_stop']
          cc = network_number_range.data['GT_NumberRanges_cc']
          ndc = network_number_range.data['GT_NumberRanges_ndc']
        end
      when 168
        p '168'
        cc = network_number_range.data['MSRN - cc']
        ndc = network_number_range.data['MSRN - ndc']
        start = network_number_range.data['MSRN - range_start']
        stop = network_number_range.data['MSRN - range_stop']
        if start.nil?
          start = network_number_range.data['MSRN_NumberRanges_range_start']
          stop = network_number_range.data['MSRN_NumberRanges_range_stop']
          cc = network_number_range.data['MSRN_NumberRanges_cc']
          ndc = network_number_range.data['MSRN_NumberRanges_ndc']
        end
      when 169
        p '169'
        cc = network_number_range.data['MSISDN - cc']
        ndc = network_number_range.data['MSISDN - ndc']
        start = network_number_range.data['MSISDN - range_start']
        stop = network_number_range.data['MSISDN - range_stop']
        if start.nil?
          start = network_number_range.data['MSISDN_NumberRanges_range_start']
          stop = network_number_range.data['MSISDN_NumberRanges_range_stop']
          cc = network_number_range.data['"MSISDN_NumberRanges_cc']
          ndc = network_number_range.data['MSISDN_NumberRanges_ndc']
        end
      else
        CodeLineRangeIssue.create(code_range_version_id: code_range_version_id, message: "No line range detected, possible blank line")
      end
      cr = CodeRange.create(range_start: start, code_id: self.id, range_stop: stop, network_id: network.id, original_range_start: start, original_range_stop: stop, code_range_version_id: code_range_version.id, cc: cc, ndc: ndc, source: 2)
    end
  end

  # Add line ranges in file, and mark network as matched if it exists both in file and IR21
  def add_file_line_ranges(code_range_version)
    incoming_file = "#{Rails.root}/data/upload/code_base_file.csv"
    csv_text = File.read(incoming_file, encoding: "ISO8859-1:utf-8")
    csv_import = CSV.parse(csv_text, :quote_char => '"', :col_sep => ";", :row_sep => :auto, encoding: "ISO-8859")[1 .. -1]
    @line_ranges_added = nil
    csv_import.each do |csv_import_row|
      @mnc_mcc = csv_import_row[4]
      @country = csv_import_row[1]
      @rosi_id = csv_import_row[0]
      @network = csv_import_row[2]
      @line_ranges = csv_import_row[3]

      next unless @mnc_mcc

      p csv_import_row
      p @line_ranges
      ############################
      ## More than one network ####
      if @mnc_mcc.split(';').count > 1
        @mnc_mcc.split(';').each_with_index do |mnc_mcc, i|
          mcc = mnc_mcc.split('-').first.strip
          mnc = mnc_mcc.split('-').last.strip
          p @mnc_mcc
          p mcc
          p mnc
          # Find network
          network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ? AND organisation_id IS NOT NULL AND id = ?", mcc, mnc, self.network.id).first
          unless network
            network = Network.where(rosi_id: @rosi_id).where('organisation_id IS NOT NULL AND id = ?', self.network.id).first
          end
          if network
            self.update(imported_from: 'IR21 - matched')
            # Adding file based line ranges
            unless @line_ranges.nil?
              @line_ranges.split("\r\n").each do |range|
                p "range is #{range}"
                check_existing = CodeRange.joins(:code).where(range_start: range.split(' ').first, code_range_version_id: code_range_version.id, archived: nil).where('codes.rosi_id = ?', @rosi_id).first
                unless check_existing
                  p 'adding existing'
                  CodeRange.create(range_start: range.split(' ').first, range_stop: range.split(' ').last, code_id: self.id, source: 1, network_id: self.network.id, code_range_version_id: code_range_version.id)
                end
              end
              @line_ranges_added = true
            end
            #end  
          else
            # Do nothing
          end
        end
        @line_ranges_added = nil
      else

        #########################
        ## One network #########
        mcc = @mnc_mcc.split('-').first.strip
        mnc = @mnc_mcc.split('-').last.strip
        p mcc
        p mnc
        # Find network
        network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ? AND organisation_id IS NOT NULL AND id = ?", mcc, mnc, self.network.id).first
        unless network
          network = Network.where(rosi_id: @rosi_id).where('organisation_id IS NOT NULL AND id = ?', self.network.id).first
        end
        if network
          sms_hub = self.update(imported_from: 'IR21 - matched')
          unless @line_ranges.nil?
            @line_ranges.split("\r\n").each do |range|
              check_existing = CodeRange.joins(:code).where(range_start: range.split(' ').first, code_range_version_id: code_range_version.id, archived: nil).where('codes.rosi_id = ?', @rosi_id).first
              unless check_existing
                CodeRange.create(range_start: range.split(' ').first, range_stop: range.split(' ').last, code_id: self.id, source: 1, network_id: self.network.id, code_range_version_id: code_range_version.id)
              end
            end
          end
        else
          # Do nothing
        end
      end
      @unmatched_line_ranges_added = nil
    end
  end

  def self.add_unmatched_networks
    CodeRange.where(network_id: nil).update_all(archived: :true)
    incoming_file = "#{Rails.root}/data/upload/code_base_file.csv"
    csv_text = File.read(incoming_file, encoding: "ISO8859-1:utf-8")
    csv_import = CSV.parse(csv_text, :quote_char => '"', :col_sep => ";", :row_sep => :auto, encoding: "ISO-8859")[1 .. -1]
    @line_ranges_added = nil
    csv_import.each do |csv_import_row|
      @mnc_mcc = csv_import_row[4]
      @country = csv_import_row[1]
      @rosi_id = csv_import_row[0]
      @network = csv_import_row[2]
      @line_ranges = csv_import_row[3]

      next unless @mnc_mcc

      p csv_import_row
      p @line_ranges
      ############################
      ## More than one network ####
      if @mnc_mcc.split(';').count > 1
        @mnc_mcc.split(';').each_with_index do |mnc_mcc, i|
          mcc = mnc_mcc.split('-').first.strip
          mnc = mnc_mcc.split('-').last.strip
          p @mnc_mcc
          p mcc
          p mnc
          # Find network
          network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ? AND organisation_id IS NOT NULL", mcc, mnc).first
          if network
            # Nothing
          else
            same_code_check = Code.where("rosi_id = ? AND network_id IS NOT NULL", @rosi_id).first
            unless same_code_check
              code = Code.where(name: @network,
                country_name: @country,
                imsi: mnc_mcc.strip,
                imported_from: 'File unmatched',
                rosi_id: @rosi_id).first_or_create

              if @line_ranges_added.nil?
                # Line ranges
                unless @line_ranges.nil?
                  @line_ranges.split("\r\n").each do |range|
                    range = CodeRange.create(range_start: range.split(' ')
                     .first, range_stop: range.split(' ')
                     .last, code_id: code.id, source: 1, original_range_start: range.split(' ')
                     .first, original_range_stop: range.split(' ').last)
                    range.remove_001
                    range.remove_0_and_9
                    range.filter_uneven
                    range.large_range_fix
                    range.save
                  end
                  consume_line_ranges(code.code_ranges)
                  @line_ranges_added = true
                end
              end
            end
          end
        end
        @line_ranges_added = nil
      else
        #########################
        ## One network #########
        mcc = @mnc_mcc.split('-').first.strip
        mnc = @mnc_mcc.split('-').last.strip
        p mcc
        p mnc
        # Find network
        network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ? AND organisation_id IS NOT NULL", mcc, mnc).first
        if network
          # Nothing
        else
          same_code_check = Code.where("rosi_id = ? AND network_id IS NOT NULL", @rosi_id).first
          unless same_code_check
            code = Code.where(name: @network,
              country_name: @country,
              imsi: @mnc_mcc,
              imported_from: 'File unmatched',
              rosi_id: @rosi_id).first_or_create
            # Line ranges
            unless @line_ranges.nil?
              @line_ranges.split("\r\n").each do |range|
                range = CodeRange.create(range_start: range.split(' ').first, range_stop: range.split(' ').last, code_id: code.id, source: 1, original_range_start: range.split(' ').first, original_range_stop: range.split(' ').last)
                range.remove_001
                range.remove_0_and_9
                range.filter_uneven
                range.large_range_fix
                range.save
              end
              consume_line_ranges(code.code_ranges)
            end
          end
        end
      end
      @unmatched_line_ranges_added = nil
    end
  end

  ####################################################
  ## Filtering section 
  ####################################################
  # Basic filtering inside organisation
  def filter_line_ranges
    self.code_ranges.each do |range|
      range.filter_line_range
    end
  end

  # Reduce all 0-9 line ranges to 0
  def self.remove_ten_range
    coderanges = CodeRange.where('range_start LIKE ? AND archived IS NULL', '%0').where('removed IS NULL')
    coderanges.each_with_index do |range, index|
      p "looping a candidate - #{range}"
      range.remove_ten_range
    end
    CodeRange.where('range_start LIKE ? AND archived IS NULL', '%0').where('removed IS NULL').each do |range|
      codes = Code.where(rosi_id: range.code.rosi_id).pluck(:id)
      check = CodeRange.where(range_start: range.range_start.to_i+1..range.range_start.to_i + 9).where("code_id IN(?) AND archived IS NULL", codes.to_a).where('removed IS NULL').all
      if check.size == 9 && range.range_start.last == '0'
        p 'calling remove ten again'
        self.remove_ten_range
      end
    end
  end

  def self.check_ren_range_possibilities
    CodeRange.where('range_start LIKE ? AND archived IS NULL', '%0').where('removed IS NULL').each do |range|
      codes = Code.where(rosi_id: range.code.rosi_id).pluck(:id)
      check = CodeRange.where(range_start: range.range_start.to_i+1..range.range_start.to_i + 9).where("code_id IN(?) AND archived IS NULL", codes.to_a).where('removed IS NULL').all
      if check.size == 9 && range.range_start.last == '0'
        p "there is a range :: #{check.pluck(:range_start)}"
      end
    end
  end

  def self.remove_organisation_doubles
    doubles = CodeRange.joins({:code => {:network => :organisation}}).where(range_start: CodeRange.select(:range_start).group(:range_start, :removed).having("count(*) > 1").select(:range_start)).where("codes.network_id IS NOT NULL AND archived IS NULL").where(removed: [nil, false], filtered: false)
    doubles.each_with_index do |double, index|
      duplicate = CodeRange.joins({:code => {:network => :organisation}})
      .where("range_start = ? AND code_ranges.id != ? AND codes.network_id IS NOT NULL AND organisations.id != ? AND archived IS NULL", double.range_start, double.id, double.network.try(:organisation).try(:id))
      .where(removed: [nil, false])
      .order("networks.xml_creation_date")
      next if duplicate.blank?
      if duplicate.size < 2
        own_xml_creation_date = double.code.network.xml_creation_date
        other_xml_creation_date = duplicate.first.code.network.xml_creation_date
        if own_xml_creation_date > other_xml_creation_date
          duplicate.first.update_columns(removed: true, message: "Removed because of cc duplicate with #{double.range_start} (#{double.code.network.imsi}) ")
        else
          double.update_columns(removed: true, message: "Removed because of cc duplicate with #{duplicate.first.range_start} (#{duplicate.first.code.network.imsi})")
        end
      else
        most_recent = duplicate.last
        duplicate.where("code_ranges.id != ? AND archived IS NULL", most_recent.id).update_all(removed: true, message: "Removed because of cc duplicate with #{most_recent.range_start} (#{most_recent.code.imsi})")
      end
      double.update_columns(filtered: true)
    end
  end

  # Remove rosi id doubles
  def self.remove_rosi_id_doubles
    doubles = CodeRange.where(archived: [nil,false]).joins(:code)
    .where(range_start: CodeRange.select(:range_start)
      .group(:range_start)
      .having("count(*) > 1")
      .select(:range_start))
    .where('removed IS NULL')
    .where("archived IS NULL")

    doubles.each_with_index do |double, index|
      p "processing #{index} of doubles.size"
      p "range start is #{double.range_start}"
      duplicate = CodeRange.joins(:code)
      .where('range_start = ? AND codes.rosi_id = ?', double.range_start, double.code.rosi_id)
      .where('code_ranges.id != ?', double.id)
      .where('codes.rosi_id IS NOT NULL AND archived IS NULL')
      .where('removed IS NULL')
      .first
      next unless duplicate
      if CodeRange.find(duplicate.id).removed || CodeRange.find(double.id).removed
        p 'one of them is deleted'
        p "#{duplicate.range_start} -  #{double.range_start}"
        p "duplicate = #{CodeRange.find(duplicate.id).removed}" 
        p "double = #{CodeRange.find(double.id).removed}"
        next
      end
      if duplicate.code.network_id && double.code.network_id
        own_xml_creation_date = double.code.network.xml_creation_date
        other_xml_creation_date = duplicate.code.network.xml_creation_date
        if own_xml_creation_date > other_xml_creation_date
          duplicate.update_columns(removed: true, message: "Removed because of rosi duplicate with 1 #{double.range_start} (#{double.code.imsi}) ", generated_by_step: 5)
        else
          double.update_columns(removed: true, message: "Removed because of rosi duplicate with 2 #{duplicate.range_start} (#{duplicate.code.imsi})", generated_by_step: 5)
        end
        next
      end
      if duplicate.network_id
        double.update_columns(removed: true, message: "Removed because of rosi duplicate with 3 #{duplicate.range_start} (#{duplicate.code.imsi}) ", generated_by_step: 5)
        next
      end
      if double.network_id
        duplicate.update_columns(removed: true, message: "Removed because of rosi duplicate with 4 #{double.range_start} (#{double.code.imsi}) ", generated_by_step: 5)
        next
      end
      duplicate.update_columns(removed: true, message: "Removed because of rosi duplicate with file based #{double.range_start} (#{double.code.imsi}) ", generated_by_step: 5)
      # Delete more copies
      more_copies = CodeRange.where(range_start: duplicate.range_start).where('network_id IS NULL').where.not(id: double.id).update_all(removed: true, message: 'deleted by global rosi id selection')
    end
  end

  def self.remove_spaces
    CodeRange.where("RIGHT(range_start::varchar, 1) = ' '").each do |range|
      range.range_start.gsub!(' ', '')
      range.message = 'removed whitespace'
      range.save
    end
  end

  # Find bigger ranges
  def self.consume_line_ranges(ranges=nil)
    p 'runnning consume'
    unless ranges
      ranges = CodeRange.where('archived IS NOT TRUE').where('removed IS NOT TRUE')
    end
    ranges.each_with_index do |range,index|
      p "handeling #{range.range_start}"
      next if range.range_start.nil?
      next if range.range_start.blank?
      bigger_line_range = CodeRange.joins(:code).where("length(range_start) > ? AND range_start LIKE ? AND codes.rosi_id = ? AND code_ranges.id != ?", "#{range.range_start.try(:length)}", "#{range.range_start}%", range.code.rosi_id, range.id).where('archived IS NOT TRUE').where('removed IS NOT TRUE').all
      unless bigger_line_range.blank?
        p "removing #{bigger_line_range.first.range_start}"
        bigger_line_range.update_all(removed: true, message: "line range covered by #{range.range_start}", filtered: true, generated_by_step: 3, removed_by_id: range.id)
      end
    end
  end

  # Same as consume, but find smaller ranges
  def self.deconsume_line_ranges(code)
    p 'running deconsume'
    imported_code_ranges = code.code_range_versions.last.code_ranges
    ranges = CodeRange.joins(:code).where.not(archived: true).where.not(removed: true).where('codes.rosi_id = ? AND code_id != ?',code.rosi_id, code.id).all
    ranges.each do |range|
      bigger_line_range = imported_code_ranges.where('length(range_start) > ? AND range_start LIKE ?', "#{range.range_start.try(:length)}", "#{range.range_start}%").all
      unless bigger_line_range.blank?
        bigger_line_range.update_all(removed: true, message: "line range covered by #{range.range_start}", filtered: true, generated_by_step: 3, removed_by_id: range.id)
      end
    end
  end

  def self.set_post_filtered_range_start
    ranges = CodeRange.all.each_with_index do |range,index|
      p "processing #{index} of #{ranges.length}"
      range.update_columns(original_range_start: range.range_start)
    end
  end

  # Function to remove line range between file and IR21 line ranges
  def self.remove_crossover_lineranges
    doubles = CodeRange
    .where(archived: nil, range_start: CodeRange.select(:range_start)
      .select(:range_start)
      .where('network_id IS NULL'))
    .where(removed: [nil,false]) 
    doubles.each_with_index do |double, index|
      duplicate = CodeRange
      .where('range_start = ?', double.range_start)
      .where('id != ?', double.id)
      .where("network_id IS NOT NULL AND archived IS NULL")
      .where(removed: [nil, false])
      .first
      if duplicate
        double.update_columns(removed: true, message: "Removed because of duplicate with a IR21 line range (#{double.code.imsi})")
      end
    end
  end
end