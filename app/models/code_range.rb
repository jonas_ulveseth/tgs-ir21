class CodeRange < ActiveRecord::Base
  belongs_to :code
  belongs_to :code_range_version
  belongs_to :network

  def large_range_fix
    return unless self.range_start
    p "range (#{self.id}) start::#{self.range_start} -- #{self.range_stop}"
    combined = self.range_start.split("").zip self.range_stop.split("")
    combined.each_with_index do |c,c_index|
      p 'started looping combined'
      p c
      unless c.first == c.last
        combined = combined.shift(c_index + 1)
      end
    end
    old_start = self.range_start
    old_stop  = self.range_stop
    a, b = combined.transpose
    self.range_start = a.join('')
    self.range_stop = b.join('')
    diffrence = self.range_stop.to_i - self.range_start.to_i
    # If last is 0 and 9 do removal of last digits and save the line range right away
    if self.range_start.last == '0' && self.range_stop.last == '9' && diffrence < 20
      self.update(range_start: self.range_start[0...-1].strip)
    else
      # Increment ranges, skipt the first
      if diffrence < 950
        p "#{self.range_start} - #{self.range_stop}"
        p "was before #{old_start} - #{old_stop}"
        (self.range_start..self.range_stop).each_with_index do |n,i|
          next if i == 0
          if network
            CodeRange.create(range_start: n.strip, network_id: network.id, code_id: self.code_id, message: "new range created from #{old_start} - #{old_stop} | originally to big range", code_range_version_id: self.code_range_version_id, original_range_start: n.strip)
          else
            CodeRange.create(range_start: n.strip, code_id: self.code_id, message: "new range created from #{old_start} - #{old_stop}", code_range_version_id: self.code_range_version_id, original_range_start: n.strip)
          end
        end
      else
        large_range_fix_2
      end
      self.save
    end
  end

  def large_range_fix_2
    p 'slice 001 and 999 if line range is to big'
    start_index = self.range_start.index('001')
    stop_index  = self.range_stop.index('999')
    diffrence = self.start_range - self.range_stop
    if start_index == stop_index && diffrence < 950
      old_start = self.range_start
      old_stop  = self.range_stop
      self.range_start.slice!('001')
      self.range_stop.slice!('999')
      (self.range_start..self.range_start).each do |n|
        p "removed 999"
        p "start is #{self.range_start} stop is #{self.range_stop}"
        CodeRange.create(range_start: n.strip, network_id: network.id, code_id: self.code.id, message: "new range created from #{old_start} - #{old_stop} | originally to big range", code_range_version_id: self.code_range_version_id)
      end
    else
      ## Save untreated line range
      CodeLineRangeIssue.create(code_range_version_id: self.code_range_version.id,message: "To big line range #{self.range_start} - #{self.range_stop}")
    end
  end

  def remove_001
    if self.range_start
      start_index = self.range_start.index('001')
      stop_index  = self.range_stop.index('999')
      if start_index == stop_index
        self.range_start.slice!('001')
        self.range_stop.slice!('999')
        p 'Modified 009 and 001'
        p "they are now #{self.range_start} - #{self.range_stop}"
      else
        if start_index
          p "filtration by index"
          p "the result is #{self.range_stop[start_index..self.range_stop.length]}"
          if self.range_stop[start_index..self.range_stop.length] == '999'
            p 'filtration worked'
            self.range_stop.slice!(start_index, 3)
            self.range_start.slice!('001')
          end
        end
      end
      self.save
    end 
  end

  def fixing_start_length
    if self.range_start.length != self.range_stop.length
      p "FIXING LENGTH"
      p "start before fixing #{start}"
      p "stop before fixing #{stop}"
      if stop > start
        uneven_amount = stop.length - start.length
        equalize_number = "#{stop}%0#{uneven_amount}o" % '0'
        start = equalize_number
        p "Equalize number is #{equalize_number}"
        p "stop after equalize is #{stop}"
        p "start after equalize is #{start}"
      else
        uneven_amount = stop.length - start.length
        equalize_number = "%0#{uneven_amount}o" % '0'
        equalize_number = equalize_number.gsub('0','9')
        stop = equalize_number
        p "Equalize number is #{equalize_number}"
        p "stop after equalize is #{stop}"
        p "start after equalize is #{start}"
      end
    end
    self.save
  end

  # This is the global filtration method used between line ranges but in same organisation
  def filter_line_range
    p "deleting higher numbers #{self.range_start}"
    return unless self.range_start

    # check against other networks
    # Get all sms hub with same rosi id
    if self.code.try(:rosi_id)
      p "checking for same - #{self.range_start}"
      if self.code.network
        same_networks = Network.where(organisation_id: self.code.network.organisation_id).pluck(:id).to_a
        same_sms_hubs = Code.where("rosi_id = ? OR network_id IN(?)", self.code.rosi_id, same_networks).pluck(:id).to_a
      else
        same_sms_hubs = Code.where("rosi_id = ?", self.code.rosi_id).pluck(:id).to_a
      end 
      same = CodeRange.where("range_start = ? AND id != ? AND code_id IN(?) AND archived IS NULL", self.range_start, self.id, same_sms_hubs).where(removed: [nil, false]).first
    end
    if same
      p 'FOUND SAME'
      unless same.network
        p 'Setting the other network as deleted'
        same.update_columns(removed: true, message: "duplicate override by line range #{self.range_start}", removed_by_id: self.id, filtered: true, generated_by_step: 3)
      else
        p 'it has a network'
        unless self.network
          p 'removing by date - own'
          self.update_columns(removed: true, message: "duplicate override by line range #{same.range_start}", removed_by_id: same.id, generated_by_step: 3)    
        else  
          if self.try(:network).try(:xml_creation_date) > same.try(:network).try(:xml_creation_date)
            p 'removing by date - other'
            same.update_columns(removed: true, message: "duplicate override by line range #{self.range_start}", removed_by_id: self.id, filtered: true, generated_by_step: 3)
          else
            p 'removing by date - own'
            self.update_columns(removed: true, message: "duplicate override by line range #{same.range_start}", removed_by_id: same.id, generated_by_step: 3)
          end
        end
      end
    else
      p 'No duplicate found'
    end
  end


  ############################################
  ## Global filtration
  ############################################
  def self.remove_cc_doubles(range_start = nil)
    #Run this in console to keep track of updated records in case of interuption
    #SmsHubLineRange.all.update_all(filtered: false)
    if range_start
      doubles = CodeRange.where(range_start: range_start)
    else
      doubles = CodeRange.joins(:sms_hub).where(range_start: SmsHubLineRange.select(:range_start).group(:range_start, :removed).having("count(*) > 1").select(:range_start)).where("sms_hubs.network_id IS NOT NULL").where(removed: [nil, false], filtered: false)
    end
    p doubles.size
    doubles.each_with_index do |double, index|
      p "processing #{index} of #{doubles.length}"
      duplicate = CodeRange.joins({:sms_hub => {:network => :organisation}})
      .where("range_start = ? AND sms_hub_line_ranges.id != ? AND sms_hubs.network_id IS NOT NULL AND organisations.id != ? AND archived IS NULL", double.range_start, double.id, double.network.try(:organisation).try(:id))
      .where(removed: [nil, false])
      .order("networks.xml_creation_date")
      next if duplicate.blank?
      if duplicate.size < 2
        p 'one duplicate'
        own_xml_creation_date = double.sms_hub.network.xml_creation_date
        other_xml_creation_date = duplicate.first.sms_hub.network.xml_creation_date
        if own_xml_creation_date > other_xml_creation_date
          p 'duplicate removed'
          duplicate.first.update_columns(removed: true, message: "Removed because of cc duplicate with #{double.range_start} (#{double.sms_hub.imsi}) ")
        else
          p 'own line range removed'
          double.update_columns(removed: true, message: "Removed because of cc duplicate with #{duplicate.first.range_start} (#{duplicate.first.sms_hub.imsi})")
        end
      else
        p 'more duplicates'
        p duplicate.size
        most_recent = duplicate.last
        duplicate.where("code_ranges.id != ?", most_recent.id).update_all(removed: true, message: "Removed because of cc duplicate with #{most_recent.range_start} (#{most_recent.sms_hub.imsi})")
      end
      double.update_columns(filtered: true)
    end
  end

  def remove_ten_range
    if self.range_start.last == '0'
      codes = Code.where(rosi_id: self.code.rosi_id).pluck(:id)
      check = CodeRange.where(range_start: self.range_start.to_i+1..self.range_start.to_i + 9).where("code_id IN(?) AND archived IS NULL", codes.to_a).where(removed: [false, nil]).all

      if check.size == 9 && self.range_start.last == '0'
        p 'it can be redused'
        # remove first one from record
        if self.network
          check.update_all(removed: true, message: "replaced by first record #{self.range_start}(#{self.code.imsi}) which covers all other ranges", generated_by_step: 4)
        else
          check.update_all(removed: true, message: "replaced by first record #{self.range_start}(#{self.code.imsi}) which covers all other ranges", generated_by_step: 4)
        end
        self.range_start.chop!
        self.generated_by_step = 4
        p self.range_start
        self.removed = nil
        self.message = 'Last zero removed from range because of all 0-9 existing'
        self.save
      end
    end
  end

  def remove_0_and_9
    # Delete 0 and 9
    return if self.range_start.blank?
    p 'removing 9 and 0'
    p "before it is #{self.range_start} #{self.range_stop}"
    self.range_start = self.range_start.gsub(/0+$/, '')
    self.range_stop = self.range_stop.gsub(/9+$/, '')
    self.save
    p "after it is #{self.range_start} #{self.range_stop}"
  end

  def filter_uneven
    return unless self.range_start && self.range_stop 
    if self.range_start.length != self.range_stop.length
      p "FIXING LENGTH"
      p "start before fixing #{self.range_start}"
      p "stop before fixing #{self.range_stop}"
      start = self.range_start 
      stop = self.range_stop
      if stop > start
        uneven_amount = stop.length - start.length
        equalize_number = "#{start}%0#{uneven_amount}o" % '0'
        start = equalize_number
        p "Equalize number is #{equalize_number}"
        p "stop after equalize is #{stop}"
        p "start after equalize is #{start}"
      else
        uneven_amount = start.length - stop.length
        equalize_number = "%0#{uneven_amount}o" % '0'
        equalize_number = equalize_number.gsub('0','9')
        stop = "#{stop}#{equalize_number}"
        p "Equalize number is #{equalize_number}"
        p "stop after equalize is #{stop}"
        p "start after equalize is #{start}"
      end
      self.range_start = start
      self.range_stop = stop 
      self.save
    end
  end
end
