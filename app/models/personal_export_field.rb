class PersonalExportField < ActiveRecord::Base
  belongs_to :personal_export
  belongs_to :export_field
end
