class Network < ActiveRecord::Base
  # before_create :set_rosi_id
  include ActiveModel::Dirty
  has_paper_trail ignore: [:created_at, :updated_at], meta: { network_id: :id }
  acts_as_paranoid
  belongs_to :import
  has_many :imports
  has_many :network_rows, dependent: :destroy
  has_one :hermes_operator
  has_many :change_histories
  belongs_to :country
  belongs_to :organisation
  has_many :notify_groups_network, dependent: :destroy
  has_many :network_diffs
  has_many :network_line_ranges, dependent: :destroy
  has_many :sms_hub_line_ranges, dependent: :destroy
  has_many :code_range_versions
  has_many :code_ranges
  has_many :codes
  has_many :pdf_networks
  has_many :network_statuses
  has_many :sor_gt_line_ranges

  store_accessor :routing, :network_id, :e212_mcc, :e212_mnc, :e214_cc, :e214_nc, :number_portability, :sim_header, :test, :one, :two, :three

  def imsi
    "#{self.routing['e212_mcc',]}-#{self.routing['e212_mnc']}"  
  end

  def create_e164_range()
    p 'starting to create e164'
    array       = []
    networks    = Network.where(rosi_id: rosi_id).pluck(:id)
    # Check if there is a line range fetched from sms hub import
    rows = SmsHubLineRange.where("network_id IN(?)", networks.to_a).order(network_id: :asc,range_start: :asc).to_a 
    
    if rows.blank?
      rows = NetworkLineRange.where("network_id IN(?) AND removed IS NULL", networks.to_a).order(network_id: :asc,range_start: :asc).to_a
    end
    
    if rows
      rows.each do |r|
        array << %Q[#{r.range_start} #{r.range_stop}]
      end
    else
      'Could not find any ranges'
    end
    array.join("\n")
  end

  def create_mno_ip_range
    array = []
    rows = NetworkRow.where(network_id: id, network_section_id: 64).all
    rows.in_groups_of(2, nil).each_with_index do |group, _index|
      group.each.with_index do |item, index|
        array << if index.zero?
         "#{item.data['ip']};"
       else
         array << if item == rows.last
          item.data['ip'].to_s
        else
          "#{item.data['ip']}\n"
        end
      end
    end
  end
  array.join
end

def self.hermes_csv_export(current_user = nil)
  networks = Network.select("DISTINCT ON(rosi_id) rosi_id , *").all
    #networks = Network.uniq(:rosi_id).includes(:hermes_operator, :country).where("id in(?)", rosi_ids.to_a)
    export = Export.create!(user_id: current_user.id)
    export.update_columns(file: "hermes-#{export.id}.csv")
    p networks
    fields = ['SMS number administrationID', 'Country', 'Carrier', 'NumberRange', 'IMSI range']
    data = CSV.open("data/export/hermes-#{export.id}.csv", "wb", col_sep: ';', encoding: 'WINDOWS-1252', force_quotes: false) do |csv|
      #csv << fields
      networks.each do |x|
        # Check here if more networks are connected to the same ROSI ID
        imsis = Network.where(rosi_id: x.rosi_id).all
        unless x.rosi_id
          imsis = Network.where(id: x.id).all
        end
        if imsis.count == 1 && x.routing
          imsi = "#{x.routing['e212_mcc',]}-#{x.routing['e212_mnc']}"
        else
          # When there is multiple Networks with the sam rosi_id, create a imsi range separated with semikolon
          array = []
          imsis.each do |i|
            next unless i.routing
            array.push(" #{i.routing['e212_mcc',]}-#{i.routing['e212_mnc']}")
          end
          imsi = array.join(';')
        end
        p x.country.try(:name)
        #next unless x.network_line_ranges
        #hermes = HermesOperator.where("data -> 'sms_number_administration_id' = ?", x.rosi_id.to_s).first
        #next unless hermes
        csv << [x.rosi_id, x.country.try(:name), x.display_name.encode('WINDOWS-1252', {invalid: :replace, undef: :replace, replace: '?'}), x.create_e164_range,
          imsi]
        end
      end
    #export.data = data
    #export.user_id = current_user.id
    #export.file = "hermes.csv-#{Time.now}"
    #export.save
  end

  def self.lte_sccp_ip_csv_export(current_user = nil)
    Export.new.export_sccp_ip
  end

  def self.lte_mno_ip_csv_export(current_user = nil)
    Export.new.export_mno_ip
  end

  def self.to_csv(_object, _fields, _custom_csv = nil, _options = {})
  end

  # Export All networks to excel
  # @return object
  def self.export_to_excel(current_user = nil)
    fields = ['Organisation Name', 'Display name', 'Network Name', 'TADIG', 'Country', 'MCC-MNC', 'E214-CC', 'E214-NC', 'DPC', 'DPC-hex', 'DPC-decimal']
    data = CSV.generate(col_sep: "\t") do |excel|
      excel << fields
      all.select('id, organisation_id, display_name, network_name, tadig_code, routing, country_id').each do |network|
        organisation = Organisation.find_by(id: network.organisation_id)
        array = []
        if organisation
          array.push(organisation.name.to_s) if organisation
        else
          array.push('')
        end
        array.push(network.display_name)
        array.push(network.network_name)
        array.push(network.tadig_code)
        array.push(network.country.try(:name))
        if network.routing
          mcc_mnc = "#{network.routing['e212_mcc']}-#{network.routing['e212_mnc']}"
          cc  = network.routing['e214_cc'].to_s
          nc  = network.routing['e214_nc'].to_s
        else
          mcc_mnc = ' '
          cc  = ' '
          nc  = ' '
        end
        array.push(mcc_mnc)
        array.push(cc)
        array.push(nc)
        # Fetch DPCs
        dpcs = NetworkRow.where(network_section_id: 149, network_id: network.id).all
        dpc_array = []
        dpc_hex_array = []
        dpc_decimal_array = []
        dpcs.each do |dpc|
          dpc_array.push(dpc.data['dpc'])

          # DPC decimal and hex
          decimal = convert_from_itu_format(dpc.data['dpc'])
          hex = convert_from_itu_format(dpc.data['dpc']).to_s(16)
          dpc_decimal_array.push(decimal)
          dpc_hex_array.push(hex)
        end
        dpc_string = dpc_array.join(',')
        array.push(dpc_string)
        array.push(dpc_hex_array.join(','))
        array.push(dpc_decimal_array.join(','))
        excel << array
      end
    end

    export = Export.new
    export.data = data
    export.user_id = current_user.id
    export.export_type_id = 4
    export.file = "network_export-#{Time.now}"
    export.save
  end

  def self.create_excel(current_user)
    require 'base64'
    # Create a new Excel Workbook
    filename = "all_networks-#{Time.now}.xls"
    workbook = WriteExcel.new("#{Rails.root}/data/export/#{filename}")
    # Add worksheet(s)
    worksheet = workbook.add_worksheet
    fields = ['Oranisation ID', 'Organisation Name', 'Display name', 'Network Name', 'TADIG', 'Country','Iso 2','Iso 3', 'MCC', 'MNC', 'E214-CC', 'E214-NC']
    worksheet.write_row('A1', fields)

    @array = []
    excel_counter = 1

    all.where('organisation_id IS NOT NULL').select('id, organisation_id, display_name, network_name, tadig_code, routing, country_id').each do |network|
      organisation = Organisation.find_by(id: network.organisation_id)
      #### Organisation ####
      if organisation
        worksheet.write(excel_counter, 0, organisation.try(:id))
        worksheet.write(excel_counter, 1, ActiveSupport::Inflector.transliterate(organisation.try(:name)))
      else
        worksheet.write(excel_counter, 0, ' ')
      end
      #### Display Name ####
      worksheet.write(excel_counter, 2, ActiveSupport::Inflector.transliterate(network.display_name))

      #### Network Name ####
      if network.network_name
        network_name = ActiveSupport::Inflector.transliterate(network.network_name)
      else
        network_name = ''
      end
      worksheet.write(excel_counter, 3, network_name)

      #### Tadig code ####
      worksheet.write(excel_counter, 4, network.tadig_code)

      #### Country ####
      worksheet.write(excel_counter, 5, network.country.try(:name))

      #### Iso 2 ####
      worksheet.write(excel_counter, 6, network.country.try(:iso2_code))      

      #### Iso 3 ####
      worksheet.write(excel_counter, 7, network.country.try(:iso_code))      

      if network.routing
        mcc = network.routing['e212_mcc'].to_s
        mnc = network.routing['e212_mnc'].to_s
        cc  = network.routing['e214_cc'].to_s
        nc  = network.routing['e214_nc'].to_s
      else
        mcc = ''
        mnc = ''
        cc  = ''
        nc  = ''
      end

      worksheet.write(excel_counter, 8, mcc)
      worksheet.write(excel_counter, 9, mnc)
      worksheet.write(excel_counter, 10, cc)
      worksheet.write(excel_counter, 11, nc)
      excel_counter += 1
    end
    workbook.close

    export = Export.new
    export.data = nil
    export.user_id = current_user.id
    export.export_type_id = 4
    export.file = filename
    export.save
  end

  # xlsx version of above function
  def self.create_excel_xlsx(current_user)
    require 'base64'
    # Create a new Excel Workbook
    filename = "all_networks-xlsx#{Time.now}.xlsx"
    workbook = WriteXLSX.new("#{Rails.root}/data/export/#{filename}")
    # Add worksheet(s)
    worksheet = workbook.add_worksheet
    fields = ['Oranisation ID', 'Organisation Name', 'Display name', 'Network Name', 'TADIG', 'Country','Iso 2','Iso 3', 'MCC', 'MNC', 'E214-CC', 'E214-NC']
    worksheet.write_row('A1', fields)

    @array = []
    excel_counter = 1

    all.where('organisation_id IS NOT NULL').select('id, organisation_id, display_name, network_name, tadig_code, routing, country_id').each do |network|
      organisation = Organisation.find_by(id: network.organisation_id)
      #### Organisation ####
      if organisation
        worksheet.write(excel_counter, 0, organisation.try(:id))
        worksheet.write(excel_counter, 1, ActiveSupport::Inflector.transliterate(organisation.try(:name)))
      else
        worksheet.write(excel_counter, 0, ' ')
      end
      #### Display Name ####
      worksheet.write(excel_counter, 2, ActiveSupport::Inflector.transliterate(network.display_name))

      #### Network Name ####
      if network.network_name
        network_name = ActiveSupport::Inflector.transliterate(network.network_name)
      else
        network_name = ''
      end
      worksheet.write(excel_counter, 3, network_name)

      #### Tadig code ####
      worksheet.write(excel_counter, 4, network.tadig_code)

      #### Country ####
      worksheet.write(excel_counter, 5, network.country.try(:name))

      #### Iso 2 ####
      worksheet.write(excel_counter, 6, network.country.try(:iso2_code))      

      #### Iso 3 ####
      worksheet.write(excel_counter, 7, network.country.try(:iso_code))      

      if network.routing
        mcc = network.routing['e212_mcc'].to_s
        mnc = network.routing['e212_mnc'].to_s
        cc  = network.routing['e214_cc'].to_s
        nc  = network.routing['e214_nc'].to_s
      else
        mcc = ''
        mnc = ''
        cc  = ''
        nc  = ''
      end

      worksheet.write(excel_counter, 8, mcc)
      worksheet.write(excel_counter, 9, mnc)
      worksheet.write(excel_counter, 10, cc)
      worksheet.write(excel_counter, 11, nc)
      excel_counter += 1
    end
    workbook.close

    export = Export.new
    export.data = nil
    export.user_id = current_user.id
    export.export_type_id = 4
    export.file = filename
    export.save
  end

    #Called from export_controller/export_all_networks_advanced
  def self.create_advanced_network(current_user)
    export = Export.create!(user_id: current_user.id)
    export.update_columns(file: "advanced_network_export-#{Time.now}.csv")
    fields = ['Organisation Name', 'Organisation-ID', 'Display name', 'Network Name', 'Rosi-ID', 'TADIG', 'Country', 'Iso 2','Iso 3', 'MCC-MNC', 'E214-CC', 'E214-NC']
    data = CSV.open("data/export/advanced_network_export-#{Time.now}.csv", "wb", col_sep: ';', encoding: 'UTF-8', force_quotes: false) do |csv|
        csv << fields
        all.where('organisation_id IS NOT NULL').select('id, rosi_id, tadig_code, organisation_id, display_name, network_name, tadig_code, routing, country_id').each do |n|
          #Get organisations
          organisation = Organisation.find_by(id: n.organisation_id)
          if n.routing
            mcc_mnc = "#{n.routing['e212_mcc']}-#{n.routing['e212_mnc']}"
            cc  = n.routing['e214_cc'].to_s
            nc  = n.routing['e214_nc'].to_s
          else
            mcc_mnc = ' '
            cc  = ' '
            nc  = ' '
          end

          csv << [organisation.try(:name), 
                  organisation.try(:id), 
                  ActiveSupport::Inflector.transliterate(n.display_name),
                  n.network_name,
                  n.rosi_id,
                  n.tadig_code,
                  n.country.try(:name),
                  n.country.try(:iso2_code),
                  n.country.try(:iso_code),
                  mcc_mnc,
                  cc,
                  nc]
      end
    end
    export.data = nil
    export.user_id = current_user.id
    export.export_type_id = 12
    export.save
  end

  # Export excel file for Connexion
  # @return file
  def self.export_telenor_connexion(current_user)
    require 'base64'
    # Create a new Excel Workbook
    filename = "telenor_connexion-#{Time.now}.xls"
    workbook = WriteExcel.new("#{Rails.root}/data/export/#{filename}")
    # Add worksheet(s)
    worksheet = workbook.add_worksheet
    fields = ['Organisation Name','Network Name', 'TADIG', 'Country', 'Country initials', 'MCC', 'MNC', 'CC', 'NC', 'Last update']
    worksheet.write_row('A1', fields)

    @array = []
    excel_counter = 1

    all.select('id, organisation_id, display_name, network_name, tadig_code, routing, country_id, updated_at').each do |network|
      organisation = Organisation.find_by(id: network.organisation_id)
      #### Organisation name ####
      if organisation
        worksheet.write(excel_counter, 0, organisation.try(:name))
      else
        worksheet.write(excel_counter, 0, ' ')
      end

      #### Display Name ####
      worksheet.write(excel_counter, 1, network.display_name)

      #### Tadig code ####
      worksheet.write(excel_counter, 2, network.tadig_code)

      #### Country ####
      worksheet.write(excel_counter, 3, network.country.try(:name))

      #### Country initials ####
      worksheet.write(excel_counter, 4, network.country.try(:iso_code))      

      if network.routing
        mcc = network.routing['e212_mcc'].to_s
        mnc = network.routing['e212_mnc'].to_s
        cc  = network.routing['e214_cc'].to_s
        nc  = network.routing['e214_nc'].to_s
      else
        mcc = ''
        mnc = ''
        cc  = ''
        nc  = ''
      end

      worksheet.write(excel_counter, 5, mcc)
      worksheet.write(excel_counter, 6, mnc)
      worksheet.write(excel_counter, 7, cc)
      worksheet.write(excel_counter, 8, nc)
      worksheet.write(excel_counter, 9, network.updated_at.to_date)
      excel_counter += 1
    end
    workbook.close

    export = Export.new
    export.data = nil
    export.user_id = current_user.id
    export.export_type_id = 5
    export.file = filename
    export.save
  end

  # Function to auto-increment rosi-id
  def set_rosi_id
    self.rosi_id = Network.last.id + 1
  end

  def self.create_hermes_diff_excel
    unmatched = HermesOperator.where("network_id IS null").all
    require 'base64'
    # Create a new Excel Workbook
    filename = "hermes-network-diff.xls"
    workbook = WriteExcel.new("#{Rails.root}/data/hermes-diff/#{filename}")
    worksheet = workbook.add_worksheet
    fields = ['Name', 'MCC', 'MNC']
    worksheet.write_row('A1', fields)

    #Used to increase rows
    excel_counter = 1
    unmatched.each do |diff|
      #Add name

      next unless diff.data
      p 'data exists'
      name = diff.data['carrier']
      mcc  = diff.data['imsi_range'].split('-').first if diff.data['imsi_range']
      mnc  = diff.data['imsi_range'].split('-').last if diff.data['imsi_range']
      worksheet.write(excel_counter, 0, name)
      worksheet.write(excel_counter, 1, mnc) if diff.data['imsi_range']
      worksheet.write(excel_counter, 2, mcc) if diff.data['imsi_range']

      excel_counter += 1
    end
    workbook.close
  end

  def evaluate_line_ranges
    p 'Evaluate is called!!'
    # Get all networks connected to the same organisation id
    p "ID is: #{self.id}"
    network_ids = self.organisation.networks.pluck(:id)
    p network_ids
    lr = NetworkLineRange.where("network_id IN(?)", network_ids)
    p lr
    @previous = nil
    lr.sort_by(&:range_start).each do |range|
      p "Checking line range:: #{range.id} range count #{lr.count} network_id #{range.network_id}"
      @range_start = range.range_start
      @range_stop = range.range_stop
      break unless @range_start && @range_stop
      check_removed = NetworkLineRange.find(range.id)
      if check_removed.removed == true
        p "skipping #{range.id}"
        next
      end
      ##### Check if the line range has the same length ####
      ######################################################
      unless @range_start.length == @range_stop.length
        # range.removed = true
        # range.message = "line ranges do not match"
        # range.save
        p 'It is not even!!'
        min_value = [@range_start, @range_stop].min
        max_value = [@range_start, @range_stop].max
        diff      = max_value.length - min_value.length
        p diff
        if diff < 0
          diff *= -1
        end
        diff = diff.to_i
        p diff
        p "RANGE_START: #{@range_start} AND STOP: #{@range_stop}"
        p @range_start > @range_stop
        diff.times do |t|
          p "a new number #{@range_start.to_i < @range_stop.to_i} sss:#{@range_start} ststst::#{@range_stop}"
          if @range_start.to_i < @range_stop.to_i
            @range_start = "#{@range_start}0"
            p "increase start start: #{@range_start} stop: #{@range_start}"
            p @range_start
          else
            @range_stop = "#{@range_stop}9"
            p 'increase stop'
            p @range_stop
          end
        end
        range.message = "Uneven ranges - correcting"
        range.range_start = @range_start
        range.range_stop = @range_stop
        range.save!
        p "RAAANGGE IS NOW::::::#{range.range_start}|||||#{range.range_stop}"
      end

      #### Switched start and stop range fix ####
      ###########################################
      if @range_start > @range_stop
        p 'Start range is bigger - fixing'
        range.range_start = @range_start
        range.range_stop  = @range_stop
        range.message     = 'switched line ranges'
        range.save!
      end

      #### Fix double line ranges ####
      ################################
      double = NetworkLineRange.where('network_id = ? AND range_start = ? AND range_stop = ? AND id != ? AND removed IS NULL', self.id, @range_start.to_s, @range_stop.to_s, range.id).first
      if double
        range.removed = true
        range.message = 'Double line range removed'
        range.save
      end

      #### Partly overlapping fix ####
      ################################
      partly_overlapped_top = NetworkLineRange.where("network_id = ? AND range_start < ? AND range_stop > ? AND removed is NULL AND id != ?",self.id,@range_stop.to_s,@range_stop.to_s,range.id).order("created_at desc").all
      if partly_overlapped_top
        partly_overlapped_top.each do |overlap|
          unless overlap.removed
            next if overlap.range_start.to_i > @range_stop.to_i
            range.range_stop = overlap.range_stop
            range.removed = nil
            range.save!
            overlap.removed = true
            overlap.message = "Removed because of partly overlapped top switched to #{range.range_stop} (#{range.id})"
            overlap.save!
          end
        end
      end
      partly_overlapped_bottom = NetworkLineRange.where("network_id = ? AND range_start < ? AND range_stop > ? AND removed is NULL AND id != ?",self.id,@range_start.to_s,@range_start.to_s, range.id).order('created_at desc').all
      if partly_overlapped_bottom
        partly_overlapped_bottom.each do |overlap|
          unless overlap.removed
            next if overlap.range_stop.to_i < @range_start.to_i
            range.range_start = overlap.range_start
            range.removed = nil
            range.save!
            overlap.removed = true
            overlap.message = "Removed because of partly overlapped bottom - switched to #{range.range_start} (#{range.id})"
            overlap.save!
          end
        end
      end

      #### Overlapping fix ####
      #########################
      overlapping_ranges = NetworkLineRange.where("network_id = ? AND range_start > ? AND range_stop < ? AND removed is NULL",self.id,@range_start.to_s,@range_stop.to_s).order("created_at desc").all
      if overlapping_ranges
        overlapping_ranges.each do |overlap|
          unless overlap.removed
            p "Fixing overlapping ranges for id (#{overlap.id})"
            overlap.removed = true
            overlap.message = "overlapped by #{range.id}"
            overlap.save!
          end
        end
      end
      #### Consecutive line range fix ####
      ####################################
      p 'CONSECUTIVE FIX'
      if @previous
        p 'CHECKING FOR CONSECUTIVE'
        if @previous.try(:range_stop).to_i + 1 == range.try(:range_start).to_i
          p 'IT IS CONSECUTIVE'
        #extend the current line range with the previous
        range.range_start = @previous.range_start
        range.save
        #Delete the previous
        @previous.removed = true
        @previous.message = 'Deleted because of consecutive'
        @previous.save
        # Set current as previous
        @previous = range
      else
        p "do not match #{@previous.try(:range_stop).to_i} |||| #{range.try(:range_start).to_i}"
        @previous = range
      end
    else
      p 'SETTING PREVIOUS'
      @previous = range
    end
    range.save!
  end
end
end

def convert_from_itu_format(value)
  split = value.split('-')
  result = split[0].to_i * 2048 + split[1].to_i * 8 + split[2].to_i
  result
end
