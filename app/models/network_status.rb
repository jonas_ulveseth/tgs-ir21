class NetworkStatus < ActiveRecord::Base
  belongs_to :network_status_type
  belongs_to :network
end
