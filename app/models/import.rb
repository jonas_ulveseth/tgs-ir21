class Import < ActiveRecord::Base
  require 'rake'
  require 'active_support/core_ext/hash/conversions'
  require 'roo'
  TGSIR21::Application.load_tasks
  include ImportFunctions
  include ImportExcel
  include ImportCsv
  include ImportV2
  has_one :network
  belongs_to :network
  has_many :networks
  has_many :activities
  has_many :network_diffs
  has_many :missing_structure, class_name: "ImportMissingSection", foreign_key: "import_id"
  has_many :pdfs
  belongs_to :organisation
  belongs_to :xml_version
  has_one :pdf

  def import(file_content = nil)
    save
    self.failed = false
    self.save!
    unless file_content
      Dir.chdir("#{Rails.root}/data/upload") do
        ActiveRecord::Base.connection.transaction do
          file_content = File.read(original_filename)
        end
      end
    end

    # Set document
    doc = Nokogiri::XML(file_content)
    doc.remove_namespaces!
    # check for conflicts
    conflicts = find_conflicts(doc)
    save

    # Start parsing document
    doc.xpath('//OrganisationInfo').each do |o|
      p 'started parse'
      #if Rails.env.production?
      begin
        import_organisation(o, doc)
        import_raex_ir21_information(doc)
      rescue Exception
        self.failed = true
        unless self.error_message
          self.error_message = $!.message
          self.error_stack   = $!.backtrace
        end
        self.checked_for_diff = true
        self.save
      end
      #else
      #  import_organisation(o, doc)
      #  import_raex_ir21_information(doc)
      #end
    end
  end

  def import_pdf
    # find xml copy
    pdf_imports = Import.where('archive = ? AND (removed = ? OR removed = ?) AND (file_type = ? OR file_type = ?)',false, nil, false, 'pdf', 'PDF').all
    pdf_imports.each do |pdf_import|
      p 'found one'
      import = Import.where("global_file_name = ? AND (file_type = 'xml' OR file_type='xlsx') AND network_id IS NOT NULL", pdf_import.global_file_name).last
      begin
        if import
          p "found import"
          p import
          import.pdf_data = pdf_import.data
          import.save
          pdf = Pdf.create(import_id: pdf_import.id,name: pdf_import.file_name, original_filename: pdf_import.original_filename, organisation_id: import.network.organisation_id)
          Network.where(import_id: import.id).each do |network|
            PdfNetwork.where(network_id: network.id).delete_all
            PdfNetwork.create(pdf_id: pdf.id, network_id: network.id)
          end
          pdf_import.organisation_id = import.network.organisation.id
        end
        pdf_import.archive = true
        pdf_import.percentage_done = 100
        pdf_import.save!
      rescue Exception
        pdf_import.failed = true
        pdf_import.error_message = $!.message
        pdf_import.error_stack   = $!.backtrace
      end 
    end
  end

  private

  def import_organisation(organisation_tag, doc)
    p 'importing organisation'
    update_columns(percentage_done: 20)
    @organisation_tag = organisation_tag
    
    # Check so file has required tags
    check_requirements(doc)
    #### Find organisation by string ####
    #####################################
    name = organisation_tag.xpath('OrganisationName').first.text
    ci = organisation_tag.xpath('CountryInitials').first.text
    @country = find_country(ci)
    raise 'cannot find country' unless @country
    sender_tadig = doc.xpath('//SenderTADIG').text
    if sender_tadig
      @organisation = Organisation.where(sender_tadig: sender_tadig).first
      unless @organisation
        p 'no org'
        ###############################################
        ## Find organisation through network details ##
        organisation_array = []
        # Name match
        p "name is #{name}"
        p "country id is #{@country.id}"
        organisation = Organisation.where(name: name, country_id: @country.id).first
        p "organisation is #{organisation}"
        if organisation
          unless organisation_array.include?(organisation.id)
            organisation_array.push(organisation.id)
          end
        end

        @organisation_control = nil
        ### find previous networks from tadig ####
        ##########################################
        organisation_tag.xpath("//TADIGCode").each do |t|     
          test = Network.where(tadig_code: t.text).first
          if test.try(:organisation)
            @organisation_control = true
            @organisation = test.organisation
            p "We found a organisation through tadig:  #{test.id}"
            unless organisation_array.include?(@organisation.id)
              organisation_array.push(@organisation.id)
            end
          end
        end
        #### Find previous networks from mnc + mcc ####
        ###############################################
        organisation_tag.xpath("//CCITT_E212_NumberSeries").each do |t|
          mcc = t.xpath('MCC').text 
          mnc = t.xpath('MNC').text
          test = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ?",mcc,mnc).first
          if test.try(:organisation)
            @organisation_control = true
            @organisation = test.organisation
            p "We found a organisation #{test.organisation.try(:id)} through mnc,mcc:  #{test.id}"
            unless organisation_array.include?(@organisation.id)
              organisation_array.push(@organisation.id)
            end
          end
        end

        unless self.resolved_by_admin
          p organisation_array
          p self.id
          self.failed = true
          self.error_message = 'Import is trying to create a new organisation'
          self.organisation_override = organisation_array unless organisation_array.blank?
          self.save
          p 'saved'         
          p self.errors.messages.inspect
          p self
          exit
        else
          unless self.organisation_override.nil?
            @organisation = Organisation.find(self.organisation_override.first)
            @organisation.update_columns(sender_tadig: sender_tadig, name: name)
          else
            @organisation = Organisation.where(sender_tadig: sender_tadig).first_or_create
          end
        end
      end
      @organisation.update_columns(name: name, country_id: @country.id)
      organisation = @organisation
    else
      p 'no sender'
      # Match organisation by string
      p "name is #{name}" 
      organisation = Organisation.where(name: name, country_id: @country.id).first
      unless organisation_array.include?(organisation.id)
        organisation_array.push(organisation.id)
      end
      unless organisation
        @organisation_control = nil
        ### find previous networks from tadig ####
        ##########################################
        organisation_tag.xpath("//TADIGCode").each do |t|     
          test = Network.where(tadig_code: t.text).first
          if test.try(:organisation)
            @organisation_control = true
            organisation = test.organisation
            unless organisation_array.include?(organisation.id)
              organisation_array.push(organisation.id)
            end
          end
        end
        #### Find previous networks from mnc + mcc ####
        ###############################################
        organisation_tag.xpath("//CCITT_E212_NumberSeries").each do |t|
          mcc = t.xpath('MCC').text 
          mnc = t.xpath('MNC').text
          test = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ?",mcc,mnc).first
          if test.try(:organisation)
            @organisation_control = true
            organisation = test.organisation
            unless organisation_array.include?(organisation.id)
              organisation_array.push(organisation.id)
            end
          end
        end
        unless @organisation_control == true
          p 'creating a new organisation'
          unless self.resolved_by_admin
            self.failed = true
            self.organisation_override = organisation_array unless organisation_array.blank?
            self.error_message = 'Import is trying to create a new organisation'
            self.save
            exit
          end
          unless self.organisation_override.nil?
            organisation = Organisation.find(self.organisation_override.first)
            organisation.update_columns(sender_tadig: sender_tadig, name: name)
          else
            organisation = Organisation.create!(name: name, country_id: @country.id)
          end
        end
      end
      @organisation = organisation	
    end

    #organisation.networks.delete_all
    @previous_networks_array = organisation.networks.pluck(:id).to_a
    # import networks
    import_networks(organisation_tag, @organisation, doc)
    # Fetch new networks attached to this organisation
    @current_network_array = organisation.networks.pluck(:id).to_a
    diff = @previous_networks_array - @current_network_array
    if diff.count > 0
      Notification.notify_deleted_network(diff, organisation)
      p diff
    end
    # Change updated at
    organisation.touch
    self.update_columns(organisation_id: organisation.id)
    # Import codes
    begin
      Code.create_code_line_ranges(Network.where(id: network.id))
    rescue Exception
      code = Code.where(network_id: @network.id, rosi_id: @network.rosi_id).first_or_create
      crv  = CodeRangeVersion.create(import_id: @import.id, code_id: code.id, network_id: @network.id)
      crv.failed_message = $!.message
      crv.save    
    end
  end

  def import_raex_ir21_information(doc)
    raex = doc.xpath('//RAEXIR21FileHeader')
    @creation_time = raex.xpath('FileCreationTimestamp').text
    @version = raex.xpath('TADIGGenSchemaVersion').text
    @ir21_version = raex.xpath('TADIGRAEXIR21SchemaVersion').text
    @network.xml_creation_date = @creation_time.to_date
    self.file_creation_date    = @creation_time.to_date
    save
    @network.xml_creation_time = @creation_time.to_time
    @network.tadig_gen_schema_version = @version
    @network.raex_ir21_schema_version = @ir21_version
    @network.save
  end

  def import_networks(organisation_tag, organisation, doc)
    organisation_tag.xpath('NetworkList/Network').each_with_index do |network_tag, index|
      update_columns(percentage_done: 10)
      #### Basic network information ####
      tadig_code = network_tag.xpath('TADIGCode').first.text
      network_name = network_tag.xpath('NetworkName').text
      terrestrial = network_tag.xpath('NetworkType').first.text == 'Terrestrial'
      country_initials_and_mnn = network_tag.xpath('PresentationOfCountryInitialsAndMNN').text
      abbreviated_mnn = network_tag.xpath('AbbreviatedMNN').text
      network_colour_code = network_tag.xpath('NetworkColourCode').text
      #### Set document tag user throughout the import ####
      document_tag = network_tag.xpath('NetworkData')
      
      #### Creating new or linking to exisiting network ####
      ######################################################

      routing = document_tag.xpath('RoutingInfoSection')
      unless routing.xpath('CCITT_E212_NumberSeries/MCC').first
        routing = document_tag.xpath('RoutingInfoSection/RoutingInfo')
      end
      if routing
        mcc = routing.xpath('CCITT_E212_NumberSeries/MCC').first.text
        mnc = routing.xpath('CCITT_E212_NumberSeries/MNC').first.text
        old_network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ? AND tadig_code = ?", mcc, mnc, tadig_code).first
        old_network = Network.where(tadig_code: tadig_code).first unless old_network
        unless old_network
          # Do a check every time for networks that have the same mnc ,mcc but hav a blank tadig
          old_network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ? AND tadig_code IS ?", mcc, mnc, nil).first
        end
      end

      ###################################################
      #### OLD NETWORK EXISTS 
      ###################################################
      if defined? old_network.id
        old_network.touch
        network = old_network
        p "IMPORTING id #{network.id}"
        #Cleenup of previous networks with blank tadig
        Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ? AND tadig_code IS ? AND id != ?", mcc, mnc, nil, network.id).delete_all
        # Check if there is a parent network..
        org_networks = organisation.try(:networks).try(:all)
        org_networks.update_all(updated_at: Time.now) if org_networks
        @network = network
        @network.country_id = @country.id
        @network.import_id = id
        @network.xml_creation_date = doc.xpath('//RAEXIR21FileHeader/FileCreationTimestamp').text.to_date
        @network.xml_creation_time = doc.xpath('//RAEXIR21FileHeader/FileCreationTimestamp').text.to_time
        @network.tadig_gen_schema_version = doc.xpath('//RAEXIR21FileHeader/TADIGGenSchemaVersion').text
        @network.raex_ir21_schema_version = doc.xpath('//RAEXIR21FileHeader/TADIGRAEXIR21SchemaVersion').text
        @network.tadig_code = tadig_code
        # Check so that display name is not modified
        @network.network_name = network_name
        display_name_check = PaperTrail::Version.where('whodunnit IS NOT NULL AND item_id = ? AND object_changes LIKE ?', @network.id, '%display_name%').first
        unless display_name_check
          p 'setting network display name'
          unless @network.network_name.blank?
            @network.display_name = @network.network_name
          else
            p 'setting display name from org.'
            p "organisation_name is::#{organisation.name}"
            @network.display_name = "#{organisation.name}"
          end
        end
        @network.needs_to_resolve = false
        @network.resolve_message = nil
        @network.organisation_id = organisation.id
        @network.network_diffs.delete_all
        @network.save
        # Set a flag to know that we are updating a network, not creating a new one
        @old_network = true
        notification = Notification.create!(message: "Network was reimported", network_id: @network.id)
      ##################################################
      #### NEW NETWORK 
      ##################################################
    else  
        ### Rosi id incremental test , Test other networks within the same network ####
        reference_network = organisation.networks.first
        if reference_network
          # Check for abnormal network
          rosi_org_networks_abnormal = organisation.networks.where("id != ? AND rosi_id != ?", reference_network.id, reference_network.rosi_id).first
          unless rosi_org_networks_abnormal
            rosi_org_networks_same = organisation.networks.where("rosi_id = ?", reference_network.id).first
          end
        end

        #### Case 1 ####
        # - other networks exists, all have the same rosi_id - add the same rosi-id to the new network
        ################
        if reference_network && rosi_org_networks_abnormal.nil?
          p 'CASE1'
          rosi_increment = reference_network.rosi_id
        end

        #### Case 2 ####
        # - other networks exists, but they have diffrent rosi_id - rosi_id is left blank
        if reference_network && rosi_org_networks_abnormal
          p 'CASE2'
          rosi_increment = nil
        end

        #### Case 3 ####
        # - no other networks exists - a new incremental rosi_id is added
        unless reference_network
          p 'CASE3'
          rosi_increment = Network.where("rosi_id IS NOT NULL").order("rosi_id desc").pluck(:rosi_id).first
          unless rosi_increment
            rosi_increment = 1
          else 
            rosi_increment = rosi_increment + 1
          end
        end

        # display name logic
        unless network_name.blank?
          display_name = network_name  
        else
          display_name = "#{organisation.try(:name)}"
        end
        network = Network.create!(
          tadig_code: tadig_code,
          country_id: @country.try(:id),
          network_name: network_name,
          display_name: display_name,
          terrestrial: terrestrial,
          xml_creation_date: doc.xpath('//RAEXIR21FileHeader/FileCreationTimestamp').text.to_date,
          xml_creation_time: doc.xpath('//RAEXIR21FileHeader/FileCreationTimestamp').text.to_time,
          country_initials_and_mnn: country_initials_and_mnn,
          abbreviated_mnn: abbreviated_mnn,
          network_color_code: network_colour_code,
          organisation_id: organisation.try(:id),
          import_id: id,
          rosi_id: rosi_increment
          )
        @network = network
        @old_network = false
        notification = Notification.create!(message: "New network created", network_id: @network.id)
	@network_diff = NetworkDiff.new
        @network_diff.network_id = @network.id
        @network_diff.import_id  = id
        @network_diff.message = "New network was created"
        @network_diff.notify_field_id = 5
        @network_diff.save
      end

      #### Add parent network id to network ####
      if index == 0
        @parent_network = @network
      else
        @network.parent_id = @parent_network.try(:id)
      end

      #### Check that network count is the same ###
      check_number_of_networks(organisation_tag.xpath('NetworkList/Network'))   
      
      #### Method calls to continue the parsing ####
      network_data_tag = network_tag.xpath('NetworkData')
      set_network_status(@network)
      import_routing(network, network_data_tag)
      import_domestic_sccp_carriers(network, network_data_tag)
      import_international_sccp_carriers(network, network_data_tag)
      import_network_nodes(network_data_tag, network)
      import_mobile_application_part(network, network_data_tag)
      import_sccp_protocol(network, network_data_tag)
      import_map_optimal_routing(network, network_data_tag)
      import_test_numbers(network, network_data_tag)
      import_subscriber_identity_authentications(network, network_data_tag)
      import_map_inter_operator_sms_enhancements(network, network_data_tag)
      import_ussd_info(network, network_data_tag)
      import_camel_information(network, network_data_tag)
      import_ip_roaming_iw_info(network, network_data_tag)
      import_supported_data_service_items(network, network_data_tag)
      import_packet_data_service_info(network, network_data_tag)
      import_autonomous_system_numbers(network, network_data_tag)
      import_pmn_authoritative_dns_ips(network, network_data_tag)
      import_ping_traceroute_ips(network, network_data_tag)
      import_grx_providers(network, network_data_tag)
      import_mms_iw_info(network, network_data_tag)
      import_diameter_edge_agent_ips(network, network_data_tag)
      import_wlan_info(network, network_data_tag)
      import_incoming_radius_ips(network, network_data_tag)
      import_wlan_roaming_ip_ranges(network, network_data_tag)
      import_wlan_service_brands(network, network_data_tag)
      import_lte_roaming_info(network, network_data_tag)
      import_sms_itw(network, network_data_tag)
      import_voice_itw(network, network_data_tag)
      import_roaming_retry(network, network_data_tag)
      import_lte_information(network, network_data_tag)
      import_hpmn_info_lte_agreement_only(network, network_data_tag)
      import_vpmn_lte_only_roaming_supported(network, network_data_tag)
      import_hpmn_info_2g_3g_roaming_agreement_only(network, network_data_tag)
      import_vpmn_info_2g_3g_roaming_agreement_only(network, network_data_tag)
      import_hpmn_info_2g_3g_lte_roaming_agreement(network, network_data_tag)
      import_vpmn_info_2g_3g_lte_roaming_agreement(network, network_data_tag)
      import_lteq_os_profile_list(network, network_data_tag)
      import_ipv6_connectivity_information(network, network_data_tag)
      import_diameter_certificates_exchange(network, network_data_tag)
      import_s6a_hss_mme_hostnames(network, network_data_tag)
      import_s9_pcrf_hostnames(network, network_data_tag)
      import_ipx_inter_connection_info(network, network_data_tag)
      import_epc_realms_for_roaming(network, network_data_tag)
      
      #### update progress ####
      if failed
        self.percentage_done = nil
        save
      else
        update_columns(archive: true)
        # Notify all changes by email
        notification.notify_changes(@network)
      end
    end
    #### Add network id to import and save ####
    self.network_id = @network.id
    save!
  end

  def import_routing(network, network_tag)
    update_columns(percentage_done: 20)
    p 'started with routing'
    routing_tag = network_tag.xpath('RoutingInfoSection')
    routing_tag = network_tag.xpath('RoutingInfoSection/RoutingInfo') unless routing_tag.xpath('CCITT_E212_NumberSeries/MCC').first
    return unless routing_tag

    # tags
    mcc = routing_tag.xpath('CCITT_E212_NumberSeries/MCC').first.text
    mnc = routing_tag.xpath('CCITT_E212_NumberSeries/MNC').first.text
    e214_mgt_cc = routing_tag.xpath('CCITT_E214_MGT/MGT_CC').first.text
    e214_mgt_nc = routing_tag.xpath('CCITT_E214_MGT/MGT_NC').first.text
    number_portability = routing_tag.xpath('DoesNumberPortabilityApply').first.text
    if number_portability.include?("1") || number_portability.include?("true")
      number_portability = "true"
    else
      number_portability = "false"
    end
    sim_header = routing_tag.xpath('USIM_SIM_Header').text
    effective_date_of_change = routing_tag.xpath('EffectiveDateOfChange').text
    # /tags

    # compare information to previous file imported
    path = 'RoutingInfoSection'
    path = 'RoutingInfoSection/RoutingInfo' unless routing_tag.xpath('CCITT_E212_NumberSeries/MCC').first

    check_for_diff(path, routing_tag, 0, network.tadig_code)

    # store all routing in hstore
    network.routing = {
      'effective_date_of_change' => effective_date_of_change,
      'network_id' => network.id,
      'e212_mcc' => mcc,
      'e212_mnc' => mnc,
      'e214_cc' => e214_mgt_cc,
      'e214_nc' => e214_mgt_nc,
      'number_portability' => number_portability,
      'sim_header' => sim_header
    }
    network.save!
    p 'OK - routing is done'

    #### Check that we have all basic information ####
    check_for_empty_fields

    import_e164_number_ranges(network, routing_tag, network_tag)
  end

  def import_e164_number_ranges(network, routing_tag, network_tag)
    p 'starting to import e164 number ranges'
    e164_tag = routing_tag.xpath('CCITT_E164_NumberSeries').first
    return unless e164_tag
    network_section = NetworkSection.where(field_name: 'msisdn_number_ranges').first
    import_change_history(e164_tag, network_section)
    delete_old_network_rows(network_section, network)
    network_section = NetworkSection.where(field_name: 'gt_number_ranges').first
    delete_old_network_rows(network_section, network)
    network_section = NetworkSection.where(field_name: 'msrn_number_ranges').first
    delete_old_network_rows(network_section, network)
    network_section = NetworkSection.where(field_name: 'msisdn_number_ranges').first
    delete_old_network_rows(network_section, network)

    # < 10.1 structure
    e164_tag.xpath('MSISDN_NumberRanges/RangeData/NumberRange').each do |nr|
      cc = nr.xpath('CC').first.text
      ndc = nr.xpath('NDC').first.text
      start = nr.xpath('SN_Range/SN_RangeStart').text
      stop = nr.xpath('SN_Range/SN_RangeStop').text
      length = (cc + ndc + start + stop).length
      if start.nil?
        start = nr.xpath('SN_Range/SN_RangeStart').text
        stop = nr.xpath('SN_Range/SN_RangeStop').text
        length = (start + stop + cc + ndc).length
      end

      network_row = NetworkRow.new
      network_row.network_id = network.id
      network_row.data = {
        'MSISDN - cc' => cc,
        'MSISDN - ndc' => ndc,
        'MSISDN - range_start' => start,
        'MSISDN - range_stop' => stop,
        'length' => length
      }
      network_row.network_section_id = network_section.id
      network_row.save!
    end

    # > 10.1 structure
    e164_tag.xpath('MSISDN_NumberRanges/MSISDN_RangeData/NumberRange').each do |nr|
      p 'msisdn row > 10.1'
      cc = nr.xpath('CC').first.text
      ndc = nr.xpath('NDC').first.text
      start = nr.xpath('SN_Range/SN_RangeStart').text
      stop = nr.xpath('SN_Range/SN_RangeStop').text
      length = (start + stop + cc + ndc).length
      if start.nil?
        start = nr.xpath('SN_Range/SN_RangeStart').text
        stop = nr.xpath('SN_Range/SN_RangeStop').text
        length = (start + stop + cc + ndc).length
      end

      network_row = NetworkRow.new
      network_row.network_id = network.id
      network_row.data = {
        'MSISDN - cc' => cc,
        'MSISDN - ndc' => ndc,
        'MSISDN - range_start' => start,
        'MSISDN - range_stop' => stop,
        'length' => length
      }
      network_row.network_section_id = network_section.id
      network_row.save!
    end

    p 'msisdn added'

    e164_tag.xpath('GT_NumberRanges/RangeData/NumberRange').each do |nr|
      cc = nr.xpath('CC').first.text
      ndc = nr.xpath('NDC').first.text
      start = nr.xpath('SN_Range/SN_RangeStart').text
      stop = nr.xpath('SN_Range/SN_RangeStop').text
      length = (start + stop + cc + ndc).length
      network_row = NetworkRow.new
      network_section = NetworkSection.where(field_name: 'gt_number_ranges').first
      network_row.network_id = network.id
      network_row.data = {
        'GT - cc' => cc,
        'GT - ndc' => ndc,
        'GT - range_start' => start,
        'GT - range_stop' => stop,
        'length' => length
      }
      network_row.network_section_id = network_section.id
      network_row.save!
      p "GT - IS - #{network_row.data}"
    end

    p 'GT added'


    e164_tag.xpath('MSRN_NumberRanges/NumberRange').each do |nr|
      cc = nr.xpath('CC').first.text
      ndc = nr.xpath('NDC').first.text
      start = nr.xpath('SN_Range/SN_RangeStart').text
      stop = nr.xpath('SN_Range/SN_RangeStop').text
      length = (start + stop + cc + ndc).length
      network_row = NetworkRow.new
      network_row.network_id = network.id
      network_section = NetworkSection.where(field_name: 'msrn_number_ranges').first
      network_row.data = {
        'MSRN - cc' => cc,
        'MSRN - ndc' => ndc,
        'MSRN - range_start' => start,
        'MSRN - range_stop' => stop,
        'length' => length
      }
      network_row.network_section_id = network_section.id
      network_row.save!
    end

    p 'MSRN added'

    p 'Start to calculate line-ranges'
    # Add line range to network
    ccit = NetworkRow.where('network_id = ? AND network_section_id BETWEEN 167 AND 169', network.id).all
    NetworkLineRange.where(network_id: network.id).delete_all
    if ccit
      ccit.each do |r|
        nr = NetworkLineRange.new
        case r.network_section_id
        when 168
          if r.data['MSRN - range_start'].length < 3 || r.data['MSRN - range_stop'].length < 3
            nr.regenerate = true
            p 'IT IS EMPTY'
          end
          nr.range_start = "#{r.data['MSRN - cc']}#{r.data['MSRN - ndc']}#{r.data['MSRN - range_start']}".to_i
          nr.range_stop  = "#{r.data['MSRN - cc']}#{r.data['MSRN - ndc']}#{r.data['MSRN - range_stop']}".to_i
          nr.line_type = 'MSRN'
          #nr.message = "cc: #{r.data['MSRN - cc']} ndc: #{r.data['MSRN - ndc']} start: #{r.data['MSRN - range_start']} stop: #{r.data['MSRN - range_stop']}"
        when 167
          if r.data['GT - range_start'].length < 3 || r.data['GT - range_stop'].length < 3
            nr.regenerate = true
            p 'IT IS EMPTY'
          end
          nr.range_start = "#{r.data['GT - cc']}#{r.data['GT - ndc']}#{r.data['GT - range_start']}".to_i
          nr.range_stop  = "#{r.data['GT - cc']}#{r.data['GT - ndc']}#{r.data['GT - range_stop']}".to_i
          nr.line_type = 'GT'
        when 169
          if r.data['MSISDN - range_start'].length < 3 || r.data['MSISDN - range_stop'].length < 3
            nr.regenerate = true
            p 'IT IS EMPTY'
          end
          nr.range_start = "#{r.data['MSISDN - cc']}#{r.data['MSISDN - ndc']}#{r.data['MSISDN - range_start']}".to_i
          nr.range_stop  = "#{r.data['MSISDN - cc']}#{r.data['MSISDN - ndc']}#{r.data['MSISDN - range_stop']}".to_i
          nr.line_type = 'MSISDN'
        end

        nr.orginate_id = r.id 
        nr.network_id  = network.id
        nr.save!
      end
      # Taking out unnecassary line ranges..
      evaluate_line_range_rules(network_tag, @organisation_tag)
    end
    p 'E164 number ranges done'
  end

  #### domestic sccpcarriers ####
  def import_domestic_sccp_carriers(network, network_tag)
    update_columns(percentage_done: 40)
    p 'Started importing domestic sccp carriers'
    tag = network_tag.xpath('DomesticSCCPGatewaySection/DomesticSCCPGatewayInfo/SCCPCarrierList/SCCPCarrierItem')

    tag.each do |carrier|
      name = carrier.xpath('SCCPCarrierName').first.text
      # delete old records
      network_section = NetworkSection.where(field_name: 'domestic_sccp_carrier').first
      import_change_history(tag, network_section)
      delete_old_network_rows(network_section, network)

      carrier.xpath('DPCList/DPCItem').each do |dpc_item|
        signature = dpc_item.xpath('SCSignature').text
        type = dpc_item.xpath('SCType').text
        dpc = dpc_item.xpath('DPC').text
        comment = dpc_item.xpath('Comments').text
        # save row
        network_row = NetworkRow.new
        network_row.network_id = network.id

        puts signature
        network_row.data = {
          'name' => name,
          'signature' => signature,
          'type' => type,
          'dpc' => dpc.tr!('.', '-'),
          'dpc-decimal' => convert_from_itu_format(dpc.to_s),
          'dpc-hex' => convert_from_itu_format(dpc.to_s).to_s(16),
          'comment' => comment
        }
        network_row.network_section_id = network_section.id
        network_row.save!
      end
      p 'OK - domestic sccp carriers are done'
    end
  end

  #### /domestic sccpcarriers ####

  #### Internationl sccp carriers ####
  def import_international_sccp_carriers(network, network_tag)
    update_columns(percentage_done: 50)
    sccp_tag = network_tag.xpath('InternationalSCCPGatewaySection').first
    p 'returning now!!!' unless sccp_tag
    p sccp_tag unless sccp_tag
    return unless sccp_tag

    # check for diff with last import
    path = 'InternationalSCCPGatewaySection'

    # delete old records
    network_section = NetworkSection.where(field_name: 'international_sccp_carrier').first
    check_for_diff(path, sccp_tag, network_section.id, network.tadig_code)
    import_change_history(sccp_tag, network_section)
    delete_old_network_rows(network_section, network)
    sccp_list = sccp_tag.xpath('InternationalSCCPGatewayInfo/InternationalSCCPCarrierList/SCCPCarrierItem')
    if sccp_list.blank?
      sccp_list = sccp_tag.xpath('SCCPCarrierList/SCCPCarrierItem')
    end
    network_section_child = NetworkSection.where(field_name: 'international_dpc_item').first
    delete_old_network_rows(network_section_child, network)
    sccp_list.each do |carrier|
      p 'importing carrier'
      name = carrier.xpath('SCCPCarrierName').first.text
      connectivity_types = { 'Primary' => 1, 'Secondary' => 2, 'Backup' => 3 }
      tadig_code_items = carrier.xpath('SCCPMNOsTADIGCodeList').text
      tadig_code_items = ' ' unless tadig_code_items
      if carrier.xpath('SCCPConnectivityInformation').first
        connectivity_type = connectivity_types[carrier.xpath('SCCPConnectivityInformation').first.text]
      else
        connectivity_type = ''
      end
      comment = carrier.xpath('SCCPCarrierComments').text

      # creating rows
      network_row = NetworkRow.new
      network_row.effective_date_of_change = sccp_tag.xpath('InternationalSCCPGatewayInfo/EffectiveDateOfChange').text
      network_row.network_id = network.id

      network_row.data = {
        'name' => name,
        'connectivity_type' => connectivity_type,
        'tadig_code_item' => tadig_code_items,
        'comment' => comment
      }
      network_row.network_section_id = network_section.id
      network_row.save!

      carrier.xpath('DPCList/DPCItem').each do |dpc_item|
        p 'saving dpc-children'
        network_row_child = NetworkRow.new
        network_row_child.network_id = network.id
        network_row_child.network_row_id = network_row.id
        network_row_child.data = parse_dpc_item(dpc_item)
        network_row_child.network_section_id = network_section_child.id
        network_row_child.save!
      end
    end
  end

  ##### /International sccp carriers ####

  #### Mobile application part ####
  def import_mobile_application_part(network, network_tag)
    if (map_tag = network_tag.xpath('MAPGeneralInfoSection/MAPGeneralInfo').first)
      network_section = NetworkSection.where(field_name: 'mobile_application_part').first
      import_change_history(map_tag, network_section)
      network_row = NetworkRow.new
      network_row.network_id = network.id
      delete_old_network_rows(network_section, network)
      network_row.data = {
        'network_loc_up_inbound_roaming_msc_vlr' => map_tag.xpath('NetworkLocUp/InboundRoamingMSC_VLR').first.text,
        'network_loc_up_outbound_roaming' => map_tag.xpath('NetworkLocUp/OutboundRoaming').first.text,
        'roaming_number_enquiry_inbound_roaming_msc_vlr' => map_tag.xpath('RoamingNumberEnquiry/InboundRoamingMSC_VLR').first.text,
        'roaming_number_enquiry_outbound_roaming' => map_tag.xpath('RoamingNumberEnquiry/OutboundRoaming').first.text,
        'info_retrieval_inbound_roaming_msc_vlr' => map_tag.xpath('InfoRetrieval/InboundRoamingMSC_VLR').first.text,
        'info_retrieval_inbound_roaming_sgsn' => map_tag.xpath('InfoRetrieval/InboundRoamingSGSN').first.text,
        'info_retrieval_outbound_roaming' => map_tag.xpath('InfoRetrieval/OutboundRoaming').first.text,
        'subscriber_data_mngt_inbound_roaming_msc_vlr' => map_tag.xpath('SubscriberDataMngt/InboundRoamingMSC_VLR').first.text,
        'subscriber_data_mngt_inbound_roaming_sgsn' => map_tag.xpath('SubscriberDataMngt/InboundRoamingSGSN').first.text,
        'subscriber_data_mngt_outbound_roaming' => map_tag.xpath('SubscriberDataMngt/OutboundRoaming').first.text,
        'network_functional_ss_inbound_roaming_msc_vlr' => map_tag.xpath('NetworkFunctionalSs/InboundRoamingMSC_VLR').first.text,
        'network_functional_ss_outbound_roaming' => map_tag.xpath('NetworkFunctionalSs/OutboundRoaming').first.text,
        'mwd_mngt_inbound_roaming_msc_vlr' => map_tag.xpath('MwdMngt/InboundRoamingMSC_VLR').first.text,
        'mwd_mngt_inbound_roaming_sgsn' => map_tag.xpath('MwdMngt/InboundRoamingSGSN').first.text,
        'mwd_mngt_outbound_roaming' => map_tag.xpath('MwdMngt/OutboundRoaming').first.text,
        'short_msg_mt_relay_inbound_roaming_msc_vlr' => map_tag.xpath('ShortMsgMTRelay/InboundRoamingMSC_VLR').first.text,
        'short_msg_mt_relay_inbound_roaming_sgsn' => map_tag.xpath('ShortMsgMTRelay/InboundRoamingSGSN').first.text,
        'short_msg_mt_relay_outbound_roaming' => map_tag.xpath('ShortMsgMTRelay/OutboundRoaming').first.text,
        'short_msg_mo_relay_inbound_roaming_msc_vlr' => map_tag.xpath('ShortMsgMORelay/InboundRoamingMSC_VLR').first.text,
        'short_msg_mo_relay_inbound_roaming_sgsn' => map_tag.xpath('ShortMsgMORelay/InboundRoamingSGSN').first.text,
        'short_msg_mo_relay_outbound_roaming' => map_tag.xpath('ShortMsgMORelay/OutboundRoaming').first.text,
        'ss_invocation_notification_inbound_roaming_msc_vlr' => map_tag.xpath('SsInvocationNotification/InboundRoamingMSC_VLR').first.text,
        'ss_invocation_notification_outbound_roaming' => map_tag.xpath('SsInvocationNotification/OutboundRoaming').first.text,
        'subscriber_info_enquiry_inbound_roaming_msc_vlr' => map_tag.xpath('SubscriberInfoEnquiry/InboundRoamingMSC_VLR').first.text,
        'subscriber_info_enquiry_inbound_roaming_sgsn' => map_tag.xpath('SubscriberInfoEnquiry/InboundRoamingSGSN').first.text,
        'subscriber_info_enquiry_outbound_roaming' => map_tag.xpath('SubscriberInfoEnquiry/OutboundRoaming').first.text,
        'gprs_location_update_inbound_roaming_sgsn' => map_tag.xpath('GprsLocationUpdate/InboundRoamingSGSN').first.text,
        'gprs_location_update_outbound_roaming' => map_tag.xpath('GprsLocationUpdate/OutboundRoaming').first.text,
        'location_cancellation_inbound_roaming_msc_vlr' => map_tag.xpath('LocationCancellation/InboundRoamingMSC_VLR').first.text,
        'location_cancellation_inbound_roaming_sgsn' => map_tag.xpath('LocationCancellation/InboundRoamingSGSN').first.text,
        'location_cancellation_outbound_roaming' => map_tag.xpath('LocationCancellation/OutboundRoaming').first.text,
        'ms_purging_inbound_roaming_msc_vlr' => map_tag.xpath('MsPurging/InboundRoamingMSC_VLR').first.text,
        'ms_purging_inbound_roaming_sgsn' => map_tag.xpath('MsPurging/InboundRoamingSGSN').first.text,
        'ms_purging_outbound_roaming' => map_tag.xpath('MsPurging/OutboundRoaming').first.text,
        'reset_inbound_roaming_msc_vlr' => map_tag.xpath('Reset/InboundRoamingMSC_VLR').first.text,
        'reset_inbound_roaming_sgsn' => map_tag.xpath('Reset/InboundRoamingSGSN').first.text,
        'reset_outbound_roaming' => map_tag.xpath('Reset/OutboundRoaming').first.text,
        'network_unstructured_ss_inbound_roaming_msc_vlr' => map_tag.xpath('NetworkUnstructuredSs/InboundRoamingMSC_VLR').first.text,
        'network_unstructured_ss_outbound_roaming' => map_tag.xpath('NetworkUnstructuredSs/OutboundRoaming').first.text,
        'reporting_inbound_roaming_msc_vlr' => map_tag.xpath('Reporting/InboundRoamingMSC_VLR').first.text,
        'reporting_outbound_roaming' => map_tag.xpath('Reporting/OutboundRoaming').first.text,
        'call_completion_inbound_roaming_msc_vlr' => map_tag.xpath('CallCompletion/InboundRoamingMSC_VLR').first.text,
        'call_completion_outbound_roaming' => map_tag.xpath('CallCompletion/OutboundRoaming').first.text,
        'ist_alerting_inbound_roaming_msc_vlr' => map_tag.xpath('IstAlerting/InboundRoamingMSC_VLR').first.text,
        'ist_alerting_outbound_roaming' => map_tag.xpath('IstAlerting/OutboundRoaming').first.text,
        'service_termination_inbound_roaming_msc_vlr' => map_tag.xpath('ServiceTermination/InboundRoamingMSC_VLR').first.text,
        'service_termination_outbound_roaming' => map_tag.xpath('ServiceTermination/OutboundRoaming').first.text,
        'location_svc_gateway_outbound_roaming' => map_tag.xpath('LocationSvcGateway/OutboundRoaming').first.text,
        'mm_event_reporting_inbound_roaming_msc_vlr' => map_tag.xpath('MmEventReporting/InboundRoamingMSC_VLR').first.text,
        'mm_event_reporting_outbound_roaming' => map_tag.xpath('MmEventReporting/OutboundRoaming').first.text,
        'authentication_failure_report_inbound_roaming_msc_vlr' => map_tag.xpath('AuthenticationFailureReport/InboundRoamingMSC_VLR').first.text,
        'authentication_failure_report_inbound_roaming_sgsn' => map_tag.xpath('AuthenticationFailureReport/InboundRoamingSGSN').first.text,
        'authentication_failure_report_outbound_roaming' => map_tag.xpath('AuthenticationFailureReport/OutboundRoaming').first.text,
        'imsi_retrieval_inbound_roaming_msc_vlr' => map_tag.xpath('ImsiRetrieval/InboundRoamingMSC_VLR').first.text,
        'imsi_retrieval_outbound_roaming' => map_tag.xpath('ImsiRetrieval/OutboundRoaming').first.text,
        'gprs_notify_context_inbound_roaming_sgsn' => map_tag.xpath('GprsNotifyContext/InboundRoamingSGSN').first.text,
        'gprs_notify_context_outbound_roaming' => map_tag.xpath('GprsNotifyContext/OutboundRoaming').first.text,
        'gprs_location_info_retrieval_inbound_roaming_sgsn' => map_tag.xpath('GprsLocationInfoRetrieval/InboundRoamingSGSN').first.text,
        'gprs_location_info_retrieval_outbound_roaming' => map_tag.xpath('GprsLocationInfoRetrieval/OutboundRoaming').first.text,
        'failure_report_inbound_roaming_sgsn' => map_tag.xpath('FailureReport/InboundRoamingSGSN').first.text,
        'failure_report_outbound_roaming' => map_tag.xpath('FailureReport/OutboundRoaming').first.text,
        'secure_transport_handling_inbound_roaming_msc_vlr' => map_tag.xpath('SecureTransportHandling/InboundRoamingMSC_VLR').first.text,
        'secure_transport_handling_inbound_roaming_sgsn' => map_tag.xpath('SecureTransportHandling/InboundRoamingSGSN').first.text,
        'secure_transport_handling_outbound_roaming' => map_tag.xpath('SecureTransportHandling/OutboundRoaming').first.text
      }
      network_row.network_section_id = network_section.id
      network_row.save!
    end
  end

  #### Import network nodes ####
  def import_network_nodes(network_tag, network)
    update_columns(percentage_done: 60)
    network_section_tag = network_tag.xpath('NetworkElementsInfoSection').first
    return unless network_section_tag
    network_elements_tag = network_section_tag.xpath('NetworkElementsInfo').first
    unless network_elements_tag
      network_elements_tag = network_tag.xpath('NetworkElementsInfoSection').first
    end
    return unless network_elements_tag
    network_section = NetworkSection.where(field_name: 'network_node').first
    import_change_history(network_section_tag, network_section)
    delete_old_network_rows(network_section, network)

    network_elements_tag.xpath('NwNodeList/NwNode').each do |node_tag|
      range_start = node_tag.xpath('GTAddressInfo/SN_Range/SN_RangeStart').text
      range_stop = node_tag.xpath('GTAddressInfo/SN_Range/SN_RangeStop').text
      network_row = NetworkRow.new
      network_row.network_id = network.id
      range = node_tag.xpath('IPAddressInfo/IPAddressRange').text
      network_row.effective_date_of_change = network_elements_tag.xpath('EffectiveDateOfChange').text
      network_row.data = {
        'node_type' => node_tag.xpath('NwElementType').text,
        'node_identifier' => node_tag.xpath('NodeID').text,
        'cc' => node_tag.xpath('GTAddressInfo/CC').text,
        'ndc' => node_tag.xpath('GTAddressInfo/NDC').text,
        'e164_gt_address_range' => ("#{range_start}#{'-' if range_stop}#{range_stop}" if range_start),
        'ip_address' => node_tag.xpath('IPAddressInfo/IPAddressRange').text,
        'vendor_info' => node_tag.xpath('VendorInfo').text,
        'sw_hw_version' => node_tag.xpath('SwHwVersion').text,
        'dual_access' => node_tag.xpath('DualAccess').text,
        'location' => node_tag.xpath('Location').text,
        'utc_offset' => node_tag.xpath('UTCTimeOffset').first.text,
        'dst_start_date' => node_tag.xpath('DST/DSTStartDate').text,
        'dst_end_date' => node_tag.xpath('DST/DSTEndDate').text
      }
      network_row.network_section_id = network_section.id
      network_row.save!
    end
  end

  #### /Import network nodes ####

  #### sccp_protocol_available_at_pmn ####
  def import_sccp_protocol(network, network_data_tag)
    update_columns(percentage_done: 70)
    if (tag = network_data_tag.xpath('SCCPProtocolAvailableAtPMNSection/SCCPProtocolAvailableAtPMN').first)
      network_section = NetworkSection.where(field_name: 'sccp_protocol_available_at_pmn').first
      import_change_history(tag, network_section)
      delete_old_network_rows(network_section, network)
      etsi_itu = tag.xpath('ETSI_ITU-T').first.text
      ansi = tag.xpath('ANSI').first.text
      # add to row
      network_row = NetworkRow.new
      network_row.network_id = network.id
      delete_old_network_rows(network_section, network)
      network_row.data = {
        'etsi' => etsi_itu,
        'ansi' => ansi
      }
      network_row.network_section_id = network_section.id
      network_row.save!
    end
  end

  #### sccp_protocol_available_at_pmn ####

  #### map_optimal_routing ####
  def import_map_optimal_routing(network, network_data_tag)
    map_routing_tag = network_data_tag.xpath('MAPOptimalRoutingSection/MAPOptimalRouting').first
    return unless map_routing_tag
    network_row = NetworkRow.new
    network_row.network_id = network.id
    network_section = NetworkSection.where(field_name: 'map_optimal_routing_section').first
    import_change_history(map_routing_tag, network_section)
    delete_old_network_rows(network_section, network)

    network_row.data = {
      'call_control_transfer_inbound_roaming_vmsc' => map_routing_tag.xpath('CallControlTransfer/InboundRoamingVMSC').first.text,
      'call_control_transfer_inbound_roaming_gmsc' => map_routing_tag.xpath('CallControlTransfer/InboundRoamingGMSC').first.text,
      'location_info_retrieval_inbound_roaming_gmsc' => map_routing_tag.xpath('LocationInfoRetrieval/InboundRoamingGMSC').first.text,
      'location_info_retrieval_outbound_roaming_hlr' => map_routing_tag.xpath('LocationInfoRetrieval/OutboundRoamingHLR').first.text
    }
    network_row.network_section_id = network_section.id
    network_row.save!
  end

  #### /map_optimal_routing ####

  #### import_test_numbers ####
  def import_test_numbers(network, network_data_tag)
    test_number_tag = network_data_tag.xpath('TestNumbersInfoSection/TestNumberInfo')
    return unless test_number_tag
    network_section = NetworkSection.where(field_name: 'test_numbers').first
    delete_old_network_rows(network_section, network)
    import_change_history(test_number_tag, network_section)
    test_number_tag.xpath('TestNumberList/TestNumber').each do |test_number_tag|
      p 'running one time'
      number_type = test_number_tag.xpath('NumberType').first.text
      number = test_number_tag.xpath('Number').first.text
      location = test_number_tag.xpath('Location').text
      comment = test_number_tag.xpath('Comments').text
      network_row = NetworkRow.new
      network_row.network_id = network.id
      network_row.data = {
        'number_type' => number_type,
        'number' => number,
        'location' => location,
        'comment' => comment
      }
      network_row.network_section_id = network_section.id
      network_row.save!
    end
  end

  #### /import_test_numbers ####

  #### import subscriber identity ####
  def import_subscriber_identity_authentications(network, network_data_tag)
    update_columns(percentage_done: 80)
    if (tag = network_data_tag.xpath('SubscrIdentityAuthenticationSection/SubscrIdentityAuthenticationInfo').first)
      network_section = NetworkSection.where(field_name: 'subscriber_identity_authentication').first
      delete_old_network_rows(network_section, network)
      import_change_history(tag, network_section)
      network_data_tag.xpath('SubscrIdentityAuthenticationSection/SubscrIdentityAuthenticationInfo').each do |subscribe|
        gsm_authentication  = subscribe.xpath('GSMAuthentication').text.try(:==, 'Yes')
        gprs_authentication = subscribe.xpath('GPRSAuthentication').text.try(:==, 'Yes')
        a5_cipher_algorithm_version = tag.xpath('A5CipherAlgorithmVersion').text
        network_row = NetworkRow.new
        network_row.network_id = network.id
        network_row.data = {
          'gprs_authentication' => gprs_authentication,
          'gsm_authentication' => gsm_authentication,
          'a5_cipher_algorithm_version' => a5_cipher_algorithm_version
        }
        network_row.network_section_id = network_section.id
        network_row.save!
      end
    end
  end

  #### /import_subscriber_identity ####

  #### import_map_inter_operator_sms_enhancements ####
  def import_map_inter_operator_sms_enhancements(network, network_data_tag)
    update_columns(percentage_done: 90)
    map_routing_tag = network_data_tag.xpath('MAPInterOperatorSMSEnhancementSection/MAPInterOperatorSMSEnhancement').first
    return unless map_routing_tag
    network_section = NetworkSection.where(field_name: 'map_inter_operator_enhancement').first
    import_change_history(map_routing_tag, network_section)
    # add to row
    network_row = NetworkRow.new
    network_row.network_id = network.id
    delete_old_network_rows(network_section, network)
    network_row.data = {
      'short_msg_gateway_inbound_roaming_sms_gmsc' => map_routing_tag.xpath('ShortMsgGateway/InboundRoamingSMS_GMSC').first.text,
      'short_msg_gateway_outbound_roaming_hlr' => map_routing_tag.xpath('ShortMsgGateway/OutboungRoamingHLR').first.text,
      'short_msg_alert_inbound_roaming_sms_iwmsc' => map_routing_tag.xpath('ShortMsgAlert/InboundRoamingSMS_IWMSC').first.text,
      'short_msg_alert_outbound_roaming_hlr' => map_routing_tag.xpath('ShortMsgAlert/OutboungRoamingHLR').first.text
    }
    network_row.network_section_id = network_section.id
    network_row.save!
  end

  #### /import_map_inter_operator_sms_enhancements ####

  #### import_ussd_info ####
  def import_ussd_info(network, network_data_tag)
    ussd_info_tag = network_data_tag.xpath('USSDInfoSection/USSDInfo').first
    network_section = NetworkSection.where(field_name: 'ussd_info').first
    return unless ussd_info_tag
    import_change_history(ussd_info_tag, network_section)
    # add to row
    network_row = NetworkRow.new
    network_row.network_id = network.id
    delete_old_network_rows(network_section, network)
    ussd_info = ussd_info_tag.xpath('SupportedUSSDPhase').first.text if ussd_info_tag.xpath('SupportedUSSDPhase').first
    network_row.data = {
      'available' => ussd_info_tag.xpath('IsUSSDCapabilityAvailable').first.text,
      'supported_phase' => ussd_info
    }
    p 'importing ussd'
    network_row.network_section_id = network_section.id
    network_row.save!
    p network_row
  end

  #### /import_ussd_info ####

  #### import_camel_information ####
  def import_camel_information(network, network_data_tag)
    if (camel_information_tag = network_data_tag.xpath('CAMELInfoSection/CAMELInfo').first)
      network_section = NetworkSection.where(field_name: 'camel_information').first
      import_change_history(camel_information_tag, network_section)
      # add to row
      network_row = NetworkRow.new
      network_row.network_id = network.id
      delete_old_network_rows(network_section, network)
      network_row.data = {
        'inbound' => camel_information_tag.xpath('GSM_SSF_MSC/CAP_Version_Supported_MSC/CAP_Ver_Supp_MSC_Inbound/CAP_MSCVersion').text,
        'outbound' => camel_information_tag.xpath('GSM_SSF_MSC/CAP_Version_Supported_MSC/CAP_Ver_Supp_MSC_Outbound/CAP_MSCVersion').text
      }
      network_row.network_section_id = network_section.id
      network_row.save!
      # import_camel_re_routing_numbers(camel_information_tag, camel_information)
      # import_camel_functionalities(camel_information_tag, camel_information)
      # import_change_histories(network, ChangeHistory::Section::CAMEL_INFO, camel_information_tag)
    end
  end

  #### /import_camel_information ####

  #### import_supported_data_service_items ####
  def import_supported_data_service_items(network, network_data_tag)
    tag = network_data_tag.xpath('DataServicesSupportedList/DataServicesSupportedItem')
    return unless tag
    network_row = NetworkRow.new
    network_row.network_id = network.id
    network_section = NetworkSection.where(field_name: 'support_data_service_item').first
    delete_old_network_rows(network_section, network)

    import_change_history(tag, network_section)
    tag.each do |sdsi_tag|
      service_name = sdsi_tag.xpath('DataServiceSupported').first.text
      network_row.data = {
        'data_service_supported' => service_name
      }
      network_row.network_section_id = network_section.id
      network_row.save!
    end
  end

  #### /import_supported_data_service_items ####

  #### import_ip_roaming_iw_info ####
  def import_ip_roaming_iw_info(network, network_data_tag)
    ip_roaming_iw_info_tag = network_data_tag.xpath('IPRoaming_IW_InfoSection/IPRoaming_IW_Info').first
    path = 'IPRoaming_IW_InfoSection/IPRoaming_IW_Info'
    unless ip_roaming_iw_info_tag
      ip_roaming_iw_info_tag = network_data_tag.xpath('IPRoaming_IW_InfoSection/IPRoaming_IW_Info_General').first
      path = 'IPRoaming_IW_InfoSection/IPRoaming_IW_Info_General'
    end
    return unless ip_roaming_iw_info_tag
    network_section = NetworkSection.where(field_name: 'ip_roaming_info').first
    import_change_history(ip_roaming_iw_info_tag, network_section)

    # check for diff with last import
    check_for_diff(path, ip_roaming_iw_info_tag, network_section.id, network.tadig_code)

    delete_old_network_rows(network_section, network)

    ip_roaming_iw_info_tag.xpath('InterPMNBackboneIPList/IPAddressOrRange').each do |tag|
      p 'saving ip-roaming'
      # add to row
      network_row = NetworkRow.new
      network_row.network_id = network.id
      network_row.network_section_id = network_section.id
      network_row.effective_date_of_change = ip_roaming_iw_info_tag.xpath('EffectiveDateOfChange').text
      range = tag.xpath('IPAddressRange').text
      range = tag.xpath('IPAddress').text if range.blank?
      network_row.data = {
        'ip_address/range' => range
      }
      network_row.save!
    end
  end
  #### /import_ip_roaming_iw_info ####

  #### import_attached_networks ####
  # TODO(jonas): This is not active
  def import_attached_networks(ip_roaming_iw_info_tag, ip_roaming_iw_info)
    ip_roaming_iw_info.attached_networks.destroy_all

    ip_roaming_iw_info_tag.xpath('SectionAttachedNetworkList/tadigCodeItem').each do |tadig_code|
      ip_roaming_iw_info.attached_networks.create! tadig_code: tadig_code.text
    end
  end

  #### /import_attached_networks ####

  #### import_autonomous_system_numbers ####
  def import_autonomous_system_numbers(network, network_data_tag)
    network_section = NetworkSection.where(field_name: 'asn_list').first
    delete_old_network_rows(network_section, network)
    tag = network_data_tag.xpath('IPRoaming_IW_InfoSection/IPRoaming_IW_Info_General/ASNsList/ASN')
    unless tag
      tag = network_data_tag.xpath('IPRoaming_IW_InfoSection/IPRoaming_IW_Info/ASNsList/ASN')
    end
    import_change_history(tag, network_section)
    tag.each do |asn|
      # add to row
      network_row = NetworkRow.new
      network_row.data = {
        'asn' => asn.text
      }
      network_row.network_section_id = network_section.id
      network_row.network_id = network.id
      network_row.save!
    end
  end

  #### /import_autonomous_system_numbers ####

  #### import_pmn_authoritative_dns_ips ####
  def import_pmn_authoritative_dns_ips(network, network_data_tag)
    p 'WEEE ARE RUNNING IT'
    network_section = NetworkSection.where(field_name: 'pmn_authoritative_dns_ip_list').first
    delete_old_network_rows(network_section, network)
    # Selective path for IW_INFO/Generel
    tag = network_data_tag.xpath('IPRoaming_IW_InfoSection/IPRoaming_IW_Info/PMNAuthoritativeDNSIPList/DNSitem')
    unless tag
      tag = network_data_tag.xpath('IPRoaming_IW_InfoSection/IPRoaming_IW_Info_General/PMNAuthoritativeDNSIPList/DNSitem')
    end
    import_change_history(tag, network_section)
    tag.each do |dns_item|
      p 'saving pmn'
      # add to row
      network_row = NetworkRow.new
      network_row.data = {
        'ip_address' => dns_item.xpath('IPAddress').text,
        'dns_name' => dns_item.xpath('DNSname').text
      }
      network_row.network_section_id = network_section.id
      network_row.network_id = network.id
      network_row.save!
    end
  end

  #### /import_pmn_authoritative_dns_ips ####

  #### import_ping_traceroute_ips ####
  def import_ping_traceroute_ips(network, network_data_tag)
    network_section = NetworkSection.where(field_name: 'ping_traceroute_ip_address_list').first
    delete_old_network_rows(network_section, network)
    tag = network_data_tag.xpath('PingTracerouteIPAddressList/PingTracerouteIPAddressItem')
    import_change_history(tag, network_section)
    tag.each do |ip|
      # add to row
      network_row = NetworkRow.new
      network_row.data = {
        'address' => ip.text
      }
      network_row.network_section_id = network_section.id
      network_row.network_id = network.id
      network_row.save!
    end
  end

  #### /import_ping_traceroute_ips ####

  #### import_grx_providers ####
  def import_grx_providers(network, network_data_tag)
    network_section = NetworkSection.where(field_name: 'grx_provider').first
    delete_old_network_rows(network_section, network)
    # Selective Path if 8.4 (without General)
    tag = network_data_tag.xpath('IPRoaming_IW_InfoSection/IPRoaming_IW_Info_General/GRXProvidersList/GRXProvider')
    unless tag
      tag = network_data_tag.xpath('IPRoaming_IW_InfoSection/IPRoaming_IW_Info/GRXProvidersList/GRXProvider')
    end
    import_change_history(tag, network_section)
    tag.each do |grx_provider|
      # add to row
      network_row = NetworkRow.new
      network_row.data = {
        'provider' => grx_provider.text
      }
      network_row.network_section_id = network_section.id
      network_row.network_id = network.id
      network_row.save!
    end
  end

  #### /import_grx_providers ####

  #### import_diameter_edge_agent_ips ####
  def import_diameter_edge_agent_ips(network, network_data_tag)
    network_section = NetworkSection.where(field_name: 'diameter_edge_agent').first
    delete_old_network_rows(network_section, network)

    # Clean children sections
    @network_sub_section_1 = NetworkSection.where(field_name: 'diameter_primary_ip_address').first
    delete_old_network_rows(@network_sub_section_1, network)
    @network_sub_section_2 = NetworkSection.where(field_name: 'diameter_secondary_ip_address').first
    delete_old_network_rows(@network_sub_section_2, network)

    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/RoamingInterconnection/IPAddressesDiameterEdgeAgent/IPAddress')
    # TODO[jonas] - Conditions depending on xml file version
    if @network.raex_ir21_schema_version = '9.2'
      p 'Importing newer ============= =  == ========================'
      tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/RoamingInterconnection/Diameter/FQDNDiameterEdgeAgent')
      import_change_history(tag, network_section)
      tag.each do |ip|
        # add to row
        @network_row = NetworkRow.new
        @network_row.data = {
          'FQDN' => ip.xpath('FQDN').text
        }
        @network_row.network_section_id = network_section.id
        @network_row.network_id = network.id
        @network_row.save!

        # Add children section primary
        ip.xpath('PrimaryIPAddressDEAFQDN/IPAddress').each do |primary|
          network_sub_row_1 = NetworkRow.new
          network_sub_row_1.data = {
            'ip_address' => primary.text
          }
          network_sub_row_1.network_section_id = @network_sub_section_1.id
          network_sub_row_1.network_id = network.id
          network_sub_row_1.network_row_id = @network_row.id
          network_sub_row_1.save
        end

        # Add children section secondary
        ip.xpath('SecondaryIPAddressDEAFQDN/IPAddress').each do |primary|
          network_sub_row_2 = NetworkRow.new
          network_sub_row_2.data = {
            'ip_address' => primary.text
          }
          network_sub_row_2.network_section_id = @network_sub_section_2.id
          network_sub_row_2.network_id = network.id
          network_sub_row_2.network_row_id = @network_row.id
          network_sub_row_2.save
        end
      end
    else
      import_change_history(tag, network_section)
      tag.each do |ip|
        # add to row
        network_row = NetworkRow.new
        network_row.data = {
          'ip' => ip.text
        }
        network_row.network_section_id = network_section.id
        network_row.network_id = network.id
        network_row.save!
      end
    end
  end

  #### /import_diameter_edge_agent_ips ####

  #### import_mms_iw_info ####
  def import_mms_iw_info(network, network_data_tag)
    mms_iw_info_tag = network_data_tag.xpath('MMS_IW_InfoSection/MMS_IW_Info').first
    return unless mms_iw_info_tag
    network_section = NetworkSection.where(field_name: 'mms_iw_info').first
    delete_old_network_rows(network_section, network)
    import_change_history(mms_iw_info_tag, network_section)
    mms_iw_info_tag.xpath('MMSE_List/MMSE').each do |mms_iw_tag|
      next if mms_iw_tag.nil?
      mmsc_domain_name = mms_iw_tag.xpath('MMSCDomainName').first.text
      mmsc_ip_range = mms_iw_tag.xpath('MMSC_IP_AddressRange').first.text
      max_mms_size_allowed = mms_iw_tag.xpath('MaxMMSSizeAllowed').text
      delivery_report_allowed = mms_iw_tag.xpath('DeliveryReportAllowed').first.text
      read_report_allowed = mms_iw_tag.xpath('ReadReportAllowed').first.text
      incoming_iplist = mms_iw_tag.xpath('Incoming_MTA_IPList').first.text.to_s
      outgoing_iplist = mms_iw_tag.xpath('Outgoing_MTA_IPList').first.text.to_s
      mms_iw_hublist = mms_iw_tag.xpath('MMS_IW_HubList').text
      mms_iw_hublist = '' unless mms_iw_hublist
      # add to row
      network_row = NetworkRow.new
      network_row.data = {
        'domain_name' => mmsc_domain_name,
        'ip_address_range' => mmsc_ip_range,
        'size_allowed' => max_mms_size_allowed,
        'delivery_report_allowed' => delivery_report_allowed,
        'read_report_allowed' => read_report_allowed,
        'incoming_iplist' => incoming_iplist,
        'outgoing_iplist' => outgoing_iplist,
        'mms_iw_hublist' => mms_iw_hublist
      }
      network_row.network_section_id = network_section.id
      network_row.network_id = network.id
      network_row.save!
    end
  end

  #### /import_mms_iw_elements ####

  #### import_wlan_info ####
  def import_wlan_info(network, network_data_tag)
    network_section = NetworkSection.where(field_name: 'wlan_info').first
    delete_old_network_rows(network_section, network)
    wlan_info_tag = network_data_tag.xpath('WLANInfoSection/WLANInfo').first
    return unless wlan_info_tag
    import_change_history(wlan_info_tag, network_section)
    network_row = NetworkRow.new
    network_row.data = {
      'wlan_info' => Hash.from_xml(wlan_info_tag.to_xml)
    }
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  #### /import_wlan_info ####

  #### import_incoming_radius_ips ####
  def import_incoming_radius_ips(network, network_data_tag)
    network_section = NetworkSection.where(field_name: 'radius_server').first
    delete_old_network_rows(network_section, network)
    tag = network_data_tag.xpath('RADIUSServerAddressing/IncTraffic_RADIUSIPList/IPAddress')
    import_change_history(tag, network_section)
    tag.each do |ip|
      # add to row
      network_row = NetworkRow.new
      network_row.data = {
        'inoming-ip' => ip.text
      }
      network_row.network_section_id = network_section.id
      network_row.network_id = network.id
      network_row.save!
    end
    tag = network_data_tag.xpath('RADIUSServerAddressing/OutgTraffic_RADIUSIPList/IPAddress')
    import_change_history(tag, network_section)
    tag.each do |ip|
      # add to row
      network_row = NetworkRow.new
      network_row.data = {
        'outgoing-ip' => ip.text
      }
      network_row.network_section_id = network_section.id
      network_row.network_id = network.id
      network_row.save!
    end
  end

  #### /import_incoming_radius_ips ####

  #### import_wlan_roaming_ip_ranges ####
  def import_wlan_roaming_ip_ranges(network, network_data_tag)
    network_section = NetworkSection.where(field_name: 'signaling_ip_list').first
    delete_old_network_rows(network_section, network)
    tag = network_data_tag.xpath('WLANRoamSignallingIPList/IPAddressRange')
    import_change_history(tag, network_section)
    tag.each do |ip_range|
      # add to row
      network_row = NetworkRow.new
      network_row.data = {
        'signaling-ip' => ip_range.text
      }
      network_row.network_section_id = network_section.id
      network_row.network_id = network.id
      network_row.save!
    end
  end

  #### /import_wlan_roaming_ip_ranges ####

  def import_wlan_service_brands(network, network_data_tag)
    update_columns(percentage_done: 100)
    network_section = NetworkSection.where(field_name: 'service_brand_info').first
    delete_old_network_rows(network_section, network)
    tag = network_data_tag.xpath('WLANServiceBrandInfoList/WLANServiceBrandInfo')
    import_change_history(tag, network_section)
    tag.each do |wlan_brand|
      # add to row
      network_row = NetworkRow.new
      network_row.data = {
        'Realm' => wlan_brand.xpath('Realm').text,
        'brand_name' => wlan_brand.xpath('WLANServiceBrandName').text
      }
      network_row.network_section_id = network_section.id
      network_row.network_id = network.id
      network_row.save!
    end
  end

  def import_lte_roaming_info(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'roaming_inter_connection').first

    # Check diff with last imported network
    path = 'LTEInfoSection/LTEInfo'
    check_for_diff(path, tag, network_section.id, network.tadig_code)

    delete_old_network_rows(network_section, network)
    import_change_history(tag, network_section)
    roaming_inter_connection = tag.xpath('RoamingInterconnection')
    return unless roaming_inter_connection
    hostnames = roaming_inter_connection.xpath('S6a/HostnamesHSS_MMEList/HostName')
    hostnames_pcrf = roaming_inter_connection.xpath('S9/HostnamesPCRFList/HostName')
    effective_date_of_change = tag.xpath('EffectiveDateOfChange').text
    network_row = NetworkRow.new
    network_row.effective_date_of_change = effective_date_of_change
    unless @network.raex_ir21_schema_version = '11.1'
      network_row.data = {
        'supported_without_iwf' => roaming_inter_connection.xpath('S6a/IsS6aSupportedWithoutIWF').first.text,
        'supported_to_hss' => roaming_inter_connection.xpath('S6a/IsMAP_IWFsupportedToHSS').first.text,
        'supported_to_mme' => roaming_inter_connection.xpath('S6a/IsMAP_IWFsupportedToMME').first.text,
        'legacy_sgsn' => roaming_inter_connection.xpath('S6d/IsS6dUsedForLegacySGSN').first.text,
        'used_for_ppc' => roaming_inter_connection.xpath('S9/IsS9usedForPCC').first.text,
        'gtp_interface_avaliable' => roaming_inter_connection.xpath('S8/IsGTPinterfaceAvailable').first.text,
        'pmip_interface_avaliable' => roaming_inter_connection.xpath('S8/IsPMIPinterfaceAvailable').first.text
      }
    else
      network_row.data = {
        'supported_without_iwf' => roaming_inter_connection.xpath('S6a/IsS6aSupportedWithoutIWF').first.text,
        'supported_to_hss' => roaming_inter_connection.xpath('S6a/IsMAP_IWFsupportedToHSS').first.text,
        'supported_to_mme' => roaming_inter_connection.xpath('S6a/IsMAP_IWFsupportedToMME').first.text,
        'legacy_sgsn' => roaming_inter_connection.xpath('S6d/IsS6dUsedForLegacySGSN').first.text,
        'used_for_ppc' => roaming_inter_connection.xpath('S9/IsS9usedForPCC').first.text,
      }
    end
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  def import_sms_itw(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/SMS_ITW').first
    return unless tag

    network_section = NetworkSection.where(field_name: 'sms_itw').first
    delete_old_network_rows(network_section, network)
    sms_over_ip = tag.xpath('SMS_DeliveryMechanisms/SMSoverIP').text || ' '
    sms_over_sgs = tag.xpath('SMS_DeliveryMechanisms/SMSoverSGs').text || ' '
    network_row = NetworkRow.new
    network_row.data = {
      'sms_over_ip' => sms_over_ip,
      'sms_over_sgs' => sms_over_sgs
    }
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  def import_voice_itw(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/SMS_ITW').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'voice_itw').first
    delete_old_network_rows(network_section, network)
    voice_itw = tag.xpath('Voice_ITW').first
    return unless voice_itw
    network_row = NetworkRow.new
    network_row.data = { 'voice_itw' => voice_itw.text }
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  def import_roaming_retry(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/RoamingRetry').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'roaming_retry').first
    delete_old_network_rows(network_section, network)
    roaming_retry_supported = tag.xpath('IsRoamingRetrySupported').first.text
    network_row = NetworkRow.new
    network_row.data = { 'is_roaming_retry_supported' => roaming_retry_supported }
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  def import_hpmn_info_lte_agreement_only(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/HPMNInfoLTERoamingAgreementOnly/IsLTEOnlyRoamingSupported').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'hpmn_info_lte_roaming_agreement_only').first
    delete_old_network_rows(network_section, network)
    network_row = NetworkRow.new
    network_row.data = { 'lte_only_roaming' => tag.text }
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  def import_vpmn_lte_only_roaming_supported(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/VPMNInfoLTERoamingAgreementOnly/IsLTEOnlyRoamingSupported').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'vpmn_lte_info_roaming_agreement').first
    delete_old_network_rows(network_section, network)
    network_row = NetworkRow.new
    network_row.data = { 'vpmn_only_roaming' => tag.text }
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  def import_hpmn_info_2g_3g_roaming_agreement_only(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/HPMNInfo2G3GRoamingAgreementOnly').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'hpmn_info_2g_3g_roaming_agreement_only').first
    delete_old_network_rows(network_section, network)
    scenario_1 = tag.xpath('IsScenario1Supported').text || ' '
    scenario_2 = tag.xpath('IsScenario2Supported').text || ' '
    scenario_3 = tag.xpath('IsScenario3Supported').text || ' '
    scenario_4 = tag.xpath('IsScenario4Supported').text || ' '
    network_row = NetworkRow.new
    network_row.data = {
      'scenario_1_supported' => scenario_1,
      'scenario_2_supported' => scenario_2,
      'scenario_3_supported' => scenario_3,
      'scenario_4_supported' => scenario_4
    }
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  def import_vpmn_info_2g_3g_roaming_agreement_only(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/VPMNInfo2G3GRoamingAgreementOnly').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'vpmn_info_2g_3g_roaming_agreement_only').first
    delete_old_network_rows(network_section, network)
    scenario_1 = tag.xpath('IsScenario1Supported').text || ' '
    scenario_2 = tag.xpath('IsScenario2Supported').text || ' '
    scenario_3 = tag.xpath('IsScenario3Supported').text || ' '
    scenario_4 = tag.xpath('IsScenario4Supported').text || ' '
    network_row = NetworkRow.new

    network_row.data = {
      'scenario_1_supported' => scenario_1,
      'scenario_2_supported' => scenario_2,
      'scenario_3_supported' => scenario_3,
      'scenario_4_supported' => scenario_4
    }
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  def import_hpmn_info_2g_3g_lte_roaming_agreement(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/HPMNInfo2G3GLTERoamingAgreement').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'hpmn_info_2g_3g_lte_roaming_agreement').first
    delete_old_network_rows(network_section, network)
    scenario_1 = tag.xpath('IsScenario1Supported').text || ' '
    scenario_2 = tag.xpath('IsScenario2Supported').text || ' '
    scenario_3 = tag.xpath('IsScenario3Supported').text || ' '
    scenario_4 = tag.xpath('IsScenario4Supported').text || ' '
    network_row = NetworkRow.new
    network_row.data = {
      'scenario_1_supported' => scenario_1,
      'scenario_2_supported' => scenario_2,
      'scenario_3_supported' => scenario_3,
      'scenario_4_supported' => scenario_4
    }
    effective_date_of_change = network_data_tag.xpath('LTEInfoSection/LTEInfo/EffectiveDateOfChange').text
    network_row.network_section_id = network_section.id
    network_row.effective_date_of_change = effective_date_of_change
    network_row.network_id = network.id
    network_row.save!
  end

  def import_vpmn_info_2g_3g_lte_roaming_agreement(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/VPMNInfo2G3GLTERoamingAgreement').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'vpmn_info_2g_3g_lte_roaming_agreement').first
    delete_old_network_rows(network_section, network)
    scenario_1 = tag.xpath('IsScenario1Supported').text || ' '
    scenario_2 = tag.xpath('IsScenario2Supported').text || ' '
    scenario_3 = tag.xpath('IsScenario3Supported').text || ' '
    scenario_4 = tag.xpath('IsScenario4Supported').text || ' '
    network_row = NetworkRow.new
    network_row.data = {
      'scenario_1_supported' => scenario_1,
      'scenario_2_supported' => scenario_2,
      'scenario_3_supported' => scenario_3,
      'scenario_4_supported' => scenario_4
    }
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  def import_lteq_os_profile_list(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/LTEQosProfileList/LTEQosProfile').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'lteq_os_profile_list').first
    delete_old_network_rows(network_section, network)

    profile = tag.xpath('Profile').text || ' '
    qci_value   = tag.xpath('QCIValue').text || ' '
    qsoarp_list = tag.xpath('QOSARPList').text || ' '

    network_row = NetworkRow.new
    network_row.data = { 'profile' => profile,
     'qci_value' => qci_value,
     'qsoarp_list' => qsoarp_list }
     network_row.network_section_id = network_section.id
     network_row.network_id = network.id
     network_row.save!
   end

   def import_ipv6_connectivity_information(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/IPv6ConnectivityInformation').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'ipv6_connectivity_information').first
    delete_old_network_rows(network_section, network)
    mme_ipv6_supported        = tag.xpath('MMESupport/IPv6PDPSupported').text || ' '
    mme_ipv4_v6_pdp_supported = tag.xpath('MMESupport/IPv4v6PDPSupported').text || ' '
    sgw_ipv6_dpd_supported    = tag.xpath('SGWSupport/IPv6PDPSupported').text || ' '
    sgw_ipv4_v6_pdp_supported = tag.xpath('SGWSupport/IPv4v6PDPSupported').text || ' '
    pgw_ipv6_pdp_supported    = tag.xpath('PGWSupport/IPv6PDPSupported').text || ' '
    pgw_ipv4_v6_pdp_supported = tag.xpath('PGWSupport/IPv4v6PDPSupported').text || ' '
    network_row = NetworkRow.new
    network_row.data = {
      'mme_ipv6_supported'        => mme_ipv6_supported,
      'mme_ipv4v6_supported'      => mme_ipv4_v6_pdp_supported,
      'sgw_ipv6_dpd_supported'    => sgw_ipv6_dpd_supported,
      'sgw_ipv4_v6_pdp_supported' => sgw_ipv4_v6_pdp_supported,
      'pgw_ipv6_pdp_supported'    => pgw_ipv4_v6_pdp_supported,
      'pgw_ipv4_v6_pdp_supported' => pgw_ipv4_v6_pdp_supported
    }
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  def import_ipx_inter_connection_info(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/IPXInterconnectionInfo').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'ipx_inter_connection_info').first
    delete_old_network_rows(network_section, network)
    provider_name         = tag.xpath('IPXProviderName').text
    realm_used            = tag.xpath('IPXDEARealmUsed').text
    diameter_architecture = tag.xpath('DiameterArchitecture').text
    tag.xpath('IPAddressofDEA/PrimaryIPAddressDEA/IPAddress').each_with_index do |ip, index|
      @primary_ip_1 = ip.text if index.zero?
      p "priiiiammmaaaaaryy:: #{@primary_ip_1}"
      @primary_ip_2 = ip.text if index == 1
      @primary_ip_3 = ip.text if index == 2
      @primary_ip_4 = ip.text if index == 3
    end
    tag.xpath('IPAddressofDEA/SecondaryIPAddressDEA/IPAddress').each_with_index do |ip, index|
      @secondary_ip_1 = ip if index.zero?
      @secondary_ip_2 = ip if index == 1
      @secondary_ip_3 = ip if index == 2
      @secondary_ip_4 = ip if index == 3
    end
    network_row = NetworkRow.new
    network_row.data = {
      'provider_name' => provider_name,
      'realm-used'    => realm_used,
      'diameter_architecture' => diameter_architecture,
      'primary_ip_1' => (@primary_ip_1 if @primary_ip_1),
      'primary_ip_2' => (@primary_ip_2 if @primary_ip_2),
      'primary_ip_3' => (@primary_ip_3 if @primary_ip_3),
      'primary_ip_4' => (@primary_ip_4 if @primary_ip_4),
      'secondary_ip_1' => (@secodnary_ip_1 if @secondary_ip_1),
      'secondary_ip_2' => (@secodnary_ip_2 if @secondary_ip_2),
      'secondary_ip_3' => (@secodnary_ip_3 if @secondary_ip_3),
      'secondary_ip_4' => (@secodnary_ip_4 if @secondary_ip_4)
    }.reject { |_k, v| v.nil? }

    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  def import_epc_realms_for_roaming(network, network_data_tag)
    network_section = NetworkSection.where(field_name: 'epc_realms_for_roaming').first
    delete_old_network_rows(network_section, network)

    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/RoamingInterconnection/Diameter/EPCRealmsForRoamingList/EPCRealmsForRoaming').each do |realm|
      return unless realm
      network_row = NetworkRow.new
      address = realm.text
      network_row.data = {
        'realm' => address
      }
      network_row.network_section_id = network_section.id
      network_row.network_id = network.id
      network_row.save!
    end
  end

  def import_diameter_certificates_exchange(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/DiameterCertificatesExchange').first
    return unless tag
    return if tag.first.nil?
    network_section = NetworkSection.where(field_name: 'diameter_certificates_exchange').first
    delete_old_network_rows(network_section, network)
    network_row = NetworkRow.new
    network_row.data = { 'ipv6 connectivity' => Hash.from_xml(tag.to_xml) }
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.save!
  end

  def import_lte_information(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/LTEInformation').first
    return unless tag
    network_section = NetworkSection.where(field_name: 'lte_information').first
    effective_date_of_change = network_data_tag.xpath('LTEInfoSection/LTEInfo/EffectiveDateOfChange').text
    # Add effective date of change to parent section
    NetworkRow.new(effective_date_of_change: effective_date_of_change,
     network_section_id:  network_section.id,
     network_id: network.id)
    delete_old_network_rows(network_section, network)
    qci_value1_supported = tag.xpath('QCIValue1Supported').text || ' '
    qci_value2_supported = tag.xpath('QCIValue2Supported').text || ' '
    qci_value3_supported = tag.xpath('QCIValue3Supported').text || ' '
    qci_value4_supported = tag.xpath('QCIValue4Supported').text || ' '
    qci_value5_supported = tag.xpath('QCIValue5Supported').text || ' '
    qci_value6_supported = tag.xpath('QCIValue6Supported').text || ' '
    qci_value7_supported = tag.xpath('QCIValue7Supported').text || ' '
    qci_value8_supported = tag.xpath('QCIValue8Supported').text || ' '
    qci_value9_supported = tag.xpath('QCIValue9Supported').text || ' '
    network_row = NetworkRow.new
    network_row.data = {
      'qci_value1_supported' => qci_value1_supported,
      'qci_value2_supported' => qci_value2_supported,
      'qci_value3_supported' => qci_value3_supported,
      'qci_value4_supported' => qci_value4_supported,
      'qci_value5_supported' => qci_value5_supported,
      'qci_value6_supported' => qci_value6_supported,
      'qci_value7_supported' => qci_value7_supported,
      'qci_value8_supported' => qci_value8_supported,
      'qci_value9_supported' => qci_value9_supported
    }
    network_row.network_section_id = network_section.id
    network_row.network_id = network.id
    network_row.effective_date_of_change = effective_date_of_change
    network_row.save!
  end

  #### import_s6a_hss_mme_hostnames ####
  def import_s6a_hss_mme_hostnames(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/RoamingInterconnection/S6a/HostnamesHSS_MMEList/HostName')
    return unless tag
    network_section = NetworkSection.where(field_name: 'mme_hostnames').first
    delete_old_network_rows(network_section, network)
    tag.each do |t|
      network_row = NetworkRow.new
      network_row.data = {
        'hostname' => t.text
      }
      network_row.network_section_id = network_section.id
      network_row.network_id = network.id
      network_row.save
    end
  end
  #### /import_s6a_hss_mme_hostnames ####

  #### DONE TO HERE ####

  #### import_s9_pcrf_hostnames ####
  def import_s9_pcrf_hostnames(network, network_data_tag)
    tag = network_data_tag.xpath('LTEInfoSection/LTEInfo/RoamingInterconnection/S9/HostnamesPCRFList/HostName')
    return unless tag
    network_section = NetworkSection.where(field_name: 'pcrf_hostnames').first
    delete_old_network_rows(network_section, network)
    tag.each do |t|
      network_row = NetworkRow.new
      network_row.data = {
        'hostname' => t.text
      }
      network_row.network_section_id = network_section.id
      network_row.network_id = network.id
      network_row.save!
    end
  end

  #### /import_s9_pcrf_hostnames ####

  #### import_lte_qos_profiles ####
  def import_lte_qos_profiles(lte_roaming_info_tag, lte_roaming_info)
    lte_roaming_info.lte_qos_profiles.destroy_all

    lte_roaming_info_tag.xpath('LTEQosProfileList/LTEQosProfile').each do |tag|
      profile_name = tag.xpath('Profile').first.text
      qci = tag.xpath('QCIValue').first.text
      arp = tag.xpath('QOSARPList/ARP').map(&:text).join('; ')
      guaranteed_bit_rate_uplink = tag.xpath('GuaranteedBitRateUplink').text
      guaranteed_bit_rate_downlink = tag.xpath('GuaranteedBitRateDownlink').text
      maximum_bit_rate_uplink = tag.xpath('MaximumBitRateUplink').text
      maximum_bit_rate_downlink = tag.xpath('MaximumBitRateDownlink').text

      # TODO: (silje): Not in XML
      # pre_emption_vulnerability = tag.xpath('').text
      # pre_emption_capability = tag.xpath('').text
      # ODOT

      lte_roaming_info.lte_qos_profiles.create!(profile_name: profile_name,
        qci: qci,
        arp: arp,
        guaranteed_bit_rate_uplink: guaranteed_bit_rate_uplink,
        guaranteed_bit_rate_downlink: guaranteed_bit_rate_downlink,
        maximum_bit_rate_uplink: maximum_bit_rate_uplink,
        maximum_bit_rate_downlink: maximum_bit_rate_downlink)
    end
  end

  #### /import_lte_qos_profiles ####

  #### import_rt_contact ####
  def import_rt_contact(network_tag, network)
    network.rt_contacts.destroy_all

    contact_info_section_tag = network_tag.xpath('ContactInfoSection').first
    return unless contact_info_section_tag
    import_change_histories(network, ChangeHistory::Section::CONTACT_INFO, contact_info_section_tag)

    contact_info_tag = contact_info_section_tag.xpath('ContactInfo').first
    return unless contact_info_tag

    contact_info_tag.xpath('RTContactInfoList/RTContactInfo').each do |rt_contact_tag|
      location = rt_contact_tag.xpath('Location').first.text
      utc_offset = rt_contact_tag.xpath('UTCTimeOffset').first.text
      main_contact_team_name = rt_contact_tag.xpath('MainContact/TeamName').first.text

      escalation_contact_given_name = rt_contact_tag.xpath('EscalationContact/GivenName').first.text
      escalation_contact_family_name = rt_contact_tag.xpath('EscalationContact/FamilyName').first.text
      escalation_contact_prefix = rt_contact_tag.xpath('EscalationContact/Prefix').text
      escalation_contact_job_title = rt_contact_tag.xpath('EscalationContact/JobTitle').text
      ts24x7_contact_team_name = rt_contact_tag.xpath('TS24x7Contact/TeamName').first.text

      rt_contact = network.rt_contacts.create!(location: location,
       utc_offset: utc_offset,
       main_contact_team_name: main_contact_team_name,
       escalation_contact_given_name: escalation_contact_given_name,
       escalation_contact_family_name: escalation_contact_family_name,
       ts24x7_contact_team_name: ts24x7_contact_team_name,
       escalation_contact_prefix: escalation_contact_prefix,
       escalation_contact_job_title: escalation_contact_job_title)

      rt_contact_tag.xpath('MainContact/PhoneFixList/PhoneFix').each do |mp|
        rt_contact.add_contact_details(:main, :phone, mp.text)
      end

      rt_contact_tag.xpath('MainContact/FaxList/Fax').each do |mp|
        rt_contact.add_contact_details(:main, :fax, mp.text)
      end

      rt_contact_tag.xpath('MainContact/PhoneMobileList/PhoneMobile').each do |mp|
        rt_contact.add_contact_details(:main, :mobile, mp.text)
      end

      rt_contact_tag.xpath('MainContact/EmailList/Email').each do |mp|
        rt_contact.add_contact_details(:main, :email, mp.text)
      end

      rt_contact_tag.xpath('EscalationContact/PhoneFixList/PhoneFix').each do |mp|
        rt_contact.add_contact_details(:escalation, :phone, mp.text)
      end

      rt_contact_tag.xpath('EscalationContact/FaxList/Fax').each do |mp|
        rt_contact.add_contact_details(:escalation, :fax, mp.text)
      end

      rt_contact_tag.xpath('EscalationContact/PhoneMobileList/PhoneMobile').each do |mp|
        rt_contact.add_contact_details(:escalation, :mobile, mp.text)
      end

      rt_contact_tag.xpath('EscalationContact/EmailList/Email').each do |mp|
        rt_contact.add_contact_details(:escalation, :email, mp.text)
      end

      rt_contact_tag.xpath('TS24x7Contact/PhoneFixList/PhoneFix').each do |mp|
        rt_contact.add_contact_details('24x7', :phone, mp.text)
      end

      rt_contact_tag.xpath('TS24x7Contact/FaxList/Fax').each do |mp|
        rt_contact.add_contact_details('24x7', :fax, mp.text)
      end

      rt_contact_tag.xpath('TS24x7Contact/PhoneMobileList/PhoneMobile').each do |mp|
        rt_contact.add_contact_details('24x7', :mobile, mp.text)
      end

      rt_contact_tag.xpath('TS24x7Contact/EmailList/Email').each do |mp|
        rt_contact.add_contact_details('24x7', :email, mp.text)
      end

      import_office_hours(rt_contact_tag, rt_contact)
    end

    import_change_histories(network, ChangeHistory::Section::CONTACT_INFO, contact_info_tag)
  end

  #### /import_rt_contact ####

  #### import_office_hours ####
  def import_office_hours(rt_contact_tag, rt_contact)
    rt_contact.office_hours.destroy_all

    rt_contact_tag.xpath('OfficeHours/OfficeHour').each do |office_hour_tag|
      start_time = office_hour_tag.xpath('startTime').first.text
      end_time = office_hour_tag.xpath('endTime').first.text
      inclusive_all_week_days = office_hour_tag.xpath('inclusiveAllWeekDays').first.text
      monday = tuesday = wednesday = thursday = friday = saturday = sunday = false

      office_hour_tag.xpath('WeekDayList/WeekDay').each do |wd|
        case wd.text
        when 'Mon'
          monday = true
        when 'Tue'
          tuesday = true
        when 'Wed'
          wednesday = true
        when 'Thu'
          thursday = true
        when 'Fri'
          friday = true
        when 'Sat'
          saturday = true
        when 'Sun'
          sunday = true
        end
      end

      rt_contact.office_hours.create!(start_time: start_time,
        end_time: end_time,
        inclusive_all_week_days: inclusive_all_week_days,
        monday: monday,
        tuesday: tuesday,
        wednesday: wednesday,
        thursday: thursday,
        friday: friday,
        saturday: saturday,
        sunday: sunday)
    end
  end

  #### /import_office_hours ####

  #### import_additional_contacts ####
  def import_additional_contacts(network_tag, network)
    network.additional_contacts.destroy_all

    contact_tag_map = {
      'GPRSContact' => 'GPRS Contact',
      'GRXConnectivityContactAtPMN' => 'Contact person(s) (in PMN) for GRX connectivity',
      'IREGTests' => 'IREG Tests',
      'IW_MMS_Contact' => 'Contact person(s) for IW SMS',
      'Other_Contact' => 'Other contacts',
      'RoamingCoordinator' => 'Roaming Coordinator',
      'SCCPInquiriesAndOrderingOfSS7Routes' => 'SCCP inquiries and ordering of SS7 routes',
      'TADIGTests' => 'TADIG Tests'
    }

    contact_tag_map.keys.each do |ct_tag|
      network_tag.xpath("ContactInfoSection/ContactInfo/#{ct_tag}/ContactPerson").each do |cp|
        given_name = cp.xpath('GivenName').first.text
        family_name = cp.xpath('FamilyName').first.text
        job_title = cp.xpath('JobTitle').text
        prefix = cp.xpath('Prefix').text
        contact_type = contact_tag_map[ct_tag]

        additional_contact = network.additional_contacts.create!(given_name: given_name,
         family_name: family_name,
         job_title: job_title,
         prefix: prefix,
         contact_type: contact_type)

        cp.xpath('PhoneFixList/PhoneFix').each do |details|
          additional_contact.additional_contact_details.create!(additional_contact_type: RtContactDetail::CONTACT_TYPES['phone'],
            details: details.text.strip)
        end

        cp.xpath('FaxList/Fax').each do |details|
          additional_contact.additional_contact_details.create!(additional_contact_type: RtContactDetail::CONTACT_TYPES['fax'],
            details: details.text.strip)
        end

        cp.xpath('PhoneMobileList/PhoneMobile').each do |details|
          additional_contact.additional_contact_details.create!(additional_contact_type: RtContactDetail::CONTACT_TYPES['mobile'],
            details: details.text.strip)
        end

        cp.xpath('EmailList/Email').each do |details|
          additional_contact.additional_contact_details.create!(additional_contact_type: RtContactDetail::CONTACT_TYPES['email'],
            details: details.text.strip)
        end
      end
    end
  end

  #### /import_additional_contacts ####

  #### import_ir21_distribution_emails ####
  def import_ir21_distribution_emails(network_tag, network)
    network.ir21_distribution_emails.destroy_all

    network_tag.xpath('ContactInfoSection/ContactInfo/IR21DistributionEmailAddressList/IR21DistributionEmailAddress').each do |email|
      network.ir21_distribution_emails.create!(email: email.text.strip)
    end
  end

  #### /import_ir21_distribution_emails ####

  #### import_technology_frequencies ####
  def import_technology_frequencies(network_tag, network)
    network.technology_frequencies.destroy_all

    network_tag.xpath('SupportedTechnologyFrequencies/GSMFrequencyList/GSMFrequencyItem').each do |tf|
      network.technology_frequencies.create!(frequency_type: 'GSM', frequency_text: tf.text.strip)
    end

    network_tag.xpath('SupportedTechnologyFrequencies/UTRAFDDFrequencyList/UTRAFDDFrequencyItem').each do |tf|
      network.technology_frequencies.create!(frequency_type: 'UTRA/FDD', frequency_text: tf.text.strip)
    end

    # TODO(silje): not in XML
    network_tag.xpath('SupportedTechnologyFrequencies/UTRATDDFrequencyList/UTRATDDFrequencyItem').each do |tf|
      network.technology_frequencies.create!(frequency_type: 'UTRA/TDD', frequency_text: tf.text.strip)
    end
    # ODOT

    network_tag.xpath('SupportedTechnologyFrequencies/EUTRAFrequencyList/EUTRAFrequencyItem').each do |tf|
      network.technology_frequencies.create!(frequency_type: 'E-UTRA', frequency_text: tf.text.strip)
    end

    # TODO(silje): not in XML other frequencies
  end

  #### /import_technology_frequencies ####

  ########## HELP METHODS ##############
  def parse_dpc_item(dpc_item_tag)
    signature = dpc_item_tag.xpath('SCSignature').first.text
    type = dpc_item_tag.xpath('SCType').text
    dpc = dpc_item_tag.xpath('DPC').first.text
    comment = dpc_item_tag.xpath('Comments').text
    { 'sc_signature' => signature, 'sc_type' => type, 'dpc' => dpc.tr!('.', '-'), 'comment' => comment, 'dpc_decimal' => convert_from_itu_format(dpc.to_s), 'dpc_hex' => convert_from_itu_format(dpc.to_s).to_s(16) }
  end

  def find_conflicts(network_section)
    p 'RUNNING FIND CONFLICTS'
    xml_creation_time = network_section.xpath('//RAEXIR21FileHeader/FileCreationTimestamp').first
    return unless xml_creation_time
    network_section.xpath('//OrganisationInfo/NetworkList/Network').each do |network|
      path = network.xpath('NetworkData/RoutingInfoSection/CCITT_E212_NumberSeries')
      mcc = path.xpath('MCC').first
      mnc = path.xpath('MNC').first
      return unless mcc && mnc
      old_network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ?", mcc.text, mnc.text).first
      if old_network
        versions = PaperTrail::Version.where("network_id = ? AND created_at > ? AND whodunnit IS NOT NULL AND whodunnit != ? AND object_changes NOT LIKE '%rosi_id%'", old_network.id, xml_creation_time.text, 'HERMES').all
      end
      if versions && versions.count > 0
        self.failed = true
        self.error_message = 'Please resolve conflicts to finish this import'
        self.conflicts = versions.count
      end
      versions.count if versions
    end
  end

  def find_country(cc)
    if Rails.env.development?
      Country.first
    else
      Country.where(iso_code: cc).first
    end
  end

  def delete_old_network_rows(network_section, network)
    if network_section && network
      NetworkRow.destroy_all(network_section_id: network_section.id, network_id: network.id)
    end
  end

  # (D1 * 2048) + (d2 * 8) + d3
  def convert_to_itu_format(value)
    a = value / 2048
    b = (value - (a * 2048)) / 8
    c = value - a * 2048 - b * 8
    "#{a}-#{b}-#{c}"
  end

  def convert_from_itu_format(value)
    split = value.split('-')
    result = split[0].to_i * 2048 + split[1].to_i * 8 + split[2].to_i
    p "result: #{value}"
    result
  end

  def import_change_history(tag, network_section)
    # delete all previous history
    return if network_section.nil?
    ChangeHistory.destroy_all(network_section_id: network_section.id, network_id: @network.id)
    tag.xpath('ChangeHistory').each do |item|
      ChangeHistory.create(data: Hash.from_xml(item.to_xml), network_section_id: network_section.id, network_id: @network.id)
    end
  end

  class Numeric
    Alph = ('a'..'z').to_a
    def alph
      s = ''
      q = self
      (q, r = (q - 1).divmod(26)); s.prepend(Alph[r]) until q.zero?
      s
    end
  end

  # compare current section to previous section to ensure it is 100% alike
  def check_for_diff(path, new_xml_section, section_id, tadig_code)
    p "Checking for diff || checked_for_diff::#{checked_for_diff}"
    return if checked_for_diff
    if @old_network == false
      return false
    else
      # Get the previous file
      networks = @network.organisation.networks.pluck(:id).to_a	
      old_import = Import.where('network_id IN(?) AND id != ? AND file_type = ? AND removed = ?', networks, id, file_type, false).last
      p old_import
      if old_import
        p 'found old import'
        return unless old_import

        Dir.chdir("#{Rails.root}/data/upload") do
          ActiveRecord::Base.connection.transaction do
            begin
              @file_content = File.read(old_import.original_filename)
            rescue
              return
            end
          end
        end

        doc = Nokogiri::XML(@file_content)
        doc.remove_namespaces!
        doc.xpath('//OrganisationInfo/NetworkList/Network').each do |network|
          next unless network.xpath('TADIGCode').text == tadig_code
          unless path == 'NetworkName'
            network_tag = network.xpath('NetworkData')
          else
            network_tag = network.xpath('Network')
          end
          
          # check old structure
          old_structure = network_tag.xpath(path)
          p "PATH_IS:::#{path}"
          new_structure = new_xml_section
          match = true if old_structure.text.squish == new_structure.text.squish
          next if match

          p 'it does not match'
          p "New structure::#{new_structure.text}"
          p "Old structure::#{old_structure.text}"
          @network_diff = NetworkDiff.new
          @network_diff.created_at = @network.updated_at
          @network_diff.network_id = @network.id
          @network_diff.import_id  = id
          @network_diff.old_effective_date_of_change = old_structure.css('EffectiveDateOfChange').text
          @network_diff.section_id = section_id
          @network_diff.message    = path.split('/').last
          # Connect it to the righ notification group
          if section_id
            notify_field = NotifyField.where(network_section_id: section_id).first
            if notify_field
              @network_diff.notify_field_id = notify_field.id
            end
          else
            # In case no section id is present, we know that it is routing section and hardcode it
            @network.notify_field_id = 4
          end
          @network_diff.snapshot   = "#{old_structure.text}  =================================================
          #{new_structure.text}"
          @network_diff.save
          return true
        end
      end
    end
  end

  # Check that number of networks are the same
  def check_number_of_networks(new_tag)
    old_import = Import.where('network_id = ? AND id != ? AND file_type = ?', @network.id, id, file_type).last
    if old_import
      Dir.chdir("#{Rails.root}/data/upload") do
        # p 'changed dir'
        ActiveRecord::Base.connection.transaction do
          # p 'connection up'
          begin
            @file_content = File.read(old_import.original_filename)
          rescue
            return
          end
        end
      end	

      doc = Nokogiri::XML(@file_content)
      doc.remove_namespaces!
      old_count = doc.xpath('//OrganisationInfo/NetworkList/Network').count
      p "OLD COUNT:/:/:/#{old_count}"
      return if old_count.zero?
      new_count = new_tag.count
      unless old_count == new_count
        @network_diff = NetworkDiff.new
        @network_diff.network_id = @network.id
        @network_diff.import_id  = id
        @network_diff.message = "The number of networks do not match (#{old_count}(old) - #{new_count}(new) )"
        @network_diff.notify_field_id = 5
        @network_diff.save
      end
    end	
  end

  # Check if line-range should be added or deleted
  def evaluate_line_range_rules(network_tag, organisation_tag)
    p 'Evaluat line range rules'
      #### Empty Line ranges ####
      ###########################
      lr = NetworkLineRange.where(network_id: @network.id)
      lr.sort_by(&:range_start).each do |range|
        if range.regenerate #range is flagged for recreation
          p 'Rewriting line range'
          first_network = organisation_tag.xpath('NetworkList/Network').first
          network_tag.xpath('NetworkElementsInfoSection/NetworkElementsInfo/NwNodeList/NwNode').each do |node|
            length = node.xpath('GTAddressInfo/NDC').text.length + node.xpath('GTAddressInfo/CC').text.length + node.xpath('GTAddressInfo/SN_Range/SN_RangeStart').text.length
            # If nothing is found here, use the first network
            if length < 3
              p 'no length is found'
              length_1 = first_network.at('NetworkData/NetworkElementsInfoSection/NetworkElementsInfo/NwNodeList/NwNode/GTAddressInfo/NDC').try(:text).try(:length)
              length_2 = first_network.at('NetworkData/NetworkElementsInfoSection/NetworkElementsInfo/NwNodeList/NwNode/GTAddressInfo/SN_Range/SN_RangeStart').try(:text).try(:length)
              if length_1 && length_2
                length = length_1 + length_2
              else
                lengt = length_1
              end
              p "length is now #{length}"         
            end
            p "length is #{length}"
            # Fetch original network row
            original_network_row = NetworkRow.find(range.orginate_id)
            diff_numbers = length - "#{original_network_row.data['length']}".to_i
            old_length = "#{original_network_row.data.values[0]}#{original_network_row.data.values[1]}#".length
            number_start = "%0#{diff_numbers}o" % '0'
            number_stop = number_start.tr('0', '9')
            p original_network_row.data
            old_range = range
            range.range_start = "#{original_network_row.data.values[1]}#{original_network_row.data.values[2]}#{number_start}"
            range.range_stop = "#{original_network_row.data.values[1]}#{original_network_row.data.values[2]}#{number_stop}"
            range.message = "Modified this range because of empty line range old range was (#{old_range.range_start} - #{old_range.range_stop})"
            p "OLD_RANGE_START :: #{range.range_start}"
            p "OLD_RANGE_STOP :: #{range.range_stop}"
            p range.range_start
            p range.range_stop
            p range.id
          end
        end
        range.save!
      end
      @network.evaluate_line_ranges unless @network.network_line_ranges.blank?
    end

    def check_for_empty_fields
      unless @network.tadig_code && @network.routing["e212_mnc"] && @network.routing["e212_mcc"] && @network.routing["e214_cc"]
        # Add network diff
        network_diff = NetworkDiff.new
        network_diff.created_at = @network.updated_at
        network_diff.network_id = @network.id
        network_diff.import_id  = id
        network_diff.message = "main fields are missing"
        network_diff.save
      end
    end

    def set_network_status(network)
      last_status_sccp    = NetworkStatus.order('created_at desc').where(network_id: network.id, network_status_type_id: 1).first_or_create
      last_status_routing = NetworkStatus.order('created_at desc').where(network_id: network.id, network_status_type_id: 2).first_or_create
      # sccp
      network_section = NetworkSection.where(field_name: 'international_sccp_carrier').first
      nr = NetworkRow.where(network_section_id: network_section.id, network_id: network.id).last
      if nr.try(:effective_date_of_change)
        if last_status_sccp.sccp_effective_date.nil?
          last_status_sccp.update_columns(status: nil, sccp_effective_date: nr.effective_date_of_change)
        end
        if nr.effective_date_of_change > last_status_sccp.sccp_effective_date
          last_status_sccp.update_columns(status: nil)
        end
      end
      # routing
      if @network.routing.try(:[], 'effective_date_of_change')
        if last_status_routing.routing_effective_date.nil?
          last_status_routing.update_columns(status: nil,routing_effective_date: @network.routing['effective_date_of_change'])
        end
        if @network.routing['effective_date_of_change'] > last_status_routing.routing_effective_date
          last_status_routing.update_columns(status: nil)
        end
      end
    end

    def check_requirements(doc)
      p 'checking requirements'
      check_mnc_mcc = doc.xpath("//CCITT_E212_NumberSeries")
      p check_mnc_mcc
      if check_mnc_mcc.blank?
        p 'Does not contain mcc + mcc'
        p self.id
        self.failed = true
        self.error_message = 'File do not contain mcc + mnc'
        if self.save!
          p 'saved'
        else
          p 'not saved'
          p 'se'
        end
        self.save
        p @import
        exit
      end
    end
  end
