class SmsHub < ActiveRecord::Base
	belongs_to :network
	has_many :sms_hub_line_ranges, dependent: :destroy

  def import_unmatched_from_ir21
  end
end
