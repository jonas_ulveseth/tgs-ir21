class Organisation < ActiveRecord::Base
	has_paper_trail
	belongs_to :country
	has_one :network
	has_many :networks
  has_many :imports
	has_many :notify_groups_organisations

  def self.preload_jobs
    Network.find_in_batches(batch_size: 100) do |networks|
      self.delay(queue: 'codes').filter_line_ranges(networks)
    end
  end

  # Function to go through all line ranges inside an organisation and fill upp with extra ranges
  def self.filter_line_ranges(networks)
    unupdated_messages = []
    #network_ids = networks.pluck(:id).to_a
    #line_ranges = NetworkLineRange.where("network_id IN (?)", network_ids).all
    section_ids = NetworkSection.where(field_name: ['msisdn_number_ranges', 'gt_number_ranges', 'msrn_number_ranges'])
    .pluck(:id).to_a
    # Loop networks
    networks.each do |network|
      # Remove all file based networks with the same rosi id
      mcc = network.routing['e212_mcc']
      mnc = network.routing['e212_mnc']
      #SmsHub.where("imsi = ?","#{mnc}-#{mcc}" ).destroy_all
      sms_hub_check = SmsHub.where(network_id: network.id).first
      if sms_hub_check
        #p 'EXISTS FROM BEFORE'
        sms_hub = sms_hub_check
      else
        p 'moving on'
        next
        #p 'DO NOT EXIST, CREATING'
        #sms_hub = SmsHub.create(imported_from: 'IR21 - unmatched', rosi_id: network.rosi_id, network_id: network.try(:id), name: network.try(:display_name), country_id: network.try(:country_id))
      end
      network_number_ranges = NetworkRow.where("network_section_id IN(?) AND network_id = ?", section_ids, network.id).all
      
      #####################################################
      # Fetch and build line ranges from database
      #####################################################
      network_number_ranges.each do |network_number_range|
        # Check for double line ranges
        p network_number_range 
        case network_number_range.network_section_id
        when 167
          p '167'
          p network_number_range.data  
          cc  = network_number_range.data['GT - cc']
          ndc = network_number_range.data['GT - ndc']
          start = network_number_range.data['GT - range_start']
          stop = network_number_range.data['GT - range_stop']
          if start.nil?
            start = network_number_range.data['GT_NumberRanges_range_start']
            stop = network_number_range.data['GT_NumberRanges_range_stop']
            cc  = network_number_range.data['GT_NumberRanges_cc']
            ndc = network_number_range.data['GT_NumberRanges_ndc']
          end
        when 168
          p '168'
          p network_number_range.data  
          cc  = network_number_range.data['MSRN - cc']
          ndc = network_number_range.data['MSRN - ndc']
          start = network_number_range.data['MSRN - range_start']
          stop = network_number_range.data['MSRN - range_stop']
          if start.nil?
            start = network_number_range.data['MSRN_NumberRanges_range_start']
            stop = network_number_range.data['MSRN_NumberRanges_range_stop']
            cc  = network_number_range.data['MSRN_NumberRanges_cc']
            ndc = network_number_range.data['MSRN_NumberRanges_ndc']
          end
        when 169
          p '169'
          p network_number_range.data  
          cc  = network_number_range.data['MSISDN - cc']
          ndc = network_number_range.data['MSISDN - ndc']
          start = network_number_range.data['MSISDN - range_start']
          stop = network_number_range.data['MSISDN - range_stop']
          if start.nil?
            start = network_number_range.data['MSISDN_NumberRanges_range_start']
            stop = network_number_range.data['MSISDN_NumberRanges_range_stop']
            cc  = network_number_range.data['"MSISDN_NumberRanges_cc']
            ndc = network_number_range.data['MSISDN_NumberRanges_ndc']
          end
          if cc.nil?

          end
        end 

        if cc.nil?
          p 'could not load cc'
          p "start is #{start}"
        end

        unless start.nil?
          start_count = start.length - start.gsub(/0+$/, '').length
          stop_count = stop.length - stop.gsub(/9+$/, '').length
          p "start is #{start}"
          p "stop is #{stop}"

          ####################################
          # Remove 001 - 999
          ####################################
          start_index = start.index('001')
          stop_index  = stop.index('999')
          if start_index == stop_index
            start.slice!('001')
            stop.slice!('999')
            p 'Modified 009 and 001'
            p "they are now #{start} - #{stop}"
          else
            if start_index
              p "filtration by index"
              p "the result is #{stop[start_index..stop.length]}"
              p "the stop is #{stop}"
              p "the start is #{start}"
              if stop[start_index..stop.length] == '999'
                p 'filtration worked'
                stop.slice!(start_index, 3)
                start.slice!('001')
                p "they are now #{start} - #{stop}" 
              end
            end
          end

          p "start_count is #{start_count} and stop_count is #{stop_count}"
          if start_count > stop_count
            p 'start is biggest'
            p "start has #{start_count} and stop has #{stop_count}"
            p "stop count is #{stop_count}"
            unless stop_count == 0
              start = start[0...-stop_count]
              stop = stop[0...-stop_count]
            end
          else
            p 'stop is biggest'
            p "start has #{start_count} and stop has #{stop_count}"
            p "start count is#{start_count}"
            unless start_count == 0
              start = start[0...-start_count.to_i]
              stop = stop[0...-start_count.to_i]
            end
          end
          p "start after fix #{start}"
          p "stop after fix #{stop}"
        end

        start = "#{cc}#{ndc}#{start}"
        stop = "#{cc}#{ndc}#{stop}"

        if start.length != stop.length
          p "FIXING LENGTH"
          p "start before fixing #{start}"
          p "stop before fixing #{stop}"
          if stop > start
            uneven_amount = stop.length - start.length
            equalize_number = "#{start}%0#{uneven_amount}o" % '0'
            start = equalize_number
            p "Equalize number is #{equalize_number}"
            p "stop after equalize is #{stop}"
            p "start after equalize is #{start}"
          else
            uneven_amount = start.length - stop.length
            equalize_number = "%0#{uneven_amount}o" % '0'
            equalize_number = equalize_number.gsub('0','9')
            stop = "#{stop}#{equalize_number}"
            p "Equalize number is #{equalize_number}"
            p "stop after equalize is #{stop}"
            p "start after equalize is #{start}"
          end
        end

        # Skip if line range is blank
        next if start.blank?

        ####################################
        ## Checking if we have a smaller line range
        ####################################
        #sms_hub.sms_hub_line_ranges.each do |lr|
        #  SmsHubLineRange.where("range_start LIKE ? AND sms_hub_id = ? AND id != ? AND removed is NULL","#{range_start}%", sms_hub.id, self.id)
        #end

        ####################################
        ## All processing is done,
        ## adding to sms hub here
        ####################################
        p "final start #{start}"
        p "final stop #{stop}"
        if start == stop
          SmsHubLineRange.create(range_start: start.strip, sms_hub_id: sms_hub.id, network_id: network.id)
        else
          #increment
          p "it is not the same #{start} - #{stop}"
          diffrence = stop.to_i - start.to_i
          p "diffrence is #{diffrence}"
          unless diffrence > 950
            (start..stop).each do |n|
              p "adding inline number #{start} .. #{stop}"
              #p "#{cc}#{ndc}#{start} - #{cc}#{ndc}#{stop} "
              #p network_number_range
              #p n
              SmsHubLineRange.create(range_start: n, network_id: network.id, sms_hub_id: sms_hub.id)
            end
          else
            #############################################################################################
            # Create equal numbers, by looking at length, and keeping only the latest number in the end.
            ###########
            p 'diffrence is beyond 950'
            combined = start.split("").zip stop.split("")
            p 'combined created'
            p "diffrence is #{diffrence}"
            p combined
            @done = nil
            combined.each_with_index do |c,c_index|
              p 'started looping combined'
              p c 
              unless c.first == c.last
                combined = combined.shift(c_index + 1)
              end
            end
            old_start = start
            old_stop  = stop 
            start, stop = combined.transpose
            start = start.join('')
            stop = stop.join('')
            diffrence = stop.to_i - start.to_i
            # If last is 0 and 9 do removal of last digits and save the line range right away
            if start.last == '0' && stop.last == '9' && diffrence < 20
              SmsHubLineRange.create(range_start: start[0...-1].strip, network_id: network.id, sms_hub_id: sms_hub.id)
              next
            end
            ####################################################
            ## Create intermediate line ranges for everything that is under 950
            ####################################################
            if diffrence < 950
              p 'coding karate'
              p "#{start} - #{stop}"
              p "was before #{old_start} - #{old_stop}"
              (start..stop).each do |n|
                p "#{start} - #{stop}"
                SmsHubLineRange.create(range_start: n.strip, network_id: network.id, sms_hub_id: sms_hub.id, message: "new range created from #{old_start} - #{old_stop} | originally to big range", filtered: true)
              end
            else            
              #################################################
              ## Line range would here still create more than 950 ranges,
              ## and we make an attempt to run some logic to fix that 
              #################################################
              p 'slice 001 and 999 if line range is to big' 
              start_index = start.index('001')
              stop_index  = stop.index('999')
              diffrence = stop - start
              if start_index == stop_index && diffrence < 950
                old_start = start
                old_stop  = stop 
                start.slice!('001')
                stop.slice!('999')
                (start..stop).each do |n|
                  p "removed 999"
                  p "start is #{start} stop is #{stop}"
                  SmsHubLineRange.create(range_start: n.strip, network_id: network.id, sms_hub_id: sms_hub.id, message: "new range created from #{old_start} - #{old_stop} | originally to big range", filtered: true)
                end
              else
                ## Save untreated line range
                unupdated_messages.push("COULD NOT UPDATE range id#{network_number_range.id} And network is #{network_number_range.network_id} range start is #{start} and range stop is #{stop}")
                p "COULD NOT UPDATE range id#{network_number_range.id} And network is #{network_number_range.network_id} range start is #{start} and range stop is #{stop}"
              end
            end  
          end
        end
      end
    end
    p 'DONE:::'
    p unupdated_messages
  end
end
