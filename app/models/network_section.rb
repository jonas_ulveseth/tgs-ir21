class NetworkSection < ActiveRecord::Base
	has_many :network_rows, -> {order(:created_at)}
	has_many :network_section_fields
	has_many :change_histories
	extend ActsAsTree::TreeWalker
  acts_as_tree order: 'name'
end
