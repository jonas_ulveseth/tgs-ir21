# This model controls what changes should trigger notifaction emailing
class NotifyField < ActiveRecord::Base
	has_many :notify_field_users 
end
