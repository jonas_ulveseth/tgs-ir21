class NetworkLineRange < ActiveRecord::Base
	has_paper_trail
	belongs_to :network
	

	# Convert censecutive line ranges into single one, and return object
	def self.condense(object)
		previous = nil
		unless object
			object = self
		end
		p object.count
		@delete_array = []
		object.each_with_index do |range, index|			
			#break if range.removed
			p 'one row indeed'
			# If there are previous numbers
			if previous.try(:range_stop)
				p "Got a previous - and start is#{previous.range_start}"
				p "processing line range with start : #{range.range_start}"
				# Create a range in array and start over
				if previous.try(:range_stop).to_i + 1 == range.try(:range_start).to_i
					#import current stop range into previous stop range
					p "It is found #{previous.try(:range_stop)} |||| #{range.try(:range_start)}"
					range.range_start = previous.range_start
					#Delete this range
					pr = NetworkLineRange.find(previous.id)
					#object.delete(pr)
					@delete_array.push(pr)
					previous = range
					p "It´s deleted #{pr.range_start}"
					p "previous start is #{range.range_start}"
				else
					p "it is not a match #{previous.range_stop} ||||| #{range.try(:range_start)}"
					previous = range
				end
			else
				previous = range
				p 'previous is set'
			end
		end
		#Deliver array
		p object.count
		@delete_array.each do |d|
			object.delete(d)
		end
		object
	end

	def reevaluate_line_ranges
		p "Evaluating a range - #{self.id}"
		@range_start = self.try(:range_start)
		@range_stop = self.try(:range_stop)
		return unless @range_start && @range_stop
		return if self.removed

    # Check if the line range has the same length
    unless @range_start.length == @range_stop.length
    	p "FIXING LENGTH"
    	min_value = [@range_start, @range_stop].min
    	max_value = [@range_start, @range_stop].max
    	diff      = max_value.length - min_value.length
    	diff.times do |t|
    		if @range_start < @range_stop
    			@range_start = "#{@range_start}0"
    			p @range_start
    		else
    			@range_stop = "#{@range_stop}9"
    			p @range_stop
    		end  
    	end
    	self.message = "Uneven ranges - correcting"
    	self.range_start = @range_start
    	self.range_stop = @range_stop
    	self.save!
    end

    #### Switched start and stop range fix ####
    ###########################################
    if @range_start > @range_stop
    	p 'Start range is bigger - fixing'
    	range.range_start = @range_start
    	range.range_stop  = @range_stop
    	range.message     = 'switched line ranges'
    	range.save!
    end

    #### Fix double line ranges ####
    ################################
    double = NetworkLineRange.where('network_id = ? AND range_start = ? AND range_stop = ? AND id != ? AND removed IS NULL', self.network.try(:id), @range_start.to_s, @range_stop.to_s, self.id).first
    if double
    	p 'FIXING DOUBLE LINE RANGES'
    	self.removed = true
    	self.message = "Double line range removed #{self.range_start} #{self.range_stop}"
    	self.save
    end

    #### Overlapping fix ####
    ########################
    overlapping_ranges = NetworkLineRange.where("network_id = ? AND range_start < ? AND range_stop > ? AND removed is NULL",self.network.try(:id),@range_start.to_s,@range_stop.to_s).first
    if overlapping_ranges
    	p 'FOUND AN OVERLAP'
    	self.removed = true
    	self.message = "overlapping ranges #{self.range_start} #{self.range_stop}"
    	self.save!
    end

    #### Partly overlapping fix ####
    ################################
    partly_overlapped_top = NetworkLineRange.where("network_id = ? AND range_start < ? AND range_stop > ? AND removed is NULL",self.network.try(:id),@range_stop.to_s,@range_stop.to_s).all 
    if partly_overlapped_top.first
    	partly_overlapped_top.each do |overlap|
    		p 'FOUND A PARTLY OVERLAP TOP'
    		self.range_stop = overlap.range_stop
    		self.save!
    		overlap.removed = true
    		overlap.message = "Removed because of partly overlapping top #{overlap.range_start} #{overlap.range_stop}"
    		overlap.save!
    	end
    end
    partly_overlapped_bottom = NetworkLineRange.where("network_id = ? AND range_start < ? AND range_stop > ? AND removed is NULL",self.network.try(:id),@range_start.to_s,@range_start.to_s).all 
    if partly_overlapped_bottom.first
    	p 'FOUND A PARTLY OVERLAP BOTTOM'
    	partly_overlapped_bottom.each do |overlap|
    		self.range_start = overlap.range_start
    		self.save!
    		overlap.removed = true
    		overlap.message = "Removed because of partly overlapping bottom #{self.range_start} #{self.range_stop}"
    		overlap.save!
    	end
    end
  end
end
