class Notification < ActiveRecord::Base
	belongs_to :network

	# Notify groups of changes made to network
	def notify_changes(network)
		# Find all notify groups
		nfgn  = NotifyGroupsNetwork.where(network_id: network.id).all
		nfgu = NotifyGroupsUser.select("DISTINCT ON(user_id) *").where("notify_group_id = ?", 3)
		nfgu.each do |nfgu|
			# Check that user has access to atleast one notify group
			#NotifyFieldUser.where(user_id: nfgu.user_id)
			user = nfgu.user
			UserMailer.notify_network_change(user, network).deliver
		end
		p 'delivered'
	end

	def self.notify_deleted_network(diff,organisation)
		nfgu = NotifyGroupsUser.select("DISTINCT ON(user_id) *").where("notify_group_id = ?", 3)
		nfgu.each do |nfgu|
			# Check that user has access to atleast one notify group
			#NotifyFieldUser.where(user_id: nfgu.user_id)
			user = nfgu.user
			UserMailer.notify_deleted_network(user, organisation, diff).deliver
		end
		p 'delivered'
	end
end
