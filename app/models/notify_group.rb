  class NotifyGroup < ActiveRecord::Base
	has_many :notify_groups_network, dependent: :destroy
	has_many :networks, through: :notify_groups_network
	has_many :notify_groups_organisation, dependent: :destroy
	has_many :organisations, through: :notify_groups_organisation
	has_many :notify_groups_user
	has_many :users, through: :notify_groups_user
	has_many :organisation, through: :notify_groups_network 
end
