class IndexController < ApplicationController
	skip_before_filter :require_login
	include JqueryTablesorter::RailsUtils::Ajax::ActionController

	def index
		@user_session = UserSession.new
	end

	def import_diffs
		@hermes_imports 	 = HermesOperator.order("network_id ASC").all
		@hermes_matches 	 = HermesOperator.where("network_id IS not null").all
		@hermes_unmatched  = HermesOperator.where("network_id IS null").all
		@rosi_matches 		 = Network.where("rosi_id IS NOT NULL").all
		@rosi_total 	 		 = Network.all 
	end

	def settings
		raise current_user.has_role?(:admin).to_yaml unless current_user.has_any_role? :admin, :superadmin
	end

	def view_settings
	end

	def show_sms_hub_data
		@sms_hub = SmsHub.all
	end

	def line_range_overview_ir21
		@organisations = Organisation.all
	end

	def line_range_overview_export
		@line_ranges = SmsHubLineRange.includes(:sms_hub).take(5)
	end

	def line_range_code_export
		@line_ranges = CodeRange.includes(:code).where(source: 1).all
	end

	def line_range_overview_export_ajax_data
		p 'is it blank'
		p params[:organisation_name].blank?
		unless params[:organisation_name].blank?
			p 'this is run'
			organisations_ids = Organisation.where('name LIKE ?', "#{params[:organisation_name]}%").pluck(:id).to_a
			network_ids = Network.where('organisation_id IN(?)', organisations_ids).pluck(:id).to_a
		end
		unless params[:range_start].blank?
			if @line_ranges
				@line_ranges.where('range_start LIKE ?',"#{params[:range_start]}%").includes(:sms_hub).all 
			else
				@line_ranges = SmsHubLineRange.where('range_start LIKE ?',"#{params[:range_start]}%").includes(:sms_hub).all 
			end
		end
		if organisations_ids
			p 'this is also run'
			if @line_ranges
				@line_ranges.where('sms_hubs.network_id IN(?)', network_ids).all	
			else
				@line_ranges = SmsHubLineRange.where('sms_hubs.network_id IN(?)', network_ids).all	
			end
		end
		unless params[:imsi].blank?
			p 'this is run now'
			if @line_ranges
				@line_ranges.where('sms_hubs.imsi LIKE ?', "#{params[:imsi].to_s}%").all	
			else
				@line_ranges = SmsHubLineRange.joins(:sms_hub).where('sms_hubs.imsi LIKE ?', "#{params[:imsi].to_s}%").all
			end
		end
		render layout: false
	end

	def line_range_overview_code_ajax_data
		p 'is it blank'
		p params[:organisation_name].blank?
		unless params[:organisation_name].blank?
			p 'this is run'
			organisations_ids = Organisation.where('name LIKE ?', "#{params[:organisation_name]}%").pluck(:id).to_a
			network_ids = Network.where('organisation_id IN(?)', organisations_ids).pluck(:id).to_a
		end
		unless params[:range_start].blank?
			if @line_ranges
				@line_ranges.where('range_start LIKE ?',"#{params[:range_start]}%").includes(:code).all 
			else
				@line_ranges = CodeRange.where('range_start LIKE ?',"#{params[:range_start]}%").includes(:code).all 
			end
		end
		if organisations_ids
			p 'this is also run'
			if @line_ranges
				@line_ranges.where('sms_hubs.network_id IN(?)', network_ids).all	
			else
				@line_ranges = CodeRange.where('sms_hubs.network_id IN(?)', network_ids).all	
			end
		end
		unless params[:imsi].blank?
			p 'this is run now'
			if @line_ranges
				@line_ranges.where('sms_hubs.imsi LIKE ?', "#{params[:imsi].to_s}%").all	
			else
				@line_ranges = CodeRange.joins(:code).where('code.imsi LIKE ?', "#{params[:imsi].to_s}%").all
			end
		end
		render layout: false
	end

	def mno_ip_overview
		@lte_mno_ip   = LteMnoIp.order('id desc').all
		@lte_sccp_ip  = LteSccpIp.order('id desc').all
	end

	def update_mno_sccp_ip
		params.each do |param|
			p "param::#{param}"
			p params[:ip_data]
			params[:ip_data].each do |key, value|
				p "key is #{key}"
				p "value is #{value}"
				type = value.first.first.split('|').first
				if type == 'sccp'
					LteSccpIp.find(key).update_column(value.first.first.split('|').last, value.first.last)
				else
					LteMnoIp.find(key).update_column(value.first.first.split('|').last, value.first.last)
				end
			end
		end
	end

	def add_mno_ip_line
		LteMnoIp.create.save
		redirect_to mno_ip_overview_path, notice: 'Line added'
	end

	def delete_mno_ip_line
		LteMnoIp.find(params[:id]).destroy
		redirect_to :back
	end

	def add_sccp_ip_line
		LteSccpIp.create.save
		redirect_to mno_ip_overview_path, notice: 'Line added'
	end

	def delete_sccp_ip_line
		LteSccpIp.find(params[:id]).destroy
		redirect_to :back
	end

	private
end
