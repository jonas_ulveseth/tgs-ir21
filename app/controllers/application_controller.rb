class ApplicationController < ActionController::Base
  helper_method :current_user, :current_user_session, :require_login
  before_action :require_login unless Rails.env.test?
  protect_from_forgery with: :exception

  def user_for_paper_trail
    if current_user
      current_user.id
    end
  end

  def require_login
    unless current_user
      redirect_to root_path, notice: "You have to login", turbolinks: false
    end
  end

  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.record
  end

  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end
end
