class ChangeHistoriesController < ApplicationController
  before_action :set_change_history, only: [:show, :edit, :update, :destroy]
  
  def index
    @change_histories = ChangeHistory.all
  end

  def show
    @change_histories = ChangeHistory.where(network_id: @change_history.network_id,
                                            network_section_id: @change_history.network_section_id).all
  end

  def new
    @change_history = ChangeHistory.new
  end

  def edit
  end

  def create
    @change_history = ChangeHistory.new(change_history_params)

    respond_to do |format|
      if @change_history.save
        format.html { redirect_to @change_history, notice: 'Change history was successfully created.' }
        format.json { render :show, status: :created, location: @change_history }
      else
        format.html { render :new }
        format.json { render json: @change_history.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @change_history.update(change_history_params)
        format.html { redirect_to @change_history, notice: 'Change history was successfully updated.' }
        format.json { render :show, status: :ok, location: @change_history }
      else
        format.html { render :edit }
        format.json { render json: @change_history.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @change_history.destroy
    respond_to do |format|
      format.html { redirect_to change_histories_url, notice: 'Change history was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_change_history
      @change_history = ChangeHistory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def change_history_params
      params.require(:change_history).permit(:data, :network_section_id)
    end
end
