class NetworkSectionFieldsController < ApplicationController
  before_action :set_network_section_field, only: [:show, :edit, :update, :destroy]

  def index
    @network_section_fields = NetworkSectionField.all
  end

  def show
  end

  def new
    @network_section_field = NetworkSectionField.new
  end

  def edit
  end

  def create
    @network_section_field = NetworkSectionField.new(network_section_field_params)

    respond_to do |format|
      if @network_section_field.save
        format.html { redirect_to network_section_fields_path, notice: 'Network section field was successfully created.' }
        format.json { render :show, status: :created, location: @network_section_field }
        format.js
      else
        format.html { render :new }
        format.json { render json: @network_section_field.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @network_section_field.update(network_section_field_params)
        format.html { redirect_to @network_section_field, notice: 'Network section field was successfully updated.' }
        format.json { render :show, status: :ok, location: @network_section_field }
        format.js
      else
        format.html { render :edit }
        format.json { render json: @network_section_field.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_field
     @network_section_field = NetworkSectionField.find(params[:id])
     @network_section_field.name = params[:name]
     if @network_section_field.save
      
     else
      raise 'Could not save field'
     end 
     respond_to do |format|
      format.js
     end
  end

  def destroy
    @network_section_field.destroy
    respond_to do |format|
      format.html { redirect_to network_section_fields_url, notice: 'Network section field was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_network_section_field
      @network_section_field = NetworkSectionField.find(params[:id])
    end

    def network_section_field_params
      params.require(:network_section_field).permit(:network_section_id, :name, :sort_order)
    end
  end
