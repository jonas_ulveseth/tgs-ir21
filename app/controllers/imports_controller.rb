class ImportsController < ApplicationController
  before_action :set_import, only: [:show, :edit, :update, :destroy]
  def index
    @imports        = Import.where(archive: false, ready_for_import: :true, removed: false)
    @failed_imports = Import.where(failed: true, archive: false, ready_for_import: :true, removed: false).order('created_at desc').all 
  end

  def show
    @import = Import.find(params[:id])
  end

  def new
    @import = Import.new
  end

  def edit
  end

  def create
    @import = Import.new(import_params)
    @import.update(user_id: current_user.id)
    uploaded_file = params[:import][:file_name]
    unique_string = (0...8).map { (65 + rand(26)).chr }.join
    basename = File.basename(uploaded_file.original_filename, File.extname(uploaded_file.original_filename))
    
    # Name with file ending, spaces taken out
    @import.global_file_name        = "#{basename.tr(' ', '_')
    .squish}"
    # Name with unique string added
    uploaded_file.original_filename = "#{basename.tr(' ', '_')
    .squish}_#{unique_string}.#{uploaded_file.original_filename.split('.').last}"
    # File name for displaying purposes
    @import.file_name               = uploaded_file.original_filename.split('.')[0..-2].join('.').tr(' ', '_').squish
    @import.file_type               = uploaded_file.original_filename.split('.').last
    # File name with string used to read file
    @import.original_filename       = uploaded_file.original_filename
    
    #Setting import as waiting
    @import.ready_for_import = false

    File.open(Rails.root.join('data', 'upload', uploaded_file.original_filename), 'wb') do |file|
      file.write(uploaded_file.read) 
    end
    Dir.chdir("#{Rails.root}/data/upload") do
      File.rename(@import.original_filename,"#{@import.file_name}.#{@import.file_type}")
    end
    
    @import.data = uploaded_file.read

    @import_check = Import.where(global_file_name: @import.global_file_name, file_type: @import.file_type, removed: false).first

    respond_to do |format|
      if @import.save
        format.html { redirect_to imports_path, notice: 'Import was successfully created.' }
        
        format.json { render :show, status: :created, location: @import }
        format.js
      else
        format.html { render :new }
        format.json { render json: @import.errors, status: :unprocessable_entity }
        format.js
      end
    end
  end

  def update
    respond_to do |format|
      if @import.update(import_params)
        format.html { redirect_to @import, notice: 'Import was successfully updated.' }
        format.json { render :show, status: :ok, location: @import }
      else
        format.html { render :edit }
        format.json { render json: @import.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @import.update_columns(removed: true)
    respond_to do |format|
      format.html { redirect_to imports_url, notice: 'Import was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def active_imports
    @imports = Import.order('created_at desc').take(100)
    render json: @imports
  end

  def resolve_conflict
    @import = Import.find(params[:import_id])
    Dir.chdir("#{Rails.root}/data/upload") do
      doc = File.read(@import.original_filename)
      file = Nokogiri::XML(doc)
      xml_creation_time = file.css('//RAEXIR21FileHeader/FileCreationTimestamp').first.text
      path = file.css('//OrganisationInfo/NetworkList/Network/NetworkData/RoutingInfoSection/CCITT_E212_NumberSeries')
      mcc = path.css('MCC').first.text
      mnc = path.css('MNC').first.text
      old_network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ?", mcc, mnc).first
      # If multiple network inside one import
      old_networks = old_network.organisation.networks
      @conflicts = PaperTrail::Version.where("network_id IN(?) AND created_at > ? AND whodunnit IS NOT NULL", old_networks.pluck(:id).to_a, xml_creation_time).all
    end
  end

  def scraping_settings
    @scraping_settings = Scraping.first
    @scraping_settings = Scraping.new unless @scraping_settings
  end

  def update_scrapings
    respond_to do |format|
      scraping = Scraping.first_or_create!

      if scraping.update(username: params[:scraping][:username], password: params[:scraping][:password])
        format.html {
          redirect_to :back, notice: "updated!"
        }
        format.json { render :show, status: :ok, location: @import }
      else
        format.html { render scraping_settings_path }
        format.json { render json: @scraping_settings.errors, status: :unprocessable_entity }
      end
    end
  end

  def scrape_test
    agent = Mechanize.new
    @content = agent.get('https://infocentre2.gsma.com/_layouts/InfoCentre/Login.aspx?ReturnUrl=%2f_layouts%2fAuthenticate.aspx%3fSource%3d%252FPages%252Fdefault%252Easpx&Source=%2FPages%2Fdefault%2Easpx')
    @form = @content.forms.first
    @form['aspnetForm[username]'] = 'endre.syvertsen@telenor.com'
    @form['aspnetForm[password]'] = 'Magnus02'
    @submit = @form.submit
  end

  def soap_test
    require 'savon'
    hostname = '10.219.57.4' #fake
    client = Savon::Client.new(endpoint: "https://osl2sms1:8888/Wing/services",
     namespace: "https://osl2sms1:8888/Wing/services",
     basic_auth: ["DATEK", "Y[m,jC4Y4*DWTeL}"],
     ssl_verify_mode: :none)
    @client = client
  end

  def link_pdf
    if params[:remove]
      network = Network.find(params[:network_id])
      import = Import.find(params[:import_id])
      pdf = Pdf.where(import_id: import.id, name: import.file_name).first
      PdfNetwork.where(pdf_id: pdf.id, network_id: params[:network_id]).delete_all
    else
      import = Import.find(params[:import_id])
      pdf = Pdf.where(import_id: import.id, name: import.file_name, original_filename: import.original_filename).first_or_create
      if params[:network_id]
        network = Network.find(params[:network_id])    
        pdf.pdf_networks.where(network_id: params[:network_id]).first_or_create
        import.organisation_id = network.organisation.id
      end
      if params[:organisation_id]
        organisation = Organsation.find(params[:organisation_id])
        organisation.networks.each do |network|
          pdf.pdf_networks.new(network_id: network.id)
        end
      end
    end
    @linked_networks = pdf.pdf_networks
  end

  def mark_pdf_networks
    import = Import.find params[:import_id]
    @pdf_networks = import.pdfs.last.try(:pdf_networks)
  end

  def import_files
    @network = Network.find(params[:network_id]) if params[:network_id]
    unless params[:start_date]
      params[:stop_date] = Time.now.to_date
      params[:start_date] = 30.days.ago.to_date
    end
    unless params[:network_id]
      @files = Import.where("created_at BETWEEN ? AND ? AND removed = false",params[:start_date], params[:stop_date].to_date.end_of_day).order("created_at DESC").all
    else
      networks_ids = Network.find(params[:network_id]).organisation.networks.pluck(:id)
      @files = Import.where("created_at BETWEEN ? AND ? AND network_id IN(?) OR organisation_id = ? AND removed = false",params[:start_date], params[:stop_date].to_date.end_of_day,networks_ids.to_a, Network.find(params[:network_id]).organisation.id).order("created_at DESC").all
    end
  end

  def read_file
    import = Import.find(params[:id])
    if import.global_file_name
      send_file "#{Rails.root}/data/upload/#{import.file_name}.#{import.file_type}", :filename => "#{import.global_file_name}.#{import.file_type}", :disposition => 'attachment'
    else
      send_file "#{Rails.root}/data/upload/#{import.file_name}.#{import.file_type}", :filename => "#{import.file_name[0...import.file_name.rindex('_')]}.#{import.file_type}", :filename => "#{import.global_file_name}.#{import.file_type}", :disposition => 'attachment'
    end
  end

  def activate_all_imports
    @imports = Import.where(ready_for_import: false, user_id: current_user.id).all
    @imports.each do |import|
        #store delayed job
        @import = import
        @name = "import"
        Dir.chdir("#{Rails.root}/data/upload") do
          case @import.file_type 
          when "xml"
            @import.delay(queue: 'import').import(File.read("#{@import.file_name}.#{@import.file_type}"))
            @name = 'xml_import'
          when "pdf"
            @import.delay(queue: 'import').import_pdf
            @name = 'pdf_import'
          when "xlsx"
            @import.delay(queue: 'import').import_excel
            @name = 'excel import'
          end
        end

      end
      @imports.update_all(ready_for_import: :true)
      render nothing: :true
      # Store activity
      Activity.create!({
        name: @name,
        user_id: current_user.id,
        import_id: @import.id   
      })
    end

    def summery
      unless params[:start_date]
        params[:stop_date] = Time.now.to_date
        params[:start_date] = 30.days.ago.to_date
      end
      @imports = Import.where("created_at BETWEEN ? AND ?", params[:start_date], params[:stop_date].to_date.end_of_day).order("created_at DESC").all

    end

    def upload_sms_hub_file
      if params[:file]
        uploaded_file = params[:file]
        File.open(Rails.root.join('data', 'upload', 'code_base_file.csv'), 'wb') do |file|
          #storing file
          file.write(uploaded_file.read)
          redirect_to networks_path, notice: 'File uploaded and re-generation initiated, please wait 2 hours before creating new file.'
        end
      end
    end

    def import_details
      @import = Import.find(params[:id])
    end

    def create_missing_structure
      @structure = ImportMissingSection.find(params[:id])
      unless @structure.network_section_id.nil?
        XmlSection.create!(name: @structure.tag_name, network_section_id: @structure.network_section_id, accept_data: @structure.accept_data, xml_version_id: @structure.import.xml_version.id)
      end
      @structure.destroy
      redirect_to :back, notice: "Structure added to version"
    end

    def resolve
      unless current_user.has_role? :superadmin
        redirect_to :imports, notice: 'You do not have access to this function, please talk to admin'
      end
      import = Import.find(params[:import_id])
      if params[:organisation_override]
        import.organisation_override = ["#{params[:organisation_override]}"]  
      else
        import.organisation_override = nil
      end
      import.resolved_by_admin = true
      import.failed = nil
      import.error_message = nil
      import.save
      Dir.chdir("#{Rails.root}/data/upload") do
        if import.file_type == 'xlsx'
          import.delay(queue: 'import').import_excel
        else
          import.delay(queue: 'import').import(File.read("#{import.file_name}.#{import.file_type}"))
        end
      end
      redirect_to :imports, notice: 'Issue resolved - Import restarted'
    end  

    private
  # Use callbacks to share common setup or constraints between actions.
  def set_import
    @import = Import.find(params[:id])
  end

  def import_params
    params.permit(:import).permit(:file_name, :file)
    params.permit(:all)
  end
end
