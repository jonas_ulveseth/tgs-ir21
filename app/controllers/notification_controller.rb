class NotificationController < ApplicationController
  skip_before_action :verify_authenticity_token
  def index
    # Set notified date
    unless Rails.env.test?
      @previous_read_date = current_user.notified 
      if current_user.notified.nil?
       @previous_read_date = current_user.created_at
     end
    end    
   if Rails.env.production? 
    @notifications = Notification.where("created_at > ?", @previous_read_date).all
   else
    @notifications = Notification.all
   end
    # Update read-date to be same as today
    unless Rails.env.test?   
      current_user.update(notified: Time.now)
    end
  end

  # GET /notify-groups
  def notify_groups
    @notify_groups = NotifyGroup.all
  end
  # GET /notify-group/:id
  def notify_group
    @notify_group   = NotifyGroup.find(params[:id])
    gon.notify_group_id  = @notify_group.id
    @added_organisations = @notify_group.organisations
    @added_networks      = @notify_group.networks
    #if @added_networks
    @networks            = Network.where.not(id: @added_networks.pluck(:id).to_a).all 
    @organisations       = Organisation.where.not(id: @added_organisations.pluck(:id).to_a).all
    # Users that are not added to this group
    @unlisted_users = User.where("ID NOT IN(?)", @notify_group.users.pluck(:id).to_a)
  end 

  # GET /add-notify-group-profile
  # Function to add a new profile
  def add_profile
    NotifyGroup.create!(name: params[:name])
    redirect_to :back
  end

  # GET /show-notify-networks
  # Showing scoped networks based on organsiations
  def show_networks
    organisation = Organisation.find(params[:organisation_id])
    @networks = organisation.networks
  end

  # POST /add-notify-group-network
  # Add a network to a profile
  def add_network
    params[:networks].each do |network|
      NotifyGroupsNetwork.create!(notify_group_id: params[:notify_group_id], network_id: network)
    end
  end

  # POST /add-notify-group-organisation
  # Add a network to a profile
  def add_organisation
    params[:organisations].each do |organisation|
      NotifyGroupsOrganisation.create!(notify_group_id: params[:notify_group_id], organisation_id: organisation)
    end
    render nothing: :true
  end

  # DELETE /delete-notify-group/:id 
  # Delete chosen group and return
  def delete_notify_group
    NotifyGroup.find(params[:id]).destroy
    redirect_to :back, notice: "Group was removed"
  end

  # Ajax function to add a notification group to a user
  def add_notify_group_to_user
    NotifyGroupsUser.create!(user_id: params[:user_id], notify_group_id: params[:notify_group_id])
    redirect_to :back, notice: "Notify group was added to user"
  end

  # Ajax function to add notification settings to user
  def add_notify_setting_to_user
    NotifyFieldUser.create!(notify_field_id: params[:notify_field_id], user_id: params[:user_id])
    render json: {'user_id': params[:user_id], 'notify_field_id': params[:notify_field_id]}
  end

  # Ajax function to remove notification setting from user
  def remove_notify_setting_from_user
    NotifyFieldUser.where(user_id: params[:user_id], notify_field_id: params[:notify_field_id]).first.delete
    render json: {'user_id': params[:user_id], 'notify_field_id': params[:notify_field_id]}
  end
end
