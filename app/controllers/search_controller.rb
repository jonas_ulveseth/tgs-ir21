class SearchController < ApplicationController
  def index

  	unless params[:q]
  		return redirect_to root_path, notice: "You have to add a search query"
  	end

  	q = params[:q].downcase
  	@organisations = Organisation.where("name @@ :q", q: q).all
  	@network_base  = Network.where("lower(network_name) @@ :q 
  																		OR lower(tadig_code) @@ :q 
  																		OR lower(country_initials_and_mnn) @@ :q
	  																	OR lower(abbreviated_mnn) @@ :q
  																		OR lower(network_color_code) @@ :q 
  																		OR lower(display_name) = :q", q: q).all
  	@networks 		   = Network.where("routing -> 'e214_cc' = :q 
                                      OR routing -> 'e214_nc' = :q 
                                      OR routing -> 'e212_mcc' = :q 
                                      OR routing -> 'e212_mnc' = :q", q: q).all
		hstore_keys      = NetworkRow.try(:hstore_keys)
		@network_rows    = NetworkRow.where(hstore_keys.collect {|c| "data -> '#{c}' @@ :q" }
                                          .join(' OR '), q: q).pluck(:network_id)
  	# Extra searching for Ranges
    @ranges = NetworkRow.where("data -> 'MSISDN - cc' @@ :q OR data -> 'MSISDN - ndc' @@ :q 
                                                            OR data -> 'MSISDN - cc' @@ :q
                                                            OR data -> 'MSISDN - range_start' @@ :q
                                                            OR data -> 'MSISDN - range_stop' @@ :q
                                                            OR data -> 'MSRN - ndc' @@ :q 
                                                            OR data -> 'MSRN - cc' @@ :q
                                                            OR data -> 'MSRN - range_start' @@ :q
                                                            OR data -> 'MSRN - range_stop' @@ :q
                                                            OR data -> 'GT - ndc' @@ :q 
                                                            OR data -> 'GT - cc' @@ :q
                                                            OR data -> 'GT - range_start' @@ :q
                                                            OR data -> 'GT - range_stop' @@ :q", q: q).pluck(:network_id)

    @ranges_network  = Network.where("ID in (?)", @ranges.to_a)
    @hstore_networks = Network.where("ID in (?)", @network_rows.to_a)
    @result 			   = (@network_base + @networks + @network_base + @hstore_networks + @ranges_network)
  end
end