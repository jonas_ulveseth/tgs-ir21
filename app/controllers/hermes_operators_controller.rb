class HermesOperatorsController < ApplicationController
  before_action :set_hermes_operator, only: [:show, :edit, :update, :destroy]

  def index
    @hermes_operators = HermesOperator.all
  end

  def show
  end

  def new
    @hermes_operator = HermesOperator.new
  end

  def edit
  end

  def create
    @hermes_operator = HermesOperator.new(hermes_operator_params)

    respond_to do |format|
      if @hermes_operator.save
        format.html { redirect_to @hermes_operator, notice: 'Hermes operator was successfully created.' }
        format.json { render :show, status: :created, location: @hermes_operator }
      else
        format.html { render :new }
        format.json { render json: @hermes_operator.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @hermes_operator.update(hermes_operator_params)
        format.html { redirect_to @hermes_operator, notice: 'Hermes operator was successfully updated.' }
        format.json { render :show, status: :ok, location: @hermes_operator }
      else
        format.html { render :edit }
        format.json { render json: @hermes_operator.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @hermes_operator.destroy
    respond_to do |format|
      format.html { redirect_to hermes_operators_url, notice: 'Hermes operator was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_hermes_operator
      @hermes_operator = HermesOperator.find(params[:id])
    end

    def hermes_operator_params
      params.require(:hermes_operator).permit(:network_id, :data)
    end
end
