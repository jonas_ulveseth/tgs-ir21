class NetworkSectionsController < ApplicationController
  before_action :set_network_section, only: [:show, :edit, :update, :destroy]

  def index
    @network_sections = NetworkSection.where("network_section_id is null").all.order("name")
  end

  def show
  end

  def new
    @network_section = NetworkSection.new
    @network_parent_fields = NetworkSection.all.order("name")
  end

  def edit
    @network_section_field = NetworkSectionField.new
    @network_parent_fields = NetworkSection.all.order("name")
  end

  def create
    @network_section = NetworkSection.new(network_section_params)

    respond_to do |format|
      if @network_section.save
        format.html { redirect_to @network_section, notice: 'Network section was successfully created.' }
        format.json { render :show, status: :created, location: @network_section }
      else
        format.html { render :new }
        format.json { render json: @network_section.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @network_section.update(network_section_params)
        format.html { redirect_to edit_network_section_path(@network_section), notice: 'Network section was successfully updated.' }
        format.json { render :show, status: :ok, location: @network_section }
      else
        format.html { render :edit }
        format.json { render json: @network_section.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @network_section.destroy
    respond_to do |format|
      format.html { redirect_to network_sections_url, notice: 'Network section was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_network_section
      @network_section = NetworkSection.find(params[:id])
    end

    def network_section_params
      params.require(:network_section).permit(:name, :collapsed, :parent_id, :xml_field, :field_name, :multi_line, :sort_order, :sortable, :is_network_section, :is_parent_section, :invisible)
    end
end
