class ExportsController < ApplicationController
  before_action :set_export, only: [:show, :edit, :update, :destroy]

  def index
    @exports             = Export.where("user_id = ? OR user_id = 99", current_user.id).where(in_progress: [nil,false]).all.order("created_at DESC")
    @export_types        = ExportType.all
    @personal_exports    = PersonalExport.where(user_id: current_user.id).all
    @exports_in_progress = Export.where("in_progress IS TRUE AND (user_id = ? OR user_id = 99)", current_user.id).all.order("created_at DESC")
  end

  def show
    if @export.data.nil?
      send_file "#{Rails.root}/data/export/#{@export.file}"
    else
      type = @export.export_type.try(:function_call)
      type = type.split(',').last if type
      unless type
        send_data @export.data, type: "csv", filename: "#{@export.file}.csv"
      else
        send_data @export.data, type: type, filename: "#{@export.file}.#{type}"
      end
    end
  end

  def new
    @export = Export.new
    redirect_to exports_path, notice: "Export successfully created"
  end

  def edit
  end

  def create
    @export = Export.new(export_params)

    respond_to do |format|
      if @export.save
        format.html { redirect_to @export, notice: 'Export was successfully created.' }
        format.json { render :show, status: :created, location: @export }
      else
        format.html { render :new }
        format.json { render json: @export.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @export.update(export_params)
        format.html { redirect_to @export, notice: 'Export was successfully updated.' }
        format.json { render :show, status: :ok, location: @export }
      else
        format.html { render :edit }
        format.json { render json: @export.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @export.destroy
    File.delete("#{Rails.root}/data/export/#{@export.file}")
    respond_to do |format|
      format.html { redirect_to exports_url, notice: 'Export was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def hermes_csv_export
    respond_to do |format|
      format.csv {
        Network.hermes_csv_export(current_user)
        redirect_to exports_path, notice: "csv created"
      }
    end
  end

  def lte_sccp_ip_csv_export
    respond_to do |format|
      format.csv {
        Network.lte_sccp_ip_csv_export(current_user)
        redirect_to exports_path, notice: "csv created"
      }
    end
  end

  def lte_mno_ip_csv_export
    respond_to do |format|
      format.csv {
        Network.lte_mno_ip_csv_export(current_user)
        redirect_to exports_path, notice: "csv created"
      }
    end
  end

  def export_all_networks
    respond_to do |format|
      format.xls {
        Network.create_excel(current_user)
        redirect_to exports_path, notice: "excel export created"
      }
    end
  end

  #Export including Rosi and ORG-id, implemented 28 mars 2022
  def export_all_networks_advanced
    respond_to do |format|
      format.csv {
        Network.create_advanced_network(current_user)
        redirect_to exports_path, notice: "excel export created"
      }
    end
  end

  def export_all_networks_xlsx
    respond_to do |format|
      format.xlsx {
        Network.create_excel_xlsx(current_user)
        redirect_to exports_path, notice: "xlsx excel export created"
      }
    end
  end

  def export_telenor_connexion
    respond_to do |format|
      format.xls {
        Network.export_telenor_connexion(current_user)
        redirect_to exports_path, notice: "excel export created"
      }
    end
  end

  def download_export
    export = Export.find(params[:export_id])
    if export.export_type_id == 4 || export.export_type_id == 5 
      send_file("#{Rails.root}/data/export/#{export.file}", :filename => "#{export.file}")
    else
      send_file("#{Rails.root}/data/export/#{export.file}", :filename => "#{export.file}")
    end
  end

  def export_sms_hub
    respond_to do |format|
      format.csv {
        Network.export_sms_hub(current_user)
        redirect_to exports_path, notice: "Sms hub export created"
      }
    end
  end

  def customized_exports
    @fields = ExportField.all
    if params[:commit]
      personal_export = PersonalExport.create(name: params[:name], user_id: current_user.id)
      params[:main_fields].each do |field|
        PersonalExportField.create(export_field_id: field.first.to_i, personal_export_id: personal_export.id)
      end
      redirect_to exports_path, notice: 'New customized export created'
    end
  end

  def create_manual_export
    personal_export = PersonalExport.includes(personal_export_fields: :export_field).find(params[:personal_export_id])

    filename = "#{personal_export.name}-#{Time.now}.xls"
    workbook = WriteExcel.new("#{Rails.root}/data/export/#{filename}")
    worksheet = workbook.add_worksheet
    export_map      = personal_export.personal_export_fields.map {|field| field.export_field.name } 
    fields = export_map
    worksheet.write_row('A1', fields)
    excel_counter  = 1
    column_counter = 0
    Network.where('organisation_id IS NOT NULL').each do |network|
     if export_map.include?("organisation")
       worksheet.write(excel_counter, column_counter, network.organisation.try(:name))        
       column_counter += 1
      end
      if export_map.include?("display_name")
        worksheet.write(excel_counter, column_counter, network.try(:display_name))        
        column_counter += 1
      end
      if export_map.include?("country_initials")
        worksheet.write(excel_counter, column_counter, network.try(:country_initials_and_mnn))        
        column_counter += 1
      end
      if export_map.include?("mcc")
        worksheet.write(excel_counter, column_counter, network.try(:e212_mcc))        
        column_counter += 1
      end
      if export_map.include?("mnc")
        worksheet.write(excel_counter, column_counter, network.try(:e212_mnc))        
        column_counter += 1
      end
      if export_map.include?("cc")
        worksheet.write(excel_counter, column_counter, network.try(:e214_cc))        
        column_counter += 1
      end
      if export_map.include?("nc")
        worksheet.write(excel_counter, column_counter, network.try(:e214_nc))        
        column_counter += 1
      end
      if export_map.include?("number portability")
        worksheet.write(excel_counter, column_counter, "portability::#{network.try(:number_portability)}")        
        column_counter += 1
      else
        p 'not finding'
      end
      if export_map.include?("xml creation date")
        p 'kör xml create'
        worksheet.write(excel_counter, column_counter, network.try(:xml_creation_date))        
        column_counter += 1
      else
        p 'kör inte'
      end
      if export_map.include?("xml creation time")
        worksheet.write(excel_counter, column_counter, network.try(:xml_creation_time))        
        column_counter += 1
      end
      if export_map.include?("tadig schema version")
        worksheet.write(excel_counter, column_counter, network.try(:tadig_gen_schema_version ))        
        column_counter += 1
      end
      if export_map.include?("RAEX schema version")
        worksheet.write(excel_counter, column_counter, network.try(:raex_ir21_schema_version))        
        column_counter += 1
      end
      if export_map.include?("Abbreviated mnn")
        worksheet.write(excel_counter, column_counter, network.try(:abbreviated_mnn))        
        column_counter += 1
      end
      if export_map.include?("Terrestrial?")
        worksheet.write(excel_counter, column_counter, network.try(:terrestrial))        
        column_counter += 1
      end
      if export_map.include?("filedate")
        worksheet.write(excel_counter, column_counter, network.import.try(:file_creation_dater))        
        column_counter += 1
      end
      if export_map.include?("comments")
        worksheet.write(excel_counter, column_counter, network.try(:comment))        
        column_counter += 1
      end
      if export_map.include?("tadig")
        worksheet.write(excel_counter, column_counter, network.try(:tadig_code))        
        column_counter += 1
      end
      if export_map.include?("gt_number_ranges")
        gt_number_ranges = NetworkRow.where("network_section_id = 167 AND network_id = ?", network.id).all
        gt_range = gt_number_ranges.map {|gt| ["#{gt.data['GT - cc']}#{gt.data['GT - ndc']}"] }
        worksheet.write(excel_counter, column_counter, gt_range.join(';').to_s)        
        column_counter += 1
      end
      if export_map.include?("msrn_number_ranges")
        msrn = NetworkRow.where("network_section_id = 168 AND network_id = ?", network.id).pluck(:data).to_a
        worksheet.write(excel_counter, column_counter, msrn)        
        column_counter += 1
      end
      if export_map.include?("msisdn_number_ranges")
        msisdn = NetworkRow.where("network_section_id = 169 AND network_id = ?", network.id).pluck(:data).to_a
        worksheet.write(excel_counter, column_counter, msisdn)        
        column_counter += 1
      end
      column_counter = 0
      excel_counter += 1
    end
    workbook.close

    export = Export.new
    export.data = nil
    export.user_id = current_user.id
    export.export_type_id = 4
    export.file = filename
    export.save
    redirect_to :back
  end

  def export_ip_addresses
    if params[:redirect]
      return redirect_to exports_path, turbolinks: :false 
    end
    #Post
    if params[:data_type]
      generate_file(params[:data_type])
    end
    @networks = Network.all
    @data_type = session[:data_type]     
  end

  def generate_codes
    respond_to do |format|
      format.csv {
        export = Export.create(in_progress: true, user_id: 99, export_type_id: 7, progress: 'started')
        Code.delay(queue: 'codes').create_file(true, export)
        redirect_to exports_path, notice: "Code file generation started"
      }
    end
  end

  def generate_single_codes
    respond_to do |format|
      format.xlsx {
        export = Export.create(in_progress: true, user_id: 99, export_type_id: 8, progress: 'started')
        Code.delay(queue: 'codes').create_single_line_file(filter=true, export)
        redirect_to exports_path, notice: "A7 file generation started"
      }
    end
  end

  def sor_export
    respond_to do |format|
      format.csv {
        Export.export_sor_csv
        redirect_to exports_path, notice: "Sor export created"
      }
    end
  end

  def advanced_sor_export
    respond_to do |format|
      format.csv {
        export = Export.create(in_progress: true, user_id: 99, export_type_id: 11, progress: 'started')
        Export.delay(queue: 'codes').export_advanced_sor(export)
        redirect_to exports_path, notice: "Advanced Sor export created"
      }
    end
  end

  def sccp_ip_export
    respond_to do |format|
      format.csv {
        Export.export_sccp_ip
        redirect_to exports_path, notice: "Sor ecport created"
      }
    end
  end

  private
    def set_export
      @export = Export.find(params[:id])
    end

    def export_params
      params.require(:export).permit(:file, :user_id)
    end

    def generate_file(datatype)
      if datatype == 'ip'
        outgoing_file = "#{Rails.root}/data/export/ip_addresses_#{Time.now}.csv" 
        data = CSV.open(outgoing_file, "wb", col_sep: ';', force_quotes: false) do |csv_export_row|       
          fields = ['mnc', 'mcc', 'Network ID', 'Network Name', 'IP addresses']
          csv_export_row << fields
          Network.all.each do |network|
            array = network.network_rows.where(network_section_id: 161).all.map {|ip| ip.data.first[1]}
            csv_export_row << [network.routing['e212_mnc'], network.routing['e212_mcc'], network.id, network.display_name, array.join("\n")]
          end
        end
      end
      if datatype == 'grx'
        outgoing_file = "#{Rails.root}/data/export/grx_providers_#{Time.now}.csv" 
        data = CSV.open(outgoing_file, "wb", col_sep: ';', force_quotes: false) do |csv_export_row|       
          fields = ['mnc', 'mcc', 'Network ID', 'Network Name', 'grx providers']
          csv_export_row << fields
          Network.all.each do |network|
            # Get all networks rows
            array = network.network_rows.where(network_section_id: 129).all.map {|ip| ip.data.first[1]}
            csv_export_row << [network.routing['e212_mnc'], network.routing['e212_mcc'],network.id, network.display_name, array.join("\n")]
          end
        end
      end
      if datatype == 'asn'
        outgoing_file = "#{Rails.root}/data/export/asn_list_#{Time.now}.csv" 
        data = CSV.open(outgoing_file, "wb", col_sep: ';', force_quotes: false) do |csv_export_row|       
          fields = ['mnc', 'mcc', 'Network ID', 'Network Name', 'asn']
          csv_export_row << fields
          Network.all.each do |network|
            # Get all networks rows
            array = network.network_rows.where(network_section_id: 128).all.map {|ip| ip.data.first[1]}
            csv_export_row << [network.routing['e212_mnc'], network.routing['e212_mcc'],network.id, network.display_name, array.join("\n")]
          end
        end
      end
      export = Export.new
      export.data = nil
      export.user_id = current_user.id
      export.export_type_id = 4
      export.file = outgoing_file.split('/').last
      export.save
    end
  end
