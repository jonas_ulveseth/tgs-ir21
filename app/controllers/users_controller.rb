  class UsersController < ApplicationController
    before_action :set_user, only: [:show, :edit, :update, :destroy]
    skip_before_action :require_login, only: [:forgot_password, :send_reset_password, :change_password, :update]

    require 'securerandom'
    # GET /users
    # GET /users.json
    def index
      @users = User.where("inactive IS NULL OR inactive = ?", false).order("created_at DESC").all
    end

    # GET /users/1
    # GET /users/1.json
    def show
    end

    # GET /users/new
    def new
      @user = User.new
    end

    # GET /users/1/edit
    def edit
      @added_groups        = NotifyGroupsUser.where(user_id: @user.id)
      @added_notify_groups = NotifyGroup.where("ID IN(?)",@added_groups.pluck(:notify_group_id).to_a)
      @views               = ViewGroup.all

      if @added_notify_groups.blank?      
        @notification_groups = NotifyGroup.all
      else
        @notification_groups = NotifyGroup.where.not("ID IN(?)",@added_notify_groups.pluck(:id).to_a)
      end
      # Get notify fields
      @notify_fields = NotifyField.includes(:notify_field_users).all  
      gon.user_id = params[:id]
    end

    # POST /users
    # POST /users.json
    def create
      @user = User.new(user_params)

      respond_to do |format|
        if @user.save
          format.html { redirect_to @user, notice: 'User was successfully created.' }
          format.json { render :show, status: :created, location: @user }
        else
          format.html { render :new }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /users/1
    # PATCH/PUT /users/1.json
    def update
      respond_to do |format|
        if @user.update(user_params)
          # If there is an password reset token is removed from user and user is logged out
          @notify_fields = NotifyField.includes(:notify_field_users).all  
          if session[:password_reset] == true
            @user.update(reset_password_token: nil)
            @user_session = UserSession.find
            @user_session.destroy
            return redirect_to root_path, notice: "Your password is reset, please login"
          end
          format.html { redirect_to @user, notice: 'User was successfully updated.' }
          format.json { render :show, status: :ok, location: @user }
        else
          format.html { render :edit }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /users/1
    # DELETE /users/1.json
    def destroy
      @user.update(inactive: true)
      respond_to do |format|
        format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    #
    # GET /forgot-password
    # Function to send out a new password
    #
    def forgot_password
      @user = User.new
    end

    def send_reset_password
      user = User.where(email: params[:user][:email]).first
      unless user
        return redirect_to :back, notice: "Unable to find a user with that email"
      end
      user.reset_password_token = SecureRandom.hex
      user.save
      UserMailer.send_password_reset(user, user.reset_password_token).deliver_now
    end

    def change_password
      #check if user is logged in, and if not check for token
      has_access = current_user.has_role? :admin if current_user
      if params[:user_id] && has_access == true
        @user = User.find(params[:user_id])
      else
        if current_user
          @user = current_user
        else
          if params[:token]
            session[:password_reset] = true
            @user = User.where(reset_password_token: params[:token]).first
            return redirect_to root_path, notice: "Token is not valid or not active" unless @user
          else
            raise 'We could not find any user'
          end
          unless @user
            raise 'We could not find any user'
          end
        end
      end
    end

    def add_role
      @user = User.find params[:user_id]
      if params[:role_id]
        role = Role.find params[:role_id]
        @user.add_role(role.name.underscore.to_sym)
        redirect_to edit_user_path(@user), notice: "User role has been updated"
      end
    end

    def remove_role
      role = Role.find params[:role_id]
      user = User.find params[:user_id]
      user.remove_role role.name.to_sym
      redirect_to :back, notice: "Role was removed from user"
    end

    def delete_from_notify_group
      @group = NotifyGroupsUser.where(user_id: params[:user_id], notify_group_id: params[:group_id]).first
      @group.destroy
      respond_to do |format|
        format.html { redirect_to :back, notice: 'User was successfully removed from group' }
        format.json { head :no_content }
      end
    end

    def add_view_setting_to_user
      User.find(params[:user_id]).update_columns(view_id: params[:view_id])
      render nothing: true
    end

    def remove_view_setting_from_user
      User.find(params[:user_id]).update_columns(view_id: nil)
      render nothing: true
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_user
        @user = User.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def user_params
        params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :reset_password_token)
      end
    end
