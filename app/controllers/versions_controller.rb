class VersionsController < ApplicationController
	def index
		ActiveRecord::Base.establish_connection("beta").connection if Rails.env == "beta"
		@network = Network.where(id: params[:network_id]).first
		@version = PaperTrail::Version.select("date(created_at),whodunnit, network_id")
																	.where(network_id: params[:network_id])
																	.group("date(created_at),whodunnit, network_id").order("date DESC")
	end

	def show
	 @version = PaperTrail::Version.find(params[:id])
	end

	def revert
		
	end
end
