class ActivitiesController < ApplicationController
  before_action :set_activity, only: [:show, :edit, :update, :destroy]

  def index
    # If no params are set, we provide basic dates for 30 days span
    unless params[:start_date]
      params[:stop_date] = Time.now.to_date
      params[:start_date] = 30.days.ago.to_date
    end

    if params[:network_id]
      import_ids  = Import.where(network_id: params[:network_id]).pluck(:id).to_a
      @network    = Network.find_by(id: params[:network_id]) 
      @activities = Activity.order("created_at DESC").where("import_id IN(?)", import_ids)
    else
      @activities = Activity.order("activities.created_at DESC").where("activities.created_at > ? AND activities.created_at < ?", params[:start_date], params[:stop_date].to_date.end_of_day).joins(:import).all
    end
  end  

  def new
    @activity = Activity.new
  end

  def edit
  end

  def create
    @activity = Activity.new(activity_params)

    respond_to do |format|
      if @activity.save
        format.html { redirect_to @activity, notice: 'Activity was successfully created.' }
        format.json { render :show, status: :created, location: @activity }
      else
        format.html { render :new }
        format.json { render json: @activity.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @activity = Activity.find(params[:id])
    @activity.lte_status = params[:activity][:lte_status] if params[:activity][:lte_status]
    @activity.ip_status = params[:activity][:ip_status] if params[:activity][:ip_status]
    @activity.routing_status = params[:activity][:routing_status] if params[:activity][:routing_status]
    @activity.ip_status = params[:activity][:ip_status] if params[:activity][:ip_status]
    @activity.stp_status = params[:activity][:stp_status] if params[:activity][:stp_status]
    @activity.save
    redirect_to :back
  end

  def destroy
    @activity.destroy
    respond_to do |format|
      format.html { redirect_to activities_url, notice: 'Activity was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def set_activity
    @activity = Activity.find(params[:id])
  end

  def activity_params
    params.permit(:start_date, :stop_date, :update_id, :lte_status, :ip_status, :routing_status, :stp_status)
    params.permit(:activity).permit(:name, :user_id, :type, :message, :status, :lte_status, :ip_status, :routing_status, :stp_status)
  end
end
