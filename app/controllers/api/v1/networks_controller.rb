class Api::V1::NetworksController < ApplicationController
  skip_before_action :require_login
  before_action :set_network, only: [:show, :edit, :update, :destroy]

  def index
    @networks = Network.all
  end

  def show
    # Realms in LTE section
    # Country iso code
    # Country full name
    # CC - country code
    # Tad code - Tadig code
    # Network name - Organisation name
    # Ip address - Ip roaming IW info general
    # Ip netmask - IP Roaming IW general IpAdressOrRange
    # Ndc = ndc (CCITT_E214_MGT (NC number))
    # Gobal title. =  GT number ranges 
    # Operator name = Organisation name ? Check if network name I filled for above
    # Technology = ?
  end


  def connexion
     @networks = Network.all  
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_network
      @network = Network.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def network_params
      params.require(:network).permit(:organisation_id, :tadig_code, :network_name, :terrestrial, :presentation_of_country_initials_and_mnn, :abbreviated_mnn, :network_color_code, :source)
    end
end
