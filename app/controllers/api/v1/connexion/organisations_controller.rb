class Api::V1::Connexion::OrganisationsController < ApplicationController
  skip_before_action :require_login
  http_basic_authenticate_with name: "connexion", password: "dRe6RtW"
  before_action :set_network, only: [:show, :edit, :update, :destroy]

  def index
    @organisations = Organisation.includes(:networks).all
    @ns = NetworkSection.where(field_name: 'ip_roaming_info').first
  end

  def show
  end


  def connexion
     @networks = Network.all  
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_network
      @network = Network.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def network_params
      params.require(:network).permit(:organisation_id, :tadig_code, :network_name, :terrestrial, :presentation_of_country_initials_and_mnn, :abbreviated_mnn, :network_color_code, :source)
    end
end