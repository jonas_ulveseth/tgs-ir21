class UserSessionsController < ApplicationController
  skip_before_action :require_login

  def new
    @user_session = UserSession.new
  end

  def create
    @user_session = UserSession.new(params[:user_session])
    if @user_session.save
      redirect_to '/networks', notice: "You where logged in <%= current_user.id %>"
    else
      render action: :new
    end
  end

  # Used to login to telenor_gs login system
  def remote_login
    #ActiveRecord::Base.establish_connection("tgs_beta").connection if Rails.env == "beta"
    ActiveRecord::Base.establish_connection("production").connection if Rails.env == "production"
    user = User.where('UPPER(email) = ?', params[:email].try(:upcase)).first
    unless Rails.env == "testserver"
      @user_session = UserSession.new(user_session_params)

      if @user_session.save
        if @user_session.user.inactive == true
          @user_session.destroy
          return redirect_to root_path, notice: "Incorrect password or username"
        end
        redirect_to networks_path, notice: "You where logged in"
      else
        redirect_to root_path, notice: "Incorrect password or username", turbolinks: false
      end
    end
    if user
      #return forgot_password(user) if params[:commit] == 'Forgot password'
      if user.authenticate(params[:password])
        session[:user_id] = user.id
        session[:user] = user
        session[:user_role] = user.access_level
        if params[:remember_me]
          cookies.permanent.signed[:user_id] = user.id
        else
          cookies.permanent.signed[:user_id] = nil
        end
        redirect_to '/networks', notice: 'Logged in!', turbolinks: false
        return
      end
      redirect_to root_path, notice: "Incorrect password or username", turbolinks: false
      ActiveRecord::Base.establish_connection("beta").connection if Rails.env == "beta"
    else
      flash.now.alert = 'Invalid email'
    end
    new
  end

  def destroy
    if Rails.env.beta?
      session.clear
    else
      begin
        current_user_session.destroy
      rescue
      end
    end
    redirect_to root_path, notice: "you where logged out"
  end
  private
  def user_session_params
    params.require(:user_session).permit(:email, :password)
  end
end
