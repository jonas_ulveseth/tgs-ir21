class NetworksController < ApplicationController
  before_action :set_network, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token
  layout false, only: [:get_rest_of_networks, :get_rest_of_networks_dynamic]

  def index
    ActiveRecord::Base.establish_connection("development").connection if Rails.env.development?
    ActiveRecord::Base.establish_connection("beta").connection if Rails.env.beta?
    @networks = Network.joins(:organisation).order("organisations.updated_at desc").order('parent_id desc').group('organisation_id, networks.id, organisations.updated_at').take(30)
    if current_user.has_role? :superadmin
      @caching_profile = 'superadmin'
    else
      @caching_profile = 'standard'
    end

    # Check if user has dynamic view set
    if current_user.view_id
      @dynamic_view = ViewGroup.find_by(id: current_user.view_id)
      return render "networks/dynamic_views/#{@dynamic_view.id}"
    end

    respond_to do |format|
      format.html
      format.csv { csv = @networks.includes(:network_rows).to_csv
        export = Export.new
        export.data = csv
        export.user_id = current_user.id
        export.file = "networks.csv-#{Time.now}"
        export.save
        redirect_to exports_path, notice: "csv created"
      }
    end
  end

  # Function used to update table with rest of networks, to make a better fluid experience
  def get_rest_of_networks
    @networks = Network.offset(30).joins(:organisation).order("organisations.updated_at desc").order('parent_id desc').group('organisation_id, networks.id, organisations.updated_at').all 
    if current_user.has_role? :superadmin
      @caching_profile = 'superadmin'
    else
      @caching_profile = 'standard'
    end
  end

  # Function used to update table with rest of networks, to make a better fluid experience
  def get_rest_of_networks_dynamic
    @networks = Network.offset(30).joins(:organisation).order("organisations.updated_at desc").group('organisation_id, networks.id, organisations.updated_at').all 
    #Setup diffrent caching profiles
    @caching_profile = params[:id]
    render 'networks/dynamic_views/get_rest_of_networks'
  end

  def show
    @network_sections = NetworkSection.includes(:network_rows)
    .order("network_sections.sort_order ASC NULLS LAST").order("network_rows.network_section_id")
    .where("network_rows.network_id = ?", params[:id])
    .includes(:change_histories).all
    @inactive_sections = NetworkSection.where("id NOT IN(?)", @network_sections.pluck(:id).to_a)
    @other_networks = Network.where("organisation_id = ? AND id != ?", @network.organisation_id, @network.id)
    
    @pdfs = PdfNetwork.where(network_id: @network.id).all

    if @pdfs.blank?
      @pdf = Import.where(id: @network.pdf_import_id).last
      # Legacy fetching where pdf_import_id is not set
      @pdf = Import.where("network_id = ? AND file_type = ?", @network.id, "pdf").last if @pdf.nil?
      # Fetch pdf with common organisation
      common_networks = Network.where(organisation_id: @network.organisation_id).pluck(:id).to_a
      @pdf = Import.where("network_id IN(?) AND file_type = ?", common_networks.to_a, "pdf").last if @pdf.nil?
    end

    @gt_number_ranges = NetworkRow.where("network_section_id = 167 AND network_id = ?", params[:id]).all
    @msisdn_number_ranges = NetworkRow.where("network_section_id = 169 AND network_id = ?", params[:id]).all
    @msrn_number_ranges = NetworkRow.where("network_section_id = 168 AND network_id = ?", params[:id]).all
    # Pass id to coffescript
    gon.network_id = params[:id]
  end

  def lazy_load_line_ranges
    @network = Network.find(params[:network_id])
    render :layout => false
  end

  def lazy_load_network_sections
    @network = Network.find(params[:id])
    @network_sections = NetworkSection.includes(:network_rows)
    .order("network_sections.sort_order ASC NULLS LAST").order("network_rows.network_section_id")
    .where("network_rows.network_id = ?", params[:id])
    .includes(:change_histories).includes(:network_rows).all
    @inactive_sections = NetworkSection.where("id NOT IN(?)", @network_sections.pluck(:id).to_a)
    render :layout => false
  end

  def new
    raise 'no access' unless current_user.has_any_role? :admin, :superadmin unless Rails.env.test?
    @network = Network.new
  end

  def edit
    raise 'no access' unless current_user.has_any_role? :admin, :superadmin unless Rails.env.test?
  end

  def create
    raise 'no access' unless current_user.has_any_role? :admin, :superadmin unless Rails.env.test?
    @network = Network.new(network_params)
    organisation = Organisation.where(name: params[:network][:organisation_id].to_s).first
    @network.organisation_id = organisation.id if organisation
    country = Country.where(iso_code: params[:network][:country_initials_and_mnn]).first
    @network.country_id = country.id if country

    respond_to do |format|
      if @network.save
        format.html { redirect_to @network, notice: 'Network was successfully created.' }
        format.json { render :show, status: :created, location: @network }
      else
        format.html { render :new }
        format.json { render json: @network.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    raise 'no access' unless current_user.has_any_role? :admin, :superadmin unless Rails.env.test?
    respond_to do |format|
      if @network.update(network_params)
        if network_params[:comment]
          return redirect_to :back
        end
        format.html { redirect_to @network, notice: 'Network was successfully updated.' }
        format.json { render :show, status: :ok }
        format.js {

        }
      else
        format.html { render :edit }
        format.json { render json: @network.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    raise 'no access' unless current_user.has_any_role? :superadmin unless Rails.env.test?
    import = Import.where(network_id: @network.id).first
    import.update_column(:network_id,nil) if import
    @network.destroy
    PaperTrail.whodunnit == current_user["id"] unless Rails.env.test?
    # Updated cache
    Organisation.first.touch
    respond_to do |format|
      format.html { redirect_to networks_url, notice: 'Network was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def update_hstore_field
    raise 'no access' unless current_user.has_any_role? :admin, :superadmin unless Rails.env.test?
    respond_to do |format|
      @network = Network.find(params[:id])
      field_name = params[:key].parameterize('_')
      @network.routing_will_change!
      @network.routing["#{field_name}"] = params[:value].squish
      puts @network.routing
      @network.save!
      @network.organisation.touch
      @versions_count = PaperTrail::Version.where(item_id: @network.id).all.count
      format.js
    end
    Activity.create!({
      name: "Network update",
      user_id: current_user.id,
      import_id: @network.import.try(:id)   
    })
  end

  def update_network_field
    raise 'no access' unless current_user.has_any_role? :admin, :superadmin unless Rails.env.test?
    respond_to do |format|
      @network = Network.find(params[:id])
      field_name = params[:key].parameterize('_')
      @network["#{field_name}"] = params[:value].squish
      @network.save
      @network.organisation.touch
      @versions_count = PaperTrail::Version.where(item_id: @network.id).all.count
      format.js
    end
    Activity.create!({
      name: "Network update",
      user_id: current_user.id,
      import_id: @network.import.try(:id)   
    })
  end

  def update_hstore_section_field
    raise 'no access' unless current_user.has_any_role? :admin, :superadmin unless Rails.env.test?
    respond_to do |format|
      @network_section = NetworkRow.find(params[:id])
      field_name = params[:key].parameterize('_')
      @network_section.data["#{field_name}"] = params[:value]
      @network_section.save!
      format.js
    end
    Activity.create!({
      name: "Network update",
      user_id: current_user.id,
      import_id: @network_section.network.import.try(:id)   
    })
  end

  def update_network_index_field
    raise 'no access' unless current_user.has_any_role? :admin, :superadmin unless Rails.env.test?
    respond_to do |format|
      @network = Network.find(params[:id])
      case params[:key]
      when "tadig"
        @network.tadig_code = params[:value]
        @network.save 
      when "display_name"
        @network.display_name = params[:value]
        @network.save
      when "organisation_name"
        organisation = @network.organisation
        organisation.name = params[:value]
        organisation.save
      when "country_name"
        country = @network.country
        country.name = params[:value]
        country.save
      when "network_name"
        @network.network_name = params[:value]
        @network.save
      when "organisation_id"
        @network.organisation_id = params[:value].to_i
        @network.save  
      when "e212_mcc"
        @network.e212_mcc = params[:value]
        p 'its alive'
        @network.save
      when "e212_mnc"
        @network.e212_mnc = params[:value]
        @network.save
      when "country_code"
        @network.routing["e214_cc"] = params[:value]
        @network.save
      when "rosi_id"
        @network.rosi_id =  params[:value]
        @network.save
        # Update code rosi id
        code = Code.find_by(network_id: @network.id)
        code.update(rosi_id: @network.rosi_id) if code
      when "e214_nc"
        @network.routing["e214_nc"] = params[:value]
        @network.save
      when "dpc_hex"
      when "dpc_decimal"
      when "sc_type"    
      end
      @network.organisation.try(:touch)
      # Updated cache
      ActionController::Base.new.expire_fragment(Organisation.all)
      format.js
    end

    Activity.create!({
      name: "Network update",
      user_id: current_user.id,
      import_id: @network.import.try(:id)   
    })
  end

  def read_xml
    @network = Network.find(params[:network_id])
    import = @network.import
    if import.file_name.split('_').last.upcase
      send_data File.read("#{Rails.root}/data/upload/#{import.file_name}.xml"), :filename => "#{import.file_name[0...import.file_name.rindex('_')]}.xml", :disposition => 'inline'
    else
      send_data File.read("#{Rails.root}/data/upload/#{import.file_name}.xml"), :filename => "#{import.file_name}.xml", :disposition => 'inline'
    end
  end

  def read_pdf
    if params[:pdf_id]
      import = Pdf.find(params[:pdf_id])
      if import.name.split('_').last.upcase
        send_file("#{Rails.root}/data/upload/#{import.name}.pdf", :filename => "#{import.name[0...import.name.rindex('_')]}.pdf", :type => "application/pdf", :disposition => 'inline')
      else
        send_file("#{Rails.root}/data/upload/#{import.name}.pdf", :filename => "#{import.name}.pdf", :type => "application/pdf", :disposition => 'inline')
      end
    else
      import = Import.find(params[:import])
      if import.file_name.split('_').last.upcase
        send_file("#{Rails.root}/data/upload/#{import.file_name}.pdf", :filename => "#{import.file_name[0...import.file_name.rindex('_')]}.pdf", :type => "application/pdf", :disposition => 'inline')
      else
        send_file("#{Rails.root}/data/upload/#{import.file_name}.pdf", :filename => "#{import.file_name}.pdf", :type => "application/pdf", :disposition => 'inline')
      end
    end
    
  end

  def read_excel
    @network = Network.find(params[:network_id])
    import = @network.import
    
    if import.file_name.split('_').last.upcase
      if import.file_name.include? "_"
        send_data File.read("#{Rails.root}/data/upload/#{import.file_name}.xlsx"), :filename => "#{import.file_name[0...import.file_name.rindex('_')]}.xlsx"
      else
        send_data File.read("#{Rails.root}/data/upload/#{import.file_name}.xlsx"), :filename => "#{import.file_name}.xlsx"
      end
    else
      send_data File.read("#{Rails.root}/data/upload/#{import.file_name}.xlsx"), :filename => "#{import.file_name}.xlsx"
    end  

  end

  def delete_row 
    raise 'no access' unless current_user.has_any_role? :admin, :superadmin unless Rails.env.test?
    nr = NetworkRow.find(params[:id]).destroy
    respond_to do |format|
      format.json { head :ok }
    end 
  end

  def flagged_networks
    @network_diffs = NetworkDiff.select("MAX(created_at) AS time, *").where("network_id IS NOT NULL").group("network_id, id, created_at").order("time DESC, network_id").all 
    # Set notified date
    @previous_read_date = current_user.notified
  end

  def show_diff_snapshot
    @diff = NetworkDiff.find(params[:id])
  end

  def add_line_range
    raise 'no access' unless current_user.has_any_role? :admin, :superadmin unless Rails.env.test?
    lr = NetworkLineRange.create(range_start: params[:start_range], range_stop: params[:stop_range], network_id: params[:network_id])
    redirect_to :back
  end

  def edit_linerange
    raise 'no access' unless current_user.has_any_role? :admin, :superadmin unless Rails.env.test?
    if params[:sms_hub]
      if params[:column] == 'range_stop'
        SmsHubLineRange.update(params[:id],range_stop: params[:value])
      else
        SmsHubLineRange.update(params[:id],range_start: params[:value])
      end
    else  
      if params[:column] == 'range_stop'
        NetworkLineRange.update(params[:id],range_stop: params[:value])
      else
        NetworkLineRange.update(params[:id],range_start: params[:value])
      end
    end
  end

  def delete_line_range
    raise 'no access' unless current_user.has_any_role? :admin, :superadmin unless Rails.env.test?
    NetworkLineRange.find(params[:id]).delete  
    return redirect_to :back
  end

  def delete_from_notify_group
    @group = NotifyGroupsNetwork.where(network_id: params[:network_id], notify_group_id: params[:notify_group_id]).first
    @group.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Network was successfully removed from group' }
      format.json { head :no_content }
    end
  end

  def show_network_comment
    @network = Network.find(params[:network_id])
  end

  def change_network_status
    ns = NetworkStatus.where(id: params[:network_status][:id]).first
    ns.update_columns(status: params[:network_status][:status])
    ns.save!
    render nothing: :true
  end
  
  private

  def set_network
    @network = Network.find(params[:id])
  end

  def network_params
    params.require(:network).permit(:organisation_id, :tadig_code, :network_name, :terrestrial, :presentation_of_country_initials_and_mnn, :abbreviated_mnn, :network_color_code, :source, :field_name, :xml_creation_date, :xml_creation_time, :tadig_gen_schema_version, :raex_ir21_schema_version, :display_name, :comment, :sccp_status, :routing_status)
  end
end
