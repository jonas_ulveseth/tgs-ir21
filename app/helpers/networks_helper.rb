module NetworksHelper
	# function to iterate over deep level hash
	def walk_tree(tree,depth= 0, &blk)
		Rails.cache.fetch("#{tree}/tree") do
			return enum_for(:walk_tree, depth) unless blk
			
			yield tree, depth
			
			tree.each do |node|
				unless node.is_a?(String)
					walk_tree(node, depth + 1, &blk)
				end
			end
		end
	end

	def get_dpc_hex(network)
		ns = NetworkSection.where(field_name: "international_dpc_item").first
		nr = Rails.cache.fetch("#{network}/dpc_hex") do NetworkRow.where(network_id: network.id, network_section_id: ns.try(:id)).first end
		if nr
			if nr.data["dpc-hex"]
				return nr.data["dpc-hex"]
			end
			if nr.data["dpc_hex"]
				return nr.data["dpc_hex"]
			end 
			dpc = NetworkRow.where(network_section_id: 149, network_id: network.id).first
			split = dpc.data['dpc'].split('-')
			split = dpc.data['dpc'].split('_') if split.nil?
			return result = split[0].to_i * 2048 + split[1].to_i * 8 + split[2].to_i
		end
	end

	def get_dpc_decimal(network)
		ns = NetworkSection.where(field_name: "international_dpc_item").first
		nr = Rails.cache.fetch("#{network}/dpc_decimal") do NetworkRow.where(network_id: network.id, network_section_id: ns.try(:id)).first end
		if nr
			if nr.data["dpc-decimal"] 
				return nr.data["dpc-decimal"] 
			end
			if nr.data["dpc_decimal"]
				return nr.data["dpc_decimal"]
			end
			
			dpc = NetworkRow.where(network_section_id: 149, network_id: network.id).first
			split = dpc.data['dpc'].split('-')
			split = dpc.data['dpc'].split('_') if split.nil?
			result = split[0].to_i * 2048 + split[1].to_i * 8 + split[2].to_i
			return result.to_s(16)
		end
	end

	def get_sc_type(network)
		ns = network_section = NetworkSection.where(field_name: "international_dpc_item").first
		nr = NetworkRow.where(network_id: network.id, network_section_id: ns.try(:id)).first
		nr.data["sc_type"] if nr
	end

	def get_country_code(network)
		if network.routing
			network.routing["e214_cc"]
		else
			"No country defined"
		end
	end
end