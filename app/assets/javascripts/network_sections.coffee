$( document ).on 'turbolinks:load', ->
	if $(".network_sections").length > 0
		# GET /network_sections/edit
		if $("#edit").length > 0
			#### Prevent form submit on enter push ####
			$('.form-horizontal').on("keyup keypress", (e)->
  			code = e.keyCode || e.which; 
  			if (code  == 13)               
    		e.preventDefault()
    		return false)
			#### Show edit icon on mouse over ####
			$('.section-field').mouseenter ->
				$(this).find(".edit-icon").show()
			$('.section-field').mouseleave ->
				$(this).find(".edit-icon").hide()
			#### Show input on click ####
			$('.section-field').click (e)->	
				unless ($(event.target).is('.section-field'))
					name = $(this).data('name')
					$(this).html("<input name='name' class='edit-field-input' placeholder='#{name}'/>")
			#### Send update field on clicking ####
			$('.section-field').keypress (e)->
				if (e.which == 13)
					data = {
					id: $(this).attr('id'),
					name: $(this).find('input').val()
					}
					$.ajax(
						url: "/network-section-update-field", 
						type: "PUT"
						datatype: "js"
						data: data)