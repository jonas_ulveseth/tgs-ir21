$( document ).on 'turbolinks:load', ->
	if $(".activities").length
		$('table').tablesorter(
        theme: 'bootstrap', widthFixed: true,
        showProcessing: true,
        headerTemplate: '{content} {icon}',
        widgets: ['uitheme', 'zebra', 'stickyHeaders', 'filter'])
		$('select').change ->
			this.form.submit()
