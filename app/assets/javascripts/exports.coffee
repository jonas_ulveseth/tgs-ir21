$( document ).on 'turbolinks:load', ->
	if $(".exports").length
		if("#export_ip_addresses").length
			$('.choice').click (e)->
				sessionStorage.setItem("export_type", $(this).data('exporttype'))
			$('.link').click ->
				unless sessionStorage.export_type
					alert('You have to set data type')
				else
					$.ajax({
						type: "POST",
						url: '/export-ip-addresses',
						data: {data_type: sessionStorage.export_type},
						success: window.location.replace("/export-ip-addresses?redirect=true"),
						dataType: 'JSON'
					})
				sessionStorage.clear()	