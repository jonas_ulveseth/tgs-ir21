$( document ).on 'turbolinks:load', ->
	if $(".users").length
		if $("#edit").length
			$('.view-checkbox').click ->
				data = 
					user_id: gon.user_id
					view_id: $(this).val()
				if $(this).prop('checked') == true
					# Call ajax function to remove setting
					$.ajax(
						url: "/ir21/add-view-setting-to-user"
						type: "POST"
						datatype: "js"
						data: data
						)
				else
					# Call ajax function to add setting
					$.ajax(
						url: "/ir21/remove-view-setting-from-user"
						type: "POST"
						datatype: "js"
						data: data
						)
			$('.notify-checkbox').click ->
				data = 
					user_id: gon.user_id
					notify_field_id: $(this).val()
				if $(this).prop('checked') == true
					# Call ajax function to remove setting
					$.ajax(
						url: "/ir21/add-notify-setting-to-user"
						type: "POST"
						datatype: "js"
						data: data
						)
				else
					# Call ajax function to add setting
					$.ajax(
						url: "/ir21/remove-notify-setting-from-user"
						type: "POST"
						datatype: "js"
						data: data
						)