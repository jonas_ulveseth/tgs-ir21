$( document ).on 'turbolinks:load', ->
#### slidein menu on right ####
  $("#menu-expand").on('click', (e)->
    e.preventDefault()
    $('ul.slidein-right').toggleClass("active")
    $(this).toggleClass("active"))

  $("html").click (e)->
    unless e.target.id == 'menu-expand' || $(e.target).hasClass('slidein-right') || $(e.target).parent().hasClass('slidein-right')
      $('ul.slidein-right').removeClass('active')
  #### notice bar on top ####
  if $('#alert').length > 0
    $(".content").delay(2000).fadeOut(1000)
    $("#alert").delay(3000).animate({width: 'toggle'}, 1000)
  #### slide in sign-out and settings for menu ####
  $(".slidein-right .user").mouseenter ->
    $(".fields").addClass('active')
  $(".slidein-right .user").mouseleave ->
    $(".fields").removeClass('active')
