$( document ).on 'turbolinks:load', ->
  if $(".index").length
    if $("#import_diffs").length
      $('table').tablesorter(
        theme: 'bootstrap', widthFixed: true,
        showProcessing: true,
        headerTemplate: '{content} {icon}',
        widgets: ['uitheme', 'zebra', 'stickyHeaders', 'filter']
        )
    if $("#import_diffs").length
      $('.imsi').click ->
        $( this ).siblings().toggle("slow")
    if $("#show_sms_hub_data").length > 0
      $('table').tablesorter(
        theme: 'bootstrap', widthFixed: true,
        showProcessing: true,
        headerTemplate: '{content} {icon}',
        widgets: ['uitheme', 'zebra', 'stickyHeaders', 'filter'])
    if $("#line_range_overview_export").length > 0
      $('table').tablesorter(
        theme: 'bootstrap', widthFixed: true,
        showProcessing: true,
        headerTemplate: '{content} {icon}',
        widgets: ['uitheme', 'zebra', 'stickyHeaders', 'filter'])
      $('.tablesorter-filter').keyup((e)->
        data = {
          organisation_name: $('.tablesorter-filter[data-column=0]').val()
          range_start: $('.tablesorter-filter[data-column=1]').val()
          imsi: $('.tablesorter-filter[data-column=2]').val()
          imported_from: $('.tablesorter-filter[data-column=4]').val()
        }
        $.ajax(
            url: "/ir21/get-sms-hub-data",
            type: "POST"
            datatype: "js"
            data: data
          )
      )
    if $("#line_range_code_export").length > 0
      $('table').tablesorter(
        theme: 'bootstrap', widthFixed: true,
        showProcessing: true,
        headerTemplate: '{content} {icon}',
        widgets: ['uitheme', 'zebra', 'stickyHeaders', 'filter'])
      $('.tablesorter-filter').keyup((e)->
        data = {
          organisation_name: $('.tablesorter-filter[data-column=0]').val()
          range_start: $('.tablesorter-filter[data-column=1]').val()
          imsi: $('.tablesorter-filter[data-column=2]').val()
          imported_from: $('.tablesorter-filter[data-column=4]').val()
        }
        $.ajax(
            url: "/ir21/get-code-data",
            type: "POST"
            datatype: "js"
            data: data
          )
      )      
    if $("#line_range_overview_ir21").length > 0
      $('table').tablesorter(
        theme: 'bootstrap', widthFixed: true,
        showProcessing: true,
        headerTemplate: '{content} {icon}',
        widgets: ['uitheme', 'zebra', 'stickyHeaders', 'filter'])
    if $('#mno_ip_overview').length
      data = {}
      $(document.body).on('keyup','.editing', (e)->
        if (e.which == 13)
          $('.data-row').each (index,elem)->
            children   = $(elem).find('.editing')
            if children.length
              ip_id      =  $(elem).data('id')
              console.log('found children')
              row_object = data[ip_id] = {}
              children.each (index,elem)->
                console.log('looping child') 
                type = $(elem).data('type')
                row_object[type] = $(elem).find('.edit-input').val() 
              #Posting data
              $.ajax(
                url: "update-mno-sccp-ip",
                type: "POST"
                datatype: "js"
                data: {ip_data: data}
              )       
        )
      $('.edit').click (e)->
        if $(this).hasClass('editing')
          e.stopImmediatePropagation()
          e.preventDefault()
          e.stopPropagation()
          return false
        text = $(this).text().trim()
        $(this).focus
        $(this).removeClass('edit')
        $(this).addClass('editing')
        console.log(text)
        $(this).html("<input class='edit-input' style='width: 100%' value='#{text}'>")