$( document ).on 'turbolinks:load', ->
  if $(".networks").length
    # GET networks
    if $("#index").length
      #### Post form on dropdown change ####
      $('select').change (e)->
        element = $(e.target)
        console.log($(this).parents('.update'))
        $(this).siblings('.update').trigger('click')
      #### Click function for rows, to not use edit function ####
      $(document.body).on "click", ".network-row", (e)->
        if ($(e.target).attr('class') != 'fa fa-pencil-square' && $(e.target).attr('class') != 'edit_field' && $(e.target).attr('id') != 'network_status_status' && $(e.target).attr('class') != 'comment' && $(e.target).attr('class') != 'update' && $(e.target).attr('class') != 'delete' && $(e.target).attr('class') != 'fa fa-times delete')
          Turbolinks.visit('/networks/' + $(this).attr('id'))   
      #### Implementation of tablesorter ####
      $('table').tablesorter(
        theme: 'bootstrap', widthFixed: true,
        showProcessing: true,
        headerTemplate: '{content} {icon}',
        widgets: ['uitheme', 'zebra', 'stickyHeaders', 'filter'])
      $(document.body).on "mouseenter", ".edit", ->
        $(this).addClass("active")
        $(this).append('<span id="edit-icon"><i class="fa fa-pencil-square"></i></span>')
      $(document.body).on "mouseleave", ".edit", ->
        $(this).removeClass("active")
        $("#edit-icon").remove()
      #### Comment click function ####
      $(document.body).on "click", ".comment", (e)->
        e.preventDefault()
        e.stopImmediatePropagation()
        $('.modal').remove()
        $.get("show-network-comment?network_id=#{$(this).data('network-id')}")
      #### click function for edit ####
      $(document.body).on "click", ".edit", (e)->
        width = $(this).width()
        if ($(e.target).is('i'))
          sibling_text = $(this).prev().html()
          $(this).html("<input id='edit_input' class='edit_field' type='data' data-type-name='#{sibling_text}' placeholder='#{$(this).text()}'>")
          $("body").attr("data-type-name", sibling_text)
          $(this).css('width', width)
      #### Send update hstore on clicking ####
      $(document.body).on "keypress", ".edit", (e)->
        if (e.which == 13)
          data = {}
          data['value'] = $(".edit > input").val()
          data['id'] = $(this).parent().attr('id')
          data['key'] = $(this).data('type-name')
          console.log($(this).parent().attr('id'))
          $.ajax(
            url: "/ir21/networks/#{data['id']}/update_network_index_field",
            type: "PUT"
            datatype: "js"
            data: data
          ).done (html) ->
      #### Hide form on clicking outside ####
      $(document.body).on "blur", "#edit_input", ->
        value = $(this).attr("placeholder")
        $(this).css('display', 'none')
        $(this).replaceWith(value)
    # GET networks/1
    if $("#show").length
      #### Lazy loading of network sections ####
      # Network sections
      $.ajax({
        url: "/ir21/lazy-load-network-sections?id=" + gon.network_id ,
        cache: false,
        success: (html)->
          $("#network-sections").html(html)
      })  

      # Line ranges
      $.ajax({
        url: "/ir21/lazy-load-line-ranges?network_id=" + gon.network_id ,
        cache: false,
        success: (html)->
          $("#line-ranges").html(html)
      })
      
      network_section_callback = "/ir21/lazy-load-network-sections?id=" + gon.network_id 

      #### Hide last parent of extra generated parents ####
      $(".extra-parent").last().remove()

      #### show edit icon on hover ####
      $(document.body).on "mouseenter", ".edit", ->
        $(this).addClass("active")
        $(this).append('<span id="edit-icon"><i class="fa fa-pencil-square"></i></span>')
      $(document.body).on "mouseleave", ".edit", ->
        $(this).removeClass("active")
        $("#edit-icon").remove()
      #### Tablesorter for ccit_e164 ####
      $('.container #network-sections').load(network_section_callback, ->
        $('.sortable').tablesorter(
          theme: 'bootstrap', widthFixed: true,
          showProcessing: true,
          headerTemplate: '{content} {icon}',
          widgets: ['uitheme', 'zebra', 'stickyHeaders', 'filter'])
        )
      
      #### show edit fields on click ####
      $(document.body).on "click", ".edit", (e)->
        width = $(this).width()
        if ($(e.target).is('i'))
          sibling_text = $(this).prev().html()
          $(this).html("<input id='edit_input' type='data' data-type-name='#{sibling_text}' placeholder='#{$(this).text()}'>")
          $("body").attr("data-type-name", sibling_text)
          $(this).css('width', width)

      #### Send update hstore on clicking ####
      $(document.body).on "keypress", ".edit-hstore", (e)->
        if (e.which == 13)
          data = {}
          data['key'] = $(this).data('row-key')
          data['value'] = $(".edit > input").val()
          data['id'] = gon.network_id
          $.ajax(
            url: "/ir21/networks/#{gon.network_id}/update_hstore_field",
            type: "PUT"
            datatype: "js"
            data: data
          )

      #### Send line range update ####
      $(document.body).on "keypress", ".edit-line-range", (e)->
        if (e.which == 13)
          data = {}
          data['sms_hub'] = $(this).data('sms-hub')
          console.log($(this).data('sms-hub'))
          data['id'] = $(this).data('id')
          data['value'] = $(".edit > input").val()
          data['column'] = $(this).data('type')
          console.log(data['column'])
          $.ajax(
            url: "/edit-linerange",
            type: "PUT"
            datatype: "js"
            data: data
          )

      #### Send update hstore-row on clicking ####
      $(document.body).on "keypress", ".edit-hstore-row", (e)->
        if (e.which == 13)
          data = {}
          data['key'] = $(this).data('row-key')
          data['value'] = $(".edit > input").val()
          data['id'] = $(this).data('row-id')
          $.ajax(
            url: "/ir21/networks/#{data['id']}/update_hstore_section_field",
            type: "PUT"
            datatype: "js"
            data: data
          )
      #### Edit network directly ####
      $('.edit-network').on "keypress", (e)->
        if (e.which == 13)
          data = {}
          data['key'] = $(".edit > input").data('type-name')
          data['value'] = $(".edit > input").val()
          data['id'] = gon.network_id
          $.ajax(
            url: "/ir21/networks/#{gon.network_id}/update_network_field",
            type: "PUT"
            datatype: "js"
            data: data
          ).done (html) ->

      #### Hide form on clicking outside ####
      $(document.body).on "blur", "#edit_input", ->
        value = $(this).attr("placeholder")
        $(this).css('display', 'none')
        $(this).replaceWith(value)

      #### Add new section on click ####
      $(document.body).on "click", ".modal .name", ->
        $(".new-section-container").show().html("<h1>#{$(this).data("name")}</h1><br/><br/><input/><input/><input/><input/><input/><input/><br/><br/>")
        $('#myModal').modal('hide')
        section = $.getJSON("/network_sections/#{$(this).attr('id')}.json", (data)->
          #parse out the fields here
        )
      #### Show add-field helpers ####
      $(document.body).on "mouseenter", ".add-section", ->
        $(this).addClass('active')
        $(this).find('.text').show()
      $(document.body).on "mouseleave", ".add-section", ->
        $(this).removeClass('active')
        $(this).find('.text').hide()

      #### Show input fields on new row button click ####
      $(".new-section-row .fa").click ->
        id = $(this).parent().attr('id')
        $("#dummy_" + id).show()

      #### Spy scrollbar ####
      setTimeout ( ->
        $('body').scrollspy({
          target: '#navbar-spy'
          offset: 100
        })
      ), 5000
      $("#navbar-spy").hover ->
        $(this).toggleClass("expanded")
      #animate smoother to right section
      $('#navbar-spy a[href^=\'#\']').on 'click', (e) ->
        e.preventDefault()
        hash = @hash
        $('html, body').animate {scrollTop: $(@hash).offset().top - 100}, 800, ->
          window.location.hash = hash - 500

      #### Show collapsed section ####
      $(document.body).on "click", ".show-collapsed", ->
        section_id = $(this).data("section-id")
        $("#table_#{section_id}").show()
        $(this).parent().find('.hide-collapsed').show()
        $(this).hide()
      #### Hide collapsed section ####
      $(document.body).on "click", ".hide-collapsed", ->
        $(this).siblings().show()
        $(this).hide()
        section_id = $(this).data("section-id")
        $("#table_#{section_id}").hide()

    # GET flagged-networks
    if $("#flagged-networks")
      $('.sortable').tablesorter(
        theme: 'bootstrap', widthFixed: true,
        showProcessing: true,
        headerTemplate: '{content} {icon}',
        widgets: ['uitheme', 'zebra', 'stickyHeaders', 'filter'])
