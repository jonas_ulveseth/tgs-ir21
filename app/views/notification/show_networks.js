$('#scoped-networks').html("<%= j render 'notification/organisation_networks', networks: @networks  %>");
$('.btn').show();
<% if @networks.blank? %>
$('#scoped-networks').html('No networks under this organsation');
$('.btn').hide();
<% end %>