json.array!(@network_sections) do |network_section|
  json.extract! network_section, :id, :name, :fields
  json.url network_section_url(network_section, format: :json)
end
