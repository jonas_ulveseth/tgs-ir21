json.array!(@activities) do |activity|
  json.extract! activity, :id, :name, :user_id, :type
  json.url activity_url(activity, format: :json)
end
