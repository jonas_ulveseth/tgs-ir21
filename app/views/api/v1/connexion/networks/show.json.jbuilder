json.network_details do
	json.operator_name @network.organisation.name
	json.ip_address @ip_address 
	json.nc @network.routing["e214_nc"]
	json.global_title @gt_number_ranges
end											
