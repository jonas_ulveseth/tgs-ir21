#json.array!(@networks) do |network|
#  json.extract! network, :id, :tadig_code, :routing, :network_name
#  json.url api_v1_connexion_organisation_network_url(network.organisation_id,id: network, format: :json)
#end

json.networks @networks do |network|
	json.id network.id
	json.tadig_code network.tadig_code
	json.mcc network.routing["e212_mcc"]
	json.mnc network.routing["e212_mnc"]
	json.country_iso_code network.country.iso_code
	json.country_name network.country.name
	json.country_code network.routing["e214_cc"]
	json.url api_v1_connexion_organisation_network_url(network.organisation_id,id: network, format: :json, protocol: :https)
end 
