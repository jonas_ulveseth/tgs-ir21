json.cache! ['v1', @organisations] do
json.organisations @organisations do |organisation|
	json.OrganisationInfo do
		json.OrganisationId organisation.id
		json.LastUpdated organisation.updated_at.to_date
		json.OrganisationName organisation.try(:name)
		json.CountryInitials organisation.try(:country).try(:iso_code)
		json.CountryName organisation.try(:country).try(:name)
		json.NetworkList do
			json.array!(organisation.networks) do |network|
				json.id network.id
				json.name network.organisation.try(:name)
				json.TADIGCode network.tadig_code
				json.MCC network.try(:routing).try(:[], 'e212_mcc')
				json.MNC network.try(:routing).try(:[], 'e212_mnc')
				json.GT_NumberRanges do
					json.array!(NetworkRow.where("network_section_id = 167 AND network_id = ?", network.id).all) do |gt|									
						json.NumberRange do 
							json.CC gt.data['GT - cc']
							json.NDC gt.data['GT - ndc']
							json.SN_RangeStart gt.data['GT - range_start']
							json.SN_RangeStop  gt.data['GT - range_stop']
						end
					end	
				end

				json.IPAddressOrRange do
					ipaddresses = NetworkRow.where(network_id: network.id, network_section_id: @ns.id).all
					ipaddresses.collect! {|d| d.data.first[1]} 
					json.IpAddress do
						json.array! ipaddresses
					end
				end

				json.APNOperatorIdentifierItem do
					apn_operator_identifier = NetworkRow.where(network_id: network.id, network_section_id: 174).all
					apn_operator_identifier.collect! {|i| i.data.first[1]}
					json.ApnOperatorIdentifier do
						json.array! apn_operator_identifier
					end
				end
				
				json.EPCRealmsForRoamingList do
					epc_realms = NetworkRow.where(network_id: network.id, network_section_id: 171).all
					epc_realms.collect! {|e| e.data.first[1]}
					json.EPCRealmsForRoaming do
						json.array! epc_realms
					end
				end
			end
		end
	end
end
end