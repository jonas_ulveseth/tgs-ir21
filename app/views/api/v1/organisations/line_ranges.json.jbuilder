json.cache! ['v1', @organisations] do
json.organisations @organisations do |organisation|
  json.OrganisationInfo do
    json.OrganisationId organisation.id
    json.LastUpdated organisation.updated_at.to_date
    json.OrganisationName organisation.try(:name)
    json.CountryInitials organisation.try(:country).try(:iso_code)
    json.CountryName organisation.try(:country).try(:name)
    json.NetworkList do
      json.array!(organisation.networks) do |network|
        json.array!(network.network_line_ranges)
      end
    end
  end
end
end