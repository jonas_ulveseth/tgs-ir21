json.array!(@organisations) do |organisation|
  json.extract! organisation, :id, :name, :country_id
  json.url api_v1_organisations_url(organisation, format: :json)
end
