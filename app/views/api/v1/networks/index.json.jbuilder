json.array!(@networks) do |network|
  json.extract! network, :id, :organisation_id, :tadig_code, :network_name, :terrestrial, :abbreviated_mnn, :network_color_code, :source
  json.url api_v1_network_url(network, format: :json)
end
