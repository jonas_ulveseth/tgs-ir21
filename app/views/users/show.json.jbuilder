json.extract! @user, :id, :first_name, :last_name, :email, :crypted_password, :password_salt, :persistence_token, :created_at, :updated_at
