json.array!(@users) do |user|
  json.extract! user, :id, :first_name, :last_name, :email, :crypted_password, :password_salt, :persistence_token
  json.url user_url(user, format: :json)
end
