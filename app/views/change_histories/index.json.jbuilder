json.array!(@change_histories) do |change_history|
  json.extract! change_history, :id, :data, :network_section_id
  json.url change_history_url(change_history, format: :json)
end
