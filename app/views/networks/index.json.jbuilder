json.array!(@networks) do |network|
  json.extract! network, :id, :organisation_id, :tadig_code, :network_name, :terrestrial, :presentation_of_country_initials_and_mnn, :abbreviated_mnn, :network_color_code, :source
  json.url network_url(network, format: :json)
end
