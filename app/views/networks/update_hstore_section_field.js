$(".network_versions").html("change history (<%= @versions_count %>)");
$(".network_versions").addClass("update").delay(500).queue(function(next){
    $(this).removeClass("update");
    next();
});
value = "<%= params[:value] %>"
$("#edit_input").css('display', 'none');
$("#edit_input").parent().replaceWith('<td>' + value + '</td>');