json.array!(@network_section_fields) do |network_section_field|
  json.extract! network_section_field, :id, :network_section_id, :name, :sort_order
  json.url network_section_field_url(network_section_field, format: :json)
end
