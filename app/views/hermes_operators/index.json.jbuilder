json.array!(@hermes_operators) do |hermes_operator|
  json.extract! hermes_operator, :id, :network_id, :data
  json.url hermes_operator_url(hermes_operator, format: :json)
end
