class UserMailer < ApplicationMailer
	default from: "IR21 - Inventory <no-reply@admin.telenorglobal.com>"
	#Send password reset
	def send_password_reset(user, reset_password_token)
		@user = user
		@reset_password_token = reset_password_token
		mail(to: @user.email, subject: "Reset your password")
	end

	def notify_network_change(user, network)
		@user = user
		@network = network
		if @network.try(:imports).count > 1
			if @network.import.try(:network_diffs)
				mail(to: user.email, subject: "IR21 - reimport with changes")
			else
				mail(to: user.email, subject: "IR21 - reimport without changes")
			end
		else
			mail(to: user.email, subject: "IR21 - Import of new file")
		end
	end

	def notify_deleted_network(user, organisation, diff)
		@user = user
		@dif  = diff
		@organisation = organisation
		mail(to: user.email, subject: "IR21 - network removed from file")
	end
end
