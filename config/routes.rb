Rails.application.routes.draw do
  get '/' => "index#index"

  scope '/ir21' do
    get 'notification/index'

    resources :countries
    resources :change_histories
    resources :activities
    resources :network_section_fields
    resources :hermes_operators
    resources :organisations
    resources :users
    resources :user_sessions
    resources :network_sections
    resources :reports

    resources :exports do
      collection do
        get 'hermes-csv-export', to: 'exports#hermes_csv_export', as: "hermes_csv_export"
        get 'lte-sccp-ip-csv-export', to: 'exports#lte_sccp_ip_csv_export', format: 'csv'
        get 'lte-mno-ip-csv-export', to: 'exports#lte_mno_ip_csv_export', format: 'csv'
        get 'export-all-networks', to: 'exports#export_all_networks', format: 'xls'
        get 'export-all-networks-xlsx', to: 'exports#export_all_networks_xlsx', format: 'xlsx'
        get 'advanced-network-export', to: 'exports#export_all_networks_advanced', format: 'csv'
        get 'export-telenor-connexion', to: 'exports#export_telenor_connexion', format: 'xls'
        get 'generate-codes', to: 'exports#generate_codes', as: :generate_codes, format: 'csv'
        get 'generate-single-codes', to: 'exports#generate_single_codes', as: :generate_single_codes, format: 'xlsx'
        get 'sor-export', to: 'exports#sor_export', as: :sor_export, format: 'csv'
        get 'advanced-sor-export', to: 'exports#advanced_sor_export', as: :advanced_sor_export, format: 'csv'
        get 'sccp-ip-export', to: 'export#sccp_ip_export', as: :sccp_ip_export, format: 'csv'
      end
    end

    resources :imports do
      get 'resolve-conflict', to: "imports#resolve_conflict", as: "resolve-conflict"
      put 'link-pdf', to: 'imports#link_pdf', as: 'link_pdf'
      put 'mark-pdf-networks', to: 'imports#mark_pdf_networks', as: 'mark_pdf_networks'
      put 'unlink-pdf', to: 'imports#unlink_pdf', as: 'unlink_pdf'
      get 'resolve', to: 'imports#resolve', as: :resolve
      collection do
        get 'summery', to: 'imports#summery', as: 'summery'
      end
    end

    resources :networks do
      resources :versions
      get 'read-xml', to: "networks#read_xml", as: "read-xml"
      get 'read-excel', to: "networks#read_excel", as: "read-excel"
    end

    api_version(:module => "api/v1", :path => {:value => "api/v1"}, defaults: {format: 'json'}) do
      resources :organisations do
        collection do 
          get 'mulesoft', to: 'organisations#mulesoft', as: :mulesoft
          get 'line-ranges', to: 'organisations#line_ranges', as: :line_ranges
        end  
      end
      resources :networks
    end

    # Api for connexion
    api_version(:module => "api/v1/connexion", :path => {:value => "api/v1/connexion"}, defaults: {format: 'json'}) do
      resources :organisations do
        resources :networks
        collection do
          get 'all-network-details', to: 'networks#all_network_details'
        end
      end
    end

    root 'index#index'
    get 'read-pdf', to: "networks#read_pdf", as: "read-pdf"
    match 'search', to: 'search#index', as: 'search', via: [:POST,:GET]
    get 'sign-in', to: 'user_sessions#new', as: 'sign-in'
    get 'sign-out', to: 'user_sessions#destroy', as: 'sign-out'
    get 'revert/:id', to: 'versions#revert', as: 'revert'
    get 'forgot-password', to: 'users#forgot_password', as: 'forgot_password'
    put 'networks/:id/update_hstore_field', to: 'networks#update_hstore_field', as: 'network_update_hstore'
    put 'networks/:id/update_network_field', to: 'networks#update_network_field', as: 'network_update'
    put 'networks/:id/update_hstore_section_field', to: 'networks#update_hstore_section_field', as: 'network_section_update_hstore'
    put 'networks/:id/update_network_index_field', to: 'networks#update_network_index_field', as: 'network_update_index'
    get 'delete-row/:id', to: 'networks#delete_row'
    get 'active-imports', to: 'imports#active_imports', as: 'active-imports'
    get 'settings', to: 'index#settings', as: 'settings'
    get 'change-password', to: 'users#change_password', as: 'change_password'
    post 'send-reset-password', to: 'users#send_reset_password', as: 'reset_password'
    get 'run-rake-import', to: 'imports#run_import', as: 'run_rake_import'
    match 'remote-login', to: 'user_sessions#remote_login', via: [:POST,:GET]
    put 'network-section-update-field', to: 'network_section_fields#update_field'
    get 'soap-test', to: 'imports#soap_test', as: 'soap-test'
    get 'import-diff', to: 'index#import_diffs', as: 'import_diff'
    get 'flagged-networks', to: 'networks#flagged_networks', as: 'flagged_networks'
    get 'import-files', to: 'imports#import_files', as: 'import_files'
    get 'read-file/:id', to: 'imports#read_file', as: 'read_file'
    get 'show-diff-snapshot/:id', to: 'networks#show_diff_snapshot', as: 'show_diff_snapshot'
    post 'add-line-range', to: 'networks#add_line_range', as: 'add_line_range'
    put 'edit-linerange', to: 'networks#edit_linerange',as: 'edit-line-range', format: 'js'
    delete 'delete-line-range/:id', to: 'networks#delete_line_range', as: 'delete_line_range'
    get 'notifications', to: 'notification#index', as: :notifications
    match 'add-role', to: 'users#add_role', as: :add_role, via: [:POST,:GET]
    get 'delete-role', to: 'users#remove_role', as: :delete_role
    get 'download-export', to: 'exports#download_export', as: :download_export
    get 'notify-groups', to: "notification#notify_groups", as: :notify_groups
    get 'notify-group/:id', to: "notification#notify_group", as: :notify_group
    post 'add-notify-group-profile', to: "notification#add_profile", as: :add_notify_group_profile
    get 'show-notify-networks', to: 'notification#show_networks', as: :show_notify_networks, format: 'js'
    post 'add-notify-group-network', to: "notification#add_network", as: :add_notify_group_network
    post 'add-notify-group-organisation', to: 'notification#add_organisation', as: :add_notify_group_organisation
    match 'lazy-load-line-ranges', to: 'networks#lazy_load_line_ranges', as: :lazy_load_line_ranges, via: [:POST, :GET, :PUT]
    match 'lazy-load-network-sections', to: 'networks#lazy_load_network_sections', as: :lazy_load_network_sections, via: [:POST, :GET, :PUT]
    delete 'delete-notify-group/:id', to: 'notification#delete_notify_group', as: :delete_notify_group
    get 'add_notify_group_to_user', to: 'notification#add_notify_group_to_user', as: :add_notify_group_to_user
    get 'activate-all-imports', to: 'imports#activate_all_imports', as: :activate_all_imports
    delete 'delete-network-from-notify-group', to: 'networks#delete_from_notify_group', as: :delete_network_from_notify_group
    delete 'delete-organisation-from-notify-group', to: 'organistions#delete_from_notify_group', as: :delete_organisation_from_notify_group
    delete 'delete-notify-group-from-user', to: 'users#delete_from_notify_group', as: :delete_from_notify_group
    get 'show-network-comment', to: 'networks#show_network_comment', as: :show_network_comment, defaults: {format: 'js'}
    match 'add-notify-setting-to-user', to: 'notification#add_notify_setting_to_user', as: :add_notify_setting_to_user, defaults: {format: 'js'}, via: [:POST,:GET]
    match 'remove-notify-setting-from-user', to: 'notification#remove_notify_setting_from_user', as: :remove_notify_setting_from_user, defaults: {format: 'js'}, via: [:POST, :GET] 
    match 'add-view-setting-to-user', to: 'users#add_view_setting_to_user', as: :add_view_setting_to_user, defaults: {format: 'js'}, via: [:POST, :GET]
    match 'remove-view-setting-from-user', to: 'users#remove_view_setting_from_user', as: :remove_view_setting_from_user, defaults: {format: 'js'}, via: [:POST, :GET]
    match 'upload-sms-hub-file', to: 'imports#upload_sms_hub_file', as: :upload_sms_hub_file, via: [:POST, :GET]
    match 'customized-exports', to: 'exports#customized_exports', as: :customized_exports, via: [:POST, :GET]
    get 'create-manual-export', to: 'exports#create_manual_export', as: :create_manual_export
    get 'view-settings', to: 'index#view_settings', as: :view_settings
    get 'show-sms-hub-data', to: 'index#show_sms_hub_data', as: :show_sms_hub_data
    get 'get-rest-of-networks', to: 'networks#get_rest_of_networks', as: :get_rest_of_networks
    get 'get-rest-of-networks-dynamic/:id', to: 'networks#get_rest_of_networks_dynamic', as: :get_rest_of_networks_dynamic
    match 'export-ip-addresses', to: 'exports#export_ip_addresses', as: :export_ip_addresses, via: [:POST, :GET]
    get 'line-range-overview-ir21', to: 'index#line_range_overview_ir21', as: :line_range_overview_ir21
    get 'line-range-overview-export', to: 'index#line_range_overview_export', as: :line_range_overview_export
    get 'line-range-code-export', to: 'index#line_range_code_export', as: :line_range_code_export
    get 'import-details/:id', to: 'imports#import_details', as: :import_details
    get 'create-missing-structutre', to: 'imports#create_missing_structure', as: :create_missing_structure
    post 'get-sms-hub-data', to: 'index#line_range_overview_export_ajax_data', as: :line_range_overview_export_ajax_data, format: :js 
    post 'get-code-data', to: 'index#line_range_overview_code_ajax_data', as: :line_range_overview_code_ajax_data, format: 'js' 
    patch 'change-network-status', to: 'networks#change_network_status', as: :change_network_status
    get 'mno-ip-overview', to: 'index#mno_ip_overview', as: :mno_ip_overview
    post 'update-mno-sccp-ip', to: 'index#update_mno_sccp_ip', as: :update_mno_sccp_ip
    get 'add-mno-ip-line', to: 'index#add_mno_ip_line', as: :add_mno_ip_line
    get 'add-sccp-ip-line', to: 'index#add_sccp_ip_line', as: :add_sccp_ip_line
  end


##### PLAIN ROUTE SECTION ########################################
# Theese extra route exist to render proxy correct
# It makes it possible to each render /ir21 scope and plain url´s
#
###################################################################
match 'remote-login', to: 'user_sessions#remote_login', via: [:POST,:GET], as: :remote_login_plain
get 'sign-out', to: 'user_sessions#destroy', as: :sign_out_plain
resources :users, as: :users_plain
resources :countries, as: :countries_plain
resources :change_histories, as: :change_histories_plain
resources :activities, as: :activities_plain
resources :network_section_fields, as: :network_sections_fields_plain
resources :hermes_operators, as: :hermes_operators_plain
resources :organisations, as: :organisations_plain
resources :users, as: :users_plain
resources :user_sessions, as: :user_sessions_plain
resources :network_sections, as: :network_sections_plain
resources :reports, as: :reports_plain

resources :networks, as: :networks do
  resources :versions, as: :versions_plain
  get 'read-xml', to: "networks#read_xml", as: "read-xml-plain"
  get 'read-excel', to: "networks#read_excel", as: "read-excel-plain"
end

resources :exports, as: :exports_plain do
  collection do
    get 'hermes-csv-export', to: 'exports#hermes_csv_export', as: "hermes_csv_export-plain"
    get 'lte-sccp-ip-csv-export', to: 'exports#lte_sccp_ip_csv_export', format: 'csv', as: 'lte-sccp-ip-csv-export-plain'
    get 'lte-mno-ip-csv-export', to: 'exports#lte_mno_ip_csv_export', format: 'csv', as: 'lte-mno-ip-csv-export-plain'
    get 'export-all-networks', to: 'exports#export_all_networks', format: 'xls', as: 'export-all-networks-plain'
    get 'export-all-networks-xlsx', to: 'exports#export_all_networks_xlsx', format: 'xlsx', as: 'export-all-networks-xlsx-plain'
    get 'advanced-network-export', to: 'exports#export_all_networks_advanced', format: 'csv', as: 'export-all-networks-advanced-plain'
    get 'export-telenor-connexion', to: 'exports#export_telenor_connexion', format: 'xls'
    get 'generate-codes', to: 'exports#generate_codes', as: 'generate-codes-plain'
    get 'generate-single-codes', to: 'exports#generate_single_codes', as: 'generate-single-codes-plain'
    get 'sor-export', to: 'exports#sor_export', as: :sor_export_plain, format: 'csv'
    get 'advanced-sor-export', to: 'exports#advanced_sor_export', as: :advanced_sor_export_plain, format: 'csv'
    get 'sccp-ip-export', to: 'export#sccp_ip_export', as: :sccp_ip_export_plain, format: 'csv'
  end
end

resources :imports, as: :imports_plain do
  get 'resolve-conflict', to: "imports#resolve_conflict", as: "resolve-conflict-plain"
  put 'imports/link-pdf', to: 'imports#link_pdf', as: 'link-pdf-plain'
  put 'imports/mark-pdf-networks', to: 'imports#mark-pdf-networks', as: 'mark-pdf-networks-plain'
  get 'resolve', to: 'imports#resolve', as: :resolve_plain
  collection do
    get 'summery', to: 'imports#summery', as: 'summery-plain'
  end
end

root 'index#index', as: :index_plain

get 'read-pdf', to: "networks#read_pdf", as: "read-pdf-plain"
match 'search', to: 'search#index', as: 'search-plain', via: [:POST,:GET]
get 'sign-in', to: 'user_sessions#new', as: 'sign-in-plain'
get 'revert/:id', to: 'versions#revert', as: 'revert-plain'
get 'forgot-password', to: 'users#forgot_password', as: 'forgot_password-plain'
put 'networks/:id/update_hstore_field', to: 'networks#update_hstore_field', as: 'network_update_hstore-plain'
put 'networks/:id/update_network_field', to: 'networks#update_network_field', as: 'network_update-plain'
put 'networks/:id/update_hstore_section_field', to: 'networks#update_hstore_section_field', as: 'network_section_update_hstore-plain'
put 'networks/:id/update_network_index_field', to: 'networks#update_network_index_field', as: 'network_update_index-plain'
get 'delete-row/:id', to: 'networks#delete_row', as: :delete_row_plain
get 'active-imports', to: 'imports#active_imports', as: 'active-imports-plain'
get 'settings', to: 'index#settings', as: 'settings-plain'
get 'change-password', to: 'users#change_password', as: 'change_password-plain'
post 'send-reset-password', to: 'users#send_reset_password', as: 'reset_password-plain'
get 'run-rake-import', to: 'imports#run_import', as: 'run_rake_import_plain'
put 'network-section-update-field', to: 'network_section_fields#update_field', as: :network_section_update_plain
get 'soap-test', to: 'imports#soap_test', as: 'soap-test-plain'
get 'import-diff', to: 'index#import_diffs', as: 'import_diff-plain'
get 'flagged-networks', to: 'networks#flagged_networks', as: 'flagged_networks-plain'
get 'import-files', to: 'imports#import_files', as: 'import_files_plain'
get 'read-file/:id', to: 'imports#read_file', as: 'read_file_plain'
get 'show-diff-snapshot/:id', to: 'networks#show_diff_snapshot', as: 'show_diff_snapshot_plain'
post 'add-line-range', to: 'networks#add_line_range', as: 'add_line_range_plain'
put 'edit-linerange', to: 'networks#edit_linerange',as: 'edit-line-range-plain', format: 'js'
delete 'delete-line-range/:id', to: 'networks#delete_line_range', as: 'delete_line_range-plain'
get 'notifications', to: 'notification#index', as: :notifications_plain
match 'add-role', to: 'users#add_role', as: :add_role_plain, via: [:POST,:GET]
get 'delete-role', to: 'users#remove_role', as: :delete_role_plain
get 'download-export', to: 'exports#download_export', as: :download_export_plain
match 'lazy-load-line-ranges', to: 'networks#lazy_load_line_ranges', as: :lazy_load_line_ranges_plain, via: [:POST, :GET, :PUT]
match 'lazy-load-network-sections', to: 'networks#lazy_load_network_sections', as: :lazy_load_network_sections_plain, via: [:POST, :GET, :PUT]
get 'notify-groups', to: "notification#notify_groups", as: :notify_groups_plain
get 'notify-group/:id', to: "notification#notify_group", as: :notify_group_plain
post 'add-notify-group-profile', to: "notification#add_profile", as: :add_notify_group_profile_plain
get 'show-notify-networks', to: 'notification#show_networks', as: :show_notify_networks_plain, format: 'js'
post 'add-notify-group-network', to: "notification#add_network", as: :add_notify_group_network_plain
post 'add-notify-group-organisation', to: 'notification#add_organisation', as: :add_notify_group_organisation_plain
delete 'delete-notify-group/:id', to: 'notification#delete_notify_group', as: :delete_notify_group_plain
get 'add_notify_group_to_user', to: 'notification#add_notify_group_to_user', as: :add_notify_group_to_user_plain
delete 'delete-notify-group-from-user', to: 'users#delete_from_notify_group', as: :delete_from_notify_group_plain
get 'activate-all-imports', to: 'imports#activate_all_imports', as: :activate_all_imports_plain
delete 'delete-network-from-notify-group', to: 'networks#delete_from_notify_group', as: :delete_network_from_notify_group_plain
delete 'delete-organisation-from-notify-group', to: 'organistions#delete_from_notify_group', as: :delete_organisation_from_notify_group_plain
get 'show-network-comment', to: 'networks#show_network_comment', as: :show_network_comment_plain, defaults: {format: 'js'}
match 'add-notify-setting-to-user', to: 'notification#add_notify_setting_to_user', as: :add_notify_setting_to_user_plain, defaults: {format: 'js'}, via: [:POST, :GET]
match 'remove-notify-setting-from-user', to: 'notification#remove_notify_setting_from_user', as: :remove_notify_setting_from_user_plain, defaults: {format: 'js'}, via: [:POST, :GET]
match 'add-view-setting-to-user', to: 'users#add_view_setting_to_user', as: :add_view_setting_to_user_plain, defaults: {format: 'js'}, via: [:POST, :GET]
match 'remove-view-setting-from-user', to: 'users#remove_view_setting_from_user', as: :remove_view_setting_from_user_plain, defaults: {format: 'js'}, via: [:POST, :GET]
match 'upload-sms-hub-file', to: 'imports#upload_sms_hub_file', as: :upload_sms_hub_file_plain, via: [:POST, :GET]
match 'customized-exports', to: 'exports#customized_exports', as: :customized_exports_plain, via: [:POST, :GET]
get 'create-manual-export', to: 'exports#create_manual_export', as: :create_manual_export_plain
get 'view-settings', to: 'index#view_settings', as: :view_settings_plain
get 'show-sms-hub-data', to: 'index#show_sms_hub_data', as: :show_sms_hub_data_plain
get 'get-rest-of-networks', to: 'networks#get_rest_of_networks', as: :get_rest_of_networks_plain
get 'get-rest-of-networks-dynamic/:id', to: 'networks#get_rest_of_networks_dynamic', as: :get_rest_of_networks_dynamic_plain
match 'export-ip-addresses', to: 'exports#export_ip_addresses', as: :export_ip_addresses_plain, via: [:POST, :GET] 
get 'line-range-overview-ir21', to: 'index#line_range_overview_ir21', as: :line_range_overview_ir21_plain
get 'line-range-overview-export', to: 'index#line_range_overview_export', as: :line_range_overview_export_plain
get 'line-range-code-export', to: 'index#line_range_code_export', as: :line_range_code_export_plain
get 'import-details/:id', to: 'imports#import_details', as: :import_details_plain
get 'create-missing-structutre', to: 'imports#create_missing_structure', as: :create_missing_structure_plain
post 'get-sms-hub-data', to: 'index#line_range_overview_export_ajax_data', as: :line_range_overview_export_ajax_data_plain, format: 'js'
post 'get-code-data', to: 'index#line_range_overview_code_ajax_data', as: :line_range_overview_code_ajax_data_plain, format: 'js'
patch 'change-network-status', to: 'networks#change_network_status', as: :change_network_status_plain
get 'mno-ip-overview', to: 'index#mno_ip_overview', as: :mno_ip_overview_plain
post 'update-mno-sccp-ip', to: 'index#update_mno_sccp_ip', as: :update_mno_sccp_ip_plain
get 'add-mno-ip-line', to: 'index#add_mno_ip_line', as: :add_mno_ip_line_plain
get 'add-sccp-ip-line', to: 'index#add_sccp_ip_line', as: :add_sccp_ip_line_plain
delete 'delete_mno_ip_line', to: 'index#delete_mno_ip_line', as: :delete_mno_ip_line
delete 'delete_sccp_ip_line', to: 'index#delete_sccp_ip_line', as: :delete_sccp_ip_line
end
