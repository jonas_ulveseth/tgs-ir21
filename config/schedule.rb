environment = ENV['RAILS_ENV'] || 'production'
every 1.day, at: '1:00 am' do
  runner "Code.recreate_line_ranges"
end
