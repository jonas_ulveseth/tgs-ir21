#!/usr/bin/env ruby
desc 'Reevaluate line ranges for all networks'
task :reevaluate_line_ranges => :environment do
	nlr = Network.order("updated_at asc").take(100).each do |network|
		p "Checking network #{network.id}"
		begin
			network.touch
			network.evaluate_line_ranges
		rescue
		end
	end
end

task :check_for_networks_without_ranges => :environment do
  networks = Network.all
  networks.each do |n|
    @gt_number_ranges = NetworkRow.where("network_section_id = 167 AND network_id = ?", n.id).first
    @msisdn_number_ranges = NetworkRow.where("network_section_id = 169 AND network_id = ?", n.id).first
    @msrn_number_ranges = NetworkRow.where("network_section_id = 168 AND network_id = ?", n.id).first
    p "processing #{n.id}"
    if @gt_number_ranges.nil? && @msisdn_number_ranges.nil? && @msrn_number_ranges.nil?
      p "Network number #{n.id} do not have any line range entry"
    else
      #p 'something is present'
    end
  end
end

task :import_network_tadig => :environment do
  Network.joins(:organisation).joins(:import).where('imports.file_type = ?', 'xml').each do |network|
    file = "#{Rails.root}/data/upload/#{network.import.file_name}.xml"
    file_content = File.read(file)
    doc = Nokogiri::XML(file_content)
    doc.remove_namespaces!
    sender_tadig = doc.xpath('//SenderTADIG').text
    unless sender_tadig.blank?
      network.organisation.update_columns(sender_tadig: sender_tadig)
    end
  end
end

task :update_organisation_countries => :environment do
  Organisation.joins(:networks).where('organisations.country_id is NULL').each do |organisation|
    p "updating #{organisation.id}"
    country_id = organisation.networks.first.country_id
    organisation.update_columns(country_id: country_id)
  end
end

task :find_networks_without_gt_ranges => :environment do
  networks = Network.joins(:import).where('imports.file_type = ?', 'xlsx')
  networks.each do |network|
    p 'Looping network'
    unless network.routing['e214_cc'].blank?
      p "there is a cc :: #{network.routing['e214_cc']}"
      nr = NetworkRow.where("network_section_id = 167 AND network_id = ?", network.id).all
      if nr.blank?
        p "Network with id: #{network.id} does not have gt range"
      else
        p "gt range is #{nr.inspect}"
      end
    end
  end
end

task :update_excel_sender_tadig => :environment do
  Network.joins(:import).where('imports.file_type = ?', 'xlsx').each do |network|
    p 'looping network'
    next unless network.organisation
    unless network.organisation.sender_tadig
      p 'updating'
      network.organisation.update_columns(sender_tadig: network.try(:tadig_code))
    end
  end
end

task :update_network_display_name => :environment do
  Network.joins(:import).each do |network|
    p 'processing network'
    display_name_check = PaperTrail::Version.where('whodunnit IS NOT NULL AND item_id = ? AND object_changes LIKE ?', network.id, '%display_name%').first
    unless display_name_check
      unless network.network_name.blank?
        network.update_columns(display_name: network.network_name)
      end
    end
  end
end
