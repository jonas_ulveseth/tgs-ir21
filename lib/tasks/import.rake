#!/usr/bin/env ruby

desc 'import xml version 2'
task :import_xml_v2 => :environment do
  require_relative '../../config/environment'
  imports = Import.where(archive: false, file_type: 'xml', ready_for_import: true).all
  imports.each do |i|
    Dir.chdir("#{Rails.root}/data/upload") do
      puts "Import XML from #{i.file_name}.xml:\n\n"
      ActiveRecord::Base.connection.transaction do
        #start import
        #begin
        i.import_xml_v2(File.read("#{i.file_name}.xml"))
        i.failed = false
        #rescue
        #end
      end
    end
  end
end


desc 'import xml files from folder'
task :import_xml => :environment do
  require_relative '../../config/environment'

  imports = Import.where(archive: false, file_type: 'xml', ready_for_import: true, removed: false).all

  imports.each do |i|
    next if i.processing
    Dir.chdir("#{Rails.root}/data/upload") do
      puts "Import XML from #{i.file_name}.xml:\n\n"
      ActiveRecord::Base.connection.transaction do
        #start import
        #begin
        i.import(File.read("#{i.file_name}.xml"))
        i.failed = false
        #rescue
        #end
      end
    end
  end
end

desc 'import pdf files and link to import'
task :import_pdf => :environment do
  Import.new.import_pdf
end

desc 'import hermes csv - import only a csv with operators'
task :import_hermes_csv do
  require 'csv'
  require_relative '../../config/environment'
  file = Import.where(file_type: 'csv').first

  CSV.foreach("#{Rails.root}/data/upload/#{file.original_filename}", headers: true, :quote_char => "\'") do |row|
    HermesOperator.delete_all
        #data = Hash[row.map { |k, v| [k.underscore.gsub(/[ ]/, '_'), v.blank? ? nil : v] }]
        p row.to_hash
        data =  {
          'sms_number_administration_id' =>  'Yeehaa'
        }
        hermes = HermesOperator.new
        hermes.data = data
        # Check for direct match first
        foreign_id = hermes.data['sms_number_administrationid']
        network_match = Network.where(rosi_id: foreign_id).first
        if network_match
          check = HermesOperator.where(network_id: network_match.id)
          hermes.network_id = network_match.id
        else
          # Or check by comparing imsi-range to mcc and mnc
          if hermes.data['imsi_range'].include?('-')
            imsi_range = hermes.data['imsi_range'].split('-')
          else
            imsi_range = hermes.data['imsi_range'].split(' ')
          end
          network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ?", imsi_range[0], imsi_range[1]).first
          # If imsi range is greater than just one entry
          if imsi_range.count == 4
            network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ?", imsi_range[2], imsi_range[3]).first
          end
          if imsi_range.count == 6
            network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ?", imsi_range[4], imsi_range[5]).first
          end
          if network
            hermes.network_id = network.id
          end
        end
        p "SAVING HERMES"
        hermes.save! if hermes.data
      end
      file.archive = true
      file.save
    end

    desc 'import hermes from mdb'
    task :import_hermes => :environment do
      require 'csv'
      require_relative '../../config/environment'

      class HermesImport
        def import
          dump_mdb unless Rails.env.beta?
          guilty_party, PaperTrail.whodunnit = PaperTrail.whodunnit, 'HERMES'
      #import_countries
      import_operators
      PaperTrail.whodunnit = guilty_party
    end

    def dump_mdb
      db_filename = "#{Rails.root}/data/upload/Hermes_base.mdb"
      tables = `mdb-tables -d , #{db_filename}`.chop.split(',')

      tables.each do |table|
        exists = system("mdb-export #{db_filename} '#{table}'")
        `mdb-export #{db_filename} '#{table}' > #{Rails.root}/data/hermes-csv/#{table.gsub(' ', '_')}.csv` if exists
      end
    end

    def import_countries
      columns = nil
      lengths = {}
      strings = []
      nulls = []
      CSV.foreach("#{Rails.root}/data/hermes-csv/tblCountry.csv", headers: true) do |row|
        values = Hash[row.map { |k, v| [k.underscore.gsub(' ', '_'), v.blank? ? nil : v] }]
        columns ||= values.keys
        values.each do |k, v|
          if v.blank?
            nulls << k unless nulls.include?(k)
          else
            strings << k if !strings.include?(k) && v.to_i.to_s != v
            lengths[k] = v.size if v.size > lengths[k].to_i
          end
          #map against network

          if k == 'tbl_country_id'
            hermes_operator = HermesOperator.where("data -> 'country' = ?", v).first
            if hermes_operator && hermes_operator.network
              country = hermes_operator.network.country
              if country
                country.name = row[1]
                if country.save
                  p 'SAVED'
                end
              end
            end
          end
        end
        column_definitions = columns.map do |k|
          length_suffix = "{#{lengths[k]}}" if lengths[k]
          type = strings.include?(k) ? "string#{length_suffix}" : 'integer'
          null_suffix = ':null' if nulls.include?(k)
          "#{k}:#{type}#{null_suffix}"
        end.join(' ')
      end
    end

    def import_operators
      CSV.foreach("#{Rails.root}/data/hermes-csv/tblOperatorInformation.csv", headers: true) do |row|
        data = Hash[row.map { |k, v| [k.underscore.gsub(/[ ]/, '_'), v.blank? ? nil : v] }]
        hermes = HermesOperator.new
        hermes.data = data

        # Check for direct match first
        foreign_id = hermes.data['sms_number_administration_id']
        network_match = Network.where(rosi_id: foreign_id).first
        if network_match
          check = HermesOperator.where(network_id: network_match.id)
          hermes.network_id = network_match.id
        else
          # Or check by comparing imsi-range to mcc and mnc
          if hermes.data['imsi_range'].include?('-')
            imsi_range = hermes.data['imsi_range'].split('-')
          else
            imsi_range = hermes.data['imsi_range'].split(' ')
          end
          network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ?", imsi_range[0], imsi_range[1]).first
          # If imsi range is greater than just one entry
          if imsi_range.count == 4
            network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ?", imsi_range[2], imsi_range[3]).first
          end
          if imsi_range.count == 6
            network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ?", imsi_range[4], imsi_range[5]).first
          end
          if network
            hermes.network_id = network.id
          end
        end
        p "SAVING HERMES"
        hermes.save! if hermes.data
      end
    end
  end
  HermesImport.new.import
  hermes_imports = HermesOperator.order("network_id ASC").all
  hermes_matches = HermesOperator.where("network_id IS not null").all
  Activity.create!(name: "hermes import",activity_type_id: 1, message: "new import with #{hermes_matches.count} matches and #{hermes_imports.count} unmatched entries")
end

# Import a rosi mdb file added in upload folder
desc 'import rosi mdb'
task :import_rosi do
  require 'csv'
  require_relative '../../config/environment'

  class RosiImport
    def import
      dump_mdb unless Rails.env.beta?
      guilty_party, PaperTrail.whodunnit = PaperTrail.whodunnit, 'HERMES'
      import_operators
    end

    def dump_mdb
      db_filename = "#{Rails.root}/data/upload/Hermes_GSM_RoSi.mdb"
      tables = `mdb-tables -d , #{db_filename}`.chop.split(',')

      tables.each do |table|
        exists = system("mdb-export #{db_filename} '#{table}'")
        `mdb-export #{db_filename} '#{table}' > #{Rails.root}/data/rosi-csv/#{table.gsub(' ', '_')}.csv` if exists
      end
    end

    def import_operators
      CSV.foreach("#{Rails.root}/data/rosi-csv/tblGSMRoSiMNO.csv", headers: true) do |row|
        data = Hash[row.map { |k, v| [k.underscore.gsub(/[ ]/, '_'), v.blank? ? nil : v] }]
        hermes_id = data['mno_name']
        p "HERMES_ID: #{hermes_id}"
        hermes = HermesOperator.where("data -> 'sms_number_administration_id' = ?", hermes_id.to_s).first
        p "hermes is here" if hermes
        if hermes.try(:network_id)
         hermes.network.update!(rosi_id: data['gsm_ro_si_mno_id'])
       end
     end
   end
 end
 RosiImport.new.import
end

desc 'recreate jobs if there is no delayed jobs'
task :recreate_delayed_jobs => :environment do
  require 'delayed_job_active_record'
  dj = Delayed::Job.count
  if dj == 0
    i = Import.where(archive: false, failed: nil).all
    puts i.count
    i.each do |x|
      x.delay.import
    end
  end
end

desc 'import from election'
task :import_election => :environment do
  CSV.foreach("#{Rails.root}/data/tmss_csv/countries_tmss.csv", headers: false) do |row|
    country = Country.where(iso_code: row[2]).first
    if country
      country.name = row[1]
      country.save!
      p "We saved it!"
    end
  end
end

desc 'reimport netwoks to fix issue with wrong address range'
task :reimport_address_range => :environment do
  networks = Network.all
  networks.each do |network|
    if network.import
      network.import.archive = false
      network.import.save
      begin
        network.import.import
        Import.new.import_excel
      rescue
      end
    end
  end
end  

desc 'check for conflicts in address range'
task :detect_address_range_conflicts => :environment do
  nr = NetworkRow.where("network_section_id IN (?)", [80, 167, 168, 169])
  nr.each do |n|
    p n.data.first[1]
    p "--------------------------"
  end
end

desc 'reimport all networks'
task :reimport_all_networks => :environment do 
  files = Dir.glob("#{Rails.root}/data/upload/*")
  files.each do |file|
    p "-------------- #{file} "
    @import = Import.new
    @import.file_name = file.split('/').last.tr(' ', '_').squish.split('.').first
    @import.file_type = file.split('.').last
    @import.original_filename = file.split('/').last
    # check if import is done for this file
    check = Import.where(original_filename: @import.original_filename).first
    next if check
    @import.save
    
    Dir.chdir("#{Rails.root}/data/upload") do
      case @import.file_type 
      when "xml"
        @import.delay.import(File.read("#{file}"))
        name = 'xml_import'
      when "pdf"
        @import.delay.import_pdf 
        name = 'pdf_import'
      when "xlsx"
        @import.delay.import_excel
        name = 'excel import'
      end
    end
  end
end

desc 'fix faulty pdf file name'
task :fix_pdf_name => :environment do
  Pdf.all.each do |pdf|
    p 'found pdf'
    import = pdf.import
    pdf.update_columns(name: import.file_name)
  end
end

desc 'import mno ip files'
task :import_mno_ip_files => :environment do
  # Create and loop filse
  LteMnoIp.delete_all
  LteSccpIp.delete_all

  p 'creating Lte Mno IP'
  incoming_file = "#{Rails.root}/data/upload/LTE_MNO_IP_2.csv"
  csv_text = File.read(incoming_file, encoding: "ISO8859-1:utf-8")
  # Skip first line removed [1 .. -1]
  csv_import = CSV.parse(csv_text, :quote_char => '"', :col_sep => ";", :row_sep => :auto, encoding: "ISO-8859")
  csv_import.each do |csv_import_row|
    LteMnoIp.create(sms_number_administration_id: csv_import_row[0], country: csv_import_row[1], carrier: csv_import_row[2], MNO_IP_Range: csv_import_row[3])
    p csv_import_row[0]
    p csv_import_row[1]
    p csv_import_row[2]
    p csv_import_row[3]
  end

  p 'creating SccpIp'
  incoming_file = "#{Rails.root}/data/upload/LTE_SCCP_IP_2.csv"
  csv_text = File.read(incoming_file, encoding: "ISO8859-1:utf-8")
  csv_import = CSV.parse(csv_text, :quote_char => '"', :col_sep => ";", :row_sep => :auto, encoding: "ISO-8859")
  csv_import.each do |csv_import_row|
    LteSccpIp.create(GSMRoSiSCCPprovider_ID: csv_import_row[0],SCCP_provider_Name: csv_import_row[1],SCCP_IP_Range: csv_import_row[2],Country: csv_import_row[3],SCCP_Hostname: csv_import_row[4])
    p csv_import_row[0]
    p csv_import_row[1]
    p csv_import_row[2]
    p csv_import_row[3]
    p "hostname::#{csv_import_row[4]}"
  end
end
























