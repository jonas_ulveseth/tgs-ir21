desc "remove old versioning files"
task :cleanup_versioning => :environment do
	PaperTrail::Version.delete_all["created at > ? ", 6.months.ago]
end

task :cleanup_network_sections => :environment do
	n = NetworkSection.order("field_name").all
	last_name = ''	
	n.each do |section|
		unless last_name == section.field_name
			last_name = section.field_name
		else
			p 'destroying'
			section.delete
		end
	end
end

task :check_for_deleted_networks => :environment do
	p 'starting to search'
	false_network_list = [] 
	Network.joins(:import).where('imports.file_type = ?', "xml").all.each do |network|
		#get latest import
		import = network.import
		next unless network.import
		next unless import.file_type = 'xml'
			Dir.chdir("#{Rails.root}/data/upload") do
			begin	
				@doc  = Nokogiri::XML(File.read("#{import.file_name}.#{import.file_type}"))
				@doc.remove_namespaces!
			rescue
				false_network_list.push(network)
				next
			end
		end
		tadig_check = @doc.xpath("//TADIGCode")
		@tadig_control = nil
		tadig_check.each do |t|			
			if t.text == network.tadig_code
				@tadig_control = true
				p 'We found a tadig'
			end
		end
		unless @tadig_control
			p "TADIG #{network.tadig_code} is not found in this file"
			false_network_list.push(network)
		end
		@tadig_control = nil
	end
	p "#{false_network_list.count} is not found in there linked file #{false_network_list}"
end