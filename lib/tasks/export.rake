desc 'Export hermes file'
task :export_hermes => :environment do
	current_user = User.find 4
	Network.hermes_csv_export(current_user)
end

task :export_sms_hub => :environment do
	incoming_file = "#{Rails.root}/data/upload/sms_hub.csv"
	outgoing_file = "#{Rails.root}/data/export/sms_hub_export#{Time.now}.csv"
	@actual_network = nil 
	@line_range_array = []
  #outgoing_diff_file = 
  

    # Create outgoing file
    data = CSV.open(outgoing_file, "wb", col_sep: ';', encoding: 'WINDOWS-1252', force_quotes: false) do |csv_export_row|

    	csv_text = File.read(incoming_file)
    	csv_import = CSV.parse(csv_text, :headers => false, :quote_char => "|")
    	csv_import.each do |csv_import_row|
    		@mnc_mcc = csv_import_row[1]
    		@name    = csv_import_row[0]
    		line_range = csv_import_row[2]
    		if csv_import_row[0].nil?
    			next
    		end
    		
    		unless @mnc_mcc.blank?
    			#p "its not blank ||| #{csv_import_row[1]} || name: #{@name}"
    			#p "Line range inside #{line_range}"
    			mcc = @mnc_mcc[0..3][1..3]
    			mnc = @mnc_mcc[4..5]
    			@network	= Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ?",mcc, mnc).first
    			next unless @network

    			#### If old network, add that here ########
    			if @rosi_id
    				p "ROSI_ID=#{@rosi_id} |||| OLD_ROSI_ID=#{@old_rosi_id}"
    				if @old_rosi_id
    					unless @old_rosi_id == @network.rosi_id
    						p "switching network ||| actual_network=#{@actual_network} ||| network=#{@network.nil?}"
    						p "Old Rosi_id::#{@old_rosi_id} |||| New Rosi_id::#{@network.rosi_id}"
    						p "Old network ::: #{@actual_network.display_name} mncmcc:: #{@actual_network.routing['e212_mcc']} #{@actual_network.routing['e212_mnc']}" if @actual_network
    						p "New network ::: #{@network.display_name}" if @network
    						p "Showed network is #{csv_import_row[0]}" unless @mnc_mcc.nil?
    						p @line_range_array
		    				# Collect all imsis
		    				imsis = Network.where(rosi_id: @old_rosi_id)
		    				old_network = imsis.first
		    				if imsis.count == 1
          				imsi = "#{old_network.routing['e212_mcc',]}-#{old_network.routing['e212_mnc']}"
        				else
				          # When there is multiple Networks with the sam rosi_id, create a imsi range separated with semikolon
				          array = []
				          imsis.each do |i|
				            next unless i.routing
				            array.push(" #{i.routing['e212_mcc',]}-#{i.routing['e212_mnc']}")
				          end
				          imsi = array.join(';')
        				end
			    			### Add an csv row ####
			    			csv_export_row << [@old_rosi_id, old_network.country.try(:name), old_network.display_name, @line_range_array.join("\n"),imsi]
		          	@line_range_array.clear
		          	@old_rosi_id = @network.rosi_id
          		end
        		else
        			@old_rosi_id = @rosi_id
        		end
      		end
      		@rosi_id = @network.rosi_id unless @rosi_id
    			#p "OK, network started: #{@network.display_name} /// #{@name}"
    		else
    			#p 'adding only line range'
    			if csv_import_row[0].length > 5
    				#p "There actually is something...#{csv_import_row[0].length}"
    				#p "#{csv_import_row[0].gsub('"', '')}"
    				if @network
    					#p "MATCHED_NETWORK:::#{@network.display_name} ID:#{@network.id}"
    					#p "Added line_range to ::: network #{@network.display_name} line range::: #{csv_import_row[0]}"
    					@line_range_array << "#{csv_import_row[0].gsub('"', '').split(' ').first} #{csv_import_row[0].gsub('"', '').split(' ').last}"	
    				end
    				#p "Line range array is notw: #{@line_range_array}"
    			end
    		end

    	# Add line-range to array when it is in the same line as a mcc/mnc
    	if line_range
    		if @network
    			@line_range_array << "#{csv_import_row[2].gsub('"', '').split(' ').first} #{csv_import_row[2].gsub('"', '').split(' ').last}"
    			#p "added #{@line_range_array}"
    		end
    	end
    end
  end
end