desc 'import soap from sms-hub'
task :import_soap => :environment do
	require 'savon'
	ENV['http_proxy'] = nil
	ENV['https_proxy'] = nil
	ENV['HTTP_PROXY'] = nil
	ENV['HTTPS_PROXY'] = nil
	client = Savon::Client.new(endpoint: "https://osl2sms1:8888/Wing/services",
                               namespace: "https://osl2sms1:8888/Wing/services",
                               basic_auth: ["DATEK", "Y[m,jC4Y4*DWTeL}"],
                               ssl_verify_mode: :none, pretty_print_xml: true,
                               logger: Rails.logger)
	p client
	ops = client.operation(:list_names)

  # build the body of the xml inside the message here        
  p ops.build(message: { menu_leaf_id: "moebius_operators" }).to_s
	call = client.call(:list_names, message: {menu_leaf_id: "moebius_operator"})
	p call
  p 'request --------------------------'
end