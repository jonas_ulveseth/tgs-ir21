module ImportFunctions
	#### import_gprs_ssf_sgsn ####
  def import_gprs_ssf_sgsn(camel_information_tag, camel_information)
    gprs_ssf_sgsn = camel_information.build_gprs_ssf_sgsn

    camel_information_tag.xpath('//GPRS_SSF_SGSN/CAP_Version_Supported_SGSN/CAP_Ver_Supp_SGSN_Inbound/CAP_SGSNVersion').each do |ibv|

      if (idx = GsmSsfMsc::CAP_VERSIONS.index(ibv.text))

      else
        raise "Unexpected inbound CAP_MSCVersion: '#{ibv.text}'"
      end
    end

    camel_information_tag.xpath('GPRS_SSF_SGSN/CAP_Version_Supported_SGSN/CAP_Ver_Supp_SGSN_Outbound/CAP_SGSNVersion').each do |ibv|
      if (idx = GsmSsfMsc::CAP_VERSIONS.index(ibv.text))
        gprs_ssf_sgsn.send("cap_v#{idx+1}_supported_outbound=", true)
      else
        raise "Unexpected outbound CAP_MSCVersion: '#{ibv.text}'"
      end
    end

    gprs_ssf_sgsn.save! if gprs_ssf_sgsn.attributes.except('camel_information_id').values.any?

    # TODO(silje): Not in XML examples:
    # t.string  :cap_version_planned
    # t.date    :cap_version_planned_date
    # t.boolean :cap4_mt_sms_csi
    # t.boolean :cap4_mg_csi
    # t.boolean :cap4_psi_enhancements_csi
    # ODOT
  end
  #### /import_gprs_ssf_sgsn ####

  #### import_camel_re_routing_numbers #### 
  def import_camel_re_routing_numbers(camel_information_tag, camel_information)
    camel_information.camel_re_routing_numbers.destroy_all

    camel_information_tag.xpath('CAMEL_Rerouting_Numbering_Info/CAMEL_Rerouting_Number').each do |cr|
      camel_information.camel_re_routing_numbers.create! number: cr.text
    end
  end
  #### /import_camel_re_routing_numbers ####

  #### import_camel_functionalities #### 
  def import_camel_functionalities(camel_information_tag, camel_information)
    camel_information.camel_functionalities.destroy_all

    camel_information_tag.xpath('CAMEL_Functionality_Info/CAMEL_Functionality_Item').each do |cf_tag|
      service_name = cf_tag.xpath('ServiceName').first.text
      sk = cf_tag.xpath('SK').first.text
      camel_version = cf_tag.xpath('CAMEL_Version').first.text

      camel_functionality = camel_information.camel_functionalities.create!(service_name: service_name,
        sk: sk,
        camel_version: camel_version)
      import_camel_functionality_addresses(cf_tag, camel_functionality)
    end
  end
  #### /import_camel_functionalities ####

  #### import_packet_data_service_info #### 
  def import_packet_data_service_info(network, network_data_tag)
    p 'IMPORT_PACKET DATA'
    network_section = NetworkSection.where(field_name: "apn_operator_identifier_list").first
    delete_old_network_rows(network_section, network)
    network_data_tag.xpath('PacketDataServiceInfoSection/PacketDataServiceInfo/APNOperatorIdentifierList/APNOperatorIdentifierItem').each do |tag|
      apn_operator_identifier = tag.xpath('APNOperatorIdentifier').text 
      p 'Looping APN'
      network_row = NetworkRow.new
      network_row.network_id = network.id 
      network_row.data = 
      {
        'apn_operator_identifier' => apn_operator_identifier
      }
      network_row.network_section_id = network_section.id
      network_row.save!
      p "saved as #{network_row.id}"
    end
  end
  #### /import_packet_data_service_info ####

  #### import_camel_functionality_addresses ####
  def import_camel_functionality_addresses(camel_functionality_tag, camel_functionality)
    camel_functionality.camel_functionality_addresses.destroy_all

    camel_functionality_tag.xpath('SCP_GT_AddressList/SCP_GT_Address').each do |address|
      camel_functionality.camel_functionality_addresses.create! e164_gt_address: address.text
    end
  end 
  #### /import_camel_functionality_addresses ####

    #### import_qos_profile2g3g_items ####
    def import_qos_profile2g3g_items(packet_data_service_info_tag, packet_data_service_info)
      packet_data_service_info.qos_profile2g3g_items.destroy_all

      packet_data_service_info_tag.xpath('QOSProfile2G3GList/QOSProfile2G3GItem').each do |qos_tag|
        profile_name = qos_tag.xpath('ProfileName2G3G').first.text
        traffic_class = QosProfile2g3gItem::TRAFFIC_CLASSES[qos_tag.xpath('TrafficClass').first.text]
        arp = qos_tag.xpath('ARP2G3GList/ARP2G3G').map(&:text).join('; ')
        evolved_arp = qos_tag.xpath('EvolvedARP').first.text
        maximum_bit_rate_uplink = qos_tag.xpath('MaximumBitRateUplink2G3G').opval
        maximum_bit_rate_downlink = qos_tag.xpath('MaximumBitRateDownlink2G3G').opval
        delivery_order = qos_tag.xpath('DeliverOrder').opval
        maximum_sdu_size = qos_tag.xpath('MaximumSDUSize2G3G').opval
        sdu_format_information = qos_tag.xpath('SDUFormatInformation2G3GList/SDUFormatInformation2G3GItem').map(&:text).join('; ')
        sdu_error_ratio = qos_tag.xpath('SDUErrorRatio').opval
        residual_ber = qos_tag.xpath('ResidualBER').opval
        guaranteed_bit_rate_uplink = qos_tag.xpath('GuaranteedBitRateUplink2G3G').opval
        guaranteed_bit_rate_downlink = qos_tag.xpath('GuaranteedBitRateDownlink2G3G').opval
        traffic_handling_priority = qos_tag.xpath('TrafficHandlingPriority').opval

      # TODO(silje): not in XML
      # delivery_of_erroneous_sdus = qos_tag.xpath('').opval
      # source_statistics_descriptor = qos_tag.xpath('').opval
      # support_of_speech_source = qos_tag.xpath('').opval
      # signalling_indication = qos_tag.xpath('').opval
      # ODOT

      packet_data_service_info.qos_profile2g3g_items.create!(profile_name: profile_name,
        traffic_class: traffic_class,
        arp: arp,
        evolved_arp: evolved_arp,
        maximum_bit_rate_uplink: maximum_bit_rate_uplink,
        maximum_bit_rate_downlink: maximum_bit_rate_downlink,
        delivery_order: delivery_order,
        maximum_sdu_size: maximum_sdu_size,
        sdu_format_information: sdu_format_information,
        sdu_error_ratio: sdu_error_ratio,
        residual_ber: residual_ber,
        guaranteed_bit_rate_uplink: guaranteed_bit_rate_uplink,
        guaranteed_bit_rate_downlink: guaranteed_bit_rate_downlink,
        traffic_handling_priority: traffic_handling_priority)
    end
  end
  #### import_qos_profile2g3g_items ####
end