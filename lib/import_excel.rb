module ImportExcel
  def import_excel
    xlsx_import = Import.where(file_type: 'xlsx', archive: false, ready_for_import: true, removed: false).all
    xlsx_import.each do |xlsx|
      @xlsx = xlsx
      begin
        next if xlsx.archive
        # Initiate excel 
        spreadsheet = Roo::Spreadsheet.open("#{Rails.root}/data/upload/#{xlsx.file_name}.xlsx")
        #### Setting up Organisation ####
        #################################
        organisation_name = spreadsheet.cell(8, 'B')
        organisation_tadig = spreadsheet.cell(8, 'D')
        cc = spreadsheet.cell(8, 'C')
        if organisation_tadig
          @country = Country.where(iso_code: cc).first_or_create!(name: "#{cc}", iso_code: cc, iso2_code: cc[0..1], e164_code: rand(1000), mnp: false)
          @organisation = Organisation.where(sender_tadig: organisation_tadig).first
          unless @organisation
            unless xlsx.resolved_by_admin
              p 'No organisation found'
              @xlsx.failed = true
              @xlsx.error_message = 'Import is trying to create a new organisation'
              @xlsx.save
              exit  
            else
              @organisation = Organisation.where(sender_tadig: organisation_tadig).first_or_create
            end
          end
          @organisation.update_columns(name: organisation_name, country_id: @country.id)
          @organisation.touch
        else
	  @xlsx.failed = true
          @xlsx.error_message = 'sender tadig is missing'
          @xlsx.save
          exit
	end
        
        #### CREATE NETWORK #####
        spreadsheet.each_with_pagename do |name, sheet|
          sheet.each_row_streaming do |row|
            row.each do |cell, index|
              case cell.value
              when "CCITT_E212_NumberSeries"
                @network_name = spreadsheet.cell(11, 'C')
                mcc = spreadsheet.cell(row.first.coordinate.row + 2, 'B')
                mnc = spreadsheet.cell(row.first.coordinate.row + 2, 'C')
                tadig_code = spreadsheet.cell(11, 'B')
                effective_date_of_change_routing = spreadsheet.cell(15, 'B')
                p "mcc ======-----======#{mcc}"
                p "mnc ======-----======#{mnc}"
                old_network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ? AND tadig_code = ?", mcc.to_s.split('.').first, mnc.to_s.split('.').first, tadig_code).first
                p "OLD NETWORK IS: #{old_network} |||||| MCC:#{mcc} |||||| MNC:#{mnc}"
                unless old_network
                    #Check if there is a match without tadig
                    old_network = Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ? AND tadig_code IS ?", mcc.to_s.split('.').first, mnc.to_s.split('.').first, nil).first
                  end
                  date = spreadsheet.cell(5, 'B')
                  begin
                    date = date.to_date
                  rescue
                    date = Time.now.to_date
                  end
                  if defined? old_network.id
                    network = old_network
                    @network = network
                    @network.import_id = self.id
                    # Check so that display name is not modified
                    display_name_check = PaperTrail::Version.where('whodunnit IS NOT NULL AND item_id = ? AND object_changes LIKE ?', @network.id, '%display_name%').first
                    unless display_name_check
                      if @network_name
                        @network.display_name = "#{@network_name}"
                      else
                        if organisation_tadig
                          @network.display_name = "#{@organisation.name}"
                        end
                      end
                    end

                    @network.network_name = @network_name
                    @network.xml_creation_date = date
                    @network.tadig_code = tadig_code
                    @network.touch
                    
                    @network.save
                    notification = Notification.create!(message: "Network was reimported", network_id: @network.id)
                    notification.notify_changes(@network)
                  else
                    # Ros id incremention
                    ### Rosi id incremental test , Test other networks within the same network ####
                    reference_network = @organisation.networks.first
                    if reference_network
                      # Check for abnormal network
                      rosi_org_networks_abnormal = @organisation.networks.where("id != ? AND rosi_id != ?", reference_network.id, reference_network.rosi_id).first
                      unless rosi_org_networks_abnormal
                        rosi_org_networks_same = @organisation.networks.where("rosi_id = ?", reference_network.id).first
                      end
                    end

                    #### Case 1 ####
                    # - other networks exists, all have the same rosi_id - add the same rosi-id to the new network
                    ################
                    if reference_network && rosi_org_networks_abnormal.nil?
                      p 'CASE1'
                      rosi_increment = reference_network.rosi_id
                    end

                    #### Case 2 ####
                    # - other networks exists, but they have diffrent rosi_id - rosi_id is left blank
                    if reference_network && rosi_org_networks_abnormal
                      p 'CASE2'
                      rosi_increment = nil
                    end

                    #### Case 3 ####
                    # - no other networks exists - a new incremental rosi_id is added
                    unless reference_network
                      p 'CASE3'
                      rosi_increment = Network.where("rosi_id IS NOT NULL").order("rosi_id desc").pluck(:rosi_id).first
                      unless rosi_increment
                        rosi_increment = 1
                      else
                        rosi_increment = rosi_increment + 1
                      end
                    end
                    # display name logic
                    unless @network_name.blank?
                      display_name = @network_name  
                    else
                      display_name = "#{organisation.try(:name)}"
                    end
                    network = Network.create!(
                      tadig_code: spreadsheet.cell(11, 'B'),
                      xml_creation_date: date,
                      country_id: @country.id,
                      network_name: @network_name,
                      display_name: display_name,
                      organisation_id: @organisation.id,
                      import_id: self.id,
                      rosi_id: rosi_increment)
                    @network = network
                    #Cleenup of previous networks with blank tadig
                    Network.where("routing -> 'e212_mcc' = ? AND routing -> 'e212_mnc' = ? AND tadig_code IS ? AND id != ?", mcc.to_s, mnc.to_s, nil, network.id).delete_all
                    Notification.create!(message: "New network created", network_id: @network.id)
                  end

                  # Delete all previously line ranges
                  NetworkLineRange.where(network_id: @network.id).delete_all

                  #connecting network to import
                  @network.import_id = xlsx.id
                  mcc = spreadsheet.cell(row.first.coordinate.row + 2, 'B').to_s.split('.').first || " "
                  mnc = spreadsheet.cell(row.first.coordinate.row + 2, 'C').to_s.split('.').first || " "
                  cc = spreadsheet.cell(row.first.coordinate.row + 5, 'B').to_s.split('.').first || " "
                  nc = spreadsheet.cell(row.first.coordinate.row + 5, 'C').to_s.split('.').first || " "
                  @network.routing = {
                    "network_id" => network.id,
                    "e212_mcc" => mcc,
                    "e212_mnc" => mnc,
                    "e214_cc" => cc,
                    "e214_nc" => nc,
                    "number_portability" => spreadsheet.cell(row.first.coordinate.row + 6, 'B').to_i.floor.to_s == "1",
                    "effective_date_of_change" => effective_date_of_change_routing
                  }
                  # Delete all network rows
                  @network.network_rows.delete_all
                end
              end
            end
          end

        #### ADD INFORMATION FROM SPREADSHEET ####
        spreadsheet.each_with_pagename do |name, sheet|
          sheet.each_row_streaming do |row|
            row.each do |cell, index|

              case cell.value
              when "FileCreationTimestamp"
                timestamp = spreadsheet.cell(5, 'B')
                begin
                  @network_creation_date = timestamp.try(:to_date)
                rescue
                  @network_creation_date = Time.now.to_date
                end

              when "MSRN_NumberRanges"

              when "carrieritem"
                connectivity_types = {"Primary" => 1, "Secondary" => 2, "Backup" => 3}

              when "SCCPCarrierItem"
                p 'Importig InternationalSCCPGatewaySection'
                network_section = NetworkSection.where(field_name: "international_sccp_carrier").first
                delete_old_network_rows(network_section, @network)
                (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                  if dpc > row.first.coordinate.row
                    @network_row = NetworkRow.new
                    @network_row.network_id = @network.id
                    break if sheet.row(dpc).first == "DPCList"
                    next if sheet.row(dpc).second == 'SCCPCarrierName'
                      #insert data
                      name = spreadsheet.cell(dpc.to_i, 'B') || " "
                      connectivity_type = spreadsheet.cell(dpc.to_i, 'C') || " "
                      tadig_code_items = spreadsheet.cell(dpc.to_i, 'D') || " "
                      comment = spreadsheet.cell(dpc.to_i, 'E') || " "
                      @network_row.data = {
                        'name' => name,
                        'connectivity_type' => connectivity_type,
                        'tadig_code_item' => tadig_code_items,
                        'comment' => comment,
                      }
                      @network_row.network_section_id = network_section.id
                      @network_row.effective_date_of_change = @effect_date_of_change_sccp
                      p @network_row
                      @network_row.save!
                      @sccp_id = @network_row.id
                    end
                  end
                when "DPCList"
                  network_section_child = NetworkSection.where(field_name: "international_dpc_item").first
                  delete_old_network_rows(network_section_child, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row
                      @network_row_child = NetworkRow.new
                      @network_row_child.network_id = @network.id
                      next if sheet.row(dpc).second == 'SCSignature'
                      break if sheet.row(dpc).first == "/InternationalSCCPGatewaySection"
                      #variables
                      signature = spreadsheet.cell(dpc.to_i, 'B') || " "
                      type = spreadsheet.cell(dpc.to_i, 'C') || " "
                      dpc_value = spreadsheet.cell(dpc.to_i, 'D') || " "
                      dpc_formatted = dpc_value || " "
                      comment = spreadsheet.cell(dpc.to_i, 'E') || " "
                      @network_row_child.data = {
                        "dpc" => dpc_formatted,
                        "sc_signature" => signature,
                        "sc_type" => type,
                        "comment" => comment,
                        "dpc-decimal" => convert_from_itu_format(dpc_value.to_s),
                        "dpc-hex" => convert_from_itu_format(dpc_value.to_s).to_s(16)}
                      # Fetch parent
                      parent = NetworkRow.where("data -> 'name' = ? AND network_id = ?", spreadsheet.cell(dpc.to_i, 'F'), @network.id).first
                      @network_row_child.network_section_id = network_section_child.id
                      @network_row_child.network_row_id = @sccp_id
                      @network_row_child.network_row_id = parent.id if parent
                      @network_row_child.save!
                      p 'saved - dpclist'
                    end
                  end
                when "MSISDN_NumberRanges"
                  network_section_gt_number_ranges = NetworkSection.where(field_name: "gt_number_ranges").first
                  network_section_msisdn_number_ranges = NetworkSection.where(field_name: "msisdn_number_ranges").first
                  network_section_msrn_number_ranges = NetworkSection.where(field_name: "msrn_number_ranges").first
                  delete_old_network_rows(network_section_gt_number_ranges, @network)
                  delete_old_network_rows(network_section_msisdn_number_ranges, @network)
                  delete_old_network_rows(network_section_msrn_number_ranges, @network)

                  @active = 'MSISDN_NumberRanges'
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      break if sheet.row(dpc).first == "CCITT_E212_NumberSeries"
                      p sheet.row(dpc).first
                      @active = sheet.row(dpc).first unless sheet.row(dpc).first == nil
                      next if sheet.row(dpc).second == 'CC'
                      @cc = spreadsheet.cell(dpc, 'B') || " "
                      @ndc = spreadsheet.cell(dpc, 'C') || " "
                      @start = spreadsheet.cell(dpc, 'D') || " "
                      @stop = spreadsheet.cell(dpc, 'E') || " "
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      case @active
                      when "MSISDN_NumberRanges"
                        @network_row.network_section_id = network_section_msisdn_number_ranges.id
                      when "GT_NumberRanges"
                        @network_row.network_section_id = network_section_gt_number_ranges.id
                      when "MSRN_NumberRanges"
                        @network_row.network_section_id = network_section_msrn_number_ranges.id
                      end
                      p "#{@active_cc}"

                      @network_row.data = {
                        "#{@active.split('_').first} - cc" => @cc.to_s.split('.').first,
                        "#{@active.split('_').first} - ndc" => @ndc.to_s.split('.').first,
                        "#{@active.split('_').first} - range_start" => @start.to_s.split('.').first,
                        "#{@active.split('_').first} - range_stop" => @stop.to_s.split('.').first
                      }
                      @network_row.save!

                      #### Line ranges ####
                      nr = NetworkLineRange.new
                      case @network_row.network_section_id
                      when 168
                        nr.range_start = "#{@network_row.data['MSRN - cc']}#{@network_row.data['MSRN - ndc']}#{@network_row.data['MSRN - range_start']}".to_i
                        nr.range_stop = "#{@network_row.data['MSRN - cc']}#{@network_row.data['MSRN - ndc']}#{@network_row.data['MSRN - range_stop']}".to_i
                        nr.line_type = 'MSRN'
                      when 167
                        nr.range_start = "#{@network_row.data['GT - cc']}#{@network_row.data['GT - ndc']}#{@network_row.data['GT - range_start']}".to_i
                        nr.range_stop = "#{@network_row.data['GT - cc']}#{@network_row.data['GT - ndc']}#{@network_row.data['GT - range_stop']}".to_i
                        nr.line_type = 'GT'
                      when 169
                        nr.range_start = "#{@network_row.data['MSISDN - cc']}#{@network_row.data['MSISDN - ndc']}#{@network_row.data['MSISDN - range_start']}".to_i
                        nr.range_stop = "#{@network_row.data['MSISDN - cc']}#{@network_row.data['MSISDN - ndc']}#{@network_row.data['MSISDN - range_stop']}".to_i
                        nr.line_type = 'MSISDN'
                      end
                      nr.network_id = @network.id

                      unless nr.range_start == "0" || nr.range_start.nil? || nr.range_start.blank?
                        nr.save
                      end
                      #### /Line ranges ####

                      p 'Saving MSISDN'
                    end
                  end
                when "InternationalSCCPGatewaySection"
                  p 'trying to add effective date of change'
                  @effect_date_of_change_sccp = sheet.row(row.first.coordinate.row + 1).second
                when "TestNumbersInfoSection"
                  p 'Importing TestNumbersInfoSection'
                  network_section = NetworkSection.where(field_name: "test_numbers").first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row
                      break if sheet.row(dpc).first == "/TestNumbersInfoSection"
                      next if sheet.row(dpc).second == 'NumberType'
                      number_type = spreadsheet.cell(dpc.to_i, 'B') || " "
                      number = spreadsheet.cell(dpc.to_i, 'C') || " "
                      location = spreadsheet.cell(dpc.to_i, 'D') || " "
                      comment = spreadsheet.cell(dpc.to_i, 'E') || " "
                      network_row = NetworkRow.new
                      network_row.network_id = @network.id
                      network_row.data = {
                        "number_type" => number_type,
                        "number" => number,
                        "location" => location,
                        "comment" => comment
                      }
                      network_row.network_section_id = network_section.id
                      network_row.save!
                      p 'Saving testnumbers'
                    end
                  end
                when "NetworkElementsInfoSection"
                  p 'importing NetworkElementsInfoSections'
                  network_section = NetworkSection.where(field_name: "network_node").first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      break if sheet.row(dpc).first == "/NetworkElementsInfoSection"
                      next if sheet.row(dpc).second == 'CC'
                      next if sheet.row(dpc).second == 'NwElementType'
                      next if sheet.row(dpc).first == 'NwNode'
                      next if sheet.row(dpc).second == ''
                      node_type = spreadsheet.cell(dpc.to_i, 'B') || " "
                      cc = spreadsheet.cell(dpc.to_i, 'C').to_s.split('.').first || " "
                      ndc = spreadsheet.cell(dpc.to_i, 'D').to_s.split('.').first || " "
                      range_start = spreadsheet.cell(dpc.to_i, 'E') || " "
                      range_stop = spreadsheet.cell(dpc.to_i, 'F') || " "
                      network_row = NetworkRow.new
                      network_row.network_id = @network.id
                      network_row.data = {
                        "node_type" => node_type,
                        "cc" => cc,
                        "ndc" => ndc,
                        "e164_gt_address_range" => ("#{range_start}#{'-' if range_stop}#{range_stop}" if range_start),
                      }
                      network_row.network_section_id = network_section.id
                      network_row.save!
                      p 'Saving NetworkElementsInfoSection'
                    end
                  end
                when "PacketDataServiceInfoSection"
                  p 'PacketDataServiceInfoSection'
                  network_section = NetworkSection.where(field_name: "epc_realms_for_roaming").first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      break if sheet.row(dpc).first == "/PacketDataServiceInfoSection"
                      next if sheet.row(dpc).second == 'APNOperatorIdentifier'
                      next if sheet.row(dpc).first == 'APNOperatorIdentifierList'
                      next if sheet.row(dpc).second == ''
                      apn_operator_identifier = spreadsheet.cell(dpc.to_i, 'B') || " "
                      network_row = NetworkRow.new
                      network_row.network_id = @network.id
                      network_row.data = {
                        "apn_operator_identifier" => apn_operator_identifier
                      }
                      network_row.network_section_id = network_section.id
                      network_row.save!
                      p 'saving PacketDataServiceInfoSection'
                    end
                  end
                when "IPRoamingIWInfoSection"
                  network_section = NetworkSection.where(field_name: "ip_roaming_info").first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row
                      p 'adding ip roaming'
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      case spreadsheet.cell(dpc.to_i, 'A')
                      when "Effective_Date_Of_Change"
                        p 'setting effective date'
                        p spreadsheet.cell(dpc.to_i, 'B')
                        @effective_date_of_change = spreadsheet.cell(dpc.to_i, 'B')
                      else
                        next if sheet.row(dpc).first == "IPRoamingIWInfoSection"
                        next if sheet.row(dpc).first == "Effective_Date_Of_Change"
                        next if sheet.row(dpc).second == "IpAddress"
                        next if sheet.row(dpc).first == "InterPMNBackboneIPList"
                        next if sheet.row(dpc).second == "InterPMNBackboneIPList"
                        break if sheet.row(dpc).first == "ASNList"
                        ip = spreadsheet.cell(dpc.to_i, 'B') || " "
                        p "roaming is saved :::#{ip}"
                        @network_row.data = {
                          "ip_address_range" => ip
                        }
                        @network_row.effective_date_of_change = @effective_date_of_change
                        p "yes effective date is #{@effective_date_of_change}"
                        @network_row.network_section_id = network_section.id
                        @network_row.network_id = @network.id
                        @network_row.save!
                        p 'Saving IPRoamingIWInfoSection'
                      end
                    end
                  end
                when "ASNList"
                  network_section = NetworkSection.where(field_name: "asn_list").first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row
                      p 'adding asn list'
                      next if sheet.row(dpc).first == "ASNList"
                      next if sheet.row(dpc).second == "ASN"
                      break if sheet.row(dpc).first == "PMNAuthoritativeDNSIPList"
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      ip = spreadsheet.cell(dpc.to_i, 'B') || " "
                      @network_row.data = {
                        "ip_address_range" => ip
                      }
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.save!
                    end
                  end
                when "PMNAuthoritativeDNSIPList"
                  network_section = NetworkSection.where(field_name: "pmn_authoritative_dns_ip_list").first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    p 'pmn_authoritative_dns_ip_list'
                    if dpc > row.first.coordinate.row
                      next if sheet.row(dpc).second == "IPAddress"
                      next if sheet.row(dpc).first == "PMNAuthoritativeDNSIPList"
                      break if sheet.row(dpc).first == "PingTracerouteIPAddressList"
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      ip = spreadsheet.cell(dpc.to_i, 'B') || " "
                      dns_name = spreadsheet.cell(dpc.to_i, 'C') || " "
                      @network_row.data = {
                        "ip_address" => ip,
                        "dns_name" => dns_name
                      }
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.save!
                    end
                  end
                when "PingTracerouteIPAddressList"
                  network_section = NetworkSection.where(field_name: "ping_traceroute_ip_address_list").first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    p 'ping_traceroute_ip_address_list'
                    if dpc > row.first.coordinate.row
                      next if sheet.row(dpc).second == "PingTracerouteIPAddressItem"
                      next if sheet.row(dpc).first == "PingTracerouteIPAddressList"
                      break if sheet.row(dpc).first == "GRXProvidersList"
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      address = spreadsheet.cell(dpc.to_i, 'B') || " "
                      @network_row.data = {
                        "address" => address
                      }
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.save!
                    end
                  end
                when "GRXProvidersList"
                  network_section = NetworkSection.where(field_name: "grx_provider").first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    p 'grx provider'
                    if dpc > row.first.coordinate.row
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      next if sheet.row(dpc).second == "GRXProvider"
                      next if sheet.row(dpc).first == "GRXProvidersList"
                      break if sheet.row(dpc).first == "/IPRoamingIWInfoSection"
                      provider = spreadsheet.cell(dpc.to_i, 'B') || " "
                      @network_row.data = {
                        "provider" => provider
                      }
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.save!
                    end
                  end
                when "IPAddressesDiameterEdgeAgent"
                  p 'importing IPAddressesDiameterEdgeAgent'
                  network_section = NetworkSection.where(field_name: 'diameter_edge_agent').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      break if sheet.row(dpc).first == "HostnamesHSS_MMEList"
                      next if sheet.row(dpc).second == 'IPAddress'
                      ip = spreadsheet.cell(dpc.to_i, 'B') || " "
                      @network_row.data = {
                        "ip" => ip
                      }
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.save!
                      p 'Saving IPAddressesDiameterEdgeAgent'
                    end
                  end
                when "HostnamesHSS_MMEList"
                  p 'importing hostname mme-list'
                  network_section = NetworkSection.where(field_name: 'mme_hostnames').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      break if sheet.row(dpc).first == "IsS6aSupportedWithoutIWF"
                      next if sheet.row(dpc).second == 'HostName'
                      hostname = spreadsheet.cell(dpc.to_i, 'B') || " "
                      @network_row.data = {
                        "hostname" => hostname
                      }
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.save!
                      p 'Saving HostnamesHSS_MMEList'
                    end
                  end
                when "IsS6aSupportedWithoutIWF"
                  p 'importing supported without IWF'
                  network_section = NetworkSection.where(field_name: 'roaming_inter_connection').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc >= row.first.coordinate.row
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      break if sheet.row(dpc).first == "Voice_ITW"
                      next if sheet.row(dpc).first == ''
                      next if sheet.row(dpc).first == ''
                      if sheet.row(dpc).first == "IsS6aSupportedWithoutIWF"
                        p "FOUND IT AND SETTING"
                        @supported_without_iwf = spreadsheet.cell(dpc.to_i, 'B') || " "
                      end
                      if sheet.row(dpc).first == "IsMAP_IWFsupportedToHSS"
                        @supported_to_hss = spreadsheet.cell(dpc.to_i, 'B') || " "
                      end
                      if sheet.row(dpc).first == "IsMAP_IWFsupportedToMME"
                        @supported_to_mme = spreadsheet.cell(dpc.to_i, 'B') || " "
                      end
                      if sheet.row(dpc).first == "IsS6dUsedForLegacySGSN"
                        @used_for_legacy = spreadsheet.cell(dpc.to_i, 'B') || " "
                      end
                      if sheet.row(dpc).first == "IsGTPinterfaceAvailable"
                        @gtp_interface_available = spreadsheet.cell(dpc.to_i, 'B') || " "
                      end
                      if sheet.row(dpc).first == "IsPMIPinterfaceAvailable"
                        @pmp_interface_available = spreadsheet.cell(dpc.to_i, 'B') || " "
                      end
                      if sheet.row(dpc).first == "IsS9usedForPCC"
                        @used_for_ppc = spreadsheet.cell(dpc.to_i, 'B') || " "
                      end
                    end
                  end
                  p "RUNNING THE MAIN LOOP once..//...//"
                  @network_row.data = {
                    "supported_without_iwf" => @supported_without_iwf,
                    "supported_to_hss" => @supported_to_hss,
                    "supported_to_mme" => @supported_to_mme,
                    "legacy_sgsn" => @used_for_legacy,
                    "used_for_ppc" => @used_for_ppc,
                    "gtp_interface_avaliable" => @gtp_interface_available,
                    "pmip_interface_avaliable" => @pmp_interface_available
                  }
                  @network_row.network_section_id = network_section.id
                  @network_row.network_id = @network.id
                  @network_row.save!
                  p 'Saving IsS6aSupportedWithoutIWF'
                when "HostnamesPCRFList"
                  p 'importing hostname pcrf list'
                  network_section = NetworkSection.where(field_name: 'pcrf_hostnames').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row
                      @network_row = NetworkRow.new
                      @network_row.network_id = @network.id
                      break if sheet.row(dpc).first == "IsS9usedForPCC"
                      next if sheet.row(dpc).second == 'HostName'
                      hostname = spreadsheet.cell(dpc.to_i, 'B') || " "
                      @network_row.data = {
                        "hostname" => hostname
                      }
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.save!
                      p 'Saving HostnamesPCRFList'
                    end
                  end
                when "Voice_ITW"
                  p 'importing voice itw'
                  network_section = NetworkSection.where(field_name: 'voice_itw').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|

                    if dpc == row.first.coordinate.row
                      p 'saving voice'
                      voice_itw = spreadsheet.cell(dpc.to_i, 'B') || " "
                      @network_row = NetworkRow.new
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.data = {
                        "voice_itw" => voice_itw
                      }
                      @network_row.save
                      p "Saving voice ITW"
                    end

                    break if spreadsheet.cell(dpc.to_i, 'A') == 'IsRoamingRetrySupported'

                  end
                when "IsRoamingRetrySupported"
                  p 'saving romaingretry'
                  network_section = NetworkSection.where(field_name: 'roaming_retry').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|

                    if dpc == row.first.coordinate.row
                      p 'saving roaming retry'
                      roaming_retry = spreadsheet.cell(dpc.to_i, 'B') || " "
                      @network_row = NetworkRow.new
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.data = {
                        "is_roaming_retry_supported" => roaming_retry
                      }
                      @network_row.save
                      p 'Saving IsRoamingRetrySupported'
                    end
                    break if spreadsheet.cell(dpc.to_i, 'A') == 'HPMNInfoLTERoamingAgreementOnly'
                  end
                when "HPMNInfoLTERoamingAgreementOnly"
                  p 'saving hpmn info lte'
                  network_section = NetworkSection.where(field_name: 'hpmn_info_lte_roaming_agreement_only').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|

                    if dpc == row.first.coordinate.row + 2
                      p 'saving hpmn info lte'
                      hpmn_info = spreadsheet.cell(dpc.to_i, 'B') || " "
                      @network_row = NetworkRow.new
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.data = {
                        "lte_only_roaming" => hpmn_info
                      }
                      @network_row.save
                      p 'Saving HPMNInfoLTERoamingAgreementOnly'
                    end
                    break if spreadsheet.cell(dpc.to_i, 'A') == 'VPMNInfoLTERoamingAgreementOnly'
                  end
                when "VPMNInfoLTERoamingAgreementOnly"
                  p 'saving vpmn info lte'
                  network_section = NetworkSection.where(field_name: 'vpmn_lte_info_roaming_agreement').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|

                    if dpc == row.first.coordinate.row + 2
                      p 'saving hpmn info lte'
                      vpmn_info = spreadsheet.cell(dpc.to_i, 'B') || " "
                      @network_row = NetworkRow.new
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.data = {
                        "vpmn_only_roaming" => vpmn_info
                      }
                      @network_row.save
                      p 'Saving VPMNInfoLTERoamingAgreementOnly'
                    end
                    break if spreadsheet.cell(dpc.to_i, 'B') == 'HPMNInfo2G3GRoamingAgreementOnly'
                  end
                when "HPMNInfo2G3GRoamingAgreementOnly"
                  p 'saving hpmn info 2g3g lte'
                  network_section = NetworkSection.where(field_name: 'hpmn_info_2g_3g_roaming_agreement_only').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|

                    if dpc == row.first.coordinate.row + 2
                      p 'saving hpmn info lte'
                      @scenario_1 = spreadsheet.cell(dpc.to_i, 'B') || " "
                      @scenario_2 = spreadsheet.cell(dpc.to_i, 'C') || " "
                      @scenario_3 = spreadsheet.cell(dpc.to_i, 'D') || " "
                      @scenario_4 = spreadsheet.cell(dpc.to_i, 'E') || " "
                      @network_row = NetworkRow.new
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.data = {
                        "scenario_1_supported" => @scenario_1,
                        "scenario_2_supported" => @scenario_2,
                        "scenario_3_supported" => @scenario_3,
                        "scenario_4_supported" => @scenario_4
                      }
                      @network_row.save
                      p 'Saving HPMNInfo2G3GRoamingAgreementOnly'
                    end
                    break if spreadsheet.cell(dpc.to_i, 'A') == 'VPMNInfo2G3GRoamingAgreementOnly'

                  end
                when "VPMNInfo2G3GRoamingAgreementOnly"
                  p 'saving vpmn info 2g3g roaming'
                  network_section = NetworkSection.where(field_name: 'vpmn_info_2g_3g_roaming_agreement_only').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|

                    if dpc == row.first.coordinate.row + 2
                      p 'saving hpmn info lte'
                      scenario_1 = spreadsheet.cell(dpc.to_i, 'B') || " "
                      scenario_2 = spreadsheet.cell(dpc.to_i, 'C') || " "
                      scenario_3 = spreadsheet.cell(dpc.to_i, 'D') || " "
                      scenario_4 = spreadsheet.cell(dpc.to_i, 'E') || " "
                      @network_row = NetworkRow.new
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.data = {
                        "scenario_1_supported" => scenario_1,
                        "scenario_2_supported" => scenario_2,
                        "scenario_3_supported" => scenario_3,
                        "scenario_4_supported" => scenario_4
                      }
                      @network_row.save!
                      p 'Saving VPMNInfo2G3GLTERoamingAgreement'
                    end
                    break if spreadsheet.cell(dpc.to_i, 'A') == 'HPMNInfo2G3GLTERoamingAgreement'

                  end
                when "HPMNInfo2G3GLTERoamingAgreement"
                  p 'saving hpmn info 2g3g lte roaming'
                  network_section = NetworkSection.where(field_name: 'hpmn_info_2g_3g_lte_roaming_agreement').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|

                    if dpc == row.first.coordinate.row + 2
                      p 'saving hpmn info 2g3g lte roaming'
                      scenario_1 = spreadsheet.cell(dpc.to_i, 'B') || " "
                      scenario_2 = spreadsheet.cell(dpc.to_i, 'C') || " "
                      scenario_3 = spreadsheet.cell(dpc.to_i, 'D') || " "
                      scenario_4 = spreadsheet.cell(dpc.to_i, 'E') || " "
                      @network_row = NetworkRow.new
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.data = {
                        "scenario_1_supported" => scenario_1,
                        "scenario_2_supported" => scenario_2,
                        "scenario_3_supported" => scenario_3,
                        "scenario_4_supported" => scenario_4
                      }
                      @network_row.save
                    end
                    p 'Saving HPMNInfo2G3GLTERoamingAgreement'
                  end
                when "VPMNInfo2G3GLTERoamingAgreement"
                  p 'saving vpmn info 2g3g roaming'
                  network_section = NetworkSection.where(field_name: 'vpmn_info_2g_3g_lte_roaming_agreement').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|

                    if dpc == row.first.coordinate.row + 2
                      p 'saving vpmn info 2g3g roaming'
                      scenario_1 = spreadsheet.cell(dpc.to_i, 'B') || " "
                      scenario_2 = spreadsheet.cell(dpc.to_i, 'C') || " "
                      scenario_3 = spreadsheet.cell(dpc.to_i, 'D') || " "
                      scenario_4 = spreadsheet.cell(dpc.to_i, 'E') || " "
                      @network_row = NetworkRow.new
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.data = {
                        "scenario_1_supported" => scenario_1,
                        "scenario_2_supported" => scenario_2,
                        "scenario_3_supported" => scenario_3,
                        "scenario_4_supported" => scenario_4
                      }
                      @network_row.save!
                      p 'Saving VPMNInfo2G3GLTERoamingAgreement'
                    end
                  end
                when "LTEQosProfile"
                  p 'saving LTEQosprofile'
                  network_section = NetworkSection.where(field_name: 'lteq_os_profile_list').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|

                    if dpc == row.first.coordinate.row + 2
                      p 'saving LTEQosprofiler'
                      profile = spreadsheet.cell(dpc.to_i, 'B') || " "
                      qci_value = spreadsheet.cell(dpc.to_i, 'C') || " "
                    end
                    if dpc == row.first.coordinate.row + 5
                      arp = spreadsheet.cell(dpc.to_i, 'B') || " "
                    end
                    @network_row = NetworkRow.new
                    @network_row.network_section_id = network_section.id
                    @network_row.network_id = @network.id
                    @network_row.data = {
                      "profile" => profile,
                      "qci_value" => qci_value,
                      "arp" => arp
                    }
                    @network_row.save
                    p 'Saved LTEQosprofile'
                  end
                when "IPv6ConnectivityInformation"
                  p 'saving IPV6'
                  network_section = NetworkSection.where(field_name: 'ipv6_connectivity_information').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|

                    if dpc == row.first.coordinate.row + 3
                      mme_ipv6_supported = spreadsheet.cell(dpc.to_i, 'B') || " "
                      mme_ipv4_v6_pdp_supported = spreadsheet.cell(dpc.to_i, 'C') || " "
                    end
                    if dpc == row.first.coordinate.row + 6
                      sgw_ipv6_dpd_supported = spreadsheet.cell(dpc.to_i, 'B') || " "
                      sgw_ipv4_v6_pdp_supported = spreadsheet.cell(dpc.to_i, 'C') || " "
                    end
                    if dpc == row.first.coordinate.row + 9
                      pgw_ipv6_pdp_supported = spreadsheet.cell(dpc.to_i, 'B') || " "
                      pgw_ipv4_v6_pdp_supported = spreadsheet.cell(dpc.to_i, 'C') || " "
                    end
                    @network_row = NetworkRow.new
                    @network_row.network_section_id = network_section.id
                    @network_row.network_id = @network.id
                    @network_row.data = {
                      "mme_ipv6_supported" => mme_ipv6_supported,
                      "mme_ipv4v6_supported" => mme_ipv4_v6_pdp_supported,
                      "sgw_ipv6_dpd_supported" => sgw_ipv6_dpd_supported,
                      "sgw_ipv4_v6_pdp_supported" => sgw_ipv4_v6_pdp_supported,
                      "pgw_ipv6_pdp_supported" => pgw_ipv4_v6_pdp_supported,
                      "pgw_ipv4_v6_pdp_supported" => pgw_ipv4_v6_pdp_supported
                    }
                    @network_row.save
                    p 'Saving IPv6ConnectivityInformation'
                  end
                when "LTEInformation"
                  p 'saving LTE information'
                  network_section = NetworkSection.where(field_name: 'ipv6_connectivity_information').first
                  delete_old_network_rows(network_section, @network)
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|

                    if dpc == row.first.coordinate.row + 2
                      qci_value1_supported = spreadsheet.cell(dpc.to_i, 'B') || " "
                      qci_value2_supported = spreadsheet.cell(dpc.to_i, 'C') || " "
                      qci_value3_supported = spreadsheet.cell(dpc.to_i, 'D') || " "
                      qci_value4_supported = spreadsheet.cell(dpc.to_i, 'E') || " "
                      qci_value5_supported = spreadsheet.cell(dpc.to_i, 'F') || " "
                      qci_value6_supported = spreadsheet.cell(dpc.to_i, 'G') || " "
                      qci_value7_supported = spreadsheet.cell(dpc.to_i, 'H') || " "
                      qci_value8_supported = spreadsheet.cell(dpc.to_i, 'I') || " "
                      qci_value9_supported = spreadsheet.cell(dpc.to_i, 'J') || " "
                      @network_row = NetworkRow.new
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.data = {
                        "qci_value1_supported" => qci_value1_supported,
                        "qci_value2_supported" => qci_value2_supported,
                        "qci_value3_supported" => qci_value3_supported,
                        "qci_value4_supported" => qci_value4_supported,
                        "qci_value5_supported" => qci_value5_supported,
                        "qci_value6_supported" => qci_value6_supported,
                        "qci_value7_supported" => qci_value7_supported,
                        "qci_value8_supported" => qci_value8_supported,
                        "qci_value9_supported" => qci_value9_supported
                      }
                      @network_row.save
                      p 'Saved LTEInformation'
                    end
                  end
                when "FQDNDiameterEdgeAgent"
                  p 'saving FQDN'
                  network_section = NetworkSection.where(field_name: 'diameter_edge_agent').first
                  delete_old_network_rows(network_section, @network)

                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row + 1
                      case spreadsheet.cell(dpc.to_i, 'B')
                      when "FQDN"
                        value = spreadsheet.cell(dpc.to_i + 1, 'B')
                        @network_row = NetworkRow.new
                        @network_row.network_section_id = network_section.id
                        @network_row.network_id = @network.id
                        @network_row.data = {
                          "FQDN" => value
                        }
                        @network_row.save
                        p 'SAVED FQDN'
                        p @network_row
                        break if spreadsheet.cell(dpc.to_i, 'A') == 'PrimaryAddressList'

                      end
                    end
                  end
                when "PrimaryAddressList"
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row + 1
                      value = spreadsheet.cell(dpc.to_i + 1, 'B')
                      parent = NetworkRow.where("data -> 'FQDN' = ? AND network_id = ?", spreadsheet.cell(dpc.to_i + 1, 'C'), @network.id).first
                      network_section = NetworkSection.where(field_name: 'diameter_primary_ip_address').first
                      @network_row = NetworkRow.new
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.data = {
                        "ip_address" => value
                      }
                      @network_row.network_row_id = parent.id if parent
                      @network_row.save!
                    end
                    break if spreadsheet.cell(dpc.to_i, 'A') == 'SecondaryAddressList'
                  end

                when "SecondaryAddressList"
                  (sheet.first_row..sheet.last_row).each_with_index do |dpc, index|
                    if dpc > row.first.coordinate.row + 1
                      value = spreadsheet.cell(dpc.to_i + 1, 'B')
                      parent = NetworkRow.where("data -> 'FQDN' = ? AND network_id = ?", spreadsheet.cell(dpc.to_i + 1, 'C'), @network.id).first
                      network_section = NetworkSection.where(field_name: 'diameter_secondary_ip_address').first
                      @network_row = NetworkRow.new
                      @network_row.network_section_id = network_section.id
                      @network_row.network_id = @network.id
                      @network_row.data = {
                        "ip_address" => value
                      }
                      @network_row.network_row_id = parent.id if parent
                      @network_row.save!
                    end
                    break if spreadsheet.cell(dpc.to_i, 'A') == '/FQDNDiameterEdgeAgent'
                  end

                end
              end
            end
          #### SAVE THE NETWORK ####
          if Rails.env.production?
            xlsx.percentage_done = 100
            xlsx.archive = true
          end
          @network.organisation_id = @organisation.id if @organisation
          @network.country_id = @country.id if @country
          @network.save!
          xlsx.network_id = @network.id
          xlsx.save
          p 'saving network'
          @date = @network.xml_creation_date
          p "date: #{@date}"

          #Reset network statuses
          last_status_sccp = NetworkStatus.where(network_id: @network.id, network_status_type_id: 1).first_or_create
          last_status_routing = NetworkStatus.where(network_id: @network.id, network_status_type_id: 2).first_or_create
          # sccp
          network_section = NetworkSection.where(field_name: 'international_sccp_carrier').first
          nr = NetworkRow.where(network_section_id: network_section.id, network_id: @network.id).last
          if nr.try(:effective_date_of_change)
            p 'found nr'
            if last_status_sccp.try(:sccp_effective_date).nil?
              p 'handeling nil'
              last_status_sccp.update_columns(status: nil, sccp_effective_date: nr.effective_date_of_change)
            end
            if nr.effective_date_of_change > last_status_sccp.sccp_effective_date
              p 'updating nr'
              last_status_sccp.update_columns(status: nil, sccp_effective_date: nr.effective_date_of_change)
            else
              p 'not bigger'
              p "#{nr.effective_date_of_change} - #{last_status_sccp.updated_at}"
            end
          end
          # routing
          if @network.routing.try(:[], 'effective_date_of_change')
            if last_status_routing.routing_effective_date.nil?
              p 'handeling second nil'
              last_status_routing.update_columns(status: nil, routing_effective_date: @network.routing['effective_date_of_change'])
            end
            if @network.routing['effective_date_of_change'] > last_status_routing.routing_effective_date
              last_status_routing.update_columns(status: nil, routing_effective_date: @network.routing['effective_date_of_change'])
            end
          end

          # Check for manual changes that risks to be overwritten
          versions = PaperTrail::Version.where("network_id = ? AND created_at > ? AND whodunnit IS NOT NULL AND whodunnit != ?", @network.id, @date, "HERMES").all
          if versions && versions.count > 0
            # set import as faulty
            @network.needs_to_resolve = true
            @network.resolve_message = "Manually changes risk to be overwritten!"
            @network.save
          end
          @network.evaluate_line_ranges
          @network.save!
          @network.import.save
        end
      end
    end
    #Code.create_code_line_ranges(Network.where(id: @network.id))
  rescue Exception
    @xlsx.failed = true
    unless @xlsx.error_message
      @xlsx.error_message = $!.message
      @xlsx.error_stack = $!.backtrace
      @xlsx.save
    end
  end
end
